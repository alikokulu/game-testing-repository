﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SDKManager : MonoBehaviour {

//#if !UNITY_EDITOR
	//Stores currently selected Zone ID
	private string ZoneID = "";

	//Stores the logs
	private string logs = "";

	//Guistyle objects
	private GUIStyle gs;

	private bool inLoop = false;

	void Start()
	{
		//GUI style for the debug text
		gs = new GUIStyle();
		gs.alignment = TextAnchor.LowerLeft;
		gs.fontSize = 30;
		gs.normal.textColor = Color.black;

		ZoneID = PlayerPrefs.GetString ("ZoneID");
	}
	void Awake()
	{


		//Register the event handlers
		//SessionStartReceived will be raised when a session has been successfully started
		FuseSDK.SessionStartReceived += FuseSDK_SessionStartReceived;

		//SessionLoginError will be raised when a session start fails for any reason
		FuseSDK.SessionLoginError += FuseSDK_SessionLoginError;

		//AdAvailabilityResponse will be raised as a response to the PreloadAdForZoneID call
		FuseSDK.AdAvailabilityResponse += FuseSDK_AdAvailabilityResponse;

		/*
		AdWillClose will be raised at the end of an ad cycle; 
			- When the ad has been closed (maunally, automatically or due to an error)
			- When no ad could be shown
		*/
		FuseSDK.AdWillClose += FuseSDK_AdWillClose;

		//RewardedAdCompletedWithObject will be raised when a Rewarded Video has been watched to completion
		FuseSDK.RewardedAdCompletedWithObject += FuseSDK_RewardedAdCompletedWithObject;
	}
	void onDestroy()
	{
		//On Destroy, unregister the event handlers
		FuseSDK.SessionStartReceived -= FuseSDK_SessionStartReceived;
		FuseSDK.SessionLoginError -= FuseSDK_SessionLoginError;
		
		FuseSDK.AdAvailabilityResponse -= FuseSDK_AdAvailabilityResponse;
		FuseSDK.AdWillClose -= FuseSDK_AdWillClose;
		FuseSDK.RewardedAdCompletedWithObject -= FuseSDK_RewardedAdCompletedWithObject;

	}


	void FuseSDK_SessionStartReceived()
	{
		printLog ("Session Start Received");
	}
	
	void FuseSDK_SessionLoginError(FuseMisc.FuseError obj)
	{
		//FuseSDK_SessionLoginError will be called when FuseSDK.SessionLoginError has been raised
		printLog("Session Login Error: " + obj.ToString());
	}

	void PreloadAd()
	{
		printLog("PreloadAdForZoneID: " + ZoneID);
		/*
			FuseSDK.PreloadAdForZoneID will trigger an ad to be downloaded to the device to cache it for the provided zone.
			If there is an ad cached on the device which can be used for the provided zone already, no new ad will be downloaded.
			Once this method completes it's cycle, it will raise the FuseSDK.AdAvailabilityResponse event.
		 */
		FuseSDK.PreloadAdForZoneID(ZoneID);
	}
	
	void FuseSDK_AdAvailabilityResponse(bool adAvailable, FuseMisc.FuseError error)
	{
		if (adAvailable)
		{
			printLog("Ad Available");
			inLoop = false;
			ShowAdForZoneID ();
		}
		else if(inLoop)
		{
			printLog("No Ad Available, trying again");
			PreloadAd ();
		}
	}

	void ShowAdForZoneID()
	{
		printLog("ShowAdForZoneID: " + ZoneID);
		FuseSDK.ShowAdForZoneID(ZoneID);
	}

	void FuseSDK_AdWillClose()
	{
		printLog("Ad Will Close");
	}


	void FuseSDK_RewardedAdCompletedWithObject(FuseMisc.RewardedInfo rewardedInfo)
	{
		printLog("Rewarded Ad Completed!" + "\n" + 
		      "PreRollMessage: " + rewardedInfo.PreRollMessage + "\n" + 
		      "RewardAmount: " + rewardedInfo.RewardAmount + "\n" + 
		      "RewardItem: " + rewardedInfo.RewardItem + "\n" + 
		      "RewardMessage: " + rewardedInfo.RewardMessage);
	}


	void OnGUI()
	{
		GUI.Label(new Rect(0,0,Screen.width,Screen.height),logs,gs);

		if (!inLoop) 
		{
			if(GUI.Button(new Rect(Screen.width - 120, 10,100,100),"Request Ad"))
			{
				printLog("Requesting Ad from Zone: " + ZoneID);
				inLoop = true;
				PlayerPrefs.SetString ("ZoneID", ZoneID);
				PlayerPrefs.Save ();
				PreloadAd();
			}	
		} 
		else 
		{
			if(GUI.Button(new Rect(Screen.width - 120, 10,100,100),"Stop Requests"))
			{
				printLog("Stopping Ad Requests");
				inLoop = false;
			}	
		}

		ZoneID = GUI.TextField (new Rect (Screen.width - 300, 10, 150, 50), ZoneID);
	}

	private void printLog(string log)
	{
			logs +=  "\n" + "> " + log ;
		if (logs.Length > 10000)
			logs = logs.Substring (logs.Length - 1000);
		
			print("> Fuse: " + log);
	}

//#endif
}
