﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/<BakeAsync>c__Iterator1
struct U3CBakeAsyncU3Ec__Iterator1_t726194327;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void JSONObject/<BakeAsync>c__Iterator1::.ctor()
extern "C"  void U3CBakeAsyncU3Ec__Iterator1__ctor_m3188929708 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<BakeAsync>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2863517552 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<BakeAsync>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m315405060 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<BakeAsync>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m2377416901 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<BakeAsync>c__Iterator1::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CBakeAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1949287823 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<BakeAsync>c__Iterator1::MoveNext()
extern "C"  bool U3CBakeAsyncU3Ec__Iterator1_MoveNext_m827557040 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<BakeAsync>c__Iterator1::Dispose()
extern "C"  void U3CBakeAsyncU3Ec__Iterator1_Dispose_m2148050409 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<BakeAsync>c__Iterator1::Reset()
extern "C"  void U3CBakeAsyncU3Ec__Iterator1_Reset_m835362649 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
