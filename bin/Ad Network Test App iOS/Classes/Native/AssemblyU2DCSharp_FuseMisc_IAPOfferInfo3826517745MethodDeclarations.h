﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPOfferInfo3826517745.h"
#include "mscorlib_System_String968488902.h"

// System.Void FuseMisc.IAPOfferInfo::.ctor(System.String)
extern "C"  void IAPOfferInfo__ctor_m3107304778 (IAPOfferInfo_t3826517745 * __this, String_t* ___infoString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseMisc.IAPOfferInfo::ToString()
extern "C"  String_t* IAPOfferInfo_ToString_m2121294261 (IAPOfferInfo_t3826517745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
