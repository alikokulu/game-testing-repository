﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SDKManager
struct SDKManager_t2583098931;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FuseMisc_FuseError444231718.h"
#include "AssemblyU2DCSharp_FuseMisc_RewardedInfo829972251.h"
#include "mscorlib_System_String968488902.h"

// System.Void SDKManager::.ctor()
extern "C"  void SDKManager__ctor_m334162888 (SDKManager_t2583098931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::Start()
extern "C"  void SDKManager_Start_m3576267976 (SDKManager_t2583098931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::Awake()
extern "C"  void SDKManager_Awake_m571768107 (SDKManager_t2583098931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::onDestroy()
extern "C"  void SDKManager_onDestroy_m904393953 (SDKManager_t2583098931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::FuseSDK_SessionStartReceived()
extern "C"  void SDKManager_FuseSDK_SessionStartReceived_m1241951151 (SDKManager_t2583098931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::FuseSDK_SessionLoginError(FuseMisc.FuseError)
extern "C"  void SDKManager_FuseSDK_SessionLoginError_m1624667535 (SDKManager_t2583098931 * __this, int32_t ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::PreloadAd()
extern "C"  void SDKManager_PreloadAd_m4269218674 (SDKManager_t2583098931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::FuseSDK_AdAvailabilityResponse(System.Boolean,FuseMisc.FuseError)
extern "C"  void SDKManager_FuseSDK_AdAvailabilityResponse_m1213840872 (SDKManager_t2583098931 * __this, bool ___adAvailable, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::ShowAdForZoneID()
extern "C"  void SDKManager_ShowAdForZoneID_m2237971062 (SDKManager_t2583098931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::FuseSDK_AdWillClose()
extern "C"  void SDKManager_FuseSDK_AdWillClose_m2278286083 (SDKManager_t2583098931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::FuseSDK_RewardedAdCompletedWithObject(FuseMisc.RewardedInfo)
extern "C"  void SDKManager_FuseSDK_RewardedAdCompletedWithObject_m1785257430 (SDKManager_t2583098931 * __this, RewardedInfo_t829972251  ___rewardedInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::OnGUI()
extern "C"  void SDKManager_OnGUI_m4124528834 (SDKManager_t2583098931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SDKManager::printLog(System.String)
extern "C"  void SDKManager_printLog_m2701162351 (SDKManager_t2583098931 * __this, String_t* ___log, const MethodInfo* method) IL2CPP_METHOD_ATTR;
