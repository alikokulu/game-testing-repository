﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuseSDKEditorSession
struct FuseSDKEditorSession_t360633328;
// System.Action
struct Action_t437523947;
// System.Action`1<FuseMisc.FuseError>
struct Action_1_t592684423;
// System.Action`2<FuseMisc.AccountType,System.String>
struct Action_2_t535153670;
// System.Action`2<System.String,System.String>
struct Action_2_t2887221574;
// System.Action`1<System.String>
struct Action_1_t1116941607;
// System.Action`2<System.String,FuseMisc.FuseError>
struct Action_2_t2362964390;
// System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>
struct Action_1_t1482123831;
// System.Action`3<System.Int32,System.String,System.String>
struct Action_3_t3047918030;
// System.Action`2<System.Boolean,FuseMisc.FuseError>
struct Action_2_t3211021459;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t3619260338;
// System.Action`1<FuseMisc.RewardedInfo>
struct Action_1_t978424956;
// System.Action`1<FuseMisc.IAPOfferInfo>
struct Action_1_t3974970450;
// System.Action`1<FuseMisc.VGOfferInfo>
struct Action_1_t1534247929;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.List`1<FuseMisc.Friend>
struct List_1_t1333671126;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPState1023998552.h"
#include "mscorlib_System_DateTime339033936.h"
#include "AssemblyU2DCSharp_FuseMisc_RewardedInfo829972251.h"
#include "AssemblyU2DCSharp_FuseMisc_VGOfferInfo1385795224.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPOfferInfo3826517745.h"
#include "AssemblyU2DCSharp_FuseMisc_Gender553483264.h"
#include "AssemblyU2DCSharp_FuseMisc_AccountType958198790.h"
#include "AssemblyU2DCSharp_FuseMisc_FuseError444231718.h"

// System.Void FuseSDKEditorSession::.ctor()
extern "C"  void FuseSDKEditorSession__ctor_m3699438763 (FuseSDKEditorSession_t360633328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::.cctor()
extern "C"  void FuseSDKEditorSession__cctor_m2531355746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_SessionStartReceived(System.Action)
extern "C"  void FuseSDKEditorSession_add_SessionStartReceived_m3335941433 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_SessionStartReceived(System.Action)
extern "C"  void FuseSDKEditorSession_remove_SessionStartReceived_m4066686156 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_SessionLoginError(System.Action`1<FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_add_SessionLoginError_m2064850608 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_SessionLoginError(System.Action`1<FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_remove_SessionLoginError_m1026003005 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_GameConfigurationReceived(System.Action)
extern "C"  void FuseSDKEditorSession_add_GameConfigurationReceived_m401833325 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_GameConfigurationReceived(System.Action)
extern "C"  void FuseSDKEditorSession_remove_GameConfigurationReceived_m403831226 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_AccountLoginComplete(System.Action`2<FuseMisc.AccountType,System.String>)
extern "C"  void FuseSDKEditorSession_add_AccountLoginComplete_m1306866631 (Il2CppObject * __this /* static, unused */, Action_2_t535153670 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_AccountLoginComplete(System.Action`2<FuseMisc.AccountType,System.String>)
extern "C"  void FuseSDKEditorSession_remove_AccountLoginComplete_m252982554 (Il2CppObject * __this /* static, unused */, Action_2_t535153670 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_AccountLoginError(System.Action`2<System.String,System.String>)
extern "C"  void FuseSDKEditorSession_add_AccountLoginError_m37286648 (Il2CppObject * __this /* static, unused */, Action_2_t2887221574 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_AccountLoginError(System.Action`2<System.String,System.String>)
extern "C"  void FuseSDKEditorSession_remove_AccountLoginError_m1309077515 (Il2CppObject * __this /* static, unused */, Action_2_t2887221574 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_NotificationAction(System.Action`1<System.String>)
extern "C"  void FuseSDKEditorSession_add_NotificationAction_m2803538548 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_NotificationAction(System.Action`1<System.String>)
extern "C"  void FuseSDKEditorSession_remove_NotificationAction_m689429121 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_NotificationWillClose(System.Action)
extern "C"  void FuseSDKEditorSession_add_NotificationWillClose_m2263922999 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_NotificationWillClose(System.Action)
extern "C"  void FuseSDKEditorSession_remove_NotificationWillClose_m3442172932 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_FriendAdded(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_add_FriendAdded_m197112318 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_FriendAdded(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_remove_FriendAdded_m1485063819 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_FriendRemoved(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_add_FriendRemoved_m3071654302 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_FriendRemoved(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_remove_FriendRemoved_m3842465515 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_FriendAccepted(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_add_FriendAccepted_m3475540687 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_FriendAccepted(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_remove_FriendAccepted_m1600884514 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_FriendRejected(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_add_FriendRejected_m4074323096 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_FriendRejected(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_remove_FriendRejected_m2199666923 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_FriendsMigrated(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_add_FriendsMigrated_m2990361522 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_FriendsMigrated(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_remove_FriendsMigrated_m710595007 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_FriendsListUpdated(System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>)
extern "C"  void FuseSDKEditorSession_add_FriendsListUpdated_m3301668488 (Il2CppObject * __this /* static, unused */, Action_1_t1482123831 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_FriendsListUpdated(System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>)
extern "C"  void FuseSDKEditorSession_remove_FriendsListUpdated_m1162357403 (Il2CppObject * __this /* static, unused */, Action_1_t1482123831 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_FriendsListError(System.Action`1<FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_add_FriendsListError_m970313252 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_FriendsListError(System.Action`1<FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_remove_FriendsListError_m936802039 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_PurchaseVerification(System.Action`3<System.Int32,System.String,System.String>)
extern "C"  void FuseSDKEditorSession_add_PurchaseVerification_m3268549484 (Il2CppObject * __this /* static, unused */, Action_3_t3047918030 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_PurchaseVerification(System.Action`3<System.Int32,System.String,System.String>)
extern "C"  void FuseSDKEditorSession_remove_PurchaseVerification_m2980156031 (Il2CppObject * __this /* static, unused */, Action_3_t3047918030 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_AdAvailabilityResponse(System.Action`2<System.Boolean,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_add_AdAvailabilityResponse_m1067774092 (Il2CppObject * __this /* static, unused */, Action_2_t3211021459 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_AdAvailabilityResponse(System.Action`2<System.Boolean,FuseMisc.FuseError>)
extern "C"  void FuseSDKEditorSession_remove_AdAvailabilityResponse_m2757106073 (Il2CppObject * __this /* static, unused */, Action_2_t3211021459 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_AdWillClose(System.Action)
extern "C"  void FuseSDKEditorSession_add_AdWillClose_m134764527 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_AdWillClose(System.Action)
extern "C"  void FuseSDKEditorSession_remove_AdWillClose_m593932028 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_AdFailedToDisplay(System.Action)
extern "C"  void FuseSDKEditorSession_add_AdFailedToDisplay_m1519259627 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_AdFailedToDisplay(System.Action)
extern "C"  void FuseSDKEditorSession_remove_AdFailedToDisplay_m3846187320 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_AdDidShow(System.Action`2<System.Int32,System.Int32>)
extern "C"  void FuseSDKEditorSession_add_AdDidShow_m2865039325 (Il2CppObject * __this /* static, unused */, Action_2_t3619260338 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_AdDidShow(System.Action`2<System.Int32,System.Int32>)
extern "C"  void FuseSDKEditorSession_remove_AdDidShow_m2831528112 (Il2CppObject * __this /* static, unused */, Action_2_t3619260338 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_RewardedAdCompletedWithObject(System.Action`1<FuseMisc.RewardedInfo>)
extern "C"  void FuseSDKEditorSession_add_RewardedAdCompletedWithObject_m1252814739 (Il2CppObject * __this /* static, unused */, Action_1_t978424956 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_RewardedAdCompletedWithObject(System.Action`1<FuseMisc.RewardedInfo>)
extern "C"  void FuseSDKEditorSession_remove_RewardedAdCompletedWithObject_m1046418022 (Il2CppObject * __this /* static, unused */, Action_1_t978424956 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_IAPOfferAcceptedWithObject(System.Action`1<FuseMisc.IAPOfferInfo>)
extern "C"  void FuseSDKEditorSession_add_IAPOfferAcceptedWithObject_m2574343878 (Il2CppObject * __this /* static, unused */, Action_1_t3974970450 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_IAPOfferAcceptedWithObject(System.Action`1<FuseMisc.IAPOfferInfo>)
extern "C"  void FuseSDKEditorSession_remove_IAPOfferAcceptedWithObject_m294577363 (Il2CppObject * __this /* static, unused */, Action_1_t3974970450 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_VirtualGoodsOfferAcceptedWithObject(System.Action`1<FuseMisc.VGOfferInfo>)
extern "C"  void FuseSDKEditorSession_add_VirtualGoodsOfferAcceptedWithObject_m3645421402 (Il2CppObject * __this /* static, unused */, Action_1_t1534247929 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_VirtualGoodsOfferAcceptedWithObject(System.Action`1<FuseMisc.VGOfferInfo>)
extern "C"  void FuseSDKEditorSession_remove_VirtualGoodsOfferAcceptedWithObject_m1039786087 (Il2CppObject * __this /* static, unused */, Action_1_t1534247929 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::add_TimeUpdated(System.Action`1<System.DateTime>)
extern "C"  void FuseSDKEditorSession_add_TimeUpdated_m1171609343 (Il2CppObject * __this /* static, unused */, Action_1_t487486641 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::remove_TimeUpdated(System.Action`1<System.DateTime>)
extern "C"  void FuseSDKEditorSession_remove_TimeUpdated_m3301540562 (Il2CppObject * __this /* static, unused */, Action_1_t487486641 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::StartSession(System.String)
extern "C"  void FuseSDKEditorSession_StartSession_m2190081557 (Il2CppObject * __this /* static, unused */, String_t* ___gameID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::RegisterAndroidInAppPurchase(FuseMisc.IAPState,System.String,System.String,System.String,System.DateTime,System.String,System.Double,System.String)
extern "C"  void FuseSDKEditorSession_RegisterAndroidInAppPurchase_m718182888 (Il2CppObject * __this /* static, unused */, int32_t ___purchaseState, String_t* ___purchaseToken, String_t* ___productId, String_t* ___orderId, DateTime_t339033936  ___purchaseTime, String_t* ___developerPayload, double ___price, String_t* ___currency, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::RegisterVirtualGoodsPurchase(System.Int32,System.Int32,System.Int32)
extern "C"  void FuseSDKEditorSession_RegisterVirtualGoodsPurchase_m1845817689 (Il2CppObject * __this /* static, unused */, int32_t ___virtualgoodID, int32_t ___currencyAmount, int32_t ___currencyID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::IsAdAvailableForZoneID(System.String)
extern "C"  bool FuseSDKEditorSession_IsAdAvailableForZoneID_m2345193409 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::ZoneHasRewarded(System.String)
extern "C"  bool FuseSDKEditorSession_ZoneHasRewarded_m2134350225 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::ZoneHasIAPOffer(System.String)
extern "C"  bool FuseSDKEditorSession_ZoneHasIAPOffer_m4280143867 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::ZoneHasVirtualGoodsOffer(System.String)
extern "C"  bool FuseSDKEditorSession_ZoneHasVirtualGoodsOffer_m3577967474 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FuseMisc.RewardedInfo FuseSDKEditorSession::GetRewardedInfoForZone(System.String)
extern "C"  RewardedInfo_t829972251  FuseSDKEditorSession_GetRewardedInfoForZone_m4264231856 (Il2CppObject * __this /* static, unused */, String_t* ___zonId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FuseMisc.VGOfferInfo FuseSDKEditorSession::GetVGOfferInfoForZone(System.String)
extern "C"  VGOfferInfo_t1385795224  FuseSDKEditorSession_GetVGOfferInfoForZone_m2596627020 (Il2CppObject * __this /* static, unused */, String_t* ___zonId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FuseMisc.IAPOfferInfo FuseSDKEditorSession::GetIAPOfferInfoForZone(System.String)
extern "C"  IAPOfferInfo_t3826517745  FuseSDKEditorSession_GetIAPOfferInfoForZone_m3575175644 (Il2CppObject * __this /* static, unused */, String_t* ___zonId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::ShowAdForZoneID(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void FuseSDKEditorSession_ShowAdForZoneID_m887360078 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, Dictionary_2_t2606186806 * ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::PreloadAdForZoneID(System.String)
extern "C"  void FuseSDKEditorSession_PreloadAdForZoneID_m1884682405 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::DisplayMoreGames()
extern "C"  void FuseSDKEditorSession_DisplayMoreGames_m1335209603 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::SetRewardedVideoUserID(System.String)
extern "C"  void FuseSDKEditorSession_SetRewardedVideoUserID_m1964975224 (Il2CppObject * __this /* static, unused */, String_t* ___userID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::DisplayNotifications()
extern "C"  void FuseSDKEditorSession_DisplayNotifications_m1436042495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::IsNotificationAvailable()
extern "C"  bool FuseSDKEditorSession_IsNotificationAvailable_m2887803049 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::RegisterGender(FuseMisc.Gender)
extern "C"  void FuseSDKEditorSession_RegisterGender_m2171014835 (Il2CppObject * __this /* static, unused */, int32_t ___gender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::RegisterAge(System.Int32)
extern "C"  void FuseSDKEditorSession_RegisterAge_m4156558838 (Il2CppObject * __this /* static, unused */, int32_t ___age, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::RegisterBirthday(System.Int32,System.Int32,System.Int32)
extern "C"  void FuseSDKEditorSession_RegisterBirthday_m230959658 (Il2CppObject * __this /* static, unused */, int32_t ___year, int32_t ___month, int32_t ___day, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::RegisterLevel(System.Int32)
extern "C"  void FuseSDKEditorSession_RegisterLevel_m3738492539 (Il2CppObject * __this /* static, unused */, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::RegisterCurrency(System.Int32,System.Int32)
extern "C"  bool FuseSDKEditorSession_RegisterCurrency_m3073762693 (Il2CppObject * __this /* static, unused */, int32_t ___currencyType, int32_t ___balance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::RegisterParentalConsent(System.Boolean)
extern "C"  void FuseSDKEditorSession_RegisterParentalConsent_m2633699426 (Il2CppObject * __this /* static, unused */, bool ___consentGranted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::RegisterCustomEvent(System.Int32,System.String)
extern "C"  bool FuseSDKEditorSession_RegisterCustomEvent_m3359217192 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::RegisterCustomEvent(System.Int32,System.Int32)
extern "C"  bool FuseSDKEditorSession_RegisterCustomEvent_m1340555627 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDKEditorSession::GetFuseId()
extern "C"  String_t* FuseSDKEditorSession_GetFuseId_m2464709336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDKEditorSession::GetOriginalAccountAlias()
extern "C"  String_t* FuseSDKEditorSession_GetOriginalAccountAlias_m300719568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDKEditorSession::GetOriginalAccountId()
extern "C"  String_t* FuseSDKEditorSession_GetOriginalAccountId_m2153417021 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FuseMisc.AccountType FuseSDKEditorSession::GetOriginalAccountType()
extern "C"  int32_t FuseSDKEditorSession_GetOriginalAccountType_m3297055152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::GameCenterLogin()
extern "C"  void FuseSDKEditorSession_GameCenterLogin_m2558806027 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::FacebookLogin(System.String,System.String,System.String)
extern "C"  void FuseSDKEditorSession_FacebookLogin_m289228718 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId, String_t* ___name, String_t* ___accessToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::TwitterLogin(System.String,System.String)
extern "C"  void FuseSDKEditorSession_TwitterLogin_m784203759 (Il2CppObject * __this /* static, unused */, String_t* ___twitterId, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::FuseLogin(System.String,System.String)
extern "C"  void FuseSDKEditorSession_FuseLogin_m3903761613 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::EmailLogin(System.String,System.String)
extern "C"  void FuseSDKEditorSession_EmailLogin_m593963224 (Il2CppObject * __this /* static, unused */, String_t* ___email, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::DeviceLogin(System.String)
extern "C"  void FuseSDKEditorSession_DeviceLogin_m1437345478 (Il2CppObject * __this /* static, unused */, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::GooglePlayLogin(System.String,System.String)
extern "C"  void FuseSDKEditorSession_GooglePlayLogin_m2605133177 (Il2CppObject * __this /* static, unused */, String_t* ___alias, String_t* ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FuseSDKEditorSession::GamesPlayed()
extern "C"  int32_t FuseSDKEditorSession_GamesPlayed_m1060032239 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDKEditorSession::LibraryVersion()
extern "C"  String_t* FuseSDKEditorSession_LibraryVersion_m792722809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::Connected()
extern "C"  bool FuseSDKEditorSession_Connected_m2272248414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::UTCTimeFromServer()
extern "C"  void FuseSDKEditorSession_UTCTimeFromServer_m1341465415 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::FuseLog(System.String)
extern "C"  void FuseSDKEditorSession_FuseLog_m3059817302 (Il2CppObject * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::EnableData()
extern "C"  void FuseSDKEditorSession_EnableData_m1121304006 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::DisableData()
extern "C"  void FuseSDKEditorSession_DisableData_m844927483 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDKEditorSession::DataEnabled()
extern "C"  bool FuseSDKEditorSession_DataEnabled_m4285368076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::UpdateFriendsListFromServer()
extern "C"  void FuseSDKEditorSession_UpdateFriendsListFromServer_m2790017152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FuseMisc.Friend> FuseSDKEditorSession::GetFriendsList()
extern "C"  List_1_t1333671126 * FuseSDKEditorSession_GetFriendsList_m3329490966 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::AddFriend(System.String)
extern "C"  void FuseSDKEditorSession_AddFriend_m3146199898 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::RemoveFriend(System.String)
extern "C"  void FuseSDKEditorSession_RemoveFriend_m3423788007 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::AcceptFriend(System.String)
extern "C"  void FuseSDKEditorSession_AcceptFriend_m481810915 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::RejectFriend(System.String)
extern "C"  void FuseSDKEditorSession_RejectFriend_m3348754348 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::MigrateFriends(System.String)
extern "C"  void FuseSDKEditorSession_MigrateFriends_m1704815551 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::UserPushNotification(System.String,System.String)
extern "C"  void FuseSDKEditorSession_UserPushNotification_m3852551125 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::FriendsPushNotification(System.String)
extern "C"  void FuseSDKEditorSession_FriendsPushNotification_m2593394111 (Il2CppObject * __this /* static, unused */, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDKEditorSession::GetGameConfigurationValue(System.String)
extern "C"  String_t* FuseSDKEditorSession_GetGameConfigurationValue_m1369309113 (Il2CppObject * __this /* static, unused */, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> FuseSDKEditorSession::GetGameConfiguration()
extern "C"  Dictionary_2_t2606186806 * FuseSDKEditorSession_GetGameConfiguration_m1537596949 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::<SessionStartReceived>m__0()
extern "C"  void FuseSDKEditorSession_U3CSessionStartReceivedU3Em__0_m888870641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::<SessionLoginError>m__1(FuseMisc.FuseError)
extern "C"  void FuseSDKEditorSession_U3CSessionLoginErrorU3Em__1_m2226130668 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDKEditorSession::<GameConfigurationReceived>m__2()
extern "C"  void FuseSDKEditorSession_U3CGameConfigurationReceivedU3Em__2_m3911220675 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
