﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FuseMisc_Friend536712157.h"

// System.String FuseMisc.Friend::ToString()
extern "C"  String_t* Friend_ToString_m3695670561 (Friend_t536712157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Friend_t536712157;
struct Friend_t536712157_marshaled;

extern "C" void Friend_t536712157_marshal(const Friend_t536712157& unmarshaled, Friend_t536712157_marshaled& marshaled);
extern "C" void Friend_t536712157_marshal_back(const Friend_t536712157_marshaled& marshaled, Friend_t536712157& unmarshaled);
extern "C" void Friend_t536712157_marshal_cleanup(Friend_t536712157_marshaled& marshaled);
