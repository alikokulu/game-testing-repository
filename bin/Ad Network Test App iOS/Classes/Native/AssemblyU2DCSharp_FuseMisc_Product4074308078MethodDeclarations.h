﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FuseMisc_Product4074308078.h"

// System.String FuseMisc.Product::ToString()
extern "C"  String_t* Product_ToString_m3186355526 (Product_t4074308078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Product_t4074308078;
struct Product_t4074308078_marshaled;

extern "C" void Product_t4074308078_marshal(const Product_t4074308078& unmarshaled, Product_t4074308078_marshaled& marshaled);
extern "C" void Product_t4074308078_marshal_back(const Product_t4074308078_marshaled& marshaled, Product_t4074308078& unmarshaled);
extern "C" void Product_t4074308078_marshal_cleanup(Product_t4074308078_marshaled& marshaled);
