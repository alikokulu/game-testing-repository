﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuseSDK_Unibill_Android
struct FuseSDK_Unibill_Android_t522151841;

#include "codegen/il2cpp-codegen.h"

// System.Void FuseSDK_Unibill_Android::.ctor()
extern "C"  void FuseSDK_Unibill_Android__ctor_m1484979434 (FuseSDK_Unibill_Android_t522151841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
