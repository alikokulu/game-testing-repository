﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t3053238937;
// System.String
struct String_t;
// FuseSDK
struct FuseSDK_t1159654649;
// System.Action
struct Action_t437523947;
// System.Action`1<FuseMisc.FuseError>
struct Action_1_t592684423;
// System.Action`2<FuseMisc.AccountType,System.String>
struct Action_2_t535153670;
// System.Action`2<System.String,FuseMisc.FuseError>
struct Action_2_t2362964390;
// System.Action`1<System.String>
struct Action_1_t1116941607;
// System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>
struct Action_1_t1482123831;
// System.Action`3<System.Int32,System.String,System.String>
struct Action_3_t3047918030;
// System.Action`2<System.Boolean,FuseMisc.FuseError>
struct Action_2_t3211021459;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t3619260338;
// System.Action`1<FuseMisc.RewardedInfo>
struct Action_1_t978424956;
// System.Action`1<FuseMisc.IAPOfferInfo>
struct Action_1_t3974970450;
// System.Action`1<FuseMisc.VGOfferInfo>
struct Action_1_t1534247929;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// System.Action`2<System.Int32,FuseMisc.FuseError>
struct Action_2_t1216077269;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Action`4<System.Int32,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Action_4_t4157318299;
// System.Collections.Generic.List`1<FuseMisc.Friend>
struct List_1_t1333671126;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Double[]
struct DoubleU5BU5D_t1048280995;
// System.Single[]
struct SingleU5BU5D_t1219431280;
// System.Object
struct Il2CppObject;
// FuseSDK_Prime31StoreKit
struct FuseSDK_Prime31StoreKit_t67136268;
// FuseSDK_Unibill_iOS
struct FuseSDK_Unibill_iOS_t2534778463;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// FuseMisc.Product[]
struct ProductU5BU5D_t4249436827;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3840643258;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// FuseSDK/<SetupPushNotifications>c__Iterator0
struct U3CSetupPushNotificationsU3Ec__Iterator0_t552020816;
// FuseSDK_Prime31_IAB
struct FuseSDK_Prime31_IAB_t3193163170;
// FuseSDK_SoomlaIAP
struct FuseSDK_SoomlaIAP_t316686275;
// FuseSDK_Unibill_Android
struct FuseSDK_Unibill_Android_t522151841;
// FuseSDKEditorSession
struct FuseSDKEditorSession_t360633328;
// System.Action`2<System.String,System.String>
struct Action_2_t2887221574;
// JSONObject
struct JSONObject_t1752376903;
// System.Collections.Generic.Dictionary`2<System.String,JSONObject>
struct Dictionary_2_t3390074807;
// JSONObject/AddJSONConents
struct AddJSONConents_t3346118721;
// JSONObject[]
struct JSONObjectU5BU5D_t1578151102;
// System.Char[]
struct CharU5BU5D_t3416858730;
// JSONObject/FieldNotFound
struct FieldNotFound_t2014471017;
// JSONObject/GetFieldResponse
struct GetFieldResponse_t1656258501;
// System.Collections.IEnumerable
struct IEnumerable_t287189635;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// UnityEngine.WWWForm
struct WWWForm_t3999572776;
// JSONObject/<BakeAsync>c__Iterator1
struct U3CBakeAsyncU3Ec__Iterator1_t726194327;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// JSONObject/<PrintAsync>c__Iterator2
struct U3CPrintAsyncU3Ec__Iterator2_t1765946676;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t2451595350;
// JSONObject/<StringifyAsync>c__Iterator3
struct U3CStringifyAsyncU3Ec__Iterator3_t555783763;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// SDKManager
struct SDKManager_t2583098931;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2777878083.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2777878083MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseMisc_AccountType958198790.h"
#include "AssemblyU2DCSharp_FuseMisc_AccountType958198790MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseMisc_Constants5852750.h"
#include "AssemblyU2DCSharp_FuseMisc_Constants5852750MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_FuseMisc_Friend536712157.h"
#include "AssemblyU2DCSharp_FuseMisc_Friend536712157MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject1752376903MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"
#include "AssemblyU2DCSharp_JSONObject_Type2622298.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharp_FuseMisc_FuseError444231718.h"
#include "AssemblyU2DCSharp_FuseMisc_FuseError444231718MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseMisc_FuseExtensions2761543572.h"
#include "AssemblyU2DCSharp_FuseMisc_FuseExtensions2761543572MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_DateTimeKind3550648708.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharp_FuseMisc_Gender553483264.h"
#include "AssemblyU2DCSharp_FuseMisc_Gender553483264MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPOfferInfo3826517745.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPOfferInfo3826517745MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding180559927MethodDeclarations.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPState1023998552.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPState1023998552MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseMisc_Product4074308078.h"
#include "AssemblyU2DCSharp_FuseMisc_Product4074308078MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseMisc_RewardedInfo829972251.h"
#include "AssemblyU2DCSharp_FuseMisc_RewardedInfo829972251MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseMisc_VGOfferInfo1385795224.h"
#include "AssemblyU2DCSharp_FuseMisc_VGOfferInfo1385795224MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseSDK1159654649.h"
#include "AssemblyU2DCSharp_FuseSDK1159654649MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1333671126MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1333671126.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_Action_1_gen592684423.h"
#include "System_Core_System_Action_2_gen535153670.h"
#include "System_Core_System_Action_2_gen2362964390.h"
#include "mscorlib_System_Action_1_gen1116941607.h"
#include "mscorlib_System_Action_1_gen1482123831.h"
#include "System_Core_System_Action_3_gen3047918030.h"
#include "System_Core_System_Action_2_gen3211021459.h"
#include "System_Core_System_Action_2_gen3619260338.h"
#include "mscorlib_System_Action_1_gen978424956.h"
#include "mscorlib_System_Action_1_gen3974970450.h"
#include "mscorlib_System_Action_1_gen1534247929.h"
#include "mscorlib_System_Action_1_gen487486641.h"
#include "System_Core_System_Action_2_gen1216077269.h"
#include "mscorlib_System_Action_1_gen2995867492.h"
#include "System_Core_System_Action_4_gen4157318299.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen592684423MethodDeclarations.h"
#include "System_Core_System_Action_3_gen3047918030MethodDeclarations.h"
#include "System_Core_System_Action_2_gen3211021459MethodDeclarations.h"
#include "System_Core_System_Action_2_gen3619260338MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen978424956MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3974970450MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1534247929MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1116941607MethodDeclarations.h"
#include "System_Core_System_Action_2_gen535153670MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2362964390MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen487486641MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1482123831MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1216077269MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492MethodDeclarations.h"
#include "System_Core_System_Action_4_gen4157318299MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "AssemblyU2DCSharp_FuseSDK_Prime31StoreKit67136268.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "AssemblyU2DCSharp_FuseSDK_Unibill_iOS2534778463.h"
#include "UnityEngine_UnityEngine_iOS_NotificationServices3111793627MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification1121285571MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification1121285571.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_Collections_Hashtable3875263730MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke634494790.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va233356604.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke634494790MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va233356604MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseSDK_U3CSetupPushNotifications552020816MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseSDK_U3CSetupPushNotifications552020816.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1917318876MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "UnityEngine_UnityEngine_iOS_NotificationType104038455.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1917318876.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_FuseSDK_Prime31_IAB3193163170.h"
#include "AssemblyU2DCSharp_FuseSDK_Prime31_IAB3193163170MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseSDK_Prime31StoreKit67136268MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseSDK_SoomlaIAP316686275.h"
#include "AssemblyU2DCSharp_FuseSDK_SoomlaIAP316686275MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseSDK_Unibill_Android522151841.h"
#include "AssemblyU2DCSharp_FuseSDK_Unibill_Android522151841MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseSDK_Unibill_iOS2534778463MethodDeclarations.h"
#include "AssemblyU2DCSharp_FuseSDKEditorSession360633328.h"
#include "AssemblyU2DCSharp_FuseSDKEditorSession360633328MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Core_System_Action_2_gen2887221574.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2549335872MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2549335872.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3390074807.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3390074807MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3157102748MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22878606105MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22878606105.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3157102748.h"
#include "AssemblyU2DCSharp_JSONObject_AddJSONConents3346118721.h"
#include "AssemblyU2DCSharp_JSONObject_AddJSONConents3346118721MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1695827251MethodDeclarations.h"
#include "System_System_Diagnostics_Stopwatch2509581612MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle3184214143.h"
#include "System_System_Diagnostics_Stopwatch2509581612.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_FormatException2404802957.h"
#include "AssemblyU2DCSharp_JSONObject_FieldNotFound2014471017.h"
#include "AssemblyU2DCSharp_JSONObject_FieldNotFound2014471017MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_GetFieldResponse1656258501.h"
#include "AssemblyU2DCSharp_JSONObject_GetFieldResponse1656258501MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_U3CBakeAsyncU3Ec__Iter726194327MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_U3CBakeAsyncU3Ec__Iter726194327.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "AssemblyU2DCSharp_JSONObject_U3CPrintAsyncU3Ec__It1765946676MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_U3CPrintAsyncU3Ec__It1765946676.h"
#include "AssemblyU2DCSharp_JSONObject_U3CStringifyAsyncU3Ec_555783763MethodDeclarations.h"
#include "AssemblyU2DCSharp_JSONObject_U3CStringifyAsyncU3Ec_555783763.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm3999572776.h"
#include "UnityEngine_UnityEngine_WWWForm3999572776MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharp_JSONObject_Type2622298MethodDeclarations.h"
#include "AssemblyU2DCSharp_SDKManager2583098931.h"
#include "AssemblyU2DCSharp_SDKManager2583098931MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1006925219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleState47287592MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3733964924MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1006925219.h"
#include "UnityEngine_UnityEngine_TextAnchor551935663.h"
#include "UnityEngine_UnityEngine_GUIStyleState47287592.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_Enum2778772662MethodDeclarations.h"
#include "mscorlib_System_Enum2778772662.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI1522956648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m441016515_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m441016515(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m441016515_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<FuseSDK>()
#define GameObject_GetComponent_TisFuseSDK_t1159654649_m1525574472(__this, method) ((  FuseSDK_t1159654649 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m441016515_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<FuseSDK_Prime31StoreKit>()
#define Component_GetComponent_TisFuseSDK_Prime31StoreKit_t67136268_m279167405(__this, method) ((  FuseSDK_Prime31StoreKit_t67136268 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m2626155086_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m2626155086(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2626155086_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<FuseSDK_Prime31StoreKit>()
#define GameObject_AddComponent_TisFuseSDK_Prime31StoreKit_t67136268_m535079968(__this, method) ((  FuseSDK_Prime31StoreKit_t67136268 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2626155086_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<FuseSDK_Unibill_iOS>()
#define Component_GetComponent_TisFuseSDK_Unibill_iOS_t2534778463_m931311226(__this, method) ((  FuseSDK_Unibill_iOS_t2534778463 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<FuseSDK_Unibill_iOS>()
#define GameObject_AddComponent_TisFuseSDK_Unibill_iOS_t2534778463_m1609091693(__this, method) ((  FuseSDK_Unibill_iOS_t2534778463 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2626155086_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m2892359027_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m2892359027(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m2892359027_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<FuseSDK>()
#define Object_FindObjectOfType_TisFuseSDK_t1159654649_m3409458168(__this /* static, unused */, method) ((  FuseSDK_t1159654649 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m2892359027_gshared)(__this /* static, unused */, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m1195909660_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m1195909660(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m1195909660_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m2199204590(__this /* static, unused */, p0, method) ((  StringU5BU5D_t2956870243* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m1195909660_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Array::IndexOf<System.Char>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisChar_t2778706699_m3290527695_gshared (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* p0, uint16_t p1, const MethodInfo* method);
#define Array_IndexOf_TisChar_t2778706699_m3290527695(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CharU5BU5D_t3416858730*, uint16_t, const MethodInfo*))Array_IndexOf_TisChar_t2778706699_m3290527695_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void <PrivateImplementationDetails>::.ctor()
extern "C"  void U3CPrivateImplementationDetailsU3E__ctor_m795736486 (U3CPrivateImplementationDetailsU3E_t3053238937 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal(const U24ArrayTypeU248_t2777878084& unmarshaled, U24ArrayTypeU248_t2777878084_marshaled& marshaled)
{
}
extern "C" void U24ArrayTypeU248_t2777878084_marshal_back(const U24ArrayTypeU248_t2777878084_marshaled& marshaled, U24ArrayTypeU248_t2777878084& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal_cleanup(U24ArrayTypeU248_t2777878084_marshaled& marshaled)
{
}
// System.Void FuseMisc.Constants::.cctor()
extern TypeInfo* Constants_t5852750_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1422388412;
extern Il2CppCodeGenString* _stringLiteral3208170945;
extern Il2CppCodeGenString* _stringLiteral2412909066;
extern Il2CppCodeGenString* _stringLiteral852994908;
extern Il2CppCodeGenString* _stringLiteral1556949703;
extern const uint32_t Constants__cctor_m1916380544_MetadataUsageId;
extern "C"  void Constants__cctor_m1916380544 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Constants__cctor_m1916380544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Constants_t5852750_StaticFields*)Constants_t5852750_il2cpp_TypeInfo_var->static_fields)->set_RewardedAdOptionKey_ShowPreRoll_0(_stringLiteral1422388412);
		((Constants_t5852750_StaticFields*)Constants_t5852750_il2cpp_TypeInfo_var->static_fields)->set_RewardedAdOptionKey_ShowPostRoll_1(_stringLiteral3208170945);
		((Constants_t5852750_StaticFields*)Constants_t5852750_il2cpp_TypeInfo_var->static_fields)->set_RewardedOptionKey_PreRollYesButtonText_2(_stringLiteral2412909066);
		((Constants_t5852750_StaticFields*)Constants_t5852750_il2cpp_TypeInfo_var->static_fields)->set_RewardedOptionKey_PreRollNoButtonText_3(_stringLiteral852994908);
		((Constants_t5852750_StaticFields*)Constants_t5852750_il2cpp_TypeInfo_var->static_fields)->set_RewardedOptionKey_PostRollContinueButtonText_4(_stringLiteral1556949703);
		return;
	}
}
// System.String FuseMisc.Friend::ToString()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2115617916;
extern Il2CppCodeGenString* _stringLiteral176937992;
extern Il2CppCodeGenString* _stringLiteral63350320;
extern Il2CppCodeGenString* _stringLiteral982065527;
extern Il2CppCodeGenString* _stringLiteral2112550590;
extern const uint32_t Friend_ToString_m3695670561_MetadataUsageId;
extern "C"  String_t* Friend_ToString_m3695670561 (Friend_t536712157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Friend_ToString_m3695670561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	JSONObject_t1752376903 * V_1 = NULL;
	{
		JSONObject_t1752376903 * L_0 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_0, 3, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_1, 3, /*hidden argument*/NULL);
		V_1 = L_1;
		JSONObject_t1752376903 * L_2 = V_1;
		String_t* L_3 = __this->get_FuseId_0();
		NullCheck(L_2);
		JSONObject_AddField_m2361299573(L_2, _stringLiteral2115617916, L_3, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_4 = V_1;
		String_t* L_5 = __this->get_AccountId_1();
		NullCheck(L_4);
		JSONObject_AddField_m2361299573(L_4, _stringLiteral176937992, L_5, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_6 = V_1;
		String_t* L_7 = __this->get_Alias_2();
		NullCheck(L_6);
		JSONObject_AddField_m2361299573(L_6, _stringLiteral63350320, L_7, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_8 = V_1;
		bool L_9 = __this->get_Pending_3();
		NullCheck(L_8);
		JSONObject_AddField_m396898404(L_8, _stringLiteral982065527, L_9, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_10 = V_0;
		JSONObject_t1752376903 * L_11 = V_1;
		NullCheck(L_10);
		JSONObject_AddField_m574028614(L_10, _stringLiteral2112550590, L_11, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String JSONObject::ToString() */, L_12);
		return L_13;
	}
}
// Conversion methods for marshalling of: FuseMisc.Friend
extern "C" void Friend_t536712157_marshal(const Friend_t536712157& unmarshaled, Friend_t536712157_marshaled& marshaled)
{
	marshaled.___FuseId_0 = il2cpp_codegen_marshal_string(unmarshaled.get_FuseId_0());
	marshaled.___AccountId_1 = il2cpp_codegen_marshal_string(unmarshaled.get_AccountId_1());
	marshaled.___Alias_2 = il2cpp_codegen_marshal_string(unmarshaled.get_Alias_2());
	marshaled.___Pending_3 = unmarshaled.get_Pending_3();
}
extern "C" void Friend_t536712157_marshal_back(const Friend_t536712157_marshaled& marshaled, Friend_t536712157& unmarshaled)
{
	unmarshaled.set_FuseId_0(il2cpp_codegen_marshal_string_result(marshaled.___FuseId_0));
	unmarshaled.set_AccountId_1(il2cpp_codegen_marshal_string_result(marshaled.___AccountId_1));
	unmarshaled.set_Alias_2(il2cpp_codegen_marshal_string_result(marshaled.___Alias_2));
	bool unmarshaled_Pending_temp = false;
	unmarshaled_Pending_temp = marshaled.___Pending_3;
	unmarshaled.set_Pending_3(unmarshaled_Pending_temp);
}
// Conversion method for clean up from marshalling of: FuseMisc.Friend
extern "C" void Friend_t536712157_marshal_cleanup(Friend_t536712157_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___FuseId_0);
	marshaled.___FuseId_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___AccountId_1);
	marshaled.___AccountId_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___Alias_2);
	marshaled.___Alias_2 = NULL;
}
// System.Void FuseMisc.FuseExtensions::.cctor()
extern TypeInfo* FuseExtensions_t2761543572_il2cpp_TypeInfo_var;
extern const uint32_t FuseExtensions__cctor_m635126936_MetadataUsageId;
extern "C"  void FuseExtensions__cctor_m635126936 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseExtensions__cctor_m635126936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t339033936  L_0;
		memset(&L_0, 0, sizeof(L_0));
		DateTime__ctor_m3805233578(&L_0, ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		((FuseExtensions_t2761543572_StaticFields*)FuseExtensions_t2761543572_il2cpp_TypeInfo_var->static_fields)->set_unixEpoch_0(L_0);
		return;
	}
}
// System.Int64 FuseMisc.FuseExtensions::ToUnixTimestamp(System.DateTime)
extern TypeInfo* FuseExtensions_t2761543572_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t FuseExtensions_ToUnixTimestamp_m1753829763_MetadataUsageId;
extern "C"  int64_t FuseExtensions_ToUnixTimestamp_m1753829763 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseExtensions_ToUnixTimestamp_m1753829763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t339033936  L_0 = ___dateTime;
		IL2CPP_RUNTIME_CLASS_INIT(FuseExtensions_t2761543572_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_1 = ((FuseExtensions_t2761543572_StaticFields*)FuseExtensions_t2761543572_il2cpp_TypeInfo_var->static_fields)->get_unixEpoch_0();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_2 = DateTime_op_Subtraction_m3612355463(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		double L_3 = TimeSpan_get_TotalSeconds_m3163750087((&V_0), /*hidden argument*/NULL);
		return (((int64_t)((int64_t)L_3)));
	}
}
// System.DateTime FuseMisc.FuseExtensions::ToDateTime(System.Int64)
extern TypeInfo* FuseExtensions_t2761543572_il2cpp_TypeInfo_var;
extern const uint32_t FuseExtensions_ToDateTime_m670214590_MetadataUsageId;
extern "C"  DateTime_t339033936  FuseExtensions_ToDateTime_m670214590 (Il2CppObject * __this /* static, unused */, int64_t ___timestamp, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseExtensions_ToDateTime_m670214590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseExtensions_t2761543572_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_0 = ((FuseExtensions_t2761543572_StaticFields*)FuseExtensions_t2761543572_il2cpp_TypeInfo_var->static_fields)->get_unixEpoch_0();
		V_0 = L_0;
		int64_t L_1 = ___timestamp;
		DateTime_t339033936  L_2 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_1))), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void FuseMisc.IAPOfferInfo::.ctor(System.String)
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* FuseExtensions_t2761543572_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1680215285;
extern const uint32_t IAPOfferInfo__ctor_m3107304778_MetadataUsageId;
extern "C"  void IAPOfferInfo__ctor_m3107304778 (IAPOfferInfo_t3826517745 * __this, String_t* ___infoString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPOfferInfo__ctor_m3107304778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int64_t V_3 = 0;
	int64_t V_4 = 0;
	Exception_t1967233988 * V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	IAPOfferInfo_t3826517745 * G_B2_0 = NULL;
	IAPOfferInfo_t3826517745 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	IAPOfferInfo_t3826517745 * G_B3_1 = NULL;
	IAPOfferInfo_t3826517745 * G_B5_0 = NULL;
	IAPOfferInfo_t3826517745 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	IAPOfferInfo_t3826517745 * G_B6_1 = NULL;
	IAPOfferInfo_t3826517745 * G_B8_0 = NULL;
	IAPOfferInfo_t3826517745 * G_B7_0 = NULL;
	int64_t G_B9_0 = 0;
	IAPOfferInfo_t3826517745 * G_B9_1 = NULL;
	IAPOfferInfo_t3826517745 * G_B11_0 = NULL;
	IAPOfferInfo_t3826517745 * G_B10_0 = NULL;
	int64_t G_B12_0 = 0;
	IAPOfferInfo_t3826517745 * G_B12_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___infoString;
			CharU5BU5D_t3416858730* L_1 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_1);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
			(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
			NullCheck(L_0);
			StringU5BU5D_t2956870243* L_2 = String_Split_m290179486(L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
			Encoding_t180559927 * L_3 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			StringU5BU5D_t2956870243* L_4 = V_0;
			NullCheck(L_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
			int32_t L_5 = 0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			ByteU5BU5D_t58506160* L_6 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_7 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_6);
			__this->set_ProductId_0(L_7);
			StringU5BU5D_t2956870243* L_8 = V_0;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
			int32_t L_9 = 1;
			bool L_10 = Single_TryParse_m3535027506(NULL /*static, unused*/, ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9))), (&V_1), /*hidden argument*/NULL);
			G_B1_0 = __this;
			if (!L_10)
			{
				G_B2_0 = __this;
				goto IL_0040;
			}
		}

IL_003a:
		{
			float L_11 = V_1;
			G_B3_0 = L_11;
			G_B3_1 = G_B1_0;
			goto IL_0045;
		}

IL_0040:
		{
			G_B3_0 = (0.0f);
			G_B3_1 = G_B2_0;
		}

IL_0045:
		{
			G_B3_1->set_ProductPrice_1(G_B3_0);
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
			Encoding_t180559927 * L_12 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			StringU5BU5D_t2956870243* L_13 = V_0;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
			int32_t L_14 = 2;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			ByteU5BU5D_t58506160* L_15 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, ((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14))), /*hidden argument*/NULL);
			NullCheck(L_12);
			String_t* L_16 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_12, L_15);
			__this->set_ItemName_2(L_16);
			StringU5BU5D_t2956870243* L_17 = V_0;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
			int32_t L_18 = 3;
			bool L_19 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), (&V_2), /*hidden argument*/NULL);
			G_B4_0 = __this;
			if (!L_19)
			{
				G_B5_0 = __this;
				goto IL_0078;
			}
		}

IL_0072:
		{
			int32_t L_20 = V_2;
			G_B6_0 = L_20;
			G_B6_1 = G_B4_0;
			goto IL_0079;
		}

IL_0078:
		{
			G_B6_0 = 0;
			G_B6_1 = G_B5_0;
		}

IL_0079:
		{
			G_B6_1->set_ItemAmount_3(G_B6_0);
			StringU5BU5D_t2956870243* L_21 = V_0;
			NullCheck(L_21);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 4);
			int32_t L_22 = 4;
			bool L_23 = Int64_TryParse_m2106581948(NULL /*static, unused*/, ((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22))), (&V_3), /*hidden argument*/NULL);
			G_B7_0 = __this;
			if (!L_23)
			{
				G_B8_0 = __this;
				goto IL_0094;
			}
		}

IL_008e:
		{
			int64_t L_24 = V_3;
			G_B9_0 = L_24;
			G_B9_1 = G_B7_0;
			goto IL_0096;
		}

IL_0094:
		{
			G_B9_0 = (((int64_t)((int64_t)0)));
			G_B9_1 = G_B8_0;
		}

IL_0096:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FuseExtensions_t2761543572_il2cpp_TypeInfo_var);
			DateTime_t339033936  L_25 = FuseExtensions_ToDateTime_m670214590(NULL /*static, unused*/, G_B9_0, /*hidden argument*/NULL);
			G_B9_1->set_StartTime_4(L_25);
			StringU5BU5D_t2956870243* L_26 = V_0;
			NullCheck(L_26);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
			int32_t L_27 = 5;
			bool L_28 = Int64_TryParse_m2106581948(NULL /*static, unused*/, ((L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27))), (&V_4), /*hidden argument*/NULL);
			G_B10_0 = __this;
			if (!L_28)
			{
				G_B11_0 = __this;
				goto IL_00b7;
			}
		}

IL_00b0:
		{
			int64_t L_29 = V_4;
			G_B12_0 = L_29;
			G_B12_1 = G_B10_0;
			goto IL_00b9;
		}

IL_00b7:
		{
			G_B12_0 = (((int64_t)((int64_t)0)));
			G_B12_1 = G_B11_0;
		}

IL_00b9:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FuseExtensions_t2761543572_il2cpp_TypeInfo_var);
			DateTime_t339033936  L_30 = FuseExtensions_ToDateTime_m670214590(NULL /*static, unused*/, G_B12_0, /*hidden argument*/NULL);
			G_B12_1->set_EndTime_5(L_30);
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
			Encoding_t180559927 * L_31 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			StringU5BU5D_t2956870243* L_32 = V_0;
			NullCheck(L_32);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 6);
			int32_t L_33 = 6;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			ByteU5BU5D_t58506160* L_34 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, ((L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_33))), /*hidden argument*/NULL);
			NullCheck(L_31);
			String_t* L_35 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_31, L_34);
			__this->set_Metadata_6(L_35);
			goto IL_00fd;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00e0;
		throw e;
	}

CATCH_00e0:
	{ // begin catch(System.Exception)
		{
			V_5 = ((Exception_t1967233988 *)__exception_local);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1680215285, /*hidden argument*/NULL);
			Exception_t1967233988 * L_36 = V_5;
			Debug_LogException_m248970745(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
			goto IL_00fd;
		}

IL_00f8:
		{
			; // IL_00f8: leave IL_00fd
		}
	} // end catch (depth: 1)

IL_00fd:
	{
		return;
	}
}
// System.String FuseMisc.IAPOfferInfo::ToString()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* FuseExtensions_t2761543572_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral952137290;
extern Il2CppCodeGenString* _stringLiteral1164969402;
extern Il2CppCodeGenString* _stringLiteral1241975902;
extern Il2CppCodeGenString* _stringLiteral3471882603;
extern Il2CppCodeGenString* _stringLiteral4169640495;
extern Il2CppCodeGenString* _stringLiteral57410088;
extern Il2CppCodeGenString* _stringLiteral3909607247;
extern Il2CppCodeGenString* _stringLiteral1107388882;
extern const uint32_t IAPOfferInfo_ToString_m2121294261_MetadataUsageId;
extern "C"  String_t* IAPOfferInfo_ToString_m2121294261 (IAPOfferInfo_t3826517745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPOfferInfo_ToString_m2121294261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	JSONObject_t1752376903 * V_1 = NULL;
	{
		JSONObject_t1752376903 * L_0 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_0, 3, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_1, 3, /*hidden argument*/NULL);
		V_1 = L_1;
		JSONObject_t1752376903 * L_2 = V_1;
		String_t* L_3 = __this->get_ProductId_0();
		NullCheck(L_2);
		JSONObject_AddField_m2361299573(L_2, _stringLiteral952137290, L_3, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_4 = V_1;
		float L_5 = __this->get_ProductPrice_1();
		NullCheck(L_4);
		JSONObject_AddField_m2042623262(L_4, _stringLiteral1164969402, L_5, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_6 = V_1;
		String_t* L_7 = __this->get_ItemName_2();
		NullCheck(L_6);
		JSONObject_AddField_m2361299573(L_6, _stringLiteral1241975902, L_7, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_8 = V_1;
		int32_t L_9 = __this->get_ItemAmount_3();
		NullCheck(L_8);
		JSONObject_AddField_m3386574718(L_8, _stringLiteral3471882603, L_9, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_10 = V_1;
		DateTime_t339033936  L_11 = __this->get_StartTime_4();
		IL2CPP_RUNTIME_CLASS_INIT(FuseExtensions_t2761543572_il2cpp_TypeInfo_var);
		int64_t L_12 = FuseExtensions_ToUnixTimestamp_m1753829763(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		JSONObject_AddField_m2042623262(L_10, _stringLiteral4169640495, (((float)((float)L_12))), /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_13 = V_1;
		DateTime_t339033936  L_14 = __this->get_EndTime_5();
		int64_t L_15 = FuseExtensions_ToUnixTimestamp_m1753829763(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		JSONObject_AddField_m2042623262(L_13, _stringLiteral57410088, (((float)((float)L_15))), /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_17 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_18 = __this->get_Metadata_6();
		NullCheck(L_17);
		ByteU5BU5D_t58506160* L_19 = VirtFuncInvoker1< ByteU5BU5D_t58506160*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_17, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_20 = Convert_ToBase64String_m1841808901(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		JSONObject_AddField_m2361299573(L_16, _stringLiteral3909607247, L_20, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_21 = V_0;
		JSONObject_t1752376903 * L_22 = V_1;
		NullCheck(L_21);
		JSONObject_AddField_m574028614(L_21, _stringLiteral1107388882, L_22, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String JSONObject::ToString() */, L_23);
		return L_24;
	}
}
// System.String FuseMisc.Product::ToString()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral952137290;
extern Il2CppCodeGenString* _stringLiteral77381929;
extern Il2CppCodeGenString* _stringLiteral3005380835;
extern Il2CppCodeGenString* _stringLiteral1355179215;
extern const uint32_t Product_ToString_m3186355526_MetadataUsageId;
extern "C"  String_t* Product_ToString_m3186355526 (Product_t4074308078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Product_ToString_m3186355526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	JSONObject_t1752376903 * V_1 = NULL;
	{
		JSONObject_t1752376903 * L_0 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_0, 3, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_1, 3, /*hidden argument*/NULL);
		V_1 = L_1;
		JSONObject_t1752376903 * L_2 = V_1;
		String_t* L_3 = __this->get_ProductId_0();
		NullCheck(L_2);
		JSONObject_AddField_m2361299573(L_2, _stringLiteral952137290, L_3, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_4 = V_1;
		float L_5 = __this->get_Price_1();
		NullCheck(L_4);
		JSONObject_AddField_m2042623262(L_4, _stringLiteral77381929, L_5, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_6 = V_1;
		String_t* L_7 = __this->get_PriceLocale_2();
		NullCheck(L_6);
		JSONObject_AddField_m2361299573(L_6, _stringLiteral3005380835, L_7, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_8 = V_0;
		JSONObject_t1752376903 * L_9 = V_1;
		NullCheck(L_8);
		JSONObject_AddField_m574028614(L_8, _stringLiteral1355179215, L_9, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String JSONObject::ToString() */, L_10);
		return L_11;
	}
}
// Conversion methods for marshalling of: FuseMisc.Product
extern "C" void Product_t4074308078_marshal(const Product_t4074308078& unmarshaled, Product_t4074308078_marshaled& marshaled)
{
	marshaled.___ProductId_0 = il2cpp_codegen_marshal_string(unmarshaled.get_ProductId_0());
	marshaled.___Price_1 = unmarshaled.get_Price_1();
	marshaled.___PriceLocale_2 = il2cpp_codegen_marshal_string(unmarshaled.get_PriceLocale_2());
}
extern "C" void Product_t4074308078_marshal_back(const Product_t4074308078_marshaled& marshaled, Product_t4074308078& unmarshaled)
{
	unmarshaled.set_ProductId_0(il2cpp_codegen_marshal_string_result(marshaled.___ProductId_0));
	float unmarshaled_Price_temp = 0.0f;
	unmarshaled_Price_temp = marshaled.___Price_1;
	unmarshaled.set_Price_1(unmarshaled_Price_temp);
	unmarshaled.set_PriceLocale_2(il2cpp_codegen_marshal_string_result(marshaled.___PriceLocale_2));
}
// Conversion method for clean up from marshalling of: FuseMisc.Product
extern "C" void Product_t4074308078_marshal_cleanup(Product_t4074308078_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___ProductId_0);
	marshaled.___ProductId_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___PriceLocale_2);
	marshaled.___PriceLocale_2 = NULL;
}
// System.Void FuseMisc.RewardedInfo::.ctor(System.String)
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral192468416;
extern const uint32_t RewardedInfo__ctor_m1550529012_MetadataUsageId;
extern "C"  void RewardedInfo__ctor_m1550529012 (RewardedInfo_t829972251 * __this, String_t* ___infoString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RewardedInfo__ctor_m1550529012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	RewardedInfo_t829972251 * G_B2_0 = NULL;
	RewardedInfo_t829972251 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	RewardedInfo_t829972251 * G_B3_1 = NULL;
	RewardedInfo_t829972251 * G_B5_0 = NULL;
	RewardedInfo_t829972251 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	RewardedInfo_t829972251 * G_B6_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___infoString;
			CharU5BU5D_t3416858730* L_1 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_1);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
			(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
			NullCheck(L_0);
			StringU5BU5D_t2956870243* L_2 = String_Split_m290179486(L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
			Encoding_t180559927 * L_3 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			StringU5BU5D_t2956870243* L_4 = V_0;
			NullCheck(L_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
			int32_t L_5 = 0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			ByteU5BU5D_t58506160* L_6 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_7 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_6);
			__this->set_PreRollMessage_0(L_7);
			Encoding_t180559927 * L_8 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			StringU5BU5D_t2956870243* L_9 = V_0;
			NullCheck(L_9);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
			int32_t L_10 = 1;
			ByteU5BU5D_t58506160* L_11 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), /*hidden argument*/NULL);
			NullCheck(L_8);
			String_t* L_12 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_8, L_11);
			__this->set_RewardMessage_1(L_12);
			Encoding_t180559927 * L_13 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			StringU5BU5D_t2956870243* L_14 = V_0;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
			int32_t L_15 = 2;
			ByteU5BU5D_t58506160* L_16 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15))), /*hidden argument*/NULL);
			NullCheck(L_13);
			String_t* L_17 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_13, L_16);
			__this->set_RewardItem_2(L_17);
			StringU5BU5D_t2956870243* L_18 = V_0;
			NullCheck(L_18);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 3);
			int32_t L_19 = 3;
			bool L_20 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19))), (&V_1), /*hidden argument*/NULL);
			G_B1_0 = __this;
			if (!L_20)
			{
				G_B2_0 = __this;
				goto IL_0070;
			}
		}

IL_006a:
		{
			int32_t L_21 = V_1;
			G_B3_0 = L_21;
			G_B3_1 = G_B1_0;
			goto IL_0071;
		}

IL_0070:
		{
			G_B3_0 = 0;
			G_B3_1 = G_B2_0;
		}

IL_0071:
		{
			G_B3_1->set_RewardAmount_4(G_B3_0);
			StringU5BU5D_t2956870243* L_22 = V_0;
			NullCheck(L_22);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 4);
			int32_t L_23 = 4;
			bool L_24 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23))), (&V_2), /*hidden argument*/NULL);
			G_B4_0 = __this;
			if (!L_24)
			{
				G_B5_0 = __this;
				goto IL_008c;
			}
		}

IL_0086:
		{
			int32_t L_25 = V_2;
			G_B6_0 = L_25;
			G_B6_1 = G_B4_0;
			goto IL_008d;
		}

IL_008c:
		{
			G_B6_0 = 0;
			G_B6_1 = G_B5_0;
		}

IL_008d:
		{
			G_B6_1->set_RewardItemId_3(G_B6_0);
			goto IL_00ad;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0097;
		throw e;
	}

CATCH_0097:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t1967233988 *)__exception_local);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral192468416, /*hidden argument*/NULL);
		Exception_t1967233988 * L_26 = V_3;
		Debug_LogException_m248970745(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		goto IL_00ad;
	} // end catch (depth: 1)

IL_00ad:
	{
		return;
	}
}
// System.String FuseMisc.RewardedInfo::ToString()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1842925127;
extern Il2CppCodeGenString* _stringLiteral1807996344;
extern Il2CppCodeGenString* _stringLiteral3684363010;
extern Il2CppCodeGenString* _stringLiteral1619803069;
extern Il2CppCodeGenString* _stringLiteral1384611975;
extern Il2CppCodeGenString* _stringLiteral2405810684;
extern const uint32_t RewardedInfo_ToString_m26200799_MetadataUsageId;
extern "C"  String_t* RewardedInfo_ToString_m26200799 (RewardedInfo_t829972251 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RewardedInfo_ToString_m26200799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	JSONObject_t1752376903 * V_1 = NULL;
	{
		JSONObject_t1752376903 * L_0 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_0, 3, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_1, 3, /*hidden argument*/NULL);
		V_1 = L_1;
		JSONObject_t1752376903 * L_2 = V_1;
		String_t* L_3 = __this->get_PreRollMessage_0();
		NullCheck(L_2);
		JSONObject_AddField_m2361299573(L_2, _stringLiteral1842925127, L_3, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_4 = V_1;
		String_t* L_5 = __this->get_RewardMessage_1();
		NullCheck(L_4);
		JSONObject_AddField_m2361299573(L_4, _stringLiteral1807996344, L_5, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_6 = V_1;
		String_t* L_7 = __this->get_RewardItem_2();
		NullCheck(L_6);
		JSONObject_AddField_m2361299573(L_6, _stringLiteral3684363010, L_7, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_8 = V_1;
		int32_t L_9 = __this->get_RewardItemId_3();
		NullCheck(L_8);
		JSONObject_AddField_m3386574718(L_8, _stringLiteral1619803069, L_9, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_10 = V_1;
		int32_t L_11 = __this->get_RewardAmount_4();
		NullCheck(L_10);
		JSONObject_AddField_m3386574718(L_10, _stringLiteral1384611975, L_11, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_12 = V_0;
		JSONObject_t1752376903 * L_13 = V_1;
		NullCheck(L_12);
		JSONObject_AddField_m574028614(L_12, _stringLiteral2405810684, L_13, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String JSONObject::ToString() */, L_14);
		return L_15;
	}
}
// Conversion methods for marshalling of: FuseMisc.RewardedInfo
extern "C" void RewardedInfo_t829972251_marshal(const RewardedInfo_t829972251& unmarshaled, RewardedInfo_t829972251_marshaled& marshaled)
{
	marshaled.___PreRollMessage_0 = il2cpp_codegen_marshal_string(unmarshaled.get_PreRollMessage_0());
	marshaled.___RewardMessage_1 = il2cpp_codegen_marshal_string(unmarshaled.get_RewardMessage_1());
	marshaled.___RewardItem_2 = il2cpp_codegen_marshal_string(unmarshaled.get_RewardItem_2());
	marshaled.___RewardItemId_3 = unmarshaled.get_RewardItemId_3();
	marshaled.___RewardAmount_4 = unmarshaled.get_RewardAmount_4();
}
extern "C" void RewardedInfo_t829972251_marshal_back(const RewardedInfo_t829972251_marshaled& marshaled, RewardedInfo_t829972251& unmarshaled)
{
	unmarshaled.set_PreRollMessage_0(il2cpp_codegen_marshal_string_result(marshaled.___PreRollMessage_0));
	unmarshaled.set_RewardMessage_1(il2cpp_codegen_marshal_string_result(marshaled.___RewardMessage_1));
	unmarshaled.set_RewardItem_2(il2cpp_codegen_marshal_string_result(marshaled.___RewardItem_2));
	int32_t unmarshaled_RewardItemId_temp = 0;
	unmarshaled_RewardItemId_temp = marshaled.___RewardItemId_3;
	unmarshaled.set_RewardItemId_3(unmarshaled_RewardItemId_temp);
	int32_t unmarshaled_RewardAmount_temp = 0;
	unmarshaled_RewardAmount_temp = marshaled.___RewardAmount_4;
	unmarshaled.set_RewardAmount_4(unmarshaled_RewardAmount_temp);
}
// Conversion method for clean up from marshalling of: FuseMisc.RewardedInfo
extern "C" void RewardedInfo_t829972251_marshal_cleanup(RewardedInfo_t829972251_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___PreRollMessage_0);
	marshaled.___PreRollMessage_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___RewardMessage_1);
	marshaled.___RewardMessage_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___RewardItem_2);
	marshaled.___RewardItem_2 = NULL;
}
// System.Void FuseMisc.VGOfferInfo::.ctor(System.String)
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* FuseExtensions_t2761543572_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral514738926;
extern const uint32_t VGOfferInfo__ctor_m259242847_MetadataUsageId;
extern "C"  void VGOfferInfo__ctor_m259242847 (VGOfferInfo_t1385795224 * __this, String_t* ___infoString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VGOfferInfo__ctor_m259242847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int64_t V_3 = 0;
	int64_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Exception_t1967233988 * V_7 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	VGOfferInfo_t1385795224 * G_B2_0 = NULL;
	VGOfferInfo_t1385795224 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	VGOfferInfo_t1385795224 * G_B3_1 = NULL;
	VGOfferInfo_t1385795224 * G_B5_0 = NULL;
	VGOfferInfo_t1385795224 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	VGOfferInfo_t1385795224 * G_B6_1 = NULL;
	VGOfferInfo_t1385795224 * G_B8_0 = NULL;
	VGOfferInfo_t1385795224 * G_B7_0 = NULL;
	int64_t G_B9_0 = 0;
	VGOfferInfo_t1385795224 * G_B9_1 = NULL;
	VGOfferInfo_t1385795224 * G_B11_0 = NULL;
	VGOfferInfo_t1385795224 * G_B10_0 = NULL;
	int64_t G_B12_0 = 0;
	VGOfferInfo_t1385795224 * G_B12_1 = NULL;
	VGOfferInfo_t1385795224 * G_B14_0 = NULL;
	VGOfferInfo_t1385795224 * G_B13_0 = NULL;
	int32_t G_B15_0 = 0;
	VGOfferInfo_t1385795224 * G_B15_1 = NULL;
	VGOfferInfo_t1385795224 * G_B17_0 = NULL;
	VGOfferInfo_t1385795224 * G_B16_0 = NULL;
	int32_t G_B18_0 = 0;
	VGOfferInfo_t1385795224 * G_B18_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___infoString;
			CharU5BU5D_t3416858730* L_1 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_1);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
			(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
			NullCheck(L_0);
			StringU5BU5D_t2956870243* L_2 = String_Split_m290179486(L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
			Encoding_t180559927 * L_3 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			StringU5BU5D_t2956870243* L_4 = V_0;
			NullCheck(L_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
			int32_t L_5 = 0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			ByteU5BU5D_t58506160* L_6 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_7 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_6);
			__this->set_PurchaseCurrency_1(L_7);
			StringU5BU5D_t2956870243* L_8 = V_0;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
			int32_t L_9 = 1;
			bool L_10 = Single_TryParse_m3535027506(NULL /*static, unused*/, ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9))), (&V_1), /*hidden argument*/NULL);
			G_B1_0 = __this;
			if (!L_10)
			{
				G_B2_0 = __this;
				goto IL_0040;
			}
		}

IL_003a:
		{
			float L_11 = V_1;
			G_B3_0 = L_11;
			G_B3_1 = G_B1_0;
			goto IL_0045;
		}

IL_0040:
		{
			G_B3_0 = (0.0f);
			G_B3_1 = G_B2_0;
		}

IL_0045:
		{
			G_B3_1->set_PurchasePrice_2(G_B3_0);
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
			Encoding_t180559927 * L_12 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			StringU5BU5D_t2956870243* L_13 = V_0;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
			int32_t L_14 = 2;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			ByteU5BU5D_t58506160* L_15 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, ((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14))), /*hidden argument*/NULL);
			NullCheck(L_12);
			String_t* L_16 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_12, L_15);
			__this->set_ItemName_4(L_16);
			StringU5BU5D_t2956870243* L_17 = V_0;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
			int32_t L_18 = 3;
			bool L_19 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), (&V_2), /*hidden argument*/NULL);
			G_B4_0 = __this;
			if (!L_19)
			{
				G_B5_0 = __this;
				goto IL_0078;
			}
		}

IL_0072:
		{
			int32_t L_20 = V_2;
			G_B6_0 = L_20;
			G_B6_1 = G_B4_0;
			goto IL_0079;
		}

IL_0078:
		{
			G_B6_0 = 0;
			G_B6_1 = G_B5_0;
		}

IL_0079:
		{
			G_B6_1->set_ItemAmount_5(G_B6_0);
			StringU5BU5D_t2956870243* L_21 = V_0;
			NullCheck(L_21);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 4);
			int32_t L_22 = 4;
			bool L_23 = Int64_TryParse_m2106581948(NULL /*static, unused*/, ((L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22))), (&V_3), /*hidden argument*/NULL);
			G_B7_0 = __this;
			if (!L_23)
			{
				G_B8_0 = __this;
				goto IL_0094;
			}
		}

IL_008e:
		{
			int64_t L_24 = V_3;
			G_B9_0 = L_24;
			G_B9_1 = G_B7_0;
			goto IL_0096;
		}

IL_0094:
		{
			G_B9_0 = (((int64_t)((int64_t)0)));
			G_B9_1 = G_B8_0;
		}

IL_0096:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FuseExtensions_t2761543572_il2cpp_TypeInfo_var);
			DateTime_t339033936  L_25 = FuseExtensions_ToDateTime_m670214590(NULL /*static, unused*/, G_B9_0, /*hidden argument*/NULL);
			G_B9_1->set_StartTime_6(L_25);
			StringU5BU5D_t2956870243* L_26 = V_0;
			NullCheck(L_26);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
			int32_t L_27 = 5;
			bool L_28 = Int64_TryParse_m2106581948(NULL /*static, unused*/, ((L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27))), (&V_4), /*hidden argument*/NULL);
			G_B10_0 = __this;
			if (!L_28)
			{
				G_B11_0 = __this;
				goto IL_00b7;
			}
		}

IL_00b0:
		{
			int64_t L_29 = V_4;
			G_B12_0 = L_29;
			G_B12_1 = G_B10_0;
			goto IL_00b9;
		}

IL_00b7:
		{
			G_B12_0 = (((int64_t)((int64_t)0)));
			G_B12_1 = G_B11_0;
		}

IL_00b9:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FuseExtensions_t2761543572_il2cpp_TypeInfo_var);
			DateTime_t339033936  L_30 = FuseExtensions_ToDateTime_m670214590(NULL /*static, unused*/, G_B12_0, /*hidden argument*/NULL);
			G_B12_1->set_EndTime_7(L_30);
			StringU5BU5D_t2956870243* L_31 = V_0;
			NullCheck(L_31);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 6);
			int32_t L_32 = 6;
			bool L_33 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32))), (&V_5), /*hidden argument*/NULL);
			G_B13_0 = __this;
			if (!L_33)
			{
				G_B14_0 = __this;
				goto IL_00da;
			}
		}

IL_00d3:
		{
			int32_t L_34 = V_5;
			G_B15_0 = L_34;
			G_B15_1 = G_B13_0;
			goto IL_00db;
		}

IL_00da:
		{
			G_B15_0 = 0;
			G_B15_1 = G_B14_0;
		}

IL_00db:
		{
			G_B15_1->set_CurrencyID_0(G_B15_0);
			StringU5BU5D_t2956870243* L_35 = V_0;
			NullCheck(L_35);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 7);
			int32_t L_36 = 7;
			bool L_37 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_36))), (&V_6), /*hidden argument*/NULL);
			G_B16_0 = __this;
			if (!L_37)
			{
				G_B17_0 = __this;
				goto IL_00f7;
			}
		}

IL_00f0:
		{
			int32_t L_38 = V_6;
			G_B18_0 = L_38;
			G_B18_1 = G_B16_0;
			goto IL_00f8;
		}

IL_00f7:
		{
			G_B18_0 = 0;
			G_B18_1 = G_B17_0;
		}

IL_00f8:
		{
			G_B18_1->set_VirtualGoodID_3(G_B18_0);
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
			Encoding_t180559927 * L_39 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			StringU5BU5D_t2956870243* L_40 = V_0;
			NullCheck(L_40);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 8);
			int32_t L_41 = 8;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			ByteU5BU5D_t58506160* L_42 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, ((L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_41))), /*hidden argument*/NULL);
			NullCheck(L_39);
			String_t* L_43 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t58506160* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_39, L_42);
			__this->set_Metadata_8(L_43);
			goto IL_0137;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_011a;
		throw e;
	}

CATCH_011a:
	{ // begin catch(System.Exception)
		{
			V_7 = ((Exception_t1967233988 *)__exception_local);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral514738926, /*hidden argument*/NULL);
			Exception_t1967233988 * L_44 = V_7;
			Debug_LogException_m248970745(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
			goto IL_0137;
		}

IL_0132:
		{
			; // IL_0132: leave IL_0137
		}
	} // end catch (depth: 1)

IL_0137:
	{
		return;
	}
}
// System.String FuseMisc.VGOfferInfo::ToString()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* FuseExtensions_t2761543572_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t180559927_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral904008972;
extern Il2CppCodeGenString* _stringLiteral1296239698;
extern Il2CppCodeGenString* _stringLiteral3756188840;
extern Il2CppCodeGenString* _stringLiteral926012771;
extern Il2CppCodeGenString* _stringLiteral1241975902;
extern Il2CppCodeGenString* _stringLiteral3471882603;
extern Il2CppCodeGenString* _stringLiteral4169640495;
extern Il2CppCodeGenString* _stringLiteral57410088;
extern Il2CppCodeGenString* _stringLiteral3909607247;
extern Il2CppCodeGenString* _stringLiteral2961633657;
extern const uint32_t VGOfferInfo_ToString_m254879216_MetadataUsageId;
extern "C"  String_t* VGOfferInfo_ToString_m254879216 (VGOfferInfo_t1385795224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VGOfferInfo_ToString_m254879216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	JSONObject_t1752376903 * V_1 = NULL;
	{
		JSONObject_t1752376903 * L_0 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_0, 3, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m196212956(L_1, 3, /*hidden argument*/NULL);
		V_1 = L_1;
		JSONObject_t1752376903 * L_2 = V_1;
		int32_t L_3 = __this->get_CurrencyID_0();
		NullCheck(L_2);
		JSONObject_AddField_m3386574718(L_2, _stringLiteral904008972, L_3, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_4 = V_1;
		String_t* L_5 = __this->get_PurchaseCurrency_1();
		NullCheck(L_4);
		JSONObject_AddField_m2361299573(L_4, _stringLiteral1296239698, L_5, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_6 = V_1;
		float L_7 = __this->get_PurchasePrice_2();
		NullCheck(L_6);
		JSONObject_AddField_m2042623262(L_6, _stringLiteral3756188840, L_7, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_8 = V_1;
		int32_t L_9 = __this->get_VirtualGoodID_3();
		NullCheck(L_8);
		JSONObject_AddField_m3386574718(L_8, _stringLiteral926012771, L_9, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_10 = V_1;
		String_t* L_11 = __this->get_ItemName_4();
		NullCheck(L_10);
		JSONObject_AddField_m2361299573(L_10, _stringLiteral1241975902, L_11, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_12 = V_1;
		int32_t L_13 = __this->get_ItemAmount_5();
		NullCheck(L_12);
		JSONObject_AddField_m3386574718(L_12, _stringLiteral3471882603, L_13, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_14 = V_1;
		DateTime_t339033936  L_15 = __this->get_StartTime_6();
		IL2CPP_RUNTIME_CLASS_INIT(FuseExtensions_t2761543572_il2cpp_TypeInfo_var);
		int64_t L_16 = FuseExtensions_ToUnixTimestamp_m1753829763(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		JSONObject_AddField_m2042623262(L_14, _stringLiteral4169640495, (((float)((float)L_16))), /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_17 = V_1;
		DateTime_t339033936  L_18 = __this->get_EndTime_7();
		int64_t L_19 = FuseExtensions_ToUnixTimestamp_m1753829763(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		JSONObject_AddField_m2042623262(L_17, _stringLiteral57410088, (((float)((float)L_19))), /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_21 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_22 = __this->get_Metadata_8();
		NullCheck(L_21);
		ByteU5BU5D_t58506160* L_23 = VirtFuncInvoker1< ByteU5BU5D_t58506160*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_21, L_22);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_24 = Convert_ToBase64String_m1841808901(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		JSONObject_AddField_m2361299573(L_20, _stringLiteral3909607247, L_24, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_25 = V_0;
		JSONObject_t1752376903 * L_26 = V_1;
		NullCheck(L_25);
		JSONObject_AddField_m574028614(L_25, _stringLiteral2961633657, L_26, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_27 = V_0;
		NullCheck(L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String JSONObject::ToString() */, L_27);
		return L_28;
	}
}
// System.Void FuseSDK::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK__ctor_m2659371666_MetadataUsageId;
extern "C"  void FuseSDK__ctor_m2659371666 (FuseSDK_t1159654649 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__ctor_m2659371666_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_StartAutomatically_4((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_GCM_SenderID_5(L_0);
		__this->set_registerForPushNotifications_6((bool)1);
		__this->set_logging_7((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::.cctor()
extern TypeInfo* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1333671126_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1597524044_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m694081182_MethodInfo_var;
extern const uint32_t FuseSDK__cctor_m354046811_MetadataUsageId;
extern "C"  void FuseSDK__cctor_m354046811 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__cctor_m354046811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2606186806 * L_0 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1597524044(L_0, /*hidden argument*/Dictionary_2__ctor_m1597524044_MethodInfo_var);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set__gameConfig_18(L_0);
		List_1_t1333671126 * L_1 = (List_1_t1333671126 *)il2cpp_codegen_object_new(List_1_t1333671126_il2cpp_TypeInfo_var);
		List_1__ctor_m694081182(L_1, /*hidden argument*/List_1__ctor_m694081182_MethodInfo_var);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set__friendsList_19(L_1);
		return;
	}
}
// System.Void FuseSDK::add_SessionStartReceived(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_SessionStartReceived_m1374611232_MetadataUsageId;
extern "C"  void FuseSDK_add_SessionStartReceived_m1374611232 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_SessionStartReceived_m1374611232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_SessionStartReceived_20();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_SessionStartReceived_20(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_SessionStartReceived(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_SessionStartReceived_m2813762949_MetadataUsageId;
extern "C"  void FuseSDK_remove_SessionStartReceived_m2813762949 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_SessionStartReceived_m2813762949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_SessionStartReceived_20();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_SessionStartReceived_20(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_SessionLoginError(System.Action`1<FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_SessionLoginError_m2857792873_MetadataUsageId;
extern "C"  void FuseSDK_add_SessionLoginError_m2857792873 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_SessionLoginError_m2857792873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_SessionLoginError_21();
		Action_1_t592684423 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_SessionLoginError_21(((Action_1_t592684423 *)CastclassSealed(L_2, Action_1_t592684423_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_SessionLoginError(System.Action`1<FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_SessionLoginError_m1248891620_MetadataUsageId;
extern "C"  void FuseSDK_remove_SessionLoginError_m1248891620 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_SessionLoginError_m1248891620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_SessionLoginError_21();
		Action_1_t592684423 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_SessionLoginError_21(((Action_1_t592684423 *)CastclassSealed(L_2, Action_1_t592684423_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_GameConfigurationReceived(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_GameConfigurationReceived_m3228441574_MetadataUsageId;
extern "C"  void FuseSDK_add_GameConfigurationReceived_m3228441574 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_GameConfigurationReceived_m3228441574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameConfigurationReceived_22();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_GameConfigurationReceived_22(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_GameConfigurationReceived(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_GameConfigurationReceived_m761371809_MetadataUsageId;
extern "C"  void FuseSDK_remove_GameConfigurationReceived_m761371809 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_GameConfigurationReceived_m761371809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameConfigurationReceived_22();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_GameConfigurationReceived_22(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_AccountLoginComplete(System.Action`2<FuseMisc.AccountType,System.String>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t535153670_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_AccountLoginComplete_m2612660334_MetadataUsageId;
extern "C"  void FuseSDK_add_AccountLoginComplete_m2612660334 (Il2CppObject * __this /* static, unused */, Action_2_t535153670 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_AccountLoginComplete_m2612660334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t535153670 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginComplete_23();
		Action_2_t535153670 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AccountLoginComplete_23(((Action_2_t535153670 *)CastclassSealed(L_2, Action_2_t535153670_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_AccountLoginComplete(System.Action`2<FuseMisc.AccountType,System.String>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t535153670_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_AccountLoginComplete_m1634388755_MetadataUsageId;
extern "C"  void FuseSDK_remove_AccountLoginComplete_m1634388755 (Il2CppObject * __this /* static, unused */, Action_2_t535153670 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_AccountLoginComplete_m1634388755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t535153670 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginComplete_23();
		Action_2_t535153670 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AccountLoginComplete_23(((Action_2_t535153670 *)CastclassSealed(L_2, Action_2_t535153670_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_AccountLoginError(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_AccountLoginError_m4102543117_MetadataUsageId;
extern "C"  void FuseSDK_add_AccountLoginError_m4102543117 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_AccountLoginError_m4102543117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginError_24();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AccountLoginError_24(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_AccountLoginError(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_AccountLoginError_m1721188680_MetadataUsageId;
extern "C"  void FuseSDK_remove_AccountLoginError_m1721188680 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_AccountLoginError_m1721188680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginError_24();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AccountLoginError_24(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_NotificationAction(System.Action`1<System.String>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_NotificationAction_m92288429_MetadataUsageId;
extern "C"  void FuseSDK_add_NotificationAction_m92288429 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_NotificationAction_m92288429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1116941607 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_NotificationAction_25();
		Action_1_t1116941607 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_NotificationAction_25(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_NotificationAction(System.Action`1<System.String>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_NotificationAction_m992102568_MetadataUsageId;
extern "C"  void FuseSDK_remove_NotificationAction_m992102568 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_NotificationAction_m992102568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1116941607 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_NotificationAction_25();
		Action_1_t1116941607 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_NotificationAction_25(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_NotificationWillClose(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_NotificationWillClose_m1592228912_MetadataUsageId;
extern "C"  void FuseSDK_add_NotificationWillClose_m1592228912 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_NotificationWillClose_m1592228912_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_NotificationWillClose_26();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_NotificationWillClose_26(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_NotificationWillClose(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_NotificationWillClose_m3256259179_MetadataUsageId;
extern "C"  void FuseSDK_remove_NotificationWillClose_m3256259179 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_NotificationWillClose_m3256259179_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_NotificationWillClose_26();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_NotificationWillClose_26(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_FriendAdded(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_FriendAdded_m1791049655_MetadataUsageId;
extern "C"  void FuseSDK_add_FriendAdded_m1791049655 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_FriendAdded_m1791049655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendAdded_27();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendAdded_27(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_FriendAdded(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_FriendAdded_m1313845810_MetadataUsageId;
extern "C"  void FuseSDK_remove_FriendAdded_m1313845810 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_FriendAdded_m1313845810_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendAdded_27();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendAdded_27(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_FriendRemoved(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_FriendRemoved_m1542110487_MetadataUsageId;
extern "C"  void FuseSDK_add_FriendRemoved_m1542110487 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_FriendRemoved_m1542110487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendRemoved_28();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendRemoved_28(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_FriendRemoved(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_FriendRemoved_m2510716114_MetadataUsageId;
extern "C"  void FuseSDK_remove_FriendRemoved_m2510716114 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_FriendRemoved_m2510716114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendRemoved_28();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendRemoved_28(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_FriendAccepted(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_FriendAccepted_m3304322678_MetadataUsageId;
extern "C"  void FuseSDK_add_FriendAccepted_m3304322678 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_FriendAccepted_m3304322678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendAccepted_29();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendAccepted_29(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_FriendAccepted(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_FriendAccepted_m3266326043_MetadataUsageId;
extern "C"  void FuseSDK_remove_FriendAccepted_m3266326043 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_FriendAccepted_m3266326043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendAccepted_29();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendAccepted_29(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_FriendRejected(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_FriendRejected_m3903105087_MetadataUsageId;
extern "C"  void FuseSDK_add_FriendRejected_m3903105087 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_FriendRejected_m3903105087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendRejected_30();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendRejected_30(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_FriendRejected(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_FriendRejected_m3865108452_MetadataUsageId;
extern "C"  void FuseSDK_remove_FriendRejected_m3865108452 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_FriendRejected_m3865108452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendRejected_30();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendRejected_30(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_FriendsMigrated(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_FriendsMigrated_m1977570539_MetadataUsageId;
extern "C"  void FuseSDK_add_FriendsMigrated_m1977570539 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_FriendsMigrated_m1977570539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsMigrated_31();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendsMigrated_31(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_FriendsMigrated(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_FriendsMigrated_m799674854_MetadataUsageId;
extern "C"  void FuseSDK_remove_FriendsMigrated_m799674854 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_FriendsMigrated_m799674854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsMigrated_31();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendsMigrated_31(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_FriendsListUpdated(System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1482123831_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_FriendsListUpdated_m2713688815_MetadataUsageId;
extern "C"  void FuseSDK_add_FriendsListUpdated_m2713688815 (Il2CppObject * __this /* static, unused */, Action_1_t1482123831 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_FriendsListUpdated_m2713688815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1482123831 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsListUpdated_32();
		Action_1_t1482123831 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendsListUpdated_32(((Action_1_t1482123831 *)CastclassSealed(L_2, Action_1_t1482123831_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_FriendsListUpdated(System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1482123831_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_FriendsListUpdated_m3831519444_MetadataUsageId;
extern "C"  void FuseSDK_remove_FriendsListUpdated_m3831519444 (Il2CppObject * __this /* static, unused */, Action_1_t1482123831 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_FriendsListUpdated_m3831519444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1482123831 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsListUpdated_32();
		Action_1_t1482123831 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendsListUpdated_32(((Action_1_t1482123831 *)CastclassSealed(L_2, Action_1_t1482123831_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_FriendsListError(System.Action`1<FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_FriendsListError_m1272986699_MetadataUsageId;
extern "C"  void FuseSDK_add_FriendsListError_m1272986699 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_FriendsListError_m1272986699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsListError_33();
		Action_1_t592684423 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendsListError_33(((Action_1_t592684423 *)CastclassSealed(L_2, Action_1_t592684423_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_FriendsListError(System.Action`1<FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_FriendsListError_m2745107312_MetadataUsageId;
extern "C"  void FuseSDK_remove_FriendsListError_m2745107312 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_FriendsListError_m2745107312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsListError_33();
		Action_1_t592684423 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_FriendsListError_33(((Action_1_t592684423 *)CastclassSealed(L_2, Action_1_t592684423_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_PurchaseVerification(System.Action`3<System.Int32,System.String,System.String>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_3_t3047918030_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_PurchaseVerification_m2364053203_MetadataUsageId;
extern "C"  void FuseSDK_add_PurchaseVerification_m2364053203 (Il2CppObject * __this /* static, unused */, Action_3_t3047918030 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_PurchaseVerification_m2364053203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_3_t3047918030 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_PurchaseVerification_34();
		Action_3_t3047918030 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_PurchaseVerification_34(((Action_3_t3047918030 *)CastclassSealed(L_2, Action_3_t3047918030_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_PurchaseVerification(System.Action`3<System.Int32,System.String,System.String>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_3_t3047918030_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_PurchaseVerification_m3756263864_MetadataUsageId;
extern "C"  void FuseSDK_remove_PurchaseVerification_m3756263864 (Il2CppObject * __this /* static, unused */, Action_3_t3047918030 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_PurchaseVerification_m3756263864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_3_t3047918030 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_PurchaseVerification_34();
		Action_3_t3047918030 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_PurchaseVerification_34(((Action_3_t3047918030 *)CastclassSealed(L_2, Action_3_t3047918030_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_AdAvailabilityResponse(System.Action`2<System.Boolean,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3211021459_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_AdAvailabilityResponse_m2892673221_MetadataUsageId;
extern "C"  void FuseSDK_add_AdAvailabilityResponse_m2892673221 (Il2CppObject * __this /* static, unused */, Action_2_t3211021459 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_AdAvailabilityResponse_m2892673221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t3211021459 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdAvailabilityResponse_35();
		Action_2_t3211021459 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AdAvailabilityResponse_35(((Action_2_t3211021459 *)CastclassSealed(L_2, Action_2_t3211021459_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_AdAvailabilityResponse(System.Action`2<System.Boolean,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3211021459_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_AdAvailabilityResponse_m2631025344_MetadataUsageId;
extern "C"  void FuseSDK_remove_AdAvailabilityResponse_m2631025344 (Il2CppObject * __this /* static, unused */, Action_2_t3211021459 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_AdAvailabilityResponse_m2631025344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t3211021459 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdAvailabilityResponse_35();
		Action_2_t3211021459 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AdAvailabilityResponse_35(((Action_2_t3211021459 *)CastclassSealed(L_2, Action_2_t3211021459_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_AdWillClose(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_AdWillClose_m1177472552_MetadataUsageId;
extern "C"  void FuseSDK_add_AdWillClose_m1177472552 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_AdWillClose_m1177472552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdWillClose_36();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AdWillClose_36(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_AdWillClose(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_AdWillClose_m2705220131_MetadataUsageId;
extern "C"  void FuseSDK_remove_AdWillClose_m2705220131 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_AdWillClose_m2705220131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdWillClose_36();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AdWillClose_36(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_AdFailedToDisplay(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_AdFailedToDisplay_m3402053476_MetadataUsageId;
extern "C"  void FuseSDK_add_AdFailedToDisplay_m3402053476 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_AdFailedToDisplay_m3402053476_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdFailedToDisplay_37();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AdFailedToDisplay_37(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_AdFailedToDisplay(System.Action)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_AdFailedToDisplay_m1884857119_MetadataUsageId;
extern "C"  void FuseSDK_remove_AdFailedToDisplay_m1884857119 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_AdFailedToDisplay_m1884857119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdFailedToDisplay_37();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AdFailedToDisplay_37(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_AdDidShow(System.Action`2<System.Int32,System.Int32>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3619260338_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_AdDidShow_m3167712772_MetadataUsageId;
extern "C"  void FuseSDK_add_AdDidShow_m3167712772 (Il2CppObject * __this /* static, unused */, Action_2_t3619260338 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_AdDidShow_m3167712772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t3619260338 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdDidShow_38();
		Action_2_t3619260338 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AdDidShow_38(((Action_2_t3619260338 *)CastclassSealed(L_2, Action_2_t3619260338_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_AdDidShow(System.Action`2<System.Int32,System.Int32>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3619260338_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_AdDidShow_m344866089_MetadataUsageId;
extern "C"  void FuseSDK_remove_AdDidShow_m344866089 (Il2CppObject * __this /* static, unused */, Action_2_t3619260338 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_AdDidShow_m344866089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t3619260338 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdDidShow_38();
		Action_2_t3619260338 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_AdDidShow_38(((Action_2_t3619260338 *)CastclassSealed(L_2, Action_2_t3619260338_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_RewardedAdCompletedWithObject(System.Action`1<FuseMisc.RewardedInfo>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t978424956_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_RewardedAdCompletedWithObject_m1341894586_MetadataUsageId;
extern "C"  void FuseSDK_add_RewardedAdCompletedWithObject_m1341894586 (Il2CppObject * __this /* static, unused */, Action_1_t978424956 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_RewardedAdCompletedWithObject_m1341894586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t978424956 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_RewardedAdCompletedWithObject_39();
		Action_1_t978424956 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_RewardedAdCompletedWithObject_39(((Action_1_t978424956 *)CastclassSealed(L_2, Action_1_t978424956_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_RewardedAdCompletedWithObject(System.Action`1<FuseMisc.RewardedInfo>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t978424956_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_RewardedAdCompletedWithObject_m534351071_MetadataUsageId;
extern "C"  void FuseSDK_remove_RewardedAdCompletedWithObject_m534351071 (Il2CppObject * __this /* static, unused */, Action_1_t978424956 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_RewardedAdCompletedWithObject_m534351071_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t978424956 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_RewardedAdCompletedWithObject_39();
		Action_1_t978424956 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_RewardedAdCompletedWithObject_39(((Action_1_t978424956 *)CastclassSealed(L_2, Action_1_t978424956_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_IAPOfferAcceptedWithObject(System.Action`1<FuseMisc.IAPOfferInfo>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t3974970450_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_IAPOfferAcceptedWithObject_m1561552895_MetadataUsageId;
extern "C"  void FuseSDK_add_IAPOfferAcceptedWithObject_m1561552895 (Il2CppObject * __this /* static, unused */, Action_1_t3974970450 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_IAPOfferAcceptedWithObject_m1561552895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t3974970450 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_IAPOfferAcceptedWithObject_40();
		Action_1_t3974970450 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_IAPOfferAcceptedWithObject_40(((Action_1_t3974970450 *)CastclassSealed(L_2, Action_1_t3974970450_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_IAPOfferAcceptedWithObject(System.Action`1<FuseMisc.IAPOfferInfo>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t3974970450_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_IAPOfferAcceptedWithObject_m383657210_MetadataUsageId;
extern "C"  void FuseSDK_remove_IAPOfferAcceptedWithObject_m383657210 (Il2CppObject * __this /* static, unused */, Action_1_t3974970450 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_IAPOfferAcceptedWithObject_m383657210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t3974970450 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_IAPOfferAcceptedWithObject_40();
		Action_1_t3974970450 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_IAPOfferAcceptedWithObject_40(((Action_1_t3974970450 *)CastclassSealed(L_2, Action_1_t3974970450_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_VirtualGoodsOfferAcceptedWithObject(System.Action`1<FuseMisc.VGOfferInfo>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1534247929_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_VirtualGoodsOfferAcceptedWithObject_m1175353235_MetadataUsageId;
extern "C"  void FuseSDK_add_VirtualGoodsOfferAcceptedWithObject_m1175353235 (Il2CppObject * __this /* static, unused */, Action_1_t1534247929 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_VirtualGoodsOfferAcceptedWithObject_m1175353235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1534247929 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_VirtualGoodsOfferAcceptedWithObject_41();
		Action_1_t1534247929 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_VirtualGoodsOfferAcceptedWithObject_41(((Action_1_t1534247929 *)CastclassSealed(L_2, Action_1_t1534247929_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_VirtualGoodsOfferAcceptedWithObject(System.Action`1<FuseMisc.VGOfferInfo>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1534247929_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_VirtualGoodsOfferAcceptedWithObject_m913705358_MetadataUsageId;
extern "C"  void FuseSDK_remove_VirtualGoodsOfferAcceptedWithObject_m913705358 (Il2CppObject * __this /* static, unused */, Action_1_t1534247929 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_VirtualGoodsOfferAcceptedWithObject_m913705358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1534247929 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_VirtualGoodsOfferAcceptedWithObject_41();
		Action_1_t1534247929 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_VirtualGoodsOfferAcceptedWithObject_41(((Action_1_t1534247929 *)CastclassSealed(L_2, Action_1_t1534247929_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_TimeUpdated(System.Action`1<System.DateTime>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t487486641_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_TimeUpdated_m1170725926_MetadataUsageId;
extern "C"  void FuseSDK_add_TimeUpdated_m1170725926 (Il2CppObject * __this /* static, unused */, Action_1_t487486641 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_TimeUpdated_m1170725926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t487486641 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_TimeUpdated_42();
		Action_1_t487486641 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_TimeUpdated_42(((Action_1_t487486641 *)CastclassSealed(L_2, Action_1_t487486641_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_TimeUpdated(System.Action`1<System.DateTime>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t487486641_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_TimeUpdated_m2753468491_MetadataUsageId;
extern "C"  void FuseSDK_remove_TimeUpdated_m2753468491 (Il2CppObject * __this /* static, unused */, Action_1_t487486641 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_TimeUpdated_m2753468491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t487486641 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_TimeUpdated_42();
		Action_1_t487486641 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_TimeUpdated_42(((Action_1_t487486641 *)CastclassSealed(L_2, Action_1_t487486641_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_GameDataError(System.Action`2<System.Int32,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t1216077269_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_GameDataError_m1659727886_MetadataUsageId;
extern "C"  void FuseSDK_add_GameDataError_m1659727886 (Il2CppObject * __this /* static, unused */, Action_2_t1216077269 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_GameDataError_m1659727886_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t1216077269 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataError_43();
		Action_2_t1216077269 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_GameDataError_43(((Action_2_t1216077269 *)CastclassSealed(L_2, Action_2_t1216077269_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_GameDataError(System.Action`2<System.Int32,FuseMisc.FuseError>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t1216077269_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_GameDataError_m4046277875_MetadataUsageId;
extern "C"  void FuseSDK_remove_GameDataError_m4046277875 (Il2CppObject * __this /* static, unused */, Action_2_t1216077269 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_GameDataError_m4046277875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t1216077269 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataError_43();
		Action_2_t1216077269 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_GameDataError_43(((Action_2_t1216077269 *)CastclassSealed(L_2, Action_2_t1216077269_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_GameDataSetAcknowledged(System.Action`1<System.Int32>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_GameDataSetAcknowledged_m928918873_MetadataUsageId;
extern "C"  void FuseSDK_add_GameDataSetAcknowledged_m928918873 (Il2CppObject * __this /* static, unused */, Action_1_t2995867492 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_GameDataSetAcknowledged_m928918873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t2995867492 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataSetAcknowledged_44();
		Action_1_t2995867492 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_GameDataSetAcknowledged_44(((Action_1_t2995867492 *)CastclassSealed(L_2, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_GameDataSetAcknowledged(System.Action`1<System.Int32>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_GameDataSetAcknowledged_m3614984916_MetadataUsageId;
extern "C"  void FuseSDK_remove_GameDataSetAcknowledged_m3614984916 (Il2CppObject * __this /* static, unused */, Action_1_t2995867492 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_GameDataSetAcknowledged_m3614984916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t2995867492 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataSetAcknowledged_44();
		Action_1_t2995867492 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_GameDataSetAcknowledged_44(((Action_1_t2995867492 *)CastclassSealed(L_2, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::add_GameDataReceived(System.Action`4<System.Int32,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_4_t4157318299_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_add_GameDataReceived_m1887878304_MetadataUsageId;
extern "C"  void FuseSDK_add_GameDataReceived_m1887878304 (Il2CppObject * __this /* static, unused */, Action_4_t4157318299 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_add_GameDataReceived_m1887878304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_4_t4157318299 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataReceived_45();
		Action_4_t4157318299 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_GameDataReceived_45(((Action_4_t4157318299 *)CastclassSealed(L_2, Action_4_t4157318299_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::remove_GameDataReceived(System.Action`4<System.Int32,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_4_t4157318299_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_remove_GameDataReceived_m4228795995_MetadataUsageId;
extern "C"  void FuseSDK_remove_GameDataReceived_m4228795995 (Il2CppObject * __this /* static, unused */, Action_4_t4157318299 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_remove_GameDataReceived_m4228795995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_4_t4157318299 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataReceived_45();
		Action_4_t4157318299 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set_GameDataReceived_45(((Action_4_t4157318299 *)CastclassSealed(L_2, Action_4_t4157318299_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDK::OnSessionStartReceived()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_OnSessionStartReceived_m2049253662_MetadataUsageId;
extern "C"  void FuseSDK_OnSessionStartReceived_m2049253662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnSessionStartReceived_m2049253662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_SessionStartReceived_20();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_SessionStartReceived_20();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void FuseSDK::OnSessionLoginError(System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m989005028_MethodInfo_var;
extern const uint32_t FuseSDK_OnSessionLoginError_m1579096951_MetadataUsageId;
extern "C"  void FuseSDK_OnSessionLoginError_m1579096951 (Il2CppObject * __this /* static, unused */, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnSessionLoginError_m1579096951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t592684423 * G_B3_0 = NULL;
	Action_1_t592684423 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Action_1_t592684423 * G_B4_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_SessionLoginError_21();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_SessionLoginError_21();
		int32_t L_2 = ___error;
		G_B2_0 = L_1;
		if ((((int32_t)L_2) >= ((int32_t)7)))
		{
			G_B3_0 = L_1;
			goto IL_001c;
		}
	}
	{
		int32_t L_3 = ___error;
		G_B4_0 = L_3;
		G_B4_1 = G_B2_0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
	}

IL_001d:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m989005028(G_B4_1, G_B4_0, /*hidden argument*/Action_1_Invoke_m989005028_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}
}
// System.Void FuseSDK::OnPurchaseVerification(System.Int32,System.String,System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_3_Invoke_m4129636852_MethodInfo_var;
extern const uint32_t FuseSDK_OnPurchaseVerification_m650257910_MetadataUsageId;
extern "C"  void FuseSDK_OnPurchaseVerification_m650257910 (Il2CppObject * __this /* static, unused */, int32_t ___verified, String_t* ___transactionId, String_t* ___originalTransactionId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnPurchaseVerification_m650257910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_3_t3047918030 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_PurchaseVerification_34();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_3_t3047918030 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_PurchaseVerification_34();
		int32_t L_2 = ___verified;
		String_t* L_3 = ___transactionId;
		String_t* L_4 = ___originalTransactionId;
		NullCheck(L_1);
		Action_3_Invoke_m4129636852(L_1, L_2, L_3, L_4, /*hidden argument*/Action_3_Invoke_m4129636852_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// System.Void FuseSDK::OnAdAvailabilityResponse(System.Int32,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m2116191450_MethodInfo_var;
extern const uint32_t FuseSDK_OnAdAvailabilityResponse_m3625410166_MetadataUsageId;
extern "C"  void FuseSDK_OnAdAvailabilityResponse_m3625410166 (Il2CppObject * __this /* static, unused */, int32_t ___available, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnAdAvailabilityResponse_m3625410166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	Action_2_t3211021459 * G_B3_1 = NULL;
	int32_t G_B2_0 = 0;
	Action_2_t3211021459 * G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B4_1 = 0;
	Action_2_t3211021459 * G_B4_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t3211021459 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdAvailabilityResponse_35();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t3211021459 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdAvailabilityResponse_35();
		int32_t L_2 = ___available;
		int32_t L_3 = ___error;
		G_B2_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B2_1 = L_1;
		if ((((int32_t)L_3) >= ((int32_t)7)))
		{
			G_B3_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			G_B3_1 = L_1;
			goto IL_0023;
		}
	}
	{
		int32_t L_4 = ___error;
		G_B4_0 = L_4;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0024:
	{
		NullCheck(G_B4_2);
		Action_2_Invoke_m2116191450(G_B4_2, (bool)G_B4_1, G_B4_0, /*hidden argument*/Action_2_Invoke_m2116191450_MethodInfo_var);
	}

IL_0029:
	{
		return;
	}
}
// System.Void FuseSDK::OnAdWillClose()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_OnAdWillClose_m2256651444_MetadataUsageId;
extern "C"  void FuseSDK_OnAdWillClose_m2256651444 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnAdWillClose_m2256651444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdWillClose_36();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdWillClose_36();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void FuseSDK::OnAdFailedToDisplay()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_OnAdFailedToDisplay_m559046328_MetadataUsageId;
extern "C"  void FuseSDK_OnAdFailedToDisplay_m559046328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnAdFailedToDisplay_m559046328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdFailedToDisplay_37();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdFailedToDisplay_37();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void FuseSDK::OnAdDidShow(System.Int32,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m972625629_MethodInfo_var;
extern const uint32_t FuseSDK_OnAdDidShow_m262328604_MetadataUsageId;
extern "C"  void FuseSDK_OnAdDidShow_m262328604 (Il2CppObject * __this /* static, unused */, int32_t ___networkId, int32_t ___mediaType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnAdDidShow_m262328604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t3619260338 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdDidShow_38();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t3619260338 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AdDidShow_38();
		int32_t L_2 = ___networkId;
		int32_t L_3 = ___mediaType;
		NullCheck(L_1);
		Action_2_Invoke_m972625629(L_1, L_2, L_3, /*hidden argument*/Action_2_Invoke_m972625629_MethodInfo_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Void FuseSDK::OnRewardedAdCompleted(FuseMisc.RewardedInfo)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2478426025_MethodInfo_var;
extern const uint32_t FuseSDK_OnRewardedAdCompleted_m3162148842_MetadataUsageId;
extern "C"  void FuseSDK_OnRewardedAdCompleted_m3162148842 (Il2CppObject * __this /* static, unused */, RewardedInfo_t829972251  ___rewardInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnRewardedAdCompleted_m3162148842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t978424956 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_RewardedAdCompletedWithObject_39();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t978424956 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_RewardedAdCompletedWithObject_39();
		RewardedInfo_t829972251  L_2 = ___rewardInfo;
		NullCheck(L_1);
		Action_1_Invoke_m2478426025(L_1, L_2, /*hidden argument*/Action_1_Invoke_m2478426025_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void FuseSDK::OnIAPOfferAccepted(FuseMisc.IAPOfferInfo)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3001813907_MethodInfo_var;
extern const uint32_t FuseSDK_OnIAPOfferAccepted_m1326213027_MetadataUsageId;
extern "C"  void FuseSDK_OnIAPOfferAccepted_m1326213027 (Il2CppObject * __this /* static, unused */, IAPOfferInfo_t3826517745  ___offerInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnIAPOfferAccepted_m1326213027_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t3974970450 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_IAPOfferAcceptedWithObject_40();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t3974970450 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_IAPOfferAcceptedWithObject_40();
		IAPOfferInfo_t3826517745  L_2 = ___offerInfo;
		NullCheck(L_1);
		Action_1_Invoke_m3001813907(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3001813907_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void FuseSDK::OnVirtualGoodsOfferAccepted(FuseMisc.VGOfferInfo)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m266750706_MethodInfo_var;
extern const uint32_t FuseSDK_OnVirtualGoodsOfferAccepted_m3383985617_MetadataUsageId;
extern "C"  void FuseSDK_OnVirtualGoodsOfferAccepted_m3383985617 (Il2CppObject * __this /* static, unused */, VGOfferInfo_t1385795224  ___offerInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnVirtualGoodsOfferAccepted_m3383985617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1534247929 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_VirtualGoodsOfferAcceptedWithObject_41();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1534247929 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_VirtualGoodsOfferAcceptedWithObject_41();
		VGOfferInfo_t1385795224  L_2 = ___offerInfo;
		NullCheck(L_1);
		Action_1_Invoke_m266750706(L_1, L_2, /*hidden argument*/Action_1_Invoke_m266750706_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void FuseSDK::OnAdClickedWithURL(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3709300246_MethodInfo_var;
extern const uint32_t FuseSDK_OnAdClickedWithURL_m3254180204_MetadataUsageId;
extern "C"  void FuseSDK_OnAdClickedWithURL_m3254180204 (Il2CppObject * __this /* static, unused */, String_t* ___url, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnAdClickedWithURL_m3254180204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1116941607 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__adClickedwithURL_17();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1116941607 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__adClickedwithURL_17();
		String_t* L_2 = ___url;
		NullCheck(L_1);
		Action_1_Invoke_m3709300246(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3709300246_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void FuseSDK::OnNotificationAction(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3709300246_MethodInfo_var;
extern const uint32_t FuseSDK_OnNotificationAction_m222392976_MetadataUsageId;
extern "C"  void FuseSDK_OnNotificationAction_m222392976 (Il2CppObject * __this /* static, unused */, String_t* ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnNotificationAction_m222392976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1116941607 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_NotificationAction_25();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1116941607 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_NotificationAction_25();
		String_t* L_2 = ___action;
		NullCheck(L_1);
		Action_1_Invoke_m3709300246(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3709300246_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void FuseSDK::OnNotificationWillClose()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_OnNotificationWillClose_m1695164140_MetadataUsageId;
extern "C"  void FuseSDK_OnNotificationWillClose_m1695164140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnNotificationWillClose_m1695164140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_NotificationWillClose_26();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_NotificationWillClose_26();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void FuseSDK::OnAccountLoginComplete(System.Int32,System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m1681436597_MethodInfo_var;
extern const uint32_t FuseSDK_OnAccountLoginComplete_m1802209427_MetadataUsageId;
extern "C"  void FuseSDK_OnAccountLoginComplete_m1802209427 (Il2CppObject * __this /* static, unused */, int32_t ___type, String_t* ___accountId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnAccountLoginComplete_m1802209427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t535153670 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginComplete_23();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t535153670 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginComplete_23();
		int32_t L_2 = ___type;
		String_t* L_3 = ___accountId;
		NullCheck(L_1);
		Action_2_Invoke_m1681436597(L_1, L_2, L_3, /*hidden argument*/Action_2_Invoke_m1681436597_MethodInfo_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Void FuseSDK::OnAccountLoginError(System.String,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m503978957_MethodInfo_var;
extern const uint32_t FuseSDK_OnAccountLoginError_m3668425490_MetadataUsageId;
extern "C"  void FuseSDK_OnAccountLoginError_m3668425490 (Il2CppObject * __this /* static, unused */, String_t* ___accountId, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnAccountLoginError_m3668425490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Action_2_t2362964390 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Action_2_t2362964390 * G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	String_t* G_B4_1 = NULL;
	Action_2_t2362964390 * G_B4_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginError_24();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginError_24();
		String_t* L_2 = ___accountId;
		int32_t L_3 = ___error;
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if ((((int32_t)L_3) >= ((int32_t)7)))
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = ___error;
		G_B4_0 = L_4;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_001e:
	{
		NullCheck(G_B4_2);
		Action_2_Invoke_m503978957(G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/Action_2_Invoke_m503978957_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void FuseSDK::OnTimeUpdated(System.DateTime)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m305199884_MethodInfo_var;
extern const uint32_t FuseSDK_OnTimeUpdated_m2041530777_MetadataUsageId;
extern "C"  void FuseSDK_OnTimeUpdated_m2041530777 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnTimeUpdated_m2041530777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t487486641 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_TimeUpdated_42();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t487486641 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_TimeUpdated_42();
		DateTime_t339033936  L_2 = ___time;
		NullCheck(L_1);
		Action_1_Invoke_m305199884(L_1, L_2, /*hidden argument*/Action_1_Invoke_m305199884_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void FuseSDK::OnFriendAdded(System.String,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m503978957_MethodInfo_var;
extern const uint32_t FuseSDK_OnFriendAdded_m4135937256_MetadataUsageId;
extern "C"  void FuseSDK_OnFriendAdded_m4135937256 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnFriendAdded_m4135937256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Action_2_t2362964390 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Action_2_t2362964390 * G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	String_t* G_B4_1 = NULL;
	Action_2_t2362964390 * G_B4_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendAdded_27();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendAdded_27();
		String_t* L_2 = ___fuseId;
		int32_t L_3 = ___error;
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if ((((int32_t)L_3) >= ((int32_t)7)))
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = ___error;
		G_B4_0 = L_4;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_001e:
	{
		NullCheck(G_B4_2);
		Action_2_Invoke_m503978957(G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/Action_2_Invoke_m503978957_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void FuseSDK::OnFriendRemoved(System.String,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m503978957_MethodInfo_var;
extern const uint32_t FuseSDK_OnFriendRemoved_m1751502792_MetadataUsageId;
extern "C"  void FuseSDK_OnFriendRemoved_m1751502792 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnFriendRemoved_m1751502792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Action_2_t2362964390 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Action_2_t2362964390 * G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	String_t* G_B4_1 = NULL;
	Action_2_t2362964390 * G_B4_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendRemoved_28();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendRemoved_28();
		String_t* L_2 = ___fuseId;
		int32_t L_3 = ___error;
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if ((((int32_t)L_3) >= ((int32_t)7)))
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = ___error;
		G_B4_0 = L_4;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_001e:
	{
		NullCheck(G_B4_2);
		Action_2_Invoke_m503978957(G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/Action_2_Invoke_m503978957_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void FuseSDK::OnFriendAccepted(System.String,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m503978957_MethodInfo_var;
extern const uint32_t FuseSDK_OnFriendAccepted_m4288202667_MetadataUsageId;
extern "C"  void FuseSDK_OnFriendAccepted_m4288202667 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnFriendAccepted_m4288202667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Action_2_t2362964390 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Action_2_t2362964390 * G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	String_t* G_B4_1 = NULL;
	Action_2_t2362964390 * G_B4_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendAccepted_29();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendAccepted_29();
		String_t* L_2 = ___fuseId;
		int32_t L_3 = ___error;
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if ((((int32_t)L_3) >= ((int32_t)7)))
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = ___error;
		G_B4_0 = L_4;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_001e:
	{
		NullCheck(G_B4_2);
		Action_2_Invoke_m503978957(G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/Action_2_Invoke_m503978957_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void FuseSDK::OnFriendRejected(System.String,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m503978957_MethodInfo_var;
extern const uint32_t FuseSDK_OnFriendRejected_m1189618690_MetadataUsageId;
extern "C"  void FuseSDK_OnFriendRejected_m1189618690 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnFriendRejected_m1189618690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Action_2_t2362964390 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Action_2_t2362964390 * G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	String_t* G_B4_1 = NULL;
	Action_2_t2362964390 * G_B4_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendRejected_30();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendRejected_30();
		String_t* L_2 = ___fuseId;
		int32_t L_3 = ___error;
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if ((((int32_t)L_3) >= ((int32_t)7)))
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = ___error;
		G_B4_0 = L_4;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_001e:
	{
		NullCheck(G_B4_2);
		Action_2_Invoke_m503978957(G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/Action_2_Invoke_m503978957_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void FuseSDK::OnFriendsMigrated(System.String,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m503978957_MethodInfo_var;
extern const uint32_t FuseSDK_OnFriendsMigrated_m4030513844_MetadataUsageId;
extern "C"  void FuseSDK_OnFriendsMigrated_m4030513844 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnFriendsMigrated_m4030513844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Action_2_t2362964390 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Action_2_t2362964390 * G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	String_t* G_B4_1 = NULL;
	Action_2_t2362964390 * G_B4_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsMigrated_31();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsMigrated_31();
		String_t* L_2 = ___fuseId;
		int32_t L_3 = ___error;
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if ((((int32_t)L_3) >= ((int32_t)7)))
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = ___error;
		G_B4_0 = L_4;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_001e:
	{
		NullCheck(G_B4_2);
		Action_2_Invoke_m503978957(G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/Action_2_Invoke_m503978957_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void FuseSDK::OnFriendsListUpdated(System.Collections.Generic.List`1<FuseMisc.Friend>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3146393101_MethodInfo_var;
extern const uint32_t FuseSDK_OnFriendsListUpdated_m502898524_MetadataUsageId;
extern "C"  void FuseSDK_OnFriendsListUpdated_m502898524 (Il2CppObject * __this /* static, unused */, List_1_t1333671126 * ___friends, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnFriendsListUpdated_m502898524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1482123831 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsListUpdated_32();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t1482123831 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsListUpdated_32();
		List_1_t1333671126 * L_2 = ___friends;
		NullCheck(L_1);
		Action_1_Invoke_m3146393101(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3146393101_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void FuseSDK::OnFriendsListError(System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m989005028_MethodInfo_var;
extern const uint32_t FuseSDK_OnFriendsListError_m1439699959_MetadataUsageId;
extern "C"  void FuseSDK_OnFriendsListError_m1439699959 (Il2CppObject * __this /* static, unused */, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnFriendsListError_m1439699959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t592684423 * G_B3_0 = NULL;
	Action_1_t592684423 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Action_1_t592684423 * G_B4_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsListError_33();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_FriendsListError_33();
		int32_t L_2 = ___error;
		G_B2_0 = L_1;
		if ((((int32_t)L_2) >= ((int32_t)7)))
		{
			G_B3_0 = L_1;
			goto IL_001c;
		}
	}
	{
		int32_t L_3 = ___error;
		G_B4_0 = L_3;
		G_B4_1 = G_B2_0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
	}

IL_001d:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m989005028(G_B4_1, G_B4_0, /*hidden argument*/Action_1_Invoke_m989005028_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}
}
// System.Void FuseSDK::OnGameConfigurationReceived()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_OnGameConfigurationReceived_m3123175414_MetadataUsageId;
extern "C"  void FuseSDK_OnGameConfigurationReceived_m3123175414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnGameConfigurationReceived_m3123175414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameConfigurationReceived_22();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_t437523947 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameConfigurationReceived_22();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void FuseSDK::OnGameDataError(System.Int32,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m1739483444_MethodInfo_var;
extern const uint32_t FuseSDK_OnGameDataError_m719612681_MetadataUsageId;
extern "C"  void FuseSDK_OnGameDataError_m719612681 (Il2CppObject * __this /* static, unused */, int32_t ___error, int32_t ___requestId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnGameDataError_m719612681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	Action_2_t1216077269 * G_B3_1 = NULL;
	int32_t G_B2_0 = 0;
	Action_2_t1216077269 * G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B4_1 = 0;
	Action_2_t1216077269 * G_B4_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t1216077269 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataError_43();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_2_t1216077269 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataError_43();
		int32_t L_2 = ___requestId;
		int32_t L_3 = ___error;
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if ((((int32_t)L_3) >= ((int32_t)7)))
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = ___error;
		G_B4_0 = L_4;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 7;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_001e:
	{
		NullCheck(G_B4_2);
		Action_2_Invoke_m1739483444(G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/Action_2_Invoke_m1739483444_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void FuseSDK::OnGameDataSetAcknowledged(System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m4053252443_MethodInfo_var;
extern const uint32_t FuseSDK_OnGameDataSetAcknowledged_m2838243536_MetadataUsageId;
extern "C"  void FuseSDK_OnGameDataSetAcknowledged_m2838243536 (Il2CppObject * __this /* static, unused */, int32_t ___requestId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnGameDataSetAcknowledged_m2838243536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t2995867492 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataSetAcknowledged_44();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_1_t2995867492 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataSetAcknowledged_44();
		int32_t L_2 = ___requestId;
		NullCheck(L_1);
		Action_1_Invoke_m4053252443(L_1, L_2, /*hidden argument*/Action_1_Invoke_m4053252443_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void FuseSDK::OnGameDataReceived(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_4_Invoke_m1198003510_MethodInfo_var;
extern const uint32_t FuseSDK_OnGameDataReceived_m883737616_MetadataUsageId;
extern "C"  void FuseSDK_OnGameDataReceived_m883737616 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___dataKey, Dictionary_2_t2606186806 * ___data, int32_t ___requestId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnGameDataReceived_m883737616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_4_t4157318299 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataReceived_45();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Action_4_t4157318299 * L_1 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get_GameDataReceived_45();
		int32_t L_2 = ___requestId;
		String_t* L_3 = ___fuseId;
		String_t* L_4 = ___dataKey;
		Dictionary_2_t2606186806 * L_5 = ___data;
		NullCheck(L_1);
		Action_4_Invoke_m1198003510(L_1, L_2, L_3, L_4, L_5, /*hidden argument*/Action_4_Invoke_m1198003510_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void FuseSDK::Native_StartSession(System.String,System.Boolean,System.Boolean,System.Boolean)
extern "C" {void DEFAULT_CALL Native_StartSession(char*, int32_t, int32_t, int32_t);}
extern "C"  void FuseSDK_Native_StartSession_m1071771623 (Il2CppObject * __this /* static, unused */, String_t* ___gameId, bool ___registerForPush, bool ___handleAdURLs, bool ___enableCrashDetection, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_StartSession;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_StartSession'"));
		}
	}

	// Marshaling of parameter '___gameId' to native representation
	char* ____gameId_marshaled = NULL;
	____gameId_marshaled = il2cpp_codegen_marshal_string(___gameId);

	// Marshaling of parameter '___registerForPush' to native representation

	// Marshaling of parameter '___handleAdURLs' to native representation

	// Marshaling of parameter '___enableCrashDetection' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____gameId_marshaled, ___registerForPush, ___handleAdURLs, ___enableCrashDetection);

	// Marshaling cleanup of parameter '___gameId' native representation
	il2cpp_codegen_marshal_free(____gameId_marshaled);
	____gameId_marshaled = NULL;

	// Marshaling cleanup of parameter '___registerForPush' native representation

	// Marshaling cleanup of parameter '___handleAdURLs' native representation

	// Marshaling cleanup of parameter '___enableCrashDetection' native representation

}
// System.Void FuseSDK::Native_RegisterPushToken(System.Byte[],System.Int32)
extern "C" {void DEFAULT_CALL Native_RegisterPushToken(uint8_t*, int32_t);}
extern "C"  void FuseSDK_Native_RegisterPushToken_m49313396 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___token, int32_t ___size, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint8_t*, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterPushToken;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterPushToken'"));
		}
	}

	// Marshaling of parameter '___token' to native representation
	uint8_t* ____token_marshaled = NULL;
	____token_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___token);

	// Marshaling of parameter '___size' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____token_marshaled, ___size);

	// Marshaling cleanup of parameter '___token' native representation

	// Marshaling cleanup of parameter '___size' native representation

}
// System.Void FuseSDK::Native_ReceivedRemoteNotification(System.String)
extern "C" {void DEFAULT_CALL Native_ReceivedRemoteNotification(char*);}
extern "C"  void FuseSDK_Native_ReceivedRemoteNotification_m1602007352 (Il2CppObject * __this /* static, unused */, String_t* ___notificationId, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_ReceivedRemoteNotification;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_ReceivedRemoteNotification'"));
		}
	}

	// Marshaling of parameter '___notificationId' to native representation
	char* ____notificationId_marshaled = NULL;
	____notificationId_marshaled = il2cpp_codegen_marshal_string(___notificationId);

	// Native function invocation
	_il2cpp_pinvoke_func(____notificationId_marshaled);

	// Marshaling cleanup of parameter '___notificationId' native representation
	il2cpp_codegen_marshal_free(____notificationId_marshaled);
	____notificationId_marshaled = NULL;

}
// System.Void FuseSDK::Native_SetUnityGameObject(System.String)
extern "C" {void DEFAULT_CALL Native_SetUnityGameObject(char*);}
extern "C"  void FuseSDK_Native_SetUnityGameObject_m3710335398 (Il2CppObject * __this /* static, unused */, String_t* ___gameObjectName, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_SetUnityGameObject;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_SetUnityGameObject'"));
		}
	}

	// Marshaling of parameter '___gameObjectName' to native representation
	char* ____gameObjectName_marshaled = NULL;
	____gameObjectName_marshaled = il2cpp_codegen_marshal_string(___gameObjectName);

	// Native function invocation
	_il2cpp_pinvoke_func(____gameObjectName_marshaled);

	// Marshaling cleanup of parameter '___gameObjectName' native representation
	il2cpp_codegen_marshal_free(____gameObjectName_marshaled);
	____gameObjectName_marshaled = NULL;

}
// System.Boolean FuseSDK::Native_RegisterEventVariable(System.String,System.String,System.String,System.String,System.Double)
extern "C" {int32_t DEFAULT_CALL Native_RegisterEventVariable(char*, char*, char*, char*, double);}
extern "C"  bool FuseSDK_Native_RegisterEventVariable_m3671701833 (Il2CppObject * __this /* static, unused */, String_t* ___name, String_t* ___paramName, String_t* ___paramValue, String_t* ___variableName, double ___variableValue, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char*, double);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterEventVariable;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterEventVariable'"));
		}
	}

	// Marshaling of parameter '___name' to native representation
	char* ____name_marshaled = NULL;
	____name_marshaled = il2cpp_codegen_marshal_string(___name);

	// Marshaling of parameter '___paramName' to native representation
	char* ____paramName_marshaled = NULL;
	____paramName_marshaled = il2cpp_codegen_marshal_string(___paramName);

	// Marshaling of parameter '___paramValue' to native representation
	char* ____paramValue_marshaled = NULL;
	____paramValue_marshaled = il2cpp_codegen_marshal_string(___paramValue);

	// Marshaling of parameter '___variableName' to native representation
	char* ____variableName_marshaled = NULL;
	____variableName_marshaled = il2cpp_codegen_marshal_string(___variableName);

	// Marshaling of parameter '___variableValue' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____name_marshaled, ____paramName_marshaled, ____paramValue_marshaled, ____variableName_marshaled, ___variableValue);

	// Marshaling cleanup of parameter '___name' native representation
	il2cpp_codegen_marshal_free(____name_marshaled);
	____name_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramName' native representation
	il2cpp_codegen_marshal_free(____paramName_marshaled);
	____paramName_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramValue' native representation
	il2cpp_codegen_marshal_free(____paramValue_marshaled);
	____paramValue_marshaled = NULL;

	// Marshaling cleanup of parameter '___variableName' native representation
	il2cpp_codegen_marshal_free(____variableName_marshaled);
	____variableName_marshaled = NULL;

	// Marshaling cleanup of parameter '___variableValue' native representation

	return _return_value;
}
// System.Boolean FuseSDK::Native_RegisterEventWithDictionary(System.String,System.String,System.String,System.String[],System.Double[],System.Int32)
extern "C" {int32_t DEFAULT_CALL Native_RegisterEventWithDictionary(char*, char*, char*, char**, double*, int32_t);}
extern "C"  bool FuseSDK_Native_RegisterEventWithDictionary_m1672630642 (Il2CppObject * __this /* static, unused */, String_t* ___message, String_t* ___paramName, String_t* ___paramValue, StringU5BU5D_t2956870243* ___keys, DoubleU5BU5D_t1048280995* ___values, int32_t ___numEntries, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char**, double*, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterEventWithDictionary;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterEventWithDictionary'"));
		}
	}

	// Marshaling of parameter '___message' to native representation
	char* ____message_marshaled = NULL;
	____message_marshaled = il2cpp_codegen_marshal_string(___message);

	// Marshaling of parameter '___paramName' to native representation
	char* ____paramName_marshaled = NULL;
	____paramName_marshaled = il2cpp_codegen_marshal_string(___paramName);

	// Marshaling of parameter '___paramValue' to native representation
	char* ____paramValue_marshaled = NULL;
	____paramValue_marshaled = il2cpp_codegen_marshal_string(___paramValue);

	// Marshaling of parameter '___keys' to native representation
	char** ____keys_marshaled = NULL;
	____keys_marshaled = il2cpp_codegen_marshal_string_array(___keys);

	// Marshaling of parameter '___values' to native representation
	double* ____values_marshaled = NULL;
	____values_marshaled = il2cpp_codegen_marshal_array<double>((Il2CppCodeGenArray*)___values);

	// Marshaling of parameter '___numEntries' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____message_marshaled, ____paramName_marshaled, ____paramValue_marshaled, ____keys_marshaled, ____values_marshaled, ___numEntries);

	// Marshaling cleanup of parameter '___message' native representation
	il2cpp_codegen_marshal_free(____message_marshaled);
	____message_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramName' native representation
	il2cpp_codegen_marshal_free(____paramName_marshaled);
	____paramName_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramValue' native representation
	il2cpp_codegen_marshal_free(____paramValue_marshaled);
	____paramValue_marshaled = NULL;

	// Marshaling cleanup of parameter '___keys' native representation
	if (___keys != NULL) il2cpp_codegen_marshal_free_string_array((void**)____keys_marshaled, ((Il2CppCodeGenArray*)___keys)->max_length);
	____keys_marshaled = NULL;

	// Marshaling cleanup of parameter '___values' native representation

	// Marshaling cleanup of parameter '___numEntries' native representation

	return _return_value;
}
// System.Void FuseSDK::Native_RegisterVirtualGoodsPurchase(System.Int32,System.Int32,System.Int32)
extern "C" {void DEFAULT_CALL Native_RegisterVirtualGoodsPurchase(int32_t, int32_t, int32_t);}
extern "C"  void FuseSDK_Native_RegisterVirtualGoodsPurchase_m519104408 (Il2CppObject * __this /* static, unused */, int32_t ___virtualgoodID, int32_t ___currencyAmount, int32_t ___currencyID, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterVirtualGoodsPurchase;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterVirtualGoodsPurchase'"));
		}
	}

	// Marshaling of parameter '___virtualgoodID' to native representation

	// Marshaling of parameter '___currencyAmount' to native representation

	// Marshaling of parameter '___currencyID' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___virtualgoodID, ___currencyAmount, ___currencyID);

	// Marshaling cleanup of parameter '___virtualgoodID' native representation

	// Marshaling cleanup of parameter '___currencyAmount' native representation

	// Marshaling cleanup of parameter '___currencyID' native representation

}
// System.Void FuseSDK::Native_RegisterInAppPurchaseList(System.String[],System.String[],System.Single[],System.Int32)
extern "C" {void DEFAULT_CALL Native_RegisterInAppPurchaseList(char**, char**, float*, int32_t);}
extern "C"  void FuseSDK_Native_RegisterInAppPurchaseList_m3231503004 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___productId, StringU5BU5D_t2956870243* ___priceLocale, SingleU5BU5D_t1219431280* ___price, int32_t ___numEntries, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char**, char**, float*, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterInAppPurchaseList;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterInAppPurchaseList'"));
		}
	}

	// Marshaling of parameter '___productId' to native representation
	char** ____productId_marshaled = NULL;
	____productId_marshaled = il2cpp_codegen_marshal_string_array(___productId);

	// Marshaling of parameter '___priceLocale' to native representation
	char** ____priceLocale_marshaled = NULL;
	____priceLocale_marshaled = il2cpp_codegen_marshal_string_array(___priceLocale);

	// Marshaling of parameter '___price' to native representation
	float* ____price_marshaled = NULL;
	____price_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___price);

	// Marshaling of parameter '___numEntries' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____productId_marshaled, ____priceLocale_marshaled, ____price_marshaled, ___numEntries);

	// Marshaling cleanup of parameter '___productId' native representation
	if (___productId != NULL) il2cpp_codegen_marshal_free_string_array((void**)____productId_marshaled, ((Il2CppCodeGenArray*)___productId)->max_length);
	____productId_marshaled = NULL;

	// Marshaling cleanup of parameter '___priceLocale' native representation
	if (___priceLocale != NULL) il2cpp_codegen_marshal_free_string_array((void**)____priceLocale_marshaled, ((Il2CppCodeGenArray*)___priceLocale)->max_length);
	____priceLocale_marshaled = NULL;

	// Marshaling cleanup of parameter '___price' native representation

	// Marshaling cleanup of parameter '___numEntries' native representation

}
// System.Void FuseSDK::Native_RegisterInAppPurchase(System.String,System.String,System.Byte[],System.Int32,System.Int32)
extern "C" {void DEFAULT_CALL Native_RegisterInAppPurchase(char*, char*, uint8_t*, int32_t, int32_t);}
extern "C"  void FuseSDK_Native_RegisterInAppPurchase_m348866365 (Il2CppObject * __this /* static, unused */, String_t* ___productId, String_t* ___transactionId, ByteU5BU5D_t58506160* ___transactionReceiptBuffer, int32_t ___transactionReceiptLength, int32_t ___transactionState, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, uint8_t*, int32_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterInAppPurchase;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterInAppPurchase'"));
		}
	}

	// Marshaling of parameter '___productId' to native representation
	char* ____productId_marshaled = NULL;
	____productId_marshaled = il2cpp_codegen_marshal_string(___productId);

	// Marshaling of parameter '___transactionId' to native representation
	char* ____transactionId_marshaled = NULL;
	____transactionId_marshaled = il2cpp_codegen_marshal_string(___transactionId);

	// Marshaling of parameter '___transactionReceiptBuffer' to native representation
	uint8_t* ____transactionReceiptBuffer_marshaled = NULL;
	____transactionReceiptBuffer_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___transactionReceiptBuffer);

	// Marshaling of parameter '___transactionReceiptLength' to native representation

	// Marshaling of parameter '___transactionState' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____productId_marshaled, ____transactionId_marshaled, ____transactionReceiptBuffer_marshaled, ___transactionReceiptLength, ___transactionState);

	// Marshaling cleanup of parameter '___productId' native representation
	il2cpp_codegen_marshal_free(____productId_marshaled);
	____productId_marshaled = NULL;

	// Marshaling cleanup of parameter '___transactionId' native representation
	il2cpp_codegen_marshal_free(____transactionId_marshaled);
	____transactionId_marshaled = NULL;

	// Marshaling cleanup of parameter '___transactionReceiptBuffer' native representation

	// Marshaling cleanup of parameter '___transactionReceiptLength' native representation

	// Marshaling cleanup of parameter '___transactionState' native representation

}
// System.Void FuseSDK::Native_RegisterUnibillPurchase(System.String,System.Byte[],System.Int32)
extern "C" {void DEFAULT_CALL Native_RegisterUnibillPurchase(char*, uint8_t*, int32_t);}
extern "C"  void FuseSDK_Native_RegisterUnibillPurchase_m3955303697 (Il2CppObject * __this /* static, unused */, String_t* ___productID, ByteU5BU5D_t58506160* ___receipt, int32_t ___receiptLength, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, uint8_t*, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterUnibillPurchase;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterUnibillPurchase'"));
		}
	}

	// Marshaling of parameter '___productID' to native representation
	char* ____productID_marshaled = NULL;
	____productID_marshaled = il2cpp_codegen_marshal_string(___productID);

	// Marshaling of parameter '___receipt' to native representation
	uint8_t* ____receipt_marshaled = NULL;
	____receipt_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___receipt);

	// Marshaling of parameter '___receiptLength' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____productID_marshaled, ____receipt_marshaled, ___receiptLength);

	// Marshaling cleanup of parameter '___productID' native representation
	il2cpp_codegen_marshal_free(____productID_marshaled);
	____productID_marshaled = NULL;

	// Marshaling cleanup of parameter '___receipt' native representation

	// Marshaling cleanup of parameter '___receiptLength' native representation

}
// System.Boolean FuseSDK::Native_IsAdAvailableForZoneID(System.String)
extern "C" {int32_t DEFAULT_CALL Native_IsAdAvailableForZoneID(char*);}
extern "C"  bool FuseSDK_Native_IsAdAvailableForZoneID_m1070722658 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_IsAdAvailableForZoneID;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_IsAdAvailableForZoneID'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = NULL;
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____zoneId_marshaled);

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

	return _return_value;
}
// System.Boolean FuseSDK::Native_ZoneHasRewarded(System.String)
extern "C" {int32_t DEFAULT_CALL Native_ZoneHasRewarded(char*);}
extern "C"  bool FuseSDK_Native_ZoneHasRewarded_m574832144 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_ZoneHasRewarded;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_ZoneHasRewarded'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = NULL;
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____zoneId_marshaled);

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

	return _return_value;
}
// System.Boolean FuseSDK::Native_ZoneHasIAPOffer(System.String)
extern "C" {int32_t DEFAULT_CALL Native_ZoneHasIAPOffer(char*);}
extern "C"  bool FuseSDK_Native_ZoneHasIAPOffer_m2720625786 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_ZoneHasIAPOffer;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_ZoneHasIAPOffer'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = NULL;
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____zoneId_marshaled);

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

	return _return_value;
}
// System.Boolean FuseSDK::Native_ZoneHasVirtualGoodsOffer(System.String)
extern "C" {int32_t DEFAULT_CALL Native_ZoneHasVirtualGoodsOffer(char*);}
extern "C"  bool FuseSDK_Native_ZoneHasVirtualGoodsOffer_m2877255123 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_ZoneHasVirtualGoodsOffer;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_ZoneHasVirtualGoodsOffer'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = NULL;
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____zoneId_marshaled);

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

	return _return_value;
}
// System.String FuseSDK::Native_GetRewardedInfoForZone(System.String)
extern "C" {char* DEFAULT_CALL Native_GetRewardedInfoForZone(char*);}
extern "C"  String_t* FuseSDK_Native_GetRewardedInfoForZone_m2171737092 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GetRewardedInfoForZone;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GetRewardedInfoForZone'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = NULL;
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func(____zoneId_marshaled);
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

	return __return_value_unmarshaled;
}
// System.String FuseSDK::Native_GetVirtualGoodsOfferInfoForZoneID(System.String)
extern "C" {char* DEFAULT_CALL Native_GetVirtualGoodsOfferInfoForZoneID(char*);}
extern "C"  String_t* FuseSDK_Native_GetVirtualGoodsOfferInfoForZoneID_m3907036564 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GetVirtualGoodsOfferInfoForZoneID;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GetVirtualGoodsOfferInfoForZoneID'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = NULL;
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func(____zoneId_marshaled);
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

	return __return_value_unmarshaled;
}
// System.String FuseSDK::Native_GetIAPOfferInfoForZoneID(System.String)
extern "C" {char* DEFAULT_CALL Native_GetIAPOfferInfoForZoneID(char*);}
extern "C"  String_t* FuseSDK_Native_GetIAPOfferInfoForZoneID_m3128281759 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GetIAPOfferInfoForZoneID;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GetIAPOfferInfoForZoneID'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = NULL;
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func(____zoneId_marshaled);
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

	return __return_value_unmarshaled;
}
// System.Void FuseSDK::Native_ShowAdForZoneID(System.String,System.String[],System.String[],System.Int32)
extern "C" {void DEFAULT_CALL Native_ShowAdForZoneID(char*, char**, char**, int32_t);}
extern "C"  void FuseSDK_Native_ShowAdForZoneID_m1395443739 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, StringU5BU5D_t2956870243* ___optionKeys, StringU5BU5D_t2956870243* ___optionValues, int32_t ___numOptions, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char**, char**, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_ShowAdForZoneID;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_ShowAdForZoneID'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = NULL;
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Marshaling of parameter '___optionKeys' to native representation
	char** ____optionKeys_marshaled = NULL;
	____optionKeys_marshaled = il2cpp_codegen_marshal_string_array(___optionKeys);

	// Marshaling of parameter '___optionValues' to native representation
	char** ____optionValues_marshaled = NULL;
	____optionValues_marshaled = il2cpp_codegen_marshal_string_array(___optionValues);

	// Marshaling of parameter '___numOptions' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____zoneId_marshaled, ____optionKeys_marshaled, ____optionValues_marshaled, ___numOptions);

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

	// Marshaling cleanup of parameter '___optionKeys' native representation
	if (___optionKeys != NULL) il2cpp_codegen_marshal_free_string_array((void**)____optionKeys_marshaled, ((Il2CppCodeGenArray*)___optionKeys)->max_length);
	____optionKeys_marshaled = NULL;

	// Marshaling cleanup of parameter '___optionValues' native representation
	if (___optionValues != NULL) il2cpp_codegen_marshal_free_string_array((void**)____optionValues_marshaled, ((Il2CppCodeGenArray*)___optionValues)->max_length);
	____optionValues_marshaled = NULL;

	// Marshaling cleanup of parameter '___numOptions' native representation

}
// System.Void FuseSDK::Native_PreloadAdForZone(System.String)
extern "C" {void DEFAULT_CALL Native_PreloadAdForZone(char*);}
extern "C"  void FuseSDK_Native_PreloadAdForZone_m2574987585 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_PreloadAdForZone;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_PreloadAdForZone'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = NULL;
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Native function invocation
	_il2cpp_pinvoke_func(____zoneId_marshaled);

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

}
// System.Void FuseSDK::Native_SetRewardedVideoUserID(System.String)
extern "C" {void DEFAULT_CALL Native_SetRewardedVideoUserID(char*);}
extern "C"  void FuseSDK_Native_SetRewardedVideoUserID_m1352186393 (Il2CppObject * __this /* static, unused */, String_t* ___userID, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_SetRewardedVideoUserID;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_SetRewardedVideoUserID'"));
		}
	}

	// Marshaling of parameter '___userID' to native representation
	char* ____userID_marshaled = NULL;
	____userID_marshaled = il2cpp_codegen_marshal_string(___userID);

	// Native function invocation
	_il2cpp_pinvoke_func(____userID_marshaled);

	// Marshaling cleanup of parameter '___userID' native representation
	il2cpp_codegen_marshal_free(____userID_marshaled);
	____userID_marshaled = NULL;

}
// System.Void FuseSDK::Native_DisplayNotifications()
extern "C" {void DEFAULT_CALL Native_DisplayNotifications();}
extern "C"  void FuseSDK_Native_DisplayNotifications_m3833872766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_DisplayNotifications;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_DisplayNotifications'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Boolean FuseSDK::Native_IsNotificationAvailable()
extern "C" {int32_t DEFAULT_CALL Native_IsNotificationAvailable();}
extern "C"  bool FuseSDK_Native_IsNotificationAvailable_m2029103306 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_IsNotificationAvailable;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_IsNotificationAvailable'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void FuseSDK::Native_RegisterGender(System.Int32)
extern "C" {void DEFAULT_CALL Native_RegisterGender(int32_t);}
extern "C"  void FuseSDK_Native_RegisterGender_m3465403021 (Il2CppObject * __this /* static, unused */, int32_t ___gender, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterGender;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterGender'"));
		}
	}

	// Marshaling of parameter '___gender' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___gender);

	// Marshaling cleanup of parameter '___gender' native representation

}
// System.Void FuseSDK::Native_RegisterLevel(System.Int32)
extern "C" {void DEFAULT_CALL Native_RegisterLevel(int32_t);}
extern "C"  void FuseSDK_Native_RegisterLevel_m3367885148 (Il2CppObject * __this /* static, unused */, int32_t ___level, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterLevel;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterLevel'"));
		}
	}

	// Marshaling of parameter '___level' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___level);

	// Marshaling cleanup of parameter '___level' native representation

}
// System.Boolean FuseSDK::Native_RegisterCurrency(System.Int32,System.Int32)
extern "C" {int32_t DEFAULT_CALL Native_RegisterCurrency(int32_t, int32_t);}
extern "C"  bool FuseSDK_Native_RegisterCurrency_m3225143142 (Il2CppObject * __this /* static, unused */, int32_t ___type, int32_t ___balance, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterCurrency;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterCurrency'"));
		}
	}

	// Marshaling of parameter '___type' to native representation

	// Marshaling of parameter '___balance' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(___type, ___balance);

	// Marshaling cleanup of parameter '___type' native representation

	// Marshaling cleanup of parameter '___balance' native representation

	return _return_value;
}
// System.Void FuseSDK::Native_RegisterAge(System.Int32)
extern "C" {void DEFAULT_CALL Native_RegisterAge(int32_t);}
extern "C"  void FuseSDK_Native_RegisterAge_m4022095127 (Il2CppObject * __this /* static, unused */, int32_t ___age, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterAge;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterAge'"));
		}
	}

	// Marshaling of parameter '___age' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___age);

	// Marshaling cleanup of parameter '___age' native representation

}
// System.Void FuseSDK::Native_RegisterBirthday(System.Int32,System.Int32,System.Int32)
extern "C" {void DEFAULT_CALL Native_RegisterBirthday(int32_t, int32_t, int32_t);}
extern "C"  void FuseSDK_Native_RegisterBirthday_m2218280681 (Il2CppObject * __this /* static, unused */, int32_t ___year, int32_t ___month, int32_t ___day, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterBirthday;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterBirthday'"));
		}
	}

	// Marshaling of parameter '___year' to native representation

	// Marshaling of parameter '___month' to native representation

	// Marshaling of parameter '___day' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___year, ___month, ___day);

	// Marshaling cleanup of parameter '___year' native representation

	// Marshaling cleanup of parameter '___month' native representation

	// Marshaling cleanup of parameter '___day' native representation

}
// System.Void FuseSDK::Native_RegisterParentalConsent(System.Boolean)
extern "C" {void DEFAULT_CALL Native_RegisterParentalConsent(int32_t);}
extern "C"  void FuseSDK_Native_RegisterParentalConsent_m2154152387 (Il2CppObject * __this /* static, unused */, bool ___consentGranted, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterParentalConsent;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterParentalConsent'"));
		}
	}

	// Marshaling of parameter '___consentGranted' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___consentGranted);

	// Marshaling cleanup of parameter '___consentGranted' native representation

}
// System.Boolean FuseSDK::Native_RegisterCustomEventInt(System.Int32,System.Int32)
extern "C" {int32_t DEFAULT_CALL Native_RegisterCustomEventInt(int32_t, int32_t);}
extern "C"  bool FuseSDK_Native_RegisterCustomEventInt_m2604151633 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, int32_t ___value, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterCustomEventInt;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterCustomEventInt'"));
		}
	}

	// Marshaling of parameter '___eventNumber' to native representation

	// Marshaling of parameter '___value' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(___eventNumber, ___value);

	// Marshaling cleanup of parameter '___eventNumber' native representation

	// Marshaling cleanup of parameter '___value' native representation

	return _return_value;
}
// System.Boolean FuseSDK::Native_RegisterCustomEventString(System.Int32,System.String)
extern "C" {int32_t DEFAULT_CALL Native_RegisterCustomEventString(int32_t, char*);}
extern "C"  bool FuseSDK_Native_RegisterCustomEventString_m946012154 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, String_t* ___value, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RegisterCustomEventString;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RegisterCustomEventString'"));
		}
	}

	// Marshaling of parameter '___eventNumber' to native representation

	// Marshaling of parameter '___value' to native representation
	char* ____value_marshaled = NULL;
	____value_marshaled = il2cpp_codegen_marshal_string(___value);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(___eventNumber, ____value_marshaled);

	// Marshaling cleanup of parameter '___eventNumber' native representation

	// Marshaling cleanup of parameter '___value' native representation
	il2cpp_codegen_marshal_free(____value_marshaled);
	____value_marshaled = NULL;

	return _return_value;
}
// System.String FuseSDK::Native_GetFuseId()
extern "C" {char* DEFAULT_CALL Native_GetFuseId();}
extern "C"  String_t* FuseSDK_Native_GetFuseId_m1427507577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GetFuseId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GetFuseId'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.String FuseSDK::Native_GetOriginalAccountAlias()
extern "C" {char* DEFAULT_CALL Native_GetOriginalAccountAlias();}
extern "C"  String_t* FuseSDK_Native_GetOriginalAccountAlias_m960986801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GetOriginalAccountAlias;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GetOriginalAccountAlias'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.String FuseSDK::Native_GetOriginalAccountId()
extern "C" {char* DEFAULT_CALL Native_GetOriginalAccountId();}
extern "C"  String_t* FuseSDK_Native_GetOriginalAccountId_m4035001340 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GetOriginalAccountId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GetOriginalAccountId'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Int32 FuseSDK::Native_GetOriginalAccountType()
extern "C" {int32_t DEFAULT_CALL Native_GetOriginalAccountType();}
extern "C"  int32_t FuseSDK_Native_GetOriginalAccountType_m783477574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GetOriginalAccountType;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GetOriginalAccountType'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void FuseSDK::Native_GameCenterLogin()
extern "C" {void DEFAULT_CALL Native_GameCenterLogin();}
extern "C"  void FuseSDK_Native_GameCenterLogin_m1956526124 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GameCenterLogin;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GameCenterLogin'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void FuseSDK::Native_FacebookLogin(System.String,System.String,System.String)
extern "C" {void DEFAULT_CALL Native_FacebookLogin(char*, char*, char*);}
extern "C"  void FuseSDK_Native_FacebookLogin_m2276549741 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId, String_t* ___name, String_t* ___accessToken, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_FacebookLogin;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_FacebookLogin'"));
		}
	}

	// Marshaling of parameter '___facebookId' to native representation
	char* ____facebookId_marshaled = NULL;
	____facebookId_marshaled = il2cpp_codegen_marshal_string(___facebookId);

	// Marshaling of parameter '___name' to native representation
	char* ____name_marshaled = NULL;
	____name_marshaled = il2cpp_codegen_marshal_string(___name);

	// Marshaling of parameter '___accessToken' to native representation
	char* ____accessToken_marshaled = NULL;
	____accessToken_marshaled = il2cpp_codegen_marshal_string(___accessToken);

	// Native function invocation
	_il2cpp_pinvoke_func(____facebookId_marshaled, ____name_marshaled, ____accessToken_marshaled);

	// Marshaling cleanup of parameter '___facebookId' native representation
	il2cpp_codegen_marshal_free(____facebookId_marshaled);
	____facebookId_marshaled = NULL;

	// Marshaling cleanup of parameter '___name' native representation
	il2cpp_codegen_marshal_free(____name_marshaled);
	____name_marshaled = NULL;

	// Marshaling cleanup of parameter '___accessToken' native representation
	il2cpp_codegen_marshal_free(____accessToken_marshaled);
	____accessToken_marshaled = NULL;

}
// System.Void FuseSDK::Native_TwitterLogin(System.String,System.String)
extern "C" {void DEFAULT_CALL Native_TwitterLogin(char*, char*);}
extern "C"  void FuseSDK_Native_TwitterLogin_m3795967248 (Il2CppObject * __this /* static, unused */, String_t* ___twitterId, String_t* ___alias, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_TwitterLogin;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_TwitterLogin'"));
		}
	}

	// Marshaling of parameter '___twitterId' to native representation
	char* ____twitterId_marshaled = NULL;
	____twitterId_marshaled = il2cpp_codegen_marshal_string(___twitterId);

	// Marshaling of parameter '___alias' to native representation
	char* ____alias_marshaled = NULL;
	____alias_marshaled = il2cpp_codegen_marshal_string(___alias);

	// Native function invocation
	_il2cpp_pinvoke_func(____twitterId_marshaled, ____alias_marshaled);

	// Marshaling cleanup of parameter '___twitterId' native representation
	il2cpp_codegen_marshal_free(____twitterId_marshaled);
	____twitterId_marshaled = NULL;

	// Marshaling cleanup of parameter '___alias' native representation
	il2cpp_codegen_marshal_free(____alias_marshaled);
	____alias_marshaled = NULL;

}
// System.Void FuseSDK::Native_FuseLogin(System.String,System.String)
extern "C" {void DEFAULT_CALL Native_FuseLogin(char*, char*);}
extern "C"  void FuseSDK_Native_FuseLogin_m2087177036 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___alias, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_FuseLogin;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_FuseLogin'"));
		}
	}

	// Marshaling of parameter '___fuseId' to native representation
	char* ____fuseId_marshaled = NULL;
	____fuseId_marshaled = il2cpp_codegen_marshal_string(___fuseId);

	// Marshaling of parameter '___alias' to native representation
	char* ____alias_marshaled = NULL;
	____alias_marshaled = il2cpp_codegen_marshal_string(___alias);

	// Native function invocation
	_il2cpp_pinvoke_func(____fuseId_marshaled, ____alias_marshaled);

	// Marshaling cleanup of parameter '___fuseId' native representation
	il2cpp_codegen_marshal_free(____fuseId_marshaled);
	____fuseId_marshaled = NULL;

	// Marshaling cleanup of parameter '___alias' native representation
	il2cpp_codegen_marshal_free(____alias_marshaled);
	____alias_marshaled = NULL;

}
// System.Void FuseSDK::Native_EmailLogin(System.String,System.String)
extern "C" {void DEFAULT_CALL Native_EmailLogin(char*, char*);}
extern "C"  void FuseSDK_Native_EmailLogin_m114416185 (Il2CppObject * __this /* static, unused */, String_t* ___email, String_t* ___alias, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_EmailLogin;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_EmailLogin'"));
		}
	}

	// Marshaling of parameter '___email' to native representation
	char* ____email_marshaled = NULL;
	____email_marshaled = il2cpp_codegen_marshal_string(___email);

	// Marshaling of parameter '___alias' to native representation
	char* ____alias_marshaled = NULL;
	____alias_marshaled = il2cpp_codegen_marshal_string(___alias);

	// Native function invocation
	_il2cpp_pinvoke_func(____email_marshaled, ____alias_marshaled);

	// Marshaling cleanup of parameter '___email' native representation
	il2cpp_codegen_marshal_free(____email_marshaled);
	____email_marshaled = NULL;

	// Marshaling cleanup of parameter '___alias' native representation
	il2cpp_codegen_marshal_free(____alias_marshaled);
	____alias_marshaled = NULL;

}
// System.Void FuseSDK::Native_DeviceLogin(System.String)
extern "C" {void DEFAULT_CALL Native_DeviceLogin(char*);}
extern "C"  void FuseSDK_Native_DeviceLogin_m1563937733 (Il2CppObject * __this /* static, unused */, String_t* ___alias, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_DeviceLogin;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_DeviceLogin'"));
		}
	}

	// Marshaling of parameter '___alias' to native representation
	char* ____alias_marshaled = NULL;
	____alias_marshaled = il2cpp_codegen_marshal_string(___alias);

	// Native function invocation
	_il2cpp_pinvoke_func(____alias_marshaled);

	// Marshaling cleanup of parameter '___alias' native representation
	il2cpp_codegen_marshal_free(____alias_marshaled);
	____alias_marshaled = NULL;

}
// System.Void FuseSDK::Native_GooglePlayLogin(System.String,System.String)
extern "C" {void DEFAULT_CALL Native_GooglePlayLogin(char*, char*);}
extern "C"  void FuseSDK_Native_GooglePlayLogin_m4184420536 (Il2CppObject * __this /* static, unused */, String_t* ___alias, String_t* ___token, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GooglePlayLogin;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GooglePlayLogin'"));
		}
	}

	// Marshaling of parameter '___alias' to native representation
	char* ____alias_marshaled = NULL;
	____alias_marshaled = il2cpp_codegen_marshal_string(___alias);

	// Marshaling of parameter '___token' to native representation
	char* ____token_marshaled = NULL;
	____token_marshaled = il2cpp_codegen_marshal_string(___token);

	// Native function invocation
	_il2cpp_pinvoke_func(____alias_marshaled, ____token_marshaled);

	// Marshaling cleanup of parameter '___alias' native representation
	il2cpp_codegen_marshal_free(____alias_marshaled);
	____alias_marshaled = NULL;

	// Marshaling cleanup of parameter '___token' native representation
	il2cpp_codegen_marshal_free(____token_marshaled);
	____token_marshaled = NULL;

}
// System.Int32 FuseSDK::Native_GamesPlayed()
extern "C" {int32_t DEFAULT_CALL Native_GamesPlayed();}
extern "C"  int32_t FuseSDK_Native_GamesPlayed_m3762996496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GamesPlayed;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GamesPlayed'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.String FuseSDK::Native_LibraryVersion()
extern "C" {char* DEFAULT_CALL Native_LibraryVersion();}
extern "C"  String_t* FuseSDK_Native_LibraryVersion_m605514616 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_LibraryVersion;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_LibraryVersion'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Boolean FuseSDK::Native_Connected()
extern "C" {int32_t DEFAULT_CALL Native_Connected();}
extern "C"  bool FuseSDK_Native_Connected_m497903679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_Connected;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_Connected'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void FuseSDK::Native_TimeFromServer()
extern "C" {void DEFAULT_CALL Native_TimeFromServer();}
extern "C"  void FuseSDK_Native_TimeFromServer_m815599794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_TimeFromServer;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_TimeFromServer'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void FuseSDK::Native_EnableData()
extern "C" {void DEFAULT_CALL Native_EnableData();}
extern "C"  void FuseSDK_Native_EnableData_m1268546821 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_EnableData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_EnableData'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void FuseSDK::Native_DisableData()
extern "C" {void DEFAULT_CALL Native_DisableData();}
extern "C"  void FuseSDK_Native_DisableData_m1114487452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_DisableData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_DisableData'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Boolean FuseSDK::Native_DataEnabled()
extern "C" {int32_t DEFAULT_CALL Native_DataEnabled();}
extern "C"  bool FuseSDK_Native_DataEnabled_m4242094253 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_DataEnabled;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_DataEnabled'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void FuseSDK::Native_UpdateFriendsListFromServer()
extern "C" {void DEFAULT_CALL Native_UpdateFriendsListFromServer();}
extern "C"  void FuseSDK_Native_UpdateFriendsListFromServer_m3118599969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_UpdateFriendsListFromServer;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_UpdateFriendsListFromServer'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void FuseSDK::Native_AddFriend(System.String)
extern "C" {void DEFAULT_CALL Native_AddFriend(char*);}
extern "C"  void FuseSDK_Native_AddFriend_m1063652377 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_AddFriend;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_AddFriend'"));
		}
	}

	// Marshaling of parameter '___fuseId' to native representation
	char* ____fuseId_marshaled = NULL;
	____fuseId_marshaled = il2cpp_codegen_marshal_string(___fuseId);

	// Native function invocation
	_il2cpp_pinvoke_func(____fuseId_marshaled);

	// Marshaling cleanup of parameter '___fuseId' native representation
	il2cpp_codegen_marshal_free(____fuseId_marshaled);
	____fuseId_marshaled = NULL;

}
// System.Void FuseSDK::Native_RemoveFriend(System.String)
extern "C" {void DEFAULT_CALL Native_RemoveFriend(char*);}
extern "C"  void FuseSDK_Native_RemoveFriend_m3053180616 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RemoveFriend;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RemoveFriend'"));
		}
	}

	// Marshaling of parameter '___fuseId' to native representation
	char* ____fuseId_marshaled = NULL;
	____fuseId_marshaled = il2cpp_codegen_marshal_string(___fuseId);

	// Native function invocation
	_il2cpp_pinvoke_func(____fuseId_marshaled);

	// Marshaling cleanup of parameter '___fuseId' native representation
	il2cpp_codegen_marshal_free(____fuseId_marshaled);
	____fuseId_marshaled = NULL;

}
// System.Void FuseSDK::Native_AcceptFriend(System.String)
extern "C" {void DEFAULT_CALL Native_AcceptFriend(char*);}
extern "C"  void FuseSDK_Native_AcceptFriend_m111203524 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_AcceptFriend;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_AcceptFriend'"));
		}
	}

	// Marshaling of parameter '___fuseId' to native representation
	char* ____fuseId_marshaled = NULL;
	____fuseId_marshaled = il2cpp_codegen_marshal_string(___fuseId);

	// Native function invocation
	_il2cpp_pinvoke_func(____fuseId_marshaled);

	// Marshaling cleanup of parameter '___fuseId' native representation
	il2cpp_codegen_marshal_free(____fuseId_marshaled);
	____fuseId_marshaled = NULL;

}
// System.Void FuseSDK::Native_RejectFriend(System.String)
extern "C" {void DEFAULT_CALL Native_RejectFriend(char*);}
extern "C"  void FuseSDK_Native_RejectFriend_m2978146957 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_RejectFriend;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_RejectFriend'"));
		}
	}

	// Marshaling of parameter '___fuseId' to native representation
	char* ____fuseId_marshaled = NULL;
	____fuseId_marshaled = il2cpp_codegen_marshal_string(___fuseId);

	// Native function invocation
	_il2cpp_pinvoke_func(____fuseId_marshaled);

	// Marshaling cleanup of parameter '___fuseId' native representation
	il2cpp_codegen_marshal_free(____fuseId_marshaled);
	____fuseId_marshaled = NULL;

}
// System.Void FuseSDK::Native_MigrateFriends(System.String)
extern "C" {void DEFAULT_CALL Native_MigrateFriends(char*);}
extern "C"  void FuseSDK_Native_MigrateFriends_m2033398368 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_MigrateFriends;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_MigrateFriends'"));
		}
	}

	// Marshaling of parameter '___fuseId' to native representation
	char* ____fuseId_marshaled = NULL;
	____fuseId_marshaled = il2cpp_codegen_marshal_string(___fuseId);

	// Native function invocation
	_il2cpp_pinvoke_func(____fuseId_marshaled);

	// Marshaling cleanup of parameter '___fuseId' native representation
	il2cpp_codegen_marshal_free(____fuseId_marshaled);
	____fuseId_marshaled = NULL;

}
// System.Void FuseSDK::Native_UserPushNotification(System.String,System.String)
extern "C" {void DEFAULT_CALL Native_UserPushNotification(char*, char*);}
extern "C"  void FuseSDK_Native_UserPushNotification_m2529849334 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___message, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_UserPushNotification;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_UserPushNotification'"));
		}
	}

	// Marshaling of parameter '___fuseId' to native representation
	char* ____fuseId_marshaled = NULL;
	____fuseId_marshaled = il2cpp_codegen_marshal_string(___fuseId);

	// Marshaling of parameter '___message' to native representation
	char* ____message_marshaled = NULL;
	____message_marshaled = il2cpp_codegen_marshal_string(___message);

	// Native function invocation
	_il2cpp_pinvoke_func(____fuseId_marshaled, ____message_marshaled);

	// Marshaling cleanup of parameter '___fuseId' native representation
	il2cpp_codegen_marshal_free(____fuseId_marshaled);
	____fuseId_marshaled = NULL;

	// Marshaling cleanup of parameter '___message' native representation
	il2cpp_codegen_marshal_free(____message_marshaled);
	____message_marshaled = NULL;

}
// System.Void FuseSDK::Native_FriendsPushNotification(System.String)
extern "C" {void DEFAULT_CALL Native_FriendsPushNotification(char*);}
extern "C"  void FuseSDK_Native_FriendsPushNotification_m776809534 (Il2CppObject * __this /* static, unused */, String_t* ___message, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_FriendsPushNotification;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_FriendsPushNotification'"));
		}
	}

	// Marshaling of parameter '___message' to native representation
	char* ____message_marshaled = NULL;
	____message_marshaled = il2cpp_codegen_marshal_string(___message);

	// Native function invocation
	_il2cpp_pinvoke_func(____message_marshaled);

	// Marshaling cleanup of parameter '___message' native representation
	il2cpp_codegen_marshal_free(____message_marshaled);
	____message_marshaled = NULL;

}
// System.String FuseSDK::Native_GetGameConfigurationValue(System.String)
extern "C" {char* DEFAULT_CALL Native_GetGameConfigurationValue(char*);}
extern "C"  String_t* FuseSDK_Native_GetGameConfigurationValue_m777724088 (Il2CppObject * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GetGameConfigurationValue;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GetGameConfigurationValue'"));
		}
	}

	// Marshaling of parameter '___key' to native representation
	char* ____key_marshaled = NULL;
	____key_marshaled = il2cpp_codegen_marshal_string(___key);

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func(____key_marshaled);
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	// Marshaling cleanup of parameter '___key' native representation
	il2cpp_codegen_marshal_free(____key_marshaled);
	____key_marshaled = NULL;

	return __return_value_unmarshaled;
}
// System.Int32 FuseSDK::Native_SetGameData(System.String,System.String,System.String[],System.String[],System.Int32)
extern "C" {int32_t DEFAULT_CALL Native_SetGameData(char*, char*, char**, char**, int32_t);}
extern "C"  int32_t FuseSDK_Native_SetGameData_m2957575807 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___key, StringU5BU5D_t2956870243* ___varKeys, StringU5BU5D_t2956870243* ___varValues, int32_t ___length, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, char*, char**, char**, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_SetGameData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_SetGameData'"));
		}
	}

	// Marshaling of parameter '___fuseId' to native representation
	char* ____fuseId_marshaled = NULL;
	____fuseId_marshaled = il2cpp_codegen_marshal_string(___fuseId);

	// Marshaling of parameter '___key' to native representation
	char* ____key_marshaled = NULL;
	____key_marshaled = il2cpp_codegen_marshal_string(___key);

	// Marshaling of parameter '___varKeys' to native representation
	char** ____varKeys_marshaled = NULL;
	____varKeys_marshaled = il2cpp_codegen_marshal_string_array(___varKeys);

	// Marshaling of parameter '___varValues' to native representation
	char** ____varValues_marshaled = NULL;
	____varValues_marshaled = il2cpp_codegen_marshal_string_array(___varValues);

	// Marshaling of parameter '___length' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____fuseId_marshaled, ____key_marshaled, ____varKeys_marshaled, ____varValues_marshaled, ___length);

	// Marshaling cleanup of parameter '___fuseId' native representation
	il2cpp_codegen_marshal_free(____fuseId_marshaled);
	____fuseId_marshaled = NULL;

	// Marshaling cleanup of parameter '___key' native representation
	il2cpp_codegen_marshal_free(____key_marshaled);
	____key_marshaled = NULL;

	// Marshaling cleanup of parameter '___varKeys' native representation
	if (___varKeys != NULL) il2cpp_codegen_marshal_free_string_array((void**)____varKeys_marshaled, ((Il2CppCodeGenArray*)___varKeys)->max_length);
	____varKeys_marshaled = NULL;

	// Marshaling cleanup of parameter '___varValues' native representation
	if (___varValues != NULL) il2cpp_codegen_marshal_free_string_array((void**)____varValues_marshaled, ((Il2CppCodeGenArray*)___varValues)->max_length);
	____varValues_marshaled = NULL;

	// Marshaling cleanup of parameter '___length' native representation

	return _return_value;
}
// System.Int32 FuseSDK::Native_GetGameData(System.String,System.String,System.String[],System.Int32)
extern "C" {int32_t DEFAULT_CALL Native_GetGameData(char*, char*, char**, int32_t);}
extern "C"  int32_t FuseSDK_Native_GetGameData_m4250402445 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___key, StringU5BU5D_t2956870243* ___keys, int32_t ___length, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, char*, char**, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Native_GetGameData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Native_GetGameData'"));
		}
	}

	// Marshaling of parameter '___fuseId' to native representation
	char* ____fuseId_marshaled = NULL;
	____fuseId_marshaled = il2cpp_codegen_marshal_string(___fuseId);

	// Marshaling of parameter '___key' to native representation
	char* ____key_marshaled = NULL;
	____key_marshaled = il2cpp_codegen_marshal_string(___key);

	// Marshaling of parameter '___keys' to native representation
	char** ____keys_marshaled = NULL;
	____keys_marshaled = il2cpp_codegen_marshal_string_array(___keys);

	// Marshaling of parameter '___length' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____fuseId_marshaled, ____key_marshaled, ____keys_marshaled, ___length);

	// Marshaling cleanup of parameter '___fuseId' native representation
	il2cpp_codegen_marshal_free(____fuseId_marshaled);
	____fuseId_marshaled = NULL;

	// Marshaling cleanup of parameter '___key' native representation
	il2cpp_codegen_marshal_free(____key_marshaled);
	____key_marshaled = NULL;

	// Marshaling cleanup of parameter '___keys' native representation
	if (___keys != NULL) il2cpp_codegen_marshal_free_string_array((void**)____keys_marshaled, ((Il2CppCodeGenArray*)___keys)->max_length);
	____keys_marshaled = NULL;

	// Marshaling cleanup of parameter '___length' native representation

	return _return_value;
}
// System.Void FuseSDK::Awake()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisFuseSDK_t1159654649_m1525574472_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisFuseSDK_Prime31StoreKit_t67136268_m279167405_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisFuseSDK_Prime31StoreKit_t67136268_m535079968_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisFuseSDK_Unibill_iOS_t2534778463_m931311226_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisFuseSDK_Unibill_iOS_t2534778463_m1609091693_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1159654649;
extern const uint32_t FuseSDK_Awake_m2896976885_MetadataUsageId;
extern "C"  void FuseSDK_Awake_m2896976885 (FuseSDK_t1159654649 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_Awake_m2896976885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral1159654649, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t4012695102 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		GameObject_t4012695102 * L_3 = V_0;
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		GameObject_t4012695102 * L_6 = V_0;
		NullCheck(L_6);
		FuseSDK_t1159654649 * L_7 = GameObject_GetComponent_TisFuseSDK_t1159654649_m1525574472(L_6, /*hidden argument*/GameObject_GetComponent_TisFuseSDK_t1159654649_m1525574472_MethodInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0045;
		}
	}
	{
		GameObject_t4012695102 * L_9 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0045:
	{
		GameObject_t4012695102 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set__instance_16(__this);
		bool L_11 = __this->get_iosStoreKit_12();
		if (!L_11)
		{
			goto IL_0087;
		}
	}
	{
		FuseSDK_Prime31StoreKit_t67136268 * L_12 = Component_GetComponent_TisFuseSDK_Prime31StoreKit_t67136268_m279167405(__this, /*hidden argument*/Component_GetComponent_TisFuseSDK_Prime31StoreKit_t67136268_m279167405_MethodInfo_var);
		bool L_13 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t4012695102 * L_14 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		FuseSDK_Prime31StoreKit_t67136268 * L_15 = GameObject_AddComponent_TisFuseSDK_Prime31StoreKit_t67136268_m535079968(L_14, /*hidden argument*/GameObject_AddComponent_TisFuseSDK_Prime31StoreKit_t67136268_m535079968_MethodInfo_var);
		bool L_16 = __this->get_logging_7();
		NullCheck(L_15);
		L_15->set_logging_2(L_16);
	}

IL_0087:
	{
		bool L_17 = __this->get_iosUnibill_13();
		if (!L_17)
		{
			goto IL_00b8;
		}
	}
	{
		FuseSDK_Unibill_iOS_t2534778463 * L_18 = Component_GetComponent_TisFuseSDK_Unibill_iOS_t2534778463_m931311226(__this, /*hidden argument*/Component_GetComponent_TisFuseSDK_Unibill_iOS_t2534778463_m931311226_MethodInfo_var);
		bool L_19 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00b8;
		}
	}
	{
		GameObject_t4012695102 * L_20 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		FuseSDK_Unibill_iOS_t2534778463 * L_21 = GameObject_AddComponent_TisFuseSDK_Unibill_iOS_t2534778463_m1609091693(L_20, /*hidden argument*/GameObject_AddComponent_TisFuseSDK_Unibill_iOS_t2534778463_m1609091693_MethodInfo_var);
		bool L_22 = __this->get_logging_7();
		NullCheck(L_21);
		L_21->set_logging_2(L_22);
	}

IL_00b8:
	{
		GameObject_t4012695102 * L_23 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24 = Object_get_name_m3709440845(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_Native_SetUnityGameObject_m3710335398(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::Start()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_Start_m1606509458_MetadataUsageId;
extern "C"  void FuseSDK_Start_m1606509458 (FuseSDK_t1159654649 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_Start_m1606509458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_iOSAppID_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002e;
		}
	}
	{
		bool L_2 = __this->get_StartAutomatically_4();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_3 = __this->get_iOSAppID_3();
		bool L_4 = __this->get_registerForPushNotifications_6();
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK__StartSession_m627633534(NULL /*static, unused*/, L_3, L_4, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void FuseSDK::OnApplicationPause(System.Boolean)
extern TypeInfo* IDictionary_t1654916945_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2961489135;
extern const uint32_t FuseSDK_OnApplicationPause_m2720010542_MetadataUsageId;
extern "C"  void FuseSDK_OnApplicationPause_m2720010542 (FuseSDK_t1159654649 * __this, bool ___pausing, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_OnApplicationPause_m2720010542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RemoteNotification_t1121285571 * V_0 = NULL;
	RemoteNotificationU5BU5D_t2509186002* V_1 = NULL;
	int32_t V_2 = 0;
	{
		bool L_0 = ___pausing;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		goto IL_005d;
	}

IL_000b:
	{
		RemoteNotificationU5BU5D_t2509186002* L_1 = NotificationServices_get_remoteNotifications_m800019510(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_004f;
	}

IL_0018:
	{
		RemoteNotificationU5BU5D_t2509186002* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		RemoteNotification_t1121285571 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = RemoteNotification_get_userInfo_m4136631987(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(3 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_6, _stringLiteral2961489135);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		RemoteNotification_t1121285571 * L_8 = V_0;
		NullCheck(L_8);
		Il2CppObject * L_9 = RemoteNotification_get_userInfo_m4136631987(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_9, _stringLiteral2961489135);
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_Native_ReceivedRemoteNotification_m1602007352(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_004b:
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_13 = V_2;
		RemoteNotificationU5BU5D_t2509186002* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		NotificationServices_ClearRemoteNotifications_m3581432662(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void FuseSDK::StartSession()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3383924797;
extern const uint32_t FuseSDK_StartSession_m3050284614_MetadataUsageId;
extern "C"  void FuseSDK_StartSession_m3050284614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_StartSession_m3050284614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_t1159654649 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_t1159654649 * L_2 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_iOSAppID_3();
		FuseSDK_t1159654649 * L_4 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		NullCheck(L_4);
		bool L_5 = L_4->get_registerForPushNotifications_6();
		FuseSDK__StartSession_m627633534(NULL /*static, unused*/, L_3, L_5, (bool)0, (bool)1, /*hidden argument*/NULL);
		goto IL_003a;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3383924797, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void FuseSDK::_StartSession(System.String,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisFuseSDK_t1159654649_m3409458168_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral344148695;
extern Il2CppCodeGenString* _stringLiteral3706976522;
extern Il2CppCodeGenString* _stringLiteral2331158196;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK__StartSession_m627633534_MetadataUsageId;
extern "C"  void FuseSDK__StartSession_m627633534 (Il2CppObject * __this /* static, unused */, String_t* ___gameId, bool ___registerForPush, bool ___handleAdURLs, bool ___enableCrashDetection, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__StartSession_m627633534_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FuseSDK_t1159654649 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		bool L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__sessionStarted_15();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral344148695, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		String_t* L_1 = ___gameId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3706976522, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		bool L_3 = ___registerForPush;
		if (!L_3)
		{
			goto IL_0044;
		}
	}
	{
		FuseSDK_t1159654649 * L_4 = Object_FindObjectOfType_TisFuseSDK_t1159654649_m3409458168(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisFuseSDK_t1159654649_m3409458168_MethodInfo_var);
		V_0 = L_4;
		FuseSDK_t1159654649 * L_5 = V_0;
		FuseSDK_t1159654649 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = FuseSDK_SetupPushNotifications_m4090347419(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		MonoBehaviour_StartCoroutine_m2135303124(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set__sessionStarted_15((bool)1);
		String_t* L_8 = ___gameId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2331158196, L_8, _stringLiteral41, /*hidden argument*/NULL);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		String_t* L_10 = ___gameId;
		bool L_11 = ___registerForPush;
		bool L_12 = ___handleAdURLs;
		bool L_13 = ___enableCrashDetection;
		FuseSDK_Native_StartSession_m1071771623(NULL /*static, unused*/, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FuseSDK::RegisterEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t2373214747_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3769107138_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3881634252_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m41001384_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m586818491_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2915303667_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2412577137;
extern Il2CppCodeGenString* _stringLiteral443662781;
extern const uint32_t FuseSDK_RegisterEvent_m3517488528_MetadataUsageId;
extern "C"  bool FuseSDK_RegisterEvent_m3517488528 (Il2CppObject * __this /* static, unused */, String_t* ___name, Dictionary_2_t2606186806 * ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterEvent_m3517488528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	KeyValuePair_2_t2094718104  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2373214747  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___name;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___name = G_B2_0;
		String_t* L_3 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2412577137, L_3, _stringLiteral443662781, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Dictionary_2_t2606186806 * L_5 = ___parameters;
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		String_t* L_6 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		bool L_7 = FuseSDK_RegisterEvent_m2414525879(NULL /*static, unused*/, L_6, (String_t*)NULL, (String_t*)NULL, (String_t*)NULL, (0.0), /*hidden argument*/NULL);
		return L_7;
	}

IL_003d:
	{
		V_0 = (bool)1;
		Dictionary_2_t2606186806 * L_8 = ___parameters;
		NullCheck(L_8);
		Enumerator_t2373214747  L_9 = Dictionary_2_GetEnumerator_m3769107138(L_8, /*hidden argument*/Dictionary_2_GetEnumerator_m3769107138_MethodInfo_var);
		V_2 = L_9;
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0074;
		}

IL_004b:
		{
			KeyValuePair_2_t2094718104  L_10 = Enumerator_get_Current_m3881634252((&V_2), /*hidden argument*/Enumerator_get_Current_m3881634252_MethodInfo_var);
			V_1 = L_10;
			bool L_11 = V_0;
			String_t* L_12 = ___name;
			String_t* L_13 = KeyValuePair_2_get_Key_m41001384((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m41001384_MethodInfo_var);
			String_t* L_14 = KeyValuePair_2_get_Value_m586818491((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m586818491_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
			bool L_15 = FuseSDK_RegisterEvent_m2414525879(NULL /*static, unused*/, L_12, L_13, L_14, (String_t*)NULL, (0.0), /*hidden argument*/NULL);
			V_0 = (bool)((int32_t)((int32_t)L_11&(int32_t)L_15));
		}

IL_0074:
		{
			bool L_16 = Enumerator_MoveNext_m2915303667((&V_2), /*hidden argument*/Enumerator_MoveNext_m2915303667_MethodInfo_var);
			if (L_16)
			{
				goto IL_004b;
			}
		}

IL_0080:
		{
			IL2CPP_LEAVE(0x91, FINALLY_0085);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0085;
	}

FINALLY_0085:
	{ // begin finally (depth: 1)
		Enumerator_t2373214747  L_17 = V_2;
		Enumerator_t2373214747  L_18 = L_17;
		Il2CppObject * L_19 = Box(Enumerator_t2373214747_il2cpp_TypeInfo_var, &L_18);
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
		IL2CPP_END_FINALLY(133)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(133)
	{
		IL2CPP_JUMP_TBL(0x91, IL_0091)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0091:
	{
		bool L_20 = V_0;
		return L_20;
	}
}
// System.Boolean FuseSDK::RegisterEvent(System.String,System.String,System.String,System.Collections.Hashtable)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t1048280995_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1967233988_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2412577137;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral3931195228;
extern Il2CppCodeGenString* _stringLiteral1939827555;
extern const uint32_t FuseSDK_RegisterEvent_m412989983_MetadataUsageId;
extern "C"  bool FuseSDK_RegisterEvent_m412989983 (Il2CppObject * __this /* static, unused */, String_t* ___name, String_t* ___paramName, String_t* ___paramValue, Hashtable_t3875263730 * ___variables, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterEvent_m412989983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	DoubleU5BU5D_t1048280995* V_1 = NULL;
	int32_t V_2 = 0;
	Exception_t1967233988 * V_3 = NULL;
	bool V_4 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B6_0 = NULL;
	String_t* G_B5_0 = NULL;
	{
		String_t* L_0 = ___name;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___name = G_B2_0;
		String_t* L_3 = ___paramName;
		String_t* L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_5;
	}

IL_001c:
	{
		___paramName = G_B4_0;
		String_t* L_6 = ___paramValue;
		String_t* L_7 = L_6;
		G_B5_0 = L_7;
		if (L_7)
		{
			G_B6_0 = L_7;
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_8;
	}

IL_002b:
	{
		___paramValue = G_B6_0;
		StringU5BU5D_t2956870243* L_9 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral2412577137);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2412577137);
		StringU5BU5D_t2956870243* L_10 = L_9;
		String_t* L_11 = ___name;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_11);
		StringU5BU5D_t2956870243* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral44);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_13 = L_12;
		String_t* L_14 = ___paramName;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_14);
		StringU5BU5D_t2956870243* L_15 = L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral44);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_16 = L_15;
		String_t* L_17 = ___paramValue;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_17);
		StringU5BU5D_t2956870243* L_18 = L_16;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, _stringLiteral3931195228);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral3931195228);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m21867311(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_20 = ___variables;
		if (L_20)
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_21 = ___name;
		String_t* L_22 = ___paramName;
		String_t* L_23 = ___paramValue;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		bool L_24 = FuseSDK_RegisterEvent_m2414525879(NULL /*static, unused*/, L_21, L_22, L_23, (String_t*)NULL, (0.0), /*hidden argument*/NULL);
		return L_24;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		{
			Hashtable_t3875263730 * L_25 = ___variables;
			NullCheck(L_25);
			int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_25);
			V_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_26));
			Hashtable_t3875263730 * L_27 = ___variables;
			NullCheck(L_27);
			int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_27);
			V_1 = ((DoubleU5BU5D_t1048280995*)SZArrayNew(DoubleU5BU5D_t1048280995_il2cpp_TypeInfo_var, (uint32_t)L_28));
			Hashtable_t3875263730 * L_29 = ___variables;
			NullCheck(L_29);
			Il2CppObject * L_30 = VirtFuncInvoker0< Il2CppObject * >::Invoke(19 /* System.Collections.ICollection System.Collections.Hashtable::get_Keys() */, L_29);
			StringU5BU5D_t2956870243* L_31 = V_0;
			NullCheck(L_30);
			InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t3761522009_il2cpp_TypeInfo_var, L_30, (Il2CppArray *)(Il2CppArray *)L_31, 0);
			V_2 = 0;
			goto IL_00c3;
		}

IL_00ae:
		{
			DoubleU5BU5D_t1048280995* L_32 = V_1;
			int32_t L_33 = V_2;
			Hashtable_t3875263730 * L_34 = ___variables;
			StringU5BU5D_t2956870243* L_35 = V_0;
			int32_t L_36 = V_2;
			NullCheck(L_35);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
			int32_t L_37 = L_36;
			NullCheck(L_34);
			Il2CppObject * L_38 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_34, ((L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37))));
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			double L_39 = Convert_ToDouble_m1941295007(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
			NullCheck(L_32);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
			(L_32)->SetAt(static_cast<il2cpp_array_size_t>(L_33), (double)L_39);
			int32_t L_40 = V_2;
			V_2 = ((int32_t)((int32_t)L_40+(int32_t)1));
		}

IL_00c3:
		{
			int32_t L_41 = V_2;
			Hashtable_t3875263730 * L_42 = ___variables;
			NullCheck(L_42);
			int32_t L_43 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_42);
			if ((((int32_t)L_41) < ((int32_t)L_43)))
			{
				goto IL_00ae;
			}
		}

IL_00cf:
		{
			String_t* L_44 = ___name;
			String_t* L_45 = ___paramName;
			String_t* L_46 = ___paramValue;
			StringU5BU5D_t2956870243* L_47 = V_0;
			DoubleU5BU5D_t1048280995* L_48 = V_1;
			StringU5BU5D_t2956870243* L_49 = V_0;
			NullCheck(L_49);
			IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
			bool L_50 = FuseSDK_Native_RegisterEventWithDictionary_m1672630642(NULL /*static, unused*/, L_44, L_45, L_46, L_47, L_48, (((int32_t)((int32_t)(((Il2CppArray *)L_49)->max_length)))), /*hidden argument*/NULL);
			V_4 = L_50;
			goto IL_0106;
		}

IL_00e3:
		{
			; // IL_00e3: leave IL_0106
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00e8;
		throw e;
	}

CATCH_00e8:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t1967233988 *)__exception_local);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1939827555, /*hidden argument*/NULL);
			Exception_t1967233988 * L_51 = V_3;
			Debug_LogException_m248970745(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
			V_4 = (bool)0;
			goto IL_0106;
		}

IL_0101:
		{
			; // IL_0101: leave IL_0106
		}
	} // end catch (depth: 1)

IL_0106:
	{
		bool L_52 = V_4;
		return L_52;
	}
}
// System.Boolean FuseSDK::RegisterEvent(System.String,System.String,System.String,System.String,System.Double)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t534516614_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2412577137;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_RegisterEvent_m2414525879_MetadataUsageId;
extern "C"  bool FuseSDK_RegisterEvent_m2414525879 (Il2CppObject * __this /* static, unused */, String_t* ___name, String_t* ___paramName, String_t* ___paramValue, String_t* ___variableName, double ___variableValue, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterEvent_m2414525879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B6_0 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B7_0 = NULL;
	{
		String_t* L_0 = ___name;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___name = G_B2_0;
		String_t* L_3 = ___paramName;
		String_t* L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_5;
	}

IL_001c:
	{
		___paramName = G_B4_0;
		String_t* L_6 = ___paramValue;
		String_t* L_7 = L_6;
		G_B5_0 = L_7;
		if (L_7)
		{
			G_B6_0 = L_7;
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_8;
	}

IL_002b:
	{
		___paramValue = G_B6_0;
		String_t* L_9 = ___variableName;
		String_t* L_10 = L_9;
		G_B7_0 = L_10;
		if (L_10)
		{
			G_B8_0 = L_10;
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_11;
	}

IL_003a:
	{
		___variableName = G_B8_0;
		ObjectU5BU5D_t11523773* L_12 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)((int32_t)11)));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		ArrayElementTypeCheck (L_12, _stringLiteral2412577137);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2412577137);
		ObjectU5BU5D_t11523773* L_13 = L_12;
		String_t* L_14 = ___name;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, _stringLiteral44);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t11523773* L_16 = L_15;
		String_t* L_17 = ___paramName;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_17);
		ObjectU5BU5D_t11523773* L_18 = L_16;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 4);
		ArrayElementTypeCheck (L_18, _stringLiteral44);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t11523773* L_19 = L_18;
		String_t* L_20 = ___paramValue;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 5);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_20);
		ObjectU5BU5D_t11523773* L_21 = L_19;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 6);
		ArrayElementTypeCheck (L_21, _stringLiteral44);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t11523773* L_22 = L_21;
		String_t* L_23 = ___variableName;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 7);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_23);
		ObjectU5BU5D_t11523773* L_24 = L_22;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 8);
		ArrayElementTypeCheck (L_24, _stringLiteral44);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t11523773* L_25 = L_24;
		double L_26 = ___variableValue;
		double L_27 = L_26;
		Il2CppObject * L_28 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)9));
		ArrayElementTypeCheck (L_25, L_28);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_28);
		ObjectU5BU5D_t11523773* L_29 = L_25;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)10));
		ArrayElementTypeCheck (L_29, _stringLiteral41);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m3016520001(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		String_t* L_31 = ___name;
		String_t* L_32 = ___paramName;
		String_t* L_33 = ___paramValue;
		String_t* L_34 = ___variableName;
		double L_35 = ___variableValue;
		bool L_36 = FuseSDK_Native_RegisterEventVariable_m3671701833(NULL /*static, unused*/, L_31, L_32, L_33, L_34, L_35, /*hidden argument*/NULL);
		return L_36;
	}
}
// System.Void FuseSDK::RegisterVirtualGoodsPurchase(System.Int32,System.Int32,System.Int32)
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1256351097;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_RegisterVirtualGoodsPurchase_m316273874_MetadataUsageId;
extern "C"  void FuseSDK_RegisterVirtualGoodsPurchase_m316273874 (Il2CppObject * __this /* static, unused */, int32_t ___virtualgoodID, int32_t ___currencyAmount, int32_t ___currencyID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterVirtualGoodsPurchase_m316273874_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1256351097);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1256351097);
		ObjectU5BU5D_t11523773* L_1 = L_0;
		int32_t L_2 = ___virtualgoodID;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t11523773* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral44);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t11523773* L_6 = L_5;
		int32_t L_7 = ___currencyAmount;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral44);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t11523773* L_11 = L_10;
		int32_t L_12 = ___currencyID;
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = L_11;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 6);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		int32_t L_17 = ___virtualgoodID;
		int32_t L_18 = ___currencyAmount;
		int32_t L_19 = ___currencyID;
		FuseSDK_Native_RegisterVirtualGoodsPurchase_m519104408(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::RegisterIOSInAppPurchaseList(FuseMisc.Product[])
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* SingleU5BU5D_t1219431280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2869691792;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_RegisterIOSInAppPurchaseList_m2037236147_MetadataUsageId;
extern "C"  void FuseSDK_RegisterIOSInAppPurchaseList_m2037236147 (Il2CppObject * __this /* static, unused */, ProductU5BU5D_t4249436827* ___products, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterIOSInAppPurchaseList_m2037236147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	SingleU5BU5D_t1219431280* V_2 = NULL;
	int32_t V_3 = 0;
	{
		ProductU5BU5D_t4249436827* L_0 = ___products;
		NullCheck(L_0);
		int32_t L_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral2869691792, L_2, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ProductU5BU5D_t4249436827* L_4 = ___products;
		NullCheck(L_4);
		V_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		ProductU5BU5D_t4249436827* L_5 = ___products;
		NullCheck(L_5);
		V_1 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))));
		ProductU5BU5D_t4249436827* L_6 = ___products;
		NullCheck(L_6);
		V_2 = ((SingleU5BU5D_t1219431280*)SZArrayNew(SingleU5BU5D_t1219431280_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		V_3 = 0;
		goto IL_006f;
	}

IL_003e:
	{
		StringU5BU5D_t2956870243* L_7 = V_0;
		int32_t L_8 = V_3;
		ProductU5BU5D_t4249436827* L_9 = ___products;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		String_t* L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_ProductId_0();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_11);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (String_t*)L_11);
		StringU5BU5D_t2956870243* L_12 = V_1;
		int32_t L_13 = V_3;
		ProductU5BU5D_t4249436827* L_14 = ___products;
		int32_t L_15 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		String_t* L_16 = ((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15)))->get_PriceLocale_2();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (String_t*)L_16);
		SingleU5BU5D_t1219431280* L_17 = V_2;
		int32_t L_18 = V_3;
		ProductU5BU5D_t4249436827* L_19 = ___products;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		float L_21 = ((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))->get_Price_1();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (float)L_21);
		int32_t L_22 = V_3;
		V_3 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_006f:
	{
		int32_t L_23 = V_3;
		ProductU5BU5D_t4249436827* L_24 = ___products;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		StringU5BU5D_t2956870243* L_25 = V_0;
		StringU5BU5D_t2956870243* L_26 = V_1;
		SingleU5BU5D_t1219431280* L_27 = V_2;
		StringU5BU5D_t2956870243* L_28 = V_0;
		NullCheck(L_28);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_Native_RegisterInAppPurchaseList_m3231503004(NULL /*static, unused*/, L_25, L_26, L_27, (((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::RegisterIOSInAppPurchase(System.String,System.String,System.Byte[],FuseMisc.IAPState)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* IAPState_t1023998552_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1651553678;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_RegisterIOSInAppPurchase_m2575870670_MetadataUsageId;
extern "C"  void FuseSDK_RegisterIOSInAppPurchase_m2575870670 (Il2CppObject * __this /* static, unused */, String_t* ___productId, String_t* ___transactionId, ByteU5BU5D_t58506160* ___transactionReceipt, int32_t ___transactionState, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterIOSInAppPurchase_m2575870670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___productId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___productId = G_B2_0;
		String_t* L_3 = ___transactionId;
		String_t* L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_5;
	}

IL_001c:
	{
		___transactionId = G_B4_0;
		ObjectU5BU5D_t11523773* L_6 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral1651553678);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1651553678);
		ObjectU5BU5D_t11523773* L_7 = L_6;
		String_t* L_8 = ___productId;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_8);
		ObjectU5BU5D_t11523773* L_9 = L_7;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		ArrayElementTypeCheck (L_9, _stringLiteral44);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t11523773* L_10 = L_9;
		ByteU5BU5D_t58506160* L_11 = ___transactionReceipt;
		NullCheck(L_11);
		int32_t L_12 = (((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))));
		Il2CppObject * L_13 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		ObjectU5BU5D_t11523773* L_14 = L_10;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 4);
		ArrayElementTypeCheck (L_14, _stringLiteral44);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t11523773* L_15 = L_14;
		int32_t L_16 = ___transactionState;
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(IAPState_t1023998552_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 5);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_18);
		ObjectU5BU5D_t11523773* L_19 = L_15;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 6);
		ArrayElementTypeCheck (L_19, _stringLiteral41);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m3016520001(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		String_t* L_21 = ___productId;
		String_t* L_22 = ___transactionId;
		ByteU5BU5D_t58506160* L_23 = ___transactionReceipt;
		ByteU5BU5D_t58506160* L_24 = ___transactionReceipt;
		NullCheck(L_24);
		int32_t L_25 = ___transactionState;
		FuseSDK_Native_RegisterInAppPurchase_m348866365(NULL /*static, unused*/, L_21, L_22, L_23, (((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length)))), L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::RegisterUnibillPurchase(System.String,System.Byte[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3249734592;
extern const uint32_t FuseSDK_RegisterUnibillPurchase_m1122362048_MetadataUsageId;
extern "C"  void FuseSDK_RegisterUnibillPurchase_m1122362048 (Il2CppObject * __this /* static, unused */, String_t* ___productID, ByteU5BU5D_t58506160* ___receipt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterUnibillPurchase_m1122362048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___productID;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___productID = G_B2_0;
		String_t* L_3 = ___productID;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3249734592, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___productID;
		ByteU5BU5D_t58506160* L_6 = ___receipt;
		ByteU5BU5D_t58506160* L_7 = ___receipt;
		NullCheck(L_7);
		FuseSDK_Native_RegisterUnibillPurchase_m3955303697(NULL /*static, unused*/, L_5, L_6, (((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::RegisterAndroidInAppPurchase(FuseMisc.IAPState,System.String,System.String,System.String,System.DateTime,System.String,System.Double,System.String)
extern "C"  void FuseSDK_RegisterAndroidInAppPurchase_m2341887311 (Il2CppObject * __this /* static, unused */, int32_t ___purchaseState, String_t* ___purchaseToken, String_t* ___productId, String_t* ___orderId, DateTime_t339033936  ___purchaseTime, String_t* ___developerPayload, double ___price, String_t* ___currency, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDK::RegisterAndroidInAppPurchase(FuseMisc.IAPState,System.String,System.String,System.String,System.Int64,System.String,System.Double,System.String)
extern "C"  void FuseSDK_RegisterAndroidInAppPurchase_m3043945475 (Il2CppObject * __this /* static, unused */, int32_t ___purchaseState, String_t* ___purchaseToken, String_t* ___productId, String_t* ___orderId, int64_t ___purchaseTime, String_t* ___developerPayload, double ___price, String_t* ___currency, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean FuseSDK::IsAdAvailableForZoneID(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral646476052;
extern const uint32_t FuseSDK_IsAdAvailableForZoneID_m380410448_MetadataUsageId;
extern "C"  bool FuseSDK_IsAdAvailableForZoneID_m380410448 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_IsAdAvailableForZoneID_m380410448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___zoneId = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral646476052, /*hidden argument*/NULL);
		String_t* L_3 = ___zoneId;
		bool L_4 = FuseSDK_Native_IsAdAvailableForZoneID_m1070722658(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean FuseSDK::ZoneHasRewarded(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral203915612;
extern const uint32_t FuseSDK_ZoneHasRewarded_m3490578914_MetadataUsageId;
extern "C"  bool FuseSDK_ZoneHasRewarded_m3490578914 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_ZoneHasRewarded_m3490578914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___zoneId = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral203915612, /*hidden argument*/NULL);
		String_t* L_3 = ___zoneId;
		bool L_4 = FuseSDK_Native_ZoneHasRewarded_m574832144(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean FuseSDK::ZoneHasIAPOffer(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2974581810;
extern const uint32_t FuseSDK_ZoneHasIAPOffer_m1341405260_MetadataUsageId;
extern "C"  bool FuseSDK_ZoneHasIAPOffer_m1341405260 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_ZoneHasIAPOffer_m1341405260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___zoneId = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2974581810, /*hidden argument*/NULL);
		String_t* L_3 = ___zoneId;
		bool L_4 = FuseSDK_Native_ZoneHasIAPOffer_m2720625786(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean FuseSDK::ZoneHasVirtualGoodsOffer(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral681673859;
extern const uint32_t FuseSDK_ZoneHasVirtualGoodsOffer_m912184897_MetadataUsageId;
extern "C"  bool FuseSDK_ZoneHasVirtualGoodsOffer_m912184897 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_ZoneHasVirtualGoodsOffer_m912184897_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___zoneId = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral681673859, /*hidden argument*/NULL);
		String_t* L_3 = ___zoneId;
		bool L_4 = FuseSDK_Native_ZoneHasVirtualGoodsOffer_m2877255123(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// FuseMisc.RewardedInfo FuseSDK::GetRewardedInfoForZone(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238738819;
extern const uint32_t FuseSDK_GetRewardedInfoForZone_m357494019_MetadataUsageId;
extern "C"  RewardedInfo_t829972251  FuseSDK_GetRewardedInfoForZone_m357494019 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetRewardedInfoForZone_m357494019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___zoneId = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3238738819, /*hidden argument*/NULL);
		String_t* L_3 = ___zoneId;
		String_t* L_4 = FuseSDK_Native_GetRewardedInfoForZone_m2171737092(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		RewardedInfo_t829972251  L_6;
		memset(&L_6, 0, sizeof(L_6));
		RewardedInfo__ctor_m1550529012(&L_6, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// FuseMisc.VGOfferInfo FuseSDK::GetVGOfferInfoForZone(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral783239698;
extern const uint32_t FuseSDK_GetVGOfferInfoForZone_m1031517147_MetadataUsageId;
extern "C"  VGOfferInfo_t1385795224  FuseSDK_GetVGOfferInfoForZone_m1031517147 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetVGOfferInfoForZone_m1031517147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___zoneId = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral783239698, /*hidden argument*/NULL);
		String_t* L_3 = ___zoneId;
		String_t* L_4 = FuseSDK_Native_GetVirtualGoodsOfferInfoForZoneID_m3907036564(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		VGOfferInfo_t1385795224  L_6;
		memset(&L_6, 0, sizeof(L_6));
		VGOfferInfo__ctor_m259242847(&L_6, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// FuseMisc.IAPOfferInfo FuseSDK::GetIAPOfferInfoForZone(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3029415661;
extern const uint32_t FuseSDK_GetIAPOfferInfoForZone_m18495939_MetadataUsageId;
extern "C"  IAPOfferInfo_t3826517745  FuseSDK_GetIAPOfferInfoForZone_m18495939 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetIAPOfferInfoForZone_m18495939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___zoneId = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3029415661, /*hidden argument*/NULL);
		String_t* L_3 = ___zoneId;
		String_t* L_4 = FuseSDK_Native_GetIAPOfferInfoForZoneID_m3128281759(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		IAPOfferInfo_t3826517745  L_6;
		memset(&L_6, 0, sizeof(L_6));
		IAPOfferInfo__ctor_m3107304778(&L_6, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void FuseSDK::ShowAdForZoneID(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m215803178_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m112638570_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3777343408;
extern const uint32_t FuseSDK_ShowAdForZoneID_m620550069_MetadataUsageId;
extern "C"  void FuseSDK_ShowAdForZoneID_m620550069 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, Dictionary_2_t2606186806 * ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_ShowAdForZoneID_m620550069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	StringU5BU5D_t2956870243* G_B5_0 = NULL;
	StringU5BU5D_t2956870243* G_B8_0 = NULL;
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___zoneId = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3777343408, /*hidden argument*/NULL);
		Dictionary_2_t2606186806 * L_3 = ___options;
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		G_B5_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)0));
		goto IL_0035;
	}

IL_002a:
	{
		Dictionary_2_t2606186806 * L_4 = ___options;
		NullCheck(L_4);
		KeyCollection_t634494790 * L_5 = Dictionary_2_get_Keys_m215803178(L_4, /*hidden argument*/Dictionary_2_get_Keys_m215803178_MethodInfo_var);
		StringU5BU5D_t2956870243* L_6 = Enumerable_ToArray_TisString_t_m2199204590(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var);
		G_B5_0 = L_6;
	}

IL_0035:
	{
		V_0 = G_B5_0;
		Dictionary_2_t2606186806 * L_7 = ___options;
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		G_B8_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)0));
		goto IL_0052;
	}

IL_0047:
	{
		Dictionary_2_t2606186806 * L_8 = ___options;
		NullCheck(L_8);
		ValueCollection_t233356604 * L_9 = Dictionary_2_get_Values_m112638570(L_8, /*hidden argument*/Dictionary_2_get_Values_m112638570_MethodInfo_var);
		StringU5BU5D_t2956870243* L_10 = Enumerable_ToArray_TisString_t_m2199204590(NULL /*static, unused*/, L_9, /*hidden argument*/Enumerable_ToArray_TisString_t_m2199204590_MethodInfo_var);
		G_B8_0 = L_10;
	}

IL_0052:
	{
		V_1 = G_B8_0;
		String_t* L_11 = ___zoneId;
		StringU5BU5D_t2956870243* L_12 = V_0;
		StringU5BU5D_t2956870243* L_13 = V_1;
		StringU5BU5D_t2956870243* L_14 = V_0;
		NullCheck(L_14);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_Native_ShowAdForZoneID_m1395443739(NULL /*static, unused*/, L_11, L_12, L_13, (((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::PreloadAdForZoneID(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2400321892;
extern const uint32_t FuseSDK_PreloadAdForZoneID_m3995970508_MetadataUsageId;
extern "C"  void FuseSDK_PreloadAdForZoneID_m3995970508 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_PreloadAdForZoneID_m3995970508_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___zoneId = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2400321892, /*hidden argument*/NULL);
		String_t* L_3 = ___zoneId;
		FuseSDK_Native_PreloadAdForZone_m2574987585(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::SetRewardedVideoUserID(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4005418481;
extern const uint32_t FuseSDK_SetRewardedVideoUserID_m202042399_MetadataUsageId;
extern "C"  void FuseSDK_SetRewardedVideoUserID_m202042399 (Il2CppObject * __this /* static, unused */, String_t* ___userID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_SetRewardedVideoUserID_m202042399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___userID;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___userID = G_B2_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral4005418481, /*hidden argument*/NULL);
		String_t* L_3 = ___userID;
		FuseSDK_Native_SetRewardedVideoUserID_m1352186393(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::DisplayNotifications()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2138205927;
extern const uint32_t FuseSDK_DisplayNotifications_m431031352_MetadataUsageId;
extern "C"  void FuseSDK_DisplayNotifications_m431031352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_DisplayNotifications_m431031352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2138205927, /*hidden argument*/NULL);
		FuseSDK_Native_DisplayNotifications_m3833872766(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FuseSDK::IsNotificationAvailable()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral166521589;
extern const uint32_t FuseSDK_IsNotificationAvailable_m3572255160_MetadataUsageId;
extern "C"  bool FuseSDK_IsNotificationAvailable_m3572255160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_IsNotificationAvailable_m3572255160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral166521589, /*hidden argument*/NULL);
		bool L_0 = FuseSDK_Native_IsNotificationAvailable_m2029103306(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void FuseSDK::RegisterGender(FuseMisc.Gender)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral735301477;
extern const uint32_t FuseSDK_RegisterGender_m135225242_MetadataUsageId;
extern "C"  void FuseSDK_RegisterGender_m135225242 (Il2CppObject * __this /* static, unused */, int32_t ___gender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterGender_m135225242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral735301477, /*hidden argument*/NULL);
		int32_t L_0 = ___gender;
		FuseSDK_Native_RegisterGender_m3465403021(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::RegisterAge(System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3675912509;
extern const uint32_t FuseSDK_RegisterAge_m4086618141_MetadataUsageId;
extern "C"  void FuseSDK_RegisterAge_m4086618141 (Il2CppObject * __this /* static, unused */, int32_t ___age, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterAge_m4086618141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3675912509, /*hidden argument*/NULL);
		int32_t L_0 = ___age;
		FuseSDK_Native_RegisterAge_m4022095127(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::RegisterBirthday(System.Int32,System.Int32,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral68997985;
extern const uint32_t FuseSDK_RegisterBirthday_m1764743971_MetadataUsageId;
extern "C"  void FuseSDK_RegisterBirthday_m1764743971 (Il2CppObject * __this /* static, unused */, int32_t ___year, int32_t ___month, int32_t ___day, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterBirthday_m1764743971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral68997985, /*hidden argument*/NULL);
		int32_t L_0 = ___year;
		int32_t L_1 = ___month;
		int32_t L_2 = ___day;
		FuseSDK_Native_RegisterBirthday_m2218280681(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::RegisterLevel(System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3221734210;
extern const uint32_t FuseSDK_RegisterLevel_m949992162_MetadataUsageId;
extern "C"  void FuseSDK_RegisterLevel_m949992162 (Il2CppObject * __this /* static, unused */, int32_t ___level, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterLevel_m949992162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3221734210, /*hidden argument*/NULL);
		int32_t L_0 = ___level;
		FuseSDK_Native_RegisterLevel_m3367885148(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FuseSDK::RegisterCurrency(System.Int32,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2101234677;
extern const uint32_t FuseSDK_RegisterCurrency_m2201307348_MetadataUsageId;
extern "C"  bool FuseSDK_RegisterCurrency_m2201307348 (Il2CppObject * __this /* static, unused */, int32_t ___currencyType, int32_t ___balance, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterCurrency_m2201307348_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2101234677, /*hidden argument*/NULL);
		int32_t L_0 = ___currencyType;
		int32_t L_1 = ___balance;
		bool L_2 = FuseSDK_Native_RegisterCurrency_m3225143142(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void FuseSDK::RegisterParentalConsent(System.Boolean)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3013062467;
extern const uint32_t FuseSDK_RegisterParentalConsent_m672369225_MetadataUsageId;
extern "C"  void FuseSDK_RegisterParentalConsent_m672369225 (Il2CppObject * __this /* static, unused */, bool ___consentGranted, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterParentalConsent_m672369225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3013062467, /*hidden argument*/NULL);
		bool L_0 = ___consentGranted;
		FuseSDK_Native_RegisterParentalConsent_m2154152387(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FuseSDK::RegisterCustomEvent(System.Int32,System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1613702759;
extern const uint32_t FuseSDK_RegisterCustomEvent_m4096309751_MetadataUsageId;
extern "C"  bool FuseSDK_RegisterCustomEvent_m4096309751 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterCustomEvent_m4096309751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral1613702759, /*hidden argument*/NULL);
		int32_t L_0 = ___eventNumber;
		String_t* L_1 = ___value;
		bool L_2 = FuseSDK_Native_RegisterCustomEventString_m946012154(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean FuseSDK::RegisterCustomEvent(System.Int32,System.Int32)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1613702759;
extern const uint32_t FuseSDK_RegisterCustomEvent_m3165448124_MetadataUsageId;
extern "C"  bool FuseSDK_RegisterCustomEvent_m3165448124 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RegisterCustomEvent_m3165448124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral1613702759, /*hidden argument*/NULL);
		int32_t L_0 = ___eventNumber;
		int32_t L_1 = ___value;
		bool L_2 = FuseSDK_Native_RegisterCustomEventInt_m2604151633(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String FuseSDK::GetFuseId()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral975444915;
extern const uint32_t FuseSDK_GetFuseId_m1990982053_MetadataUsageId;
extern "C"  String_t* FuseSDK_GetFuseId_m1990982053 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetFuseId_m1990982053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral975444915, /*hidden argument*/NULL);
		String_t* L_0 = FuseSDK_Native_GetFuseId_m1427507577(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String FuseSDK::GetOriginalAccountAlias()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4144243563;
extern const uint32_t FuseSDK_GetOriginalAccountAlias_m2540679645_MetadataUsageId;
extern "C"  String_t* FuseSDK_GetOriginalAccountAlias_m2540679645 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetOriginalAccountAlias_m2540679645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral4144243563, /*hidden argument*/NULL);
		String_t* L_0 = FuseSDK_Native_GetOriginalAccountAlias_m960986801(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String FuseSDK::GetOriginalAccountId()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral848519554;
extern const uint32_t FuseSDK_GetOriginalAccountId_m1297987664_MetadataUsageId;
extern "C"  String_t* FuseSDK_GetOriginalAccountId_m1297987664 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetOriginalAccountId_m1297987664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral848519554, /*hidden argument*/NULL);
		String_t* L_0 = FuseSDK_Native_GetOriginalAccountId_m4035001340(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// FuseMisc.AccountType FuseSDK::GetOriginalAccountType()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4014990945;
extern const uint32_t FuseSDK_GetOriginalAccountType_m3287745243_MetadataUsageId;
extern "C"  int32_t FuseSDK_GetOriginalAccountType_m3287745243 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetOriginalAccountType_m3287745243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral4014990945, /*hidden argument*/NULL);
		int32_t L_0 = FuseSDK_Native_GetOriginalAccountType_m783477574(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (int32_t)(L_0);
	}
}
// System.Void FuseSDK::GameCenterLogin()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2418117923;
extern const uint32_t FuseSDK_GameCenterLogin_m811411762_MetadataUsageId;
extern "C"  void FuseSDK_GameCenterLogin_m811411762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GameCenterLogin_m811411762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2418117923, /*hidden argument*/NULL);
		FuseSDK_Native_GameCenterLogin_m1956526124(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::FacebookLogin(System.String,System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1457235045;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_FacebookLogin_m1823013031_MetadataUsageId;
extern "C"  void FuseSDK_FacebookLogin_m1823013031 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId, String_t* ___name, String_t* ___accessToken, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_FacebookLogin_m1823013031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B6_0 = NULL;
	String_t* G_B5_0 = NULL;
	{
		String_t* L_0 = ___facebookId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___facebookId = G_B2_0;
		String_t* L_3 = ___name;
		String_t* L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_5;
	}

IL_001c:
	{
		___name = G_B4_0;
		String_t* L_6 = ___accessToken;
		String_t* L_7 = L_6;
		G_B5_0 = L_7;
		if (L_7)
		{
			G_B6_0 = L_7;
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_8;
	}

IL_002b:
	{
		___accessToken = G_B6_0;
		StringU5BU5D_t2956870243* L_9 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1457235045);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1457235045);
		StringU5BU5D_t2956870243* L_10 = L_9;
		String_t* L_11 = ___facebookId;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_11);
		StringU5BU5D_t2956870243* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral44);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_13 = L_12;
		String_t* L_14 = ___name;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_14);
		StringU5BU5D_t2956870243* L_15 = L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral44);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_16 = L_15;
		String_t* L_17 = ___accessToken;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_17);
		StringU5BU5D_t2956870243* L_18 = L_16;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, _stringLiteral41);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m21867311(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		String_t* L_20 = ___facebookId;
		String_t* L_21 = ___name;
		String_t* L_22 = ___accessToken;
		FuseSDK_Native_FacebookLogin_m2276549741(NULL /*static, unused*/, L_20, L_21, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::TwitterLogin(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3674399250;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_TwitterLogin_m1436523542_MetadataUsageId;
extern "C"  void FuseSDK_TwitterLogin_m1436523542 (Il2CppObject * __this /* static, unused */, String_t* ___twitterId, String_t* ___alias, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_TwitterLogin_m1436523542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___twitterId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___twitterId = G_B2_0;
		String_t* L_3 = ___alias;
		String_t* L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_5;
	}

IL_001c:
	{
		___alias = G_B4_0;
		String_t* L_6 = ___twitterId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3674399250, L_6, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		String_t* L_8 = ___twitterId;
		String_t* L_9 = ___alias;
		FuseSDK_Native_TwitterLogin_m3795967248(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::FuseLogin(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1022063840;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_FuseLogin_m792451590_MetadataUsageId;
extern "C"  void FuseSDK_FuseLogin_m792451590 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___alias, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_FuseLogin_m792451590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___fuseId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___fuseId = G_B2_0;
		String_t* L_3 = ___alias;
		String_t* L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_5;
	}

IL_001c:
	{
		___alias = G_B4_0;
		StringU5BU5D_t2956870243* L_6 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral1022063840);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1022063840);
		StringU5BU5D_t2956870243* L_7 = L_6;
		String_t* L_8 = ___fuseId;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
		StringU5BU5D_t2956870243* L_9 = L_7;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		ArrayElementTypeCheck (L_9, _stringLiteral44);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_10 = L_9;
		String_t* L_11 = ___alias;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_11);
		StringU5BU5D_t2956870243* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral41);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_14 = ___fuseId;
		String_t* L_15 = ___alias;
		FuseSDK_Native_FuseLogin_m2087177036(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::EmailLogin(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3179635803;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_EmailLogin_m2927600319_MetadataUsageId;
extern "C"  void FuseSDK_EmailLogin_m2927600319 (Il2CppObject * __this /* static, unused */, String_t* ___email, String_t* ___alias, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_EmailLogin_m2927600319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___email;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___email = G_B2_0;
		String_t* L_3 = ___alias;
		String_t* L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_5;
	}

IL_001c:
	{
		___alias = G_B4_0;
		StringU5BU5D_t2956870243* L_6 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral3179635803);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3179635803);
		StringU5BU5D_t2956870243* L_7 = L_6;
		String_t* L_8 = ___email;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
		StringU5BU5D_t2956870243* L_9 = L_7;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		ArrayElementTypeCheck (L_9, _stringLiteral44);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_10 = L_9;
		String_t* L_11 = ___alias;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_11);
		StringU5BU5D_t2956870243* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral41);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_14 = ___email;
		String_t* L_15 = ___alias;
		FuseSDK_Native_EmailLogin_m114416185(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::DeviceLogin(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3325871093;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_DeviceLogin_m3564151167_MetadataUsageId;
extern "C"  void FuseSDK_DeviceLogin_m3564151167 (Il2CppObject * __this /* static, unused */, String_t* ___alias, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_DeviceLogin_m3564151167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___alias;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___alias = G_B2_0;
		String_t* L_3 = ___alias;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3325871093, L_3, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___alias;
		FuseSDK_Native_DeviceLogin_m1563937733(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::GooglePlayLogin(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral832113004;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_GooglePlayLogin_m1136774130_MetadataUsageId;
extern "C"  void FuseSDK_GooglePlayLogin_m1136774130 (Il2CppObject * __this /* static, unused */, String_t* ___alias, String_t* ___token, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GooglePlayLogin_m1136774130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___alias;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___alias = G_B2_0;
		String_t* L_3 = ___token;
		String_t* L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_5;
	}

IL_001c:
	{
		___token = G_B4_0;
		StringU5BU5D_t2956870243* L_6 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral832113004);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral832113004);
		StringU5BU5D_t2956870243* L_7 = L_6;
		String_t* L_8 = ___alias;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
		StringU5BU5D_t2956870243* L_9 = L_7;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		ArrayElementTypeCheck (L_9, _stringLiteral44);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_10 = L_9;
		String_t* L_11 = ___token;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_11);
		StringU5BU5D_t2956870243* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral41);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_14 = ___alias;
		String_t* L_15 = ___token;
		FuseSDK_Native_GooglePlayLogin_m4184420536(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::ManualRegisterForPushNotifications(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisFuseSDK_t1159654649_m3409458168_MethodInfo_var;
extern const uint32_t FuseSDK_ManualRegisterForPushNotifications_m2411783554_MetadataUsageId;
extern "C"  void FuseSDK_ManualRegisterForPushNotifications_m2411783554 (Il2CppObject * __this /* static, unused */, String_t* ____, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_ManualRegisterForPushNotifications_m2411783554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FuseSDK_t1159654649 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_t1159654649 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_t1159654649 * L_2 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		NullCheck(L_2);
		bool L_3 = L_2->get_registerForPushNotifications_6();
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		FuseSDK_t1159654649 * L_4 = Object_FindObjectOfType_TisFuseSDK_t1159654649_m3409458168(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisFuseSDK_t1159654649_m3409458168_MethodInfo_var);
		V_0 = L_4;
		FuseSDK_t1159654649 * L_5 = V_0;
		FuseSDK_t1159654649 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = FuseSDK_SetupPushNotifications_m4090347419(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		MonoBehaviour_StartCoroutine_m2135303124(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Int32 FuseSDK::GamesPlayed()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3852184789;
extern const uint32_t FuseSDK_GamesPlayed_m3460410354_MetadataUsageId;
extern "C"  int32_t FuseSDK_GamesPlayed_m3460410354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GamesPlayed_m3460410354_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3852184789, /*hidden argument*/NULL);
		int32_t L_0 = FuseSDK_Native_GamesPlayed_m3762996496(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String FuseSDK::LibraryVersion()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1375579518;
extern const uint32_t FuseSDK_LibraryVersion_m2379003596_MetadataUsageId;
extern "C"  String_t* FuseSDK_LibraryVersion_m2379003596 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_LibraryVersion_m2379003596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral1375579518, /*hidden argument*/NULL);
		String_t* L_0 = FuseSDK_Native_LibraryVersion_m605514616(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean FuseSDK::Connected()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392340394;
extern const uint32_t FuseSDK_Connected_m253239725_MetadataUsageId;
extern "C"  bool FuseSDK_Connected_m253239725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_Connected_m253239725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3392340394, /*hidden argument*/NULL);
		bool L_0 = FuseSDK_Native_Connected_m497903679(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void FuseSDK::UTCTimeFromServer()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4017078491;
extern const uint32_t FuseSDK_UTCTimeFromServer_m1427789486_MetadataUsageId;
extern "C"  void FuseSDK_UTCTimeFromServer_m1427789486 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_UTCTimeFromServer_m1427789486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral4017078491, /*hidden argument*/NULL);
		FuseSDK_Native_TimeFromServer_m815599794(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::FuseLog(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2031589855;
extern const uint32_t FuseSDK_FuseLog_m2054806159_MetadataUsageId;
extern "C"  void FuseSDK_FuseLog_m2054806159 (Il2CppObject * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_FuseLog_m2054806159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_t1159654649 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_t1159654649 * L_2 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		NullCheck(L_2);
		bool L_3 = L_2->get_logging_7();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_4 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2031589855, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void FuseSDK::EnableData()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2325195182;
extern const uint32_t FuseSDK_EnableData_m2615181375_MetadataUsageId;
extern "C"  void FuseSDK_EnableData_m2615181375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_EnableData_m2615181375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2325195182, /*hidden argument*/NULL);
		FuseSDK_Native_EnableData_m1268546821(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::DisableData()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3805815571;
extern const uint32_t FuseSDK_DisableData_m4205452962_MetadataUsageId;
extern "C"  void FuseSDK_DisableData_m4205452962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_DisableData_m4205452962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3805815571, /*hidden argument*/NULL);
		FuseSDK_Native_DisableData_m1114487452(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FuseSDK::DataEnabled()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2656969560;
extern const uint32_t FuseSDK_DataEnabled_m1048268443_MetadataUsageId;
extern "C"  bool FuseSDK_DataEnabled_m1048268443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_DataEnabled_m1048268443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2656969560, /*hidden argument*/NULL);
		bool L_0 = FuseSDK_Native_DataEnabled_m4242094253(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void FuseSDK::UpdateFriendsListFromServer()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3346180504;
extern const uint32_t FuseSDK_UpdateFriendsListFromServer_m3100747559_MetadataUsageId;
extern "C"  void FuseSDK_UpdateFriendsListFromServer_m3100747559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_UpdateFriendsListFromServer_m3100747559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3346180504, /*hidden argument*/NULL);
		FuseSDK_Native_UpdateFriendsListFromServer_m3118599969(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<FuseMisc.Friend> FuseSDK::GetFriendsList()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral129169534;
extern const uint32_t FuseSDK_GetFriendsList_m2688560911_MetadataUsageId;
extern "C"  List_1_t1333671126 * FuseSDK_GetFriendsList_m2688560911 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetFriendsList_m2688560911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral129169534, /*hidden argument*/NULL);
		List_1_t1333671126 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__friendsList_19();
		return L_0;
	}
}
// System.Void FuseSDK::AddFriend(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3301604745;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_AddFriend_m3698133075_MetadataUsageId;
extern "C"  void FuseSDK_AddFriend_m3698133075 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_AddFriend_m3698133075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___fuseId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___fuseId = G_B2_0;
		String_t* L_3 = ___fuseId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3301604745, L_3, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___fuseId;
		FuseSDK_Native_AddFriend_m1063652377(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::RemoveFriend(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1394359302;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_RemoveFriend_m635287630_MetadataUsageId;
extern "C"  void FuseSDK_RemoveFriend_m635287630 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RemoveFriend_m635287630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___fuseId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___fuseId = G_B2_0;
		String_t* L_3 = ___fuseId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1394359302, L_3, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___fuseId;
		FuseSDK_Native_RemoveFriend_m3053180616(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::AcceptFriend(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2916033282;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_AcceptFriend_m1988277834_MetadataUsageId;
extern "C"  void FuseSDK_AcceptFriend_m1988277834 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_AcceptFriend_m1988277834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___fuseId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___fuseId = G_B2_0;
		String_t* L_3 = ___fuseId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2916033282, L_3, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___fuseId;
		FuseSDK_Native_AcceptFriend_m111203524(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::RejectFriend(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral514661515;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_RejectFriend_m560253971_MetadataUsageId;
extern "C"  void FuseSDK_RejectFriend_m560253971 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_RejectFriend_m560253971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___fuseId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___fuseId = G_B2_0;
		String_t* L_3 = ___fuseId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral514661515, L_3, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___fuseId;
		FuseSDK_Native_RejectFriend_m2978146957(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::MigrateFriends(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1489043422;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_MigrateFriends_m2015545958_MetadataUsageId;
extern "C"  void FuseSDK_MigrateFriends_m2015545958 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_MigrateFriends_m2015545958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___fuseId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___fuseId = G_B2_0;
		String_t* L_3 = ___fuseId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1489043422, L_3, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___fuseId;
		FuseSDK_Native_MigrateFriends_m2033398368(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::UserPushNotification(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3515630328;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_UserPushNotification_m3851667708_MetadataUsageId;
extern "C"  void FuseSDK_UserPushNotification_m3851667708 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_UserPushNotification_m3851667708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___fuseId;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___fuseId = G_B2_0;
		String_t* L_3 = ___message;
		String_t* L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_5;
	}

IL_001c:
	{
		___message = G_B4_0;
		StringU5BU5D_t2956870243* L_6 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral3515630328);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3515630328);
		StringU5BU5D_t2956870243* L_7 = L_6;
		String_t* L_8 = ___fuseId;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
		StringU5BU5D_t2956870243* L_9 = L_7;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		ArrayElementTypeCheck (L_9, _stringLiteral44);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_10 = L_9;
		String_t* L_11 = ___message;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_11);
		StringU5BU5D_t2956870243* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral41);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_14 = ___fuseId;
		String_t* L_15 = ___message;
		FuseSDK_Native_UserPushNotification_m2529849334(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::FriendsPushNotification(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral725010094;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_FriendsPushNotification_m3777051384_MetadataUsageId;
extern "C"  void FuseSDK_FriendsPushNotification_m3777051384 (Il2CppObject * __this /* static, unused */, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_FriendsPushNotification_m3777051384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___message;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___message = G_B2_0;
		String_t* L_3 = ___message;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral725010094, L_3, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___message;
		FuseSDK_Native_FriendsPushNotification_m776809534(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.String FuseSDK::GetGameConfigurationValue(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral863515653;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_GetGameConfigurationValue_m2407051276_MetadataUsageId;
extern "C"  String_t* FuseSDK_GetGameConfigurationValue_m2407051276 (Il2CppObject * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetGameConfigurationValue_m2407051276_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___key;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_000d:
	{
		___key = G_B2_0;
		String_t* L_3 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral863515653, L_3, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___key;
		String_t* L_6 = FuseSDK_Native_GetGameConfigurationValue_m777724088(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> FuseSDK::GetGameConfiguration()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral204580907;
extern const uint32_t FuseSDK_GetGameConfiguration_m586707122_MetadataUsageId;
extern "C"  Dictionary_2_t2606186806 * FuseSDK_GetGameConfiguration_m586707122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetGameConfiguration_m586707122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral204580907, /*hidden argument*/NULL);
		Dictionary_2_t2606186806 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__gameConfig_18();
		return L_0;
	}
}
// System.Int32 FuseSDK::SetGameData(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.String,System.String)
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1597524044_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m215803178_MethodInfo_var;
extern const MethodInfo* KeyCollection_CopyTo_m3854848782_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m112638570_MethodInfo_var;
extern const MethodInfo* ValueCollection_CopyTo_m698744737_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2679371466;
extern Il2CppCodeGenString* _stringLiteral4126243008;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK_SetGameData_m592660709_MetadataUsageId;
extern "C"  int32_t FuseSDK_SetGameData_m592660709 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___data, String_t* ___fuseId, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_SetGameData_m592660709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2679371466);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2679371466);
		StringU5BU5D_t2956870243* L_1 = L_0;
		String_t* L_2 = ___key;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t2956870243* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral4126243008);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral4126243008);
		StringU5BU5D_t2956870243* L_4 = L_3;
		String_t* L_5 = ___fuseId;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t2956870243* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral41);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m21867311(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		String_t* L_8 = ___fuseId;
		bool L_9 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		String_t* L_10 = FuseSDK_GetFuseId_m1990982053(NULL /*static, unused*/, /*hidden argument*/NULL);
		___fuseId = L_10;
	}

IL_0042:
	{
		Dictionary_2_t2606186806 * L_11 = ___data;
		if (L_11)
		{
			goto IL_004f;
		}
	}
	{
		Dictionary_2_t2606186806 * L_12 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1597524044(L_12, /*hidden argument*/Dictionary_2__ctor_m1597524044_MethodInfo_var);
		___data = L_12;
	}

IL_004f:
	{
		Dictionary_2_t2606186806 * L_13 = ___data;
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count() */, L_13);
		V_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_14));
		Dictionary_2_t2606186806 * L_15 = ___data;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count() */, L_15);
		V_1 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_16));
		Dictionary_2_t2606186806 * L_17 = ___data;
		NullCheck(L_17);
		KeyCollection_t634494790 * L_18 = Dictionary_2_get_Keys_m215803178(L_17, /*hidden argument*/Dictionary_2_get_Keys_m215803178_MethodInfo_var);
		StringU5BU5D_t2956870243* L_19 = V_0;
		NullCheck(L_18);
		KeyCollection_CopyTo_m3854848782(L_18, L_19, 0, /*hidden argument*/KeyCollection_CopyTo_m3854848782_MethodInfo_var);
		Dictionary_2_t2606186806 * L_20 = ___data;
		NullCheck(L_20);
		ValueCollection_t233356604 * L_21 = Dictionary_2_get_Values_m112638570(L_20, /*hidden argument*/Dictionary_2_get_Values_m112638570_MethodInfo_var);
		StringU5BU5D_t2956870243* L_22 = V_1;
		NullCheck(L_21);
		ValueCollection_CopyTo_m698744737(L_21, L_22, 0, /*hidden argument*/ValueCollection_CopyTo_m698744737_MethodInfo_var);
		String_t* L_23 = ___fuseId;
		String_t* L_24 = ___key;
		StringU5BU5D_t2956870243* L_25 = V_0;
		StringU5BU5D_t2956870243* L_26 = V_1;
		StringU5BU5D_t2956870243* L_27 = V_0;
		NullCheck(L_27);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		int32_t L_28 = FuseSDK_Native_SetGameData_m2957575807(NULL /*static, unused*/, L_23, L_24, L_25, L_26, (((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))), /*hidden argument*/NULL);
		return L_28;
	}
}
// System.Int32 FuseSDK::GetGameData(System.String[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_GetGameData_m1213875184_MetadataUsageId;
extern "C"  int32_t FuseSDK_GetGameData_m1213875184 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___keys, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetGameData_m1213875184_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		StringU5BU5D_t2956870243* L_2 = ___keys;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		int32_t L_3 = FuseSDK_GetGameDataForFuseId_m969065313(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 FuseSDK::GetGameDataForFuseId(System.String,System.String,System.String[])
extern TypeInfo* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1130378838;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral1693012687;
extern const uint32_t FuseSDK_GetGameDataForFuseId_m969065313_MetadataUsageId;
extern "C"  int32_t FuseSDK_GetGameDataForFuseId_m969065313 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___key, StringU5BU5D_t2956870243* ___keys, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_GetGameDataForFuseId_m969065313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	StringU5BU5D_t2956870243* G_B3_0 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1130378838);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1130378838);
		StringU5BU5D_t2956870243* L_1 = L_0;
		String_t* L_2 = ___fuseId;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t2956870243* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral44);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t2956870243* L_4 = L_3;
		String_t* L_5 = ___key;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t2956870243* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral1693012687);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1693012687);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m21867311(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		StringU5BU5D_t2956870243* L_8 = ___keys;
		if (L_8)
		{
			goto IL_0041;
		}
	}
	{
		G_B3_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)0));
		goto IL_0042;
	}

IL_0041:
	{
		StringU5BU5D_t2956870243* L_9 = ___keys;
		G_B3_0 = L_9;
	}

IL_0042:
	{
		V_0 = G_B3_0;
		String_t* L_10 = ___fuseId;
		String_t* L_11 = ___key;
		StringU5BU5D_t2956870243* L_12 = V_0;
		StringU5BU5D_t2956870243* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		int32_t L_14 = FuseSDK_Native_GetGameData_m4250402445(NULL /*static, unused*/, L_10, L_11, L_12, (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))), /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Collections.IEnumerator FuseSDK::SetupPushNotifications()
extern TypeInfo* U3CSetupPushNotificationsU3Ec__Iterator0_t552020816_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_SetupPushNotifications_m4090347419_MetadataUsageId;
extern "C"  Il2CppObject * FuseSDK_SetupPushNotifications_m4090347419 (FuseSDK_t1159654649 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_SetupPushNotifications_m4090347419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * V_0 = NULL;
	{
		U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * L_0 = (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 *)il2cpp_codegen_object_new(U3CSetupPushNotificationsU3Ec__Iterator0_t552020816_il2cpp_TypeInfo_var);
		U3CSetupPushNotificationsU3Ec__Iterator0__ctor_m2154447541(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * L_1 = V_0;
		return L_1;
	}
}
// System.Void FuseSDK::_CB_SessionStartReceived(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3479939918;
extern const uint32_t FuseSDK__CB_SessionStartReceived_m1829964418_MetadataUsageId;
extern "C"  void FuseSDK__CB_SessionStartReceived_m1829964418 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_SessionStartReceived_m1829964418_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral3479939918, /*hidden argument*/NULL);
		FuseSDK_OnSessionStartReceived_m2049253662(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_SessionLoginError(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral927850387;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral273263530;
extern const uint32_t FuseSDK__CB_SessionLoginError_m645033598_MetadataUsageId;
extern "C"  void FuseSDK__CB_SessionLoginError_m645033598 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_SessionLoginError_m645033598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral927850387, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		bool L_3 = Int32_TryParse_m695344220(NULL /*static, unused*/, L_2, (&V_0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnSessionLoginError_m1579096951(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral273263530, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_PurchaseVerification(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2537181644;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral3740202311;
extern const uint32_t FuseSDK__CB_PurchaseVerification_m1024716371_MetadataUsageId;
extern "C"  void FuseSDK__CB_PurchaseVerification_m1024716371 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_PurchaseVerification_m1024716371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2537181644, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_1;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)3))))
		{
			goto IL_0050;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_9 = V_0;
		StringU5BU5D_t2956870243* L_10 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		int32_t L_11 = 1;
		StringU5BU5D_t2956870243* L_12 = V_1;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		int32_t L_13 = 2;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnPurchaseVerification_m650257910(NULL /*static, unused*/, L_9, ((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), ((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13))), /*hidden argument*/NULL);
		goto IL_005a;
	}

IL_0050:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3740202311, /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_AdAvailabilityResponse(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral698774633;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral1451252810;
extern const uint32_t FuseSDK__CB_AdAvailabilityResponse_m307949232_MetadataUsageId;
extern "C"  void FuseSDK__CB_AdAvailabilityResponse_m307949232 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_AdAvailabilityResponse_m307949232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	StringU5BU5D_t2956870243* V_2 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral698774633, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_2;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_005a;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		StringU5BU5D_t2956870243* L_9 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		int32_t L_10 = 1;
		bool L_11 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), (&V_1), /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnAdAvailabilityResponse_m3625410166(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1451252810, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_AdWillClose(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1396573316;
extern const uint32_t FuseSDK__CB_AdWillClose_m3403440432_MetadataUsageId;
extern "C"  void FuseSDK__CB_AdWillClose_m3403440432 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_AdWillClose_m3403440432_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral1396573316, /*hidden argument*/NULL);
		FuseSDK_OnAdWillClose_m2256651444(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_AdFailedToDisplay(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral347308680;
extern const uint32_t FuseSDK__CB_AdFailedToDisplay_m1463180332_MetadataUsageId;
extern "C"  void FuseSDK__CB_AdFailedToDisplay_m1463180332 (FuseSDK_t1159654649 * __this, String_t* ____, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_AdFailedToDisplay_m1463180332_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral347308680, /*hidden argument*/NULL);
		FuseSDK_OnAdFailedToDisplay_m559046328(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_AdDidShow(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3943071983;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral1077654478;
extern const uint32_t FuseSDK__CB_AdDidShow_m3677769946_MetadataUsageId;
extern "C"  void FuseSDK__CB_AdDidShow_m3677769946 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_AdDidShow_m3677769946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	StringU5BU5D_t2956870243* V_2 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3943071983, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_2;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_005a;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		StringU5BU5D_t2956870243* L_9 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		int32_t L_10 = 1;
		bool L_11 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), (&V_1), /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnAdDidShow_m262328604(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1077654478, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_RewardedAdCompleted(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2392218382;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK__CB_RewardedAdCompleted_m1665520185_MetadataUsageId;
extern "C"  void FuseSDK__CB_RewardedAdCompleted_m1665520185 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_RewardedAdCompleted_m1665520185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2392218382, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		RewardedInfo_t829972251  L_3;
		memset(&L_3, 0, sizeof(L_3));
		RewardedInfo__ctor_m1550529012(&L_3, L_2, /*hidden argument*/NULL);
		FuseSDK_OnRewardedAdCompleted_m3162148842(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_IAPOfferAccepted(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2812208829;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK__CB_IAPOfferAccepted_m2747711620_MetadataUsageId;
extern "C"  void FuseSDK__CB_IAPOfferAccepted_m2747711620 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_IAPOfferAccepted_m2747711620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2812208829, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		IAPOfferInfo_t3826517745  L_3;
		memset(&L_3, 0, sizeof(L_3));
		IAPOfferInfo__ctor_m3107304778(&L_3, L_2, /*hidden argument*/NULL);
		FuseSDK_OnIAPOfferAccepted_m1326213027(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_VirtualGoodsOfferAccepted(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2912891504;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK__CB_VirtualGoodsOfferAccepted_m369764507_MetadataUsageId;
extern "C"  void FuseSDK__CB_VirtualGoodsOfferAccepted_m369764507 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_VirtualGoodsOfferAccepted_m369764507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2912891504, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		VGOfferInfo_t1385795224  L_3;
		memset(&L_3, 0, sizeof(L_3));
		VGOfferInfo__ctor_m259242847(&L_3, L_2, /*hidden argument*/NULL);
		FuseSDK_OnVirtualGoodsOfferAccepted_m3383985617(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_HandleAdClickWithURL(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1960947940;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK__CB_HandleAdClickWithURL_m3303586275_MetadataUsageId;
extern "C"  void FuseSDK__CB_HandleAdClickWithURL_m3303586275 (FuseSDK_t1159654649 * __this, String_t* ___url, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_HandleAdClickWithURL_m3303586275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___url;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1960947940, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___url;
		FuseSDK_OnAdClickedWithURL_m3254180204(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_NotificationAction(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1132286247;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t FuseSDK__CB_NotificationAction_m377101806_MetadataUsageId;
extern "C"  void FuseSDK__CB_NotificationAction_m377101806 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_NotificationAction_m377101806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1132286247, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		FuseSDK_OnNotificationAction_m222392976(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_NotificationWillClose(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral741135330;
extern const uint32_t FuseSDK__CB_NotificationWillClose_m2947106680_MetadataUsageId;
extern "C"  void FuseSDK__CB_NotificationWillClose_m2947106680 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_NotificationWillClose_m2947106680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral741135330, /*hidden argument*/NULL);
		FuseSDK_OnNotificationWillClose_m1695164140(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_AccountLoginComplete(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3399303187;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral165782048;
extern const uint32_t FuseSDK__CB_AccountLoginComplete_m3315127386_MetadataUsageId;
extern "C"  void FuseSDK__CB_AccountLoginComplete_m3315127386 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_AccountLoginComplete_m3315127386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3399303187, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_1;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_9 = V_0;
		StringU5BU5D_t2956870243* L_10 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		int32_t L_11 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnAccountLoginComplete_m1802209427(NULL /*static, unused*/, L_9, ((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral165782048, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_AccountLoginError(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1802717755;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral534078081;
extern const uint32_t FuseSDK__CB_AccountLoginError_m3569700551_MetadataUsageId;
extern "C"  void FuseSDK__CB_AccountLoginError_m3569700551 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_AccountLoginError_m3569700551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1802717755, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_1;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnAccountLoginError_m3668425490(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), L_11, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral534078081, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_TimeUpdated(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* FuseExtensions_t2761543572_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1829512410;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral699915587;
extern const uint32_t FuseSDK__CB_TimeUpdated_m2956122373_MetadataUsageId;
extern "C"  void FuseSDK__CB_TimeUpdated_m2956122373 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_TimeUpdated_m2956122373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1829512410, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		bool L_3 = Int64_TryParse_m2106581948(NULL /*static, unused*/, L_2, (&V_0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		int64_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseExtensions_t2761543572_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_5 = FuseExtensions_ToDateTime_m670214590(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnTimeUpdated_m2041530777(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_003c;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral699915587, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_FriendAdded(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1567950181;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral1590616343;
extern const uint32_t FuseSDK__CB_FriendAdded_m1285632689_MetadataUsageId;
extern "C"  void FuseSDK__CB_FriendAdded_m1285632689 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_FriendAdded_m1285632689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1567950181, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_1;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnFriendAdded_m4135937256(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), L_11, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1590616343, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_FriendRemoved(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4279574213;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral1813868215;
extern const uint32_t FuseSDK__CB_FriendRemoved_m570788689_MetadataUsageId;
extern "C"  void FuseSDK__CB_FriendRemoved_m570788689 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_FriendRemoved_m570788689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral4279574213, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_1;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnFriendRemoved_m1751502792(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), L_11, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1813868215, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_FriendAccepted(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral417630724;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral3056604208;
extern const uint32_t FuseSDK__CB_FriendAccepted_m782915338_MetadataUsageId;
extern "C"  void FuseSDK__CB_FriendAccepted_m782915338 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_FriendAccepted_m782915338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral417630724, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_1;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnFriendAccepted_m4288202667(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), L_11, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3056604208, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_FriendRejected(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral851881997;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral299665671;
extern const uint32_t FuseSDK__CB_FriendRejected_m260618579_MetadataUsageId;
extern "C"  void FuseSDK__CB_FriendRejected_m260618579 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_FriendRejected_m260618579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral851881997, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_1;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnFriendRejected_m1189618690(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), L_11, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral299665671, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_FriendsMigrated(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3617298329;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral1488526947;
extern const uint32_t FuseSDK__CB_FriendsMigrated_m2973587557_MetadataUsageId;
extern "C"  void FuseSDK__CB_FriendsMigrated_m2973587557 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_FriendsMigrated_m2973587557_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t2956870243* V_1 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3617298329, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_1;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		StringU5BU5D_t2956870243* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnFriendsMigrated_m4030513844(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), L_11, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1488526947, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_FriendsListUpdated(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Friend_t536712157_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1778972809;
extern Il2CppCodeGenString* _stringLiteral48;
extern Il2CppCodeGenString* _stringLiteral89777815;
extern const uint32_t FuseSDK__CB_FriendsListUpdated_m3324444839_MetadataUsageId;
extern "C"  void FuseSDK__CB_FriendsListUpdated_m3324444839 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_FriendsListUpdated_m3324444839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	int32_t V_2 = 0;
	StringU5BU5D_t2956870243* V_3 = NULL;
	Friend_t536712157  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral1778972809, /*hidden argument*/NULL);
		List_1_t1333671126 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__friendsList_19();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<FuseMisc.Friend>::Clear() */, L_0);
		String_t* L_1 = ___param;
		CharU5BU5D_t3416858730* L_2 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)9747));
		NullCheck(L_1);
		StringU5BU5D_t2956870243* L_3 = String_Split_m290179486(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		V_2 = 0;
		goto IL_00b1;
	}

IL_0030:
	{
		StringU5BU5D_t2956870243* L_4 = V_1;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_0 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		String_t* L_7 = V_0;
		CharU5BU5D_t3416858730* L_8 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)9731));
		NullCheck(L_7);
		StringU5BU5D_t2956870243* L_9 = String_Split_m290179486(L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		StringU5BU5D_t2956870243* L_10 = V_3;
		NullCheck(L_10);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))) == ((uint32_t)4))))
		{
			goto IL_009d;
		}
	}
	{
		Initobj (Friend_t536712157_il2cpp_TypeInfo_var, (&V_4));
		StringU5BU5D_t2956870243* L_11 = V_3;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		(&V_4)->set_FuseId_0(((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12))));
		StringU5BU5D_t2956870243* L_13 = V_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		(&V_4)->set_AccountId_1(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14))));
		StringU5BU5D_t2956870243* L_15 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		int32_t L_16 = 2;
		(&V_4)->set_Alias_2(((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16))));
		StringU5BU5D_t2956870243* L_17 = V_3;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		int32_t L_18 = 3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Inequality_m2125462205(NULL /*static, unused*/, ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), _stringLiteral48, /*hidden argument*/NULL);
		(&V_4)->set_Pending_3(L_19);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		List_1_t1333671126 * L_20 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__friendsList_19();
		Friend_t536712157  L_21 = V_4;
		NullCheck(L_20);
		VirtActionInvoker1< Friend_t536712157  >::Invoke(19 /* System.Void System.Collections.Generic.List`1<FuseMisc.Friend>::Add(!0) */, L_20, L_21);
		goto IL_00ad;
	}

IL_009d:
	{
		String_t* L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral89777815, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		int32_t L_24 = V_2;
		V_2 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_25 = V_2;
		StringU5BU5D_t2956870243* L_26 = V_1;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		List_1_t1333671126 * L_27 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__friendsList_19();
		FuseSDK_OnFriendsListUpdated_m502898524(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_FriendsListError(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1307036851;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral2798612224;
extern const uint32_t FuseSDK__CB_FriendsListError_m1257439226_MetadataUsageId;
extern "C"  void FuseSDK__CB_FriendsListError_m1257439226 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_FriendsListError_m1257439226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1307036851, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		bool L_3 = Int32_TryParse_m695344220(NULL /*static, unused*/, L_2, (&V_0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnFriendsListError_m1439699959(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2798612224, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_GameConfigurationReceived(System.String)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2404398022;
extern Il2CppCodeGenString* _stringLiteral1359055014;
extern Il2CppCodeGenString* _stringLiteral3290044583;
extern const uint32_t FuseSDK__CB_GameConfigurationReceived_m2261470638_MetadataUsageId;
extern "C"  void FuseSDK__CB_GameConfigurationReceived_m2261470638 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_GameConfigurationReceived_m2261470638_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	int32_t V_2 = 0;
	StringU5BU5D_t2956870243* V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2404398022, /*hidden argument*/NULL);
		Dictionary_2_t2606186806 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__gameConfig_18();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Clear() */, L_0);
		String_t* L_1 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0094;
		}
	}
	{
		String_t* L_3 = ___param;
		CharU5BU5D_t3416858730* L_4 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)9747));
		NullCheck(L_3);
		StringU5BU5D_t2956870243* L_5 = String_Split_m290179486(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = 0;
		goto IL_0086;
	}

IL_003b:
	{
		StringU5BU5D_t2956870243* L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_0 = ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8)));
		String_t* L_9 = V_0;
		CharU5BU5D_t3416858730* L_10 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)9731));
		NullCheck(L_9);
		StringU5BU5D_t2956870243* L_11 = String_Split_m290179486(L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		StringU5BU5D_t2956870243* L_12 = V_3;
		NullCheck(L_12);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_13 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__gameConfig_18();
		StringU5BU5D_t2956870243* L_14 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		int32_t L_15 = 0;
		StringU5BU5D_t2956870243* L_16 = V_3;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		int32_t L_17 = 1;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_13, ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15))), ((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		goto IL_0082;
	}

IL_0072:
	{
		String_t* L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1359055014, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_0082:
	{
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0086:
	{
		int32_t L_21 = V_2;
		StringU5BU5D_t2956870243* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_003b;
		}
	}
	{
		goto IL_009e;
	}

IL_0094:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3290044583, /*hidden argument*/NULL);
	}

IL_009e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnGameConfigurationReceived_m3123175414(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::_CB_GameDataSetAcknowledged(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2558701978;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral3362590979;
extern const uint32_t FuseSDK__CB_GameDataSetAcknowledged_m4268972741_MetadataUsageId;
extern "C"  void FuseSDK__CB_GameDataSetAcknowledged_m4268972741 (FuseSDK_t1159654649 * __this, String_t* ___requestId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_GameDataSetAcknowledged_m4268972741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___requestId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2558701978, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___requestId;
		bool L_3 = Int32_TryParse_m695344220(NULL /*static, unused*/, L_2, (&V_0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnGameDataSetAcknowledged_m2838243536(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3362590979, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_GameDataError(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3019215132;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral2652350337;
extern const uint32_t FuseSDK__CB_GameDataError_m1651336519_MetadataUsageId;
extern "C"  void FuseSDK__CB_GameDataError_m1651336519 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_GameDataError_m1651336519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	StringU5BU5D_t2956870243* V_2 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3019215132, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___param;
		CharU5BU5D_t3416858730* L_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_2);
		StringU5BU5D_t2956870243* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		StringU5BU5D_t2956870243* L_5 = V_2;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_005a;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		bool L_8 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		StringU5BU5D_t2956870243* L_9 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		int32_t L_10 = 1;
		bool L_11 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), (&V_1), /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnGameDataError_m719612681(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2652350337, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void FuseSDK::_CB_GameDataReceived(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1597524044_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral604249835;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral3745773000;
extern Il2CppCodeGenString* _stringLiteral3342376728;
extern const uint32_t FuseSDK__CB_GameDataReceived_m806817842_MetadataUsageId;
extern "C"  void FuseSDK__CB_GameDataReceived_m806817842 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK__CB_GameDataReceived_m806817842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	StringU5BU5D_t2956870243* V_3 = NULL;
	Dictionary_2_t2606186806 * V_4 = NULL;
	String_t* V_5 = NULL;
	StringU5BU5D_t2956870243* V_6 = NULL;
	int32_t V_7 = 0;
	StringU5BU5D_t2956870243* V_8 = NULL;
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral604249835, L_0, _stringLiteral41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = (-1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_1 = L_2;
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_3;
		String_t* L_4 = ___param;
		CharU5BU5D_t3416858730* L_5 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_4);
		StringU5BU5D_t2956870243* L_6 = String_Split_m290179486(L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		StringU5BU5D_t2956870243* L_7 = V_3;
		NullCheck(L_7);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))) == ((uint32_t)4))))
		{
			goto IL_0064;
		}
	}
	{
		StringU5BU5D_t2956870243* L_8 = V_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		bool L_10 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9))), (&V_0), /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3745773000, /*hidden argument*/NULL);
	}

IL_0057:
	{
		StringU5BU5D_t2956870243* L_11 = V_3;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		int32_t L_12 = 1;
		V_1 = ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		StringU5BU5D_t2956870243* L_13 = V_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		int32_t L_14 = 2;
		V_2 = ((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14)));
		goto IL_006f;
	}

IL_0064:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3745773000, /*hidden argument*/NULL);
		return;
	}

IL_006f:
	{
		Dictionary_2_t2606186806 * L_15 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1597524044(L_15, /*hidden argument*/Dictionary_2__ctor_m1597524044_MethodInfo_var);
		V_4 = L_15;
		StringU5BU5D_t2956870243* L_16 = V_3;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		int32_t L_17 = 3;
		CharU5BU5D_t3416858730* L_18 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)9747));
		NullCheck(((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		StringU5BU5D_t2956870243* L_19 = String_Split_m290179486(((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17))), L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		V_7 = 0;
		goto IL_00e9;
	}

IL_0096:
	{
		StringU5BU5D_t2956870243* L_20 = V_6;
		int32_t L_21 = V_7;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_5 = ((L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22)));
		String_t* L_23 = V_5;
		CharU5BU5D_t3416858730* L_24 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)9731));
		NullCheck(L_23);
		StringU5BU5D_t2956870243* L_25 = String_Split_m290179486(L_23, L_24, /*hidden argument*/NULL);
		V_8 = L_25;
		StringU5BU5D_t2956870243* L_26 = V_8;
		NullCheck(L_26);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_00d2;
		}
	}
	{
		Dictionary_2_t2606186806 * L_27 = V_4;
		StringU5BU5D_t2956870243* L_28 = V_8;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 0);
		int32_t L_29 = 0;
		StringU5BU5D_t2956870243* L_30 = V_8;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 1);
		int32_t L_31 = 1;
		NullCheck(L_27);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_27, ((L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29))), ((L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31))));
		goto IL_00e3;
	}

IL_00d2:
	{
		String_t* L_32 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3342376728, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		int32_t L_34 = V_7;
		V_7 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00e9:
	{
		int32_t L_35 = V_7;
		StringU5BU5D_t2956870243* L_36 = V_6;
		NullCheck(L_36);
		if ((((int32_t)L_35) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_36)->max_length)))))))
		{
			goto IL_0096;
		}
	}
	{
		String_t* L_37 = V_1;
		String_t* L_38 = V_2;
		Dictionary_2_t2606186806 * L_39 = V_4;
		int32_t L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_OnGameDataReceived_m883737616(NULL /*static, unused*/, L_37, L_38, L_39, L_40, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::Internal_StartSession(System.String,System.Boolean,System.Boolean)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDK_Internal_StartSession_m337825724_MetadataUsageId;
extern "C"  void FuseSDK_Internal_StartSession_m337825724 (Il2CppObject * __this /* static, unused */, String_t* ___appID, bool ___registerForPush, bool ___enableCrashDetection, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_Internal_StartSession_m337825724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___appID;
		bool L_1 = ___registerForPush;
		bool L_2 = ___enableCrashDetection;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK__StartSession_m627633534(NULL /*static, unused*/, L_0, L_1, (bool)0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK::StartSession(System.Action`1<System.String>)
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3383924797;
extern const uint32_t FuseSDK_StartSession_m441714530_MetadataUsageId;
extern "C"  void FuseSDK_StartSession_m441714530 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___adClickedWithURLHandler, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDK_StartSession_m441714530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_t1159654649 * L_0 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		Action_1_t1116941607 * L_2 = ___adClickedWithURLHandler;
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->set__adClickedwithURL_17(L_2);
		FuseSDK_t1159654649 * L_3 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		NullCheck(L_3);
		String_t* L_4 = L_3->get_iOSAppID_3();
		FuseSDK_t1159654649 * L_5 = ((FuseSDK_t1159654649_StaticFields*)FuseSDK_t1159654649_il2cpp_TypeInfo_var->static_fields)->get__instance_16();
		NullCheck(L_5);
		bool L_6 = L_5->get_registerForPushNotifications_6();
		FuseSDK__StartSession_m627633534(NULL /*static, unused*/, L_4, L_6, (bool)1, (bool)1, /*hidden argument*/NULL);
		goto IL_0040;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3383924797, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Void FuseSDK/<SetupPushNotifications>c__Iterator0::.ctor()
extern "C"  void U3CSetupPushNotificationsU3Ec__Iterator0__ctor_m2154447541 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object FuseSDK/<SetupPushNotifications>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSetupPushNotificationsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2105890503 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object FuseSDK/<SetupPushNotifications>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSetupPushNotificationsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m984860763 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean FuseSDK/<SetupPushNotifications>c__Iterator0::MoveNext()
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForEndOfFrame_t1917318876_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2012532690;
extern Il2CppCodeGenString* _stringLiteral2788083470;
extern Il2CppCodeGenString* _stringLiteral3107650532;
extern const uint32_t U3CSetupPushNotificationsU3Ec__Iterator0_MoveNext_m2881469703_MetadataUsageId;
extern "C"  bool U3CSetupPushNotificationsU3Ec__Iterator0_MoveNext_m2881469703 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSetupPushNotificationsU3Ec__Iterator0_MoveNext_m2881469703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00bf;
		}
	}
	{
		goto IL_00d0;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2012532690, /*hidden argument*/NULL);
		NotificationServices_RegisterForNotifications_m736063639(NULL /*static, unused*/, 7, (bool)1, /*hidden argument*/NULL);
	}

IL_0032:
	{
		__this->set_U3CtokenU3E__0_0((ByteU5BU5D_t58506160*)NULL);
		__this->set_U3CerrorU3E__1_1((String_t*)NULL);
		ByteU5BU5D_t58506160* L_2 = NotificationServices_get_deviceToken_m1646897969(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtokenU3E__0_0(L_2);
		String_t* L_3 = NotificationServices_get_registrationError_m720449892(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CerrorU3E__1_1(L_3);
		ByteU5BU5D_t58506160* L_4 = __this->get_U3CtokenU3E__0_0();
		if (!L_4)
		{
			goto IL_0083;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, _stringLiteral2788083470, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_5 = __this->get_U3CtokenU3E__0_0();
		ByteU5BU5D_t58506160* L_6 = __this->get_U3CtokenU3E__0_0();
		NullCheck(L_6);
		FuseSDK_Native_RegisterPushToken_m49313396(NULL /*static, unused*/, L_5, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))), /*hidden argument*/NULL);
		goto IL_00c4;
	}

IL_0083:
	{
		String_t* L_7 = __this->get_U3CerrorU3E__1_1();
		if (!L_7)
		{
			goto IL_00a8;
		}
	}
	{
		String_t* L_8 = __this->get_U3CerrorU3E__1_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3107650532, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_FuseLog_m2054806159(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_00c4;
	}

IL_00a8:
	{
		WaitForEndOfFrame_t1917318876 * L_10 = (WaitForEndOfFrame_t1917318876 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1917318876_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m4124201226(L_10, /*hidden argument*/NULL);
		__this->set_U24current_3(L_10);
		__this->set_U24PC_2(1);
		goto IL_00d2;
	}

IL_00bf:
	{
		goto IL_0032;
	}

IL_00c4:
	{
		goto IL_00d0;
	}
	// Dead block : IL_00c9: ldarg.0

IL_00d0:
	{
		return (bool)0;
	}

IL_00d2:
	{
		return (bool)1;
	}
}
// System.Void FuseSDK/<SetupPushNotifications>c__Iterator0::Dispose()
extern "C"  void U3CSetupPushNotificationsU3Ec__Iterator0_Dispose_m148133298 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void FuseSDK/<SetupPushNotifications>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CSetupPushNotificationsU3Ec__Iterator0_Reset_m4095847778_MetadataUsageId;
extern "C"  void U3CSetupPushNotificationsU3Ec__Iterator0_Reset_m4095847778 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSetupPushNotificationsU3Ec__Iterator0_Reset_m4095847778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void FuseSDK_Prime31_IAB::.ctor()
extern "C"  void FuseSDK_Prime31_IAB__ctor_m3074063369 (FuseSDK_Prime31_IAB_t3193163170 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK_Prime31StoreKit::.ctor()
extern "C"  void FuseSDK_Prime31StoreKit__ctor_m1472700895 (FuseSDK_Prime31StoreKit_t67136268 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK_SoomlaIAP::.ctor()
extern "C"  void FuseSDK_SoomlaIAP__ctor_m3887823112 (FuseSDK_SoomlaIAP_t316686275 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK_Unibill_Android::.ctor()
extern "C"  void FuseSDK_Unibill_Android__ctor_m1484979434 (FuseSDK_Unibill_Android_t522151841 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDK_Unibill_iOS::.ctor()
extern "C"  void FuseSDK_Unibill_iOS__ctor_m2034509804 (FuseSDK_Unibill_iOS_t2534778463 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDKEditorSession::.ctor()
extern "C"  void FuseSDKEditorSession__ctor_m3699438763 (FuseSDKEditorSession_t360633328 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FuseSDKEditorSession::.cctor()
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern const MethodInfo* FuseSDKEditorSession_U3CSessionStartReceivedU3Em__0_m888870641_MethodInfo_var;
extern const MethodInfo* FuseSDKEditorSession_U3CSessionLoginErrorU3Em__1_m2226130668_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2736694297_MethodInfo_var;
extern const MethodInfo* FuseSDKEditorSession_U3CGameConfigurationReceivedU3Em__2_m3911220675_MethodInfo_var;
extern const uint32_t FuseSDKEditorSession__cctor_m2531355746_MetadataUsageId;
extern "C"  void FuseSDKEditorSession__cctor_m2531355746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession__cctor_m2531355746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache17_23();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)FuseSDKEditorSession_U3CSessionStartReceivedU3Em__0_m888870641_MethodInfo_var);
		Action_t437523947 * L_2 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_2, NULL, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache17_23(L_2);
	}

IL_0018:
	{
		Action_t437523947 * L_3 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache17_23();
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_SessionStartReceived_0(L_3);
		Action_1_t592684423 * L_4 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache18_24();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)FuseSDKEditorSession_U3CSessionLoginErrorU3Em__1_m2226130668_MethodInfo_var);
		Action_1_t592684423 * L_6 = (Action_1_t592684423 *)il2cpp_codegen_object_new(Action_1_t592684423_il2cpp_TypeInfo_var);
		Action_1__ctor_m2736694297(L_6, NULL, L_5, /*hidden argument*/Action_1__ctor_m2736694297_MethodInfo_var);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache18_24(L_6);
	}

IL_003a:
	{
		Action_1_t592684423 * L_7 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache18_24();
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_SessionLoginError_1(L_7);
		Action_t437523947 * L_8 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache19_25();
		if (L_8)
		{
			goto IL_005c;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)FuseSDKEditorSession_U3CGameConfigurationReceivedU3Em__2_m3911220675_MethodInfo_var);
		Action_t437523947 * L_10 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_10, NULL, L_9, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache19_25(L_10);
	}

IL_005c:
	{
		Action_t437523947 * L_11 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache19_25();
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_GameConfigurationReceived_2(L_11);
		return;
	}
}
// System.Void FuseSDKEditorSession::add_SessionStartReceived(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_SessionStartReceived_m3335941433_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_SessionStartReceived_m3335941433 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_SessionStartReceived_m3335941433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_SessionStartReceived_0();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_SessionStartReceived_0(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_SessionStartReceived(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_SessionStartReceived_m4066686156_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_SessionStartReceived_m4066686156 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_SessionStartReceived_m4066686156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_SessionStartReceived_0();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_SessionStartReceived_0(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_SessionLoginError(System.Action`1<FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_SessionLoginError_m2064850608_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_SessionLoginError_m2064850608 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_SessionLoginError_m2064850608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_SessionLoginError_1();
		Action_1_t592684423 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_SessionLoginError_1(((Action_1_t592684423 *)CastclassSealed(L_2, Action_1_t592684423_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_SessionLoginError(System.Action`1<FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_SessionLoginError_m1026003005_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_SessionLoginError_m1026003005 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_SessionLoginError_m1026003005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_SessionLoginError_1();
		Action_1_t592684423 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_SessionLoginError_1(((Action_1_t592684423 *)CastclassSealed(L_2, Action_1_t592684423_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_GameConfigurationReceived(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_GameConfigurationReceived_m401833325_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_GameConfigurationReceived_m401833325 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_GameConfigurationReceived_m401833325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_GameConfigurationReceived_2();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_GameConfigurationReceived_2(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_GameConfigurationReceived(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_GameConfigurationReceived_m403831226_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_GameConfigurationReceived_m403831226 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_GameConfigurationReceived_m403831226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_GameConfigurationReceived_2();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_GameConfigurationReceived_2(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_AccountLoginComplete(System.Action`2<FuseMisc.AccountType,System.String>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t535153670_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_AccountLoginComplete_m1306866631_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_AccountLoginComplete_m1306866631 (Il2CppObject * __this /* static, unused */, Action_2_t535153670 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_AccountLoginComplete_m1306866631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t535153670 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginComplete_3();
		Action_2_t535153670 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AccountLoginComplete_3(((Action_2_t535153670 *)CastclassSealed(L_2, Action_2_t535153670_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_AccountLoginComplete(System.Action`2<FuseMisc.AccountType,System.String>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t535153670_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_AccountLoginComplete_m252982554_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_AccountLoginComplete_m252982554 (Il2CppObject * __this /* static, unused */, Action_2_t535153670 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_AccountLoginComplete_m252982554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t535153670 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginComplete_3();
		Action_2_t535153670 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AccountLoginComplete_3(((Action_2_t535153670 *)CastclassSealed(L_2, Action_2_t535153670_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_AccountLoginError(System.Action`2<System.String,System.String>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_AccountLoginError_m37286648_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_AccountLoginError_m37286648 (Il2CppObject * __this /* static, unused */, Action_2_t2887221574 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_AccountLoginError_m37286648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2887221574 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginError_4();
		Action_2_t2887221574 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AccountLoginError_4(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_AccountLoginError(System.Action`2<System.String,System.String>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_AccountLoginError_m1309077515_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_AccountLoginError_m1309077515 (Il2CppObject * __this /* static, unused */, Action_2_t2887221574 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_AccountLoginError_m1309077515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2887221574 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AccountLoginError_4();
		Action_2_t2887221574 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AccountLoginError_4(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_NotificationAction(System.Action`1<System.String>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_NotificationAction_m2803538548_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_NotificationAction_m2803538548 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_NotificationAction_m2803538548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t1116941607 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_NotificationAction_5();
		Action_1_t1116941607 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_NotificationAction_5(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_NotificationAction(System.Action`1<System.String>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_NotificationAction_m689429121_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_NotificationAction_m689429121 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_NotificationAction_m689429121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t1116941607 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_NotificationAction_5();
		Action_1_t1116941607 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_NotificationAction_5(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_NotificationWillClose(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_NotificationWillClose_m2263922999_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_NotificationWillClose_m2263922999 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_NotificationWillClose_m2263922999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_NotificationWillClose_6();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_NotificationWillClose_6(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_NotificationWillClose(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_NotificationWillClose_m3442172932_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_NotificationWillClose_m3442172932 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_NotificationWillClose_m3442172932_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_NotificationWillClose_6();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_NotificationWillClose_6(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_FriendAdded(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_FriendAdded_m197112318_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_FriendAdded_m197112318 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_FriendAdded_m197112318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendAdded_7();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendAdded_7(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_FriendAdded(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_FriendAdded_m1485063819_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_FriendAdded_m1485063819 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_FriendAdded_m1485063819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendAdded_7();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendAdded_7(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_FriendRemoved(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_FriendRemoved_m3071654302_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_FriendRemoved_m3071654302 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_FriendRemoved_m3071654302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendRemoved_8();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendRemoved_8(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_FriendRemoved(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_FriendRemoved_m3842465515_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_FriendRemoved_m3842465515 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_FriendRemoved_m3842465515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendRemoved_8();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendRemoved_8(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_FriendAccepted(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_FriendAccepted_m3475540687_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_FriendAccepted_m3475540687 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_FriendAccepted_m3475540687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendAccepted_9();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendAccepted_9(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_FriendAccepted(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_FriendAccepted_m1600884514_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_FriendAccepted_m1600884514 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_FriendAccepted_m1600884514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendAccepted_9();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendAccepted_9(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_FriendRejected(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_FriendRejected_m4074323096_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_FriendRejected_m4074323096 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_FriendRejected_m4074323096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendRejected_10();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendRejected_10(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_FriendRejected(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_FriendRejected_m2199666923_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_FriendRejected_m2199666923 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_FriendRejected_m2199666923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendRejected_10();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendRejected_10(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_FriendsMigrated(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_FriendsMigrated_m2990361522_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_FriendsMigrated_m2990361522 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_FriendsMigrated_m2990361522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendsMigrated_11();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendsMigrated_11(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_FriendsMigrated(System.Action`2<System.String,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t2362964390_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_FriendsMigrated_m710595007_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_FriendsMigrated_m710595007 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_FriendsMigrated_m710595007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t2362964390 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendsMigrated_11();
		Action_2_t2362964390 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendsMigrated_11(((Action_2_t2362964390 *)CastclassSealed(L_2, Action_2_t2362964390_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_FriendsListUpdated(System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1482123831_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_FriendsListUpdated_m3301668488_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_FriendsListUpdated_m3301668488 (Il2CppObject * __this /* static, unused */, Action_1_t1482123831 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_FriendsListUpdated_m3301668488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t1482123831 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendsListUpdated_12();
		Action_1_t1482123831 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendsListUpdated_12(((Action_1_t1482123831 *)CastclassSealed(L_2, Action_1_t1482123831_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_FriendsListUpdated(System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1482123831_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_FriendsListUpdated_m1162357403_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_FriendsListUpdated_m1162357403 (Il2CppObject * __this /* static, unused */, Action_1_t1482123831 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_FriendsListUpdated_m1162357403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t1482123831 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendsListUpdated_12();
		Action_1_t1482123831 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendsListUpdated_12(((Action_1_t1482123831 *)CastclassSealed(L_2, Action_1_t1482123831_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_FriendsListError(System.Action`1<FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_FriendsListError_m970313252_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_FriendsListError_m970313252 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_FriendsListError_m970313252_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendsListError_13();
		Action_1_t592684423 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendsListError_13(((Action_1_t592684423 *)CastclassSealed(L_2, Action_1_t592684423_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_FriendsListError(System.Action`1<FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_FriendsListError_m936802039_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_FriendsListError_m936802039 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_FriendsListError_m936802039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t592684423 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_FriendsListError_13();
		Action_1_t592684423 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_FriendsListError_13(((Action_1_t592684423 *)CastclassSealed(L_2, Action_1_t592684423_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_PurchaseVerification(System.Action`3<System.Int32,System.String,System.String>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_3_t3047918030_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_PurchaseVerification_m3268549484_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_PurchaseVerification_m3268549484 (Il2CppObject * __this /* static, unused */, Action_3_t3047918030 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_PurchaseVerification_m3268549484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_3_t3047918030 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_PurchaseVerification_14();
		Action_3_t3047918030 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_PurchaseVerification_14(((Action_3_t3047918030 *)CastclassSealed(L_2, Action_3_t3047918030_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_PurchaseVerification(System.Action`3<System.Int32,System.String,System.String>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_3_t3047918030_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_PurchaseVerification_m2980156031_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_PurchaseVerification_m2980156031 (Il2CppObject * __this /* static, unused */, Action_3_t3047918030 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_PurchaseVerification_m2980156031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_3_t3047918030 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_PurchaseVerification_14();
		Action_3_t3047918030 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_PurchaseVerification_14(((Action_3_t3047918030 *)CastclassSealed(L_2, Action_3_t3047918030_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_AdAvailabilityResponse(System.Action`2<System.Boolean,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3211021459_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_AdAvailabilityResponse_m1067774092_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_AdAvailabilityResponse_m1067774092 (Il2CppObject * __this /* static, unused */, Action_2_t3211021459 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_AdAvailabilityResponse_m1067774092_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t3211021459 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AdAvailabilityResponse_15();
		Action_2_t3211021459 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AdAvailabilityResponse_15(((Action_2_t3211021459 *)CastclassSealed(L_2, Action_2_t3211021459_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_AdAvailabilityResponse(System.Action`2<System.Boolean,FuseMisc.FuseError>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3211021459_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_AdAvailabilityResponse_m2757106073_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_AdAvailabilityResponse_m2757106073 (Il2CppObject * __this /* static, unused */, Action_2_t3211021459 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_AdAvailabilityResponse_m2757106073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t3211021459 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AdAvailabilityResponse_15();
		Action_2_t3211021459 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AdAvailabilityResponse_15(((Action_2_t3211021459 *)CastclassSealed(L_2, Action_2_t3211021459_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_AdWillClose(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_AdWillClose_m134764527_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_AdWillClose_m134764527 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_AdWillClose_m134764527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AdWillClose_16();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AdWillClose_16(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_AdWillClose(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_AdWillClose_m593932028_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_AdWillClose_m593932028 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_AdWillClose_m593932028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AdWillClose_16();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AdWillClose_16(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_AdFailedToDisplay(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_AdFailedToDisplay_m1519259627_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_AdFailedToDisplay_m1519259627 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_AdFailedToDisplay_m1519259627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AdFailedToDisplay_17();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AdFailedToDisplay_17(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_AdFailedToDisplay(System.Action)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_AdFailedToDisplay_m3846187320_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_AdFailedToDisplay_m3846187320 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_AdFailedToDisplay_m3846187320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_t437523947 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AdFailedToDisplay_17();
		Action_t437523947 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AdFailedToDisplay_17(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_AdDidShow(System.Action`2<System.Int32,System.Int32>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3619260338_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_AdDidShow_m2865039325_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_AdDidShow_m2865039325 (Il2CppObject * __this /* static, unused */, Action_2_t3619260338 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_AdDidShow_m2865039325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t3619260338 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AdDidShow_18();
		Action_2_t3619260338 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AdDidShow_18(((Action_2_t3619260338 *)CastclassSealed(L_2, Action_2_t3619260338_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_AdDidShow(System.Action`2<System.Int32,System.Int32>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3619260338_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_AdDidShow_m2831528112_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_AdDidShow_m2831528112 (Il2CppObject * __this /* static, unused */, Action_2_t3619260338 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_AdDidShow_m2831528112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_2_t3619260338 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_AdDidShow_18();
		Action_2_t3619260338 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_AdDidShow_18(((Action_2_t3619260338 *)CastclassSealed(L_2, Action_2_t3619260338_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_RewardedAdCompletedWithObject(System.Action`1<FuseMisc.RewardedInfo>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t978424956_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_RewardedAdCompletedWithObject_m1252814739_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_RewardedAdCompletedWithObject_m1252814739 (Il2CppObject * __this /* static, unused */, Action_1_t978424956 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_RewardedAdCompletedWithObject_m1252814739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t978424956 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_RewardedAdCompletedWithObject_19();
		Action_1_t978424956 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_RewardedAdCompletedWithObject_19(((Action_1_t978424956 *)CastclassSealed(L_2, Action_1_t978424956_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_RewardedAdCompletedWithObject(System.Action`1<FuseMisc.RewardedInfo>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t978424956_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_RewardedAdCompletedWithObject_m1046418022_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_RewardedAdCompletedWithObject_m1046418022 (Il2CppObject * __this /* static, unused */, Action_1_t978424956 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_RewardedAdCompletedWithObject_m1046418022_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t978424956 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_RewardedAdCompletedWithObject_19();
		Action_1_t978424956 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_RewardedAdCompletedWithObject_19(((Action_1_t978424956 *)CastclassSealed(L_2, Action_1_t978424956_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_IAPOfferAcceptedWithObject(System.Action`1<FuseMisc.IAPOfferInfo>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t3974970450_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_IAPOfferAcceptedWithObject_m2574343878_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_IAPOfferAcceptedWithObject_m2574343878 (Il2CppObject * __this /* static, unused */, Action_1_t3974970450 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_IAPOfferAcceptedWithObject_m2574343878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t3974970450 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_IAPOfferAcceptedWithObject_20();
		Action_1_t3974970450 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_IAPOfferAcceptedWithObject_20(((Action_1_t3974970450 *)CastclassSealed(L_2, Action_1_t3974970450_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_IAPOfferAcceptedWithObject(System.Action`1<FuseMisc.IAPOfferInfo>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t3974970450_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_IAPOfferAcceptedWithObject_m294577363_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_IAPOfferAcceptedWithObject_m294577363 (Il2CppObject * __this /* static, unused */, Action_1_t3974970450 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_IAPOfferAcceptedWithObject_m294577363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t3974970450 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_IAPOfferAcceptedWithObject_20();
		Action_1_t3974970450 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_IAPOfferAcceptedWithObject_20(((Action_1_t3974970450 *)CastclassSealed(L_2, Action_1_t3974970450_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_VirtualGoodsOfferAcceptedWithObject(System.Action`1<FuseMisc.VGOfferInfo>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1534247929_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_VirtualGoodsOfferAcceptedWithObject_m3645421402_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_VirtualGoodsOfferAcceptedWithObject_m3645421402 (Il2CppObject * __this /* static, unused */, Action_1_t1534247929 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_VirtualGoodsOfferAcceptedWithObject_m3645421402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t1534247929 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_VirtualGoodsOfferAcceptedWithObject_21();
		Action_1_t1534247929 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_VirtualGoodsOfferAcceptedWithObject_21(((Action_1_t1534247929 *)CastclassSealed(L_2, Action_1_t1534247929_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_VirtualGoodsOfferAcceptedWithObject(System.Action`1<FuseMisc.VGOfferInfo>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1534247929_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_VirtualGoodsOfferAcceptedWithObject_m1039786087_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_VirtualGoodsOfferAcceptedWithObject_m1039786087 (Il2CppObject * __this /* static, unused */, Action_1_t1534247929 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_VirtualGoodsOfferAcceptedWithObject_m1039786087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t1534247929 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_VirtualGoodsOfferAcceptedWithObject_21();
		Action_1_t1534247929 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_VirtualGoodsOfferAcceptedWithObject_21(((Action_1_t1534247929 *)CastclassSealed(L_2, Action_1_t1534247929_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::add_TimeUpdated(System.Action`1<System.DateTime>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t487486641_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_add_TimeUpdated_m1171609343_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_add_TimeUpdated_m1171609343 (Il2CppObject * __this /* static, unused */, Action_1_t487486641 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_add_TimeUpdated_m1171609343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t487486641 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_TimeUpdated_22();
		Action_1_t487486641 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_TimeUpdated_22(((Action_1_t487486641 *)CastclassSealed(L_2, Action_1_t487486641_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::remove_TimeUpdated(System.Action`1<System.DateTime>)
extern TypeInfo* FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t487486641_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_remove_TimeUpdated_m3301540562_MetadataUsageId;
extern "C"  void FuseSDKEditorSession_remove_TimeUpdated_m3301540562 (Il2CppObject * __this /* static, unused */, Action_1_t487486641 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_remove_TimeUpdated_m3301540562_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var);
		Action_1_t487486641 * L_0 = ((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->get_TimeUpdated_22();
		Action_1_t487486641 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((FuseSDKEditorSession_t360633328_StaticFields*)FuseSDKEditorSession_t360633328_il2cpp_TypeInfo_var->static_fields)->set_TimeUpdated_22(((Action_1_t487486641 *)CastclassSealed(L_2, Action_1_t487486641_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FuseSDKEditorSession::StartSession(System.String)
extern "C"  void FuseSDKEditorSession_StartSession_m2190081557 (Il2CppObject * __this /* static, unused */, String_t* ___gameID, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::RegisterAndroidInAppPurchase(FuseMisc.IAPState,System.String,System.String,System.String,System.DateTime,System.String,System.Double,System.String)
extern "C"  void FuseSDKEditorSession_RegisterAndroidInAppPurchase_m718182888 (Il2CppObject * __this /* static, unused */, int32_t ___purchaseState, String_t* ___purchaseToken, String_t* ___productId, String_t* ___orderId, DateTime_t339033936  ___purchaseTime, String_t* ___developerPayload, double ___price, String_t* ___currency, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::RegisterVirtualGoodsPurchase(System.Int32,System.Int32,System.Int32)
extern "C"  void FuseSDKEditorSession_RegisterVirtualGoodsPurchase_m1845817689 (Il2CppObject * __this /* static, unused */, int32_t ___virtualgoodID, int32_t ___currencyAmount, int32_t ___currencyID, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean FuseSDKEditorSession::IsAdAvailableForZoneID(System.String)
extern "C"  bool FuseSDKEditorSession_IsAdAvailableForZoneID_m2345193409 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean FuseSDKEditorSession::ZoneHasRewarded(System.String)
extern "C"  bool FuseSDKEditorSession_ZoneHasRewarded_m2134350225 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean FuseSDKEditorSession::ZoneHasIAPOffer(System.String)
extern "C"  bool FuseSDKEditorSession_ZoneHasIAPOffer_m4280143867 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean FuseSDKEditorSession::ZoneHasVirtualGoodsOffer(System.String)
extern "C"  bool FuseSDKEditorSession_ZoneHasVirtualGoodsOffer_m3577967474 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// FuseMisc.RewardedInfo FuseSDKEditorSession::GetRewardedInfoForZone(System.String)
extern TypeInfo* RewardedInfo_t829972251_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_GetRewardedInfoForZone_m4264231856_MetadataUsageId;
extern "C"  RewardedInfo_t829972251  FuseSDKEditorSession_GetRewardedInfoForZone_m4264231856 (Il2CppObject * __this /* static, unused */, String_t* ___zonId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_GetRewardedInfoForZone_m4264231856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RewardedInfo_t829972251  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (RewardedInfo_t829972251_il2cpp_TypeInfo_var, (&V_0));
		RewardedInfo_t829972251  L_0 = V_0;
		return L_0;
	}
}
// FuseMisc.VGOfferInfo FuseSDKEditorSession::GetVGOfferInfoForZone(System.String)
extern TypeInfo* VGOfferInfo_t1385795224_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_GetVGOfferInfoForZone_m2596627020_MetadataUsageId;
extern "C"  VGOfferInfo_t1385795224  FuseSDKEditorSession_GetVGOfferInfoForZone_m2596627020 (Il2CppObject * __this /* static, unused */, String_t* ___zonId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_GetVGOfferInfoForZone_m2596627020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VGOfferInfo_t1385795224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (VGOfferInfo_t1385795224_il2cpp_TypeInfo_var, (&V_0));
		VGOfferInfo_t1385795224  L_0 = V_0;
		return L_0;
	}
}
// FuseMisc.IAPOfferInfo FuseSDKEditorSession::GetIAPOfferInfoForZone(System.String)
extern TypeInfo* IAPOfferInfo_t3826517745_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_GetIAPOfferInfoForZone_m3575175644_MetadataUsageId;
extern "C"  IAPOfferInfo_t3826517745  FuseSDKEditorSession_GetIAPOfferInfoForZone_m3575175644 (Il2CppObject * __this /* static, unused */, String_t* ___zonId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_GetIAPOfferInfoForZone_m3575175644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IAPOfferInfo_t3826517745  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (IAPOfferInfo_t3826517745_il2cpp_TypeInfo_var, (&V_0));
		IAPOfferInfo_t3826517745  L_0 = V_0;
		return L_0;
	}
}
// System.Void FuseSDKEditorSession::ShowAdForZoneID(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void FuseSDKEditorSession_ShowAdForZoneID_m887360078 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, Dictionary_2_t2606186806 * ___options, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::PreloadAdForZoneID(System.String)
extern "C"  void FuseSDKEditorSession_PreloadAdForZoneID_m1884682405 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::DisplayMoreGames()
extern "C"  void FuseSDKEditorSession_DisplayMoreGames_m1335209603 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::SetRewardedVideoUserID(System.String)
extern "C"  void FuseSDKEditorSession_SetRewardedVideoUserID_m1964975224 (Il2CppObject * __this /* static, unused */, String_t* ___userID, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::DisplayNotifications()
extern "C"  void FuseSDKEditorSession_DisplayNotifications_m1436042495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean FuseSDKEditorSession::IsNotificationAvailable()
extern "C"  bool FuseSDKEditorSession_IsNotificationAvailable_m2887803049 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void FuseSDKEditorSession::RegisterGender(FuseMisc.Gender)
extern "C"  void FuseSDKEditorSession_RegisterGender_m2171014835 (Il2CppObject * __this /* static, unused */, int32_t ___gender, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::RegisterAge(System.Int32)
extern "C"  void FuseSDKEditorSession_RegisterAge_m4156558838 (Il2CppObject * __this /* static, unused */, int32_t ___age, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::RegisterBirthday(System.Int32,System.Int32,System.Int32)
extern "C"  void FuseSDKEditorSession_RegisterBirthday_m230959658 (Il2CppObject * __this /* static, unused */, int32_t ___year, int32_t ___month, int32_t ___day, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::RegisterLevel(System.Int32)
extern "C"  void FuseSDKEditorSession_RegisterLevel_m3738492539 (Il2CppObject * __this /* static, unused */, int32_t ___level, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean FuseSDKEditorSession::RegisterCurrency(System.Int32,System.Int32)
extern "C"  bool FuseSDKEditorSession_RegisterCurrency_m3073762693 (Il2CppObject * __this /* static, unused */, int32_t ___currencyType, int32_t ___balance, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void FuseSDKEditorSession::RegisterParentalConsent(System.Boolean)
extern "C"  void FuseSDKEditorSession_RegisterParentalConsent_m2633699426 (Il2CppObject * __this /* static, unused */, bool ___consentGranted, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean FuseSDKEditorSession::RegisterCustomEvent(System.Int32,System.String)
extern "C"  bool FuseSDKEditorSession_RegisterCustomEvent_m3359217192 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, String_t* ___value, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean FuseSDKEditorSession::RegisterCustomEvent(System.Int32,System.Int32)
extern "C"  bool FuseSDKEditorSession_RegisterCustomEvent_m1340555627 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, int32_t ___value, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.String FuseSDKEditorSession::GetFuseId()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_GetFuseId_m2464709336_MetadataUsageId;
extern "C"  String_t* FuseSDKEditorSession_GetFuseId_m2464709336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_GetFuseId_m2464709336_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// System.String FuseSDKEditorSession::GetOriginalAccountAlias()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_GetOriginalAccountAlias_m300719568_MetadataUsageId;
extern "C"  String_t* FuseSDKEditorSession_GetOriginalAccountAlias_m300719568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_GetOriginalAccountAlias_m300719568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// System.String FuseSDKEditorSession::GetOriginalAccountId()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_GetOriginalAccountId_m2153417021_MetadataUsageId;
extern "C"  String_t* FuseSDKEditorSession_GetOriginalAccountId_m2153417021 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_GetOriginalAccountId_m2153417021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// FuseMisc.AccountType FuseSDKEditorSession::GetOriginalAccountType()
extern "C"  int32_t FuseSDKEditorSession_GetOriginalAccountType_m3297055152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void FuseSDKEditorSession::GameCenterLogin()
extern "C"  void FuseSDKEditorSession_GameCenterLogin_m2558806027 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::FacebookLogin(System.String,System.String,System.String)
extern "C"  void FuseSDKEditorSession_FacebookLogin_m289228718 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId, String_t* ___name, String_t* ___accessToken, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::TwitterLogin(System.String,System.String)
extern "C"  void FuseSDKEditorSession_TwitterLogin_m784203759 (Il2CppObject * __this /* static, unused */, String_t* ___twitterId, String_t* ___alias, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::FuseLogin(System.String,System.String)
extern "C"  void FuseSDKEditorSession_FuseLogin_m3903761613 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___alias, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::EmailLogin(System.String,System.String)
extern "C"  void FuseSDKEditorSession_EmailLogin_m593963224 (Il2CppObject * __this /* static, unused */, String_t* ___email, String_t* ___alias, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::DeviceLogin(System.String)
extern "C"  void FuseSDKEditorSession_DeviceLogin_m1437345478 (Il2CppObject * __this /* static, unused */, String_t* ___alias, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::GooglePlayLogin(System.String,System.String)
extern "C"  void FuseSDKEditorSession_GooglePlayLogin_m2605133177 (Il2CppObject * __this /* static, unused */, String_t* ___alias, String_t* ___token, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 FuseSDKEditorSession::GamesPlayed()
extern "C"  int32_t FuseSDKEditorSession_GamesPlayed_m1060032239 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (-1);
	}
}
// System.String FuseSDKEditorSession::LibraryVersion()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FuseSDKEditorSession_LibraryVersion_m792722809_MetadataUsageId;
extern "C"  String_t* FuseSDKEditorSession_LibraryVersion_m792722809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FuseSDKEditorSession_LibraryVersion_m792722809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// System.Boolean FuseSDKEditorSession::Connected()
extern "C"  bool FuseSDKEditorSession_Connected_m2272248414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void FuseSDKEditorSession::UTCTimeFromServer()
extern "C"  void FuseSDKEditorSession_UTCTimeFromServer_m1341465415 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::FuseLog(System.String)
extern "C"  void FuseSDKEditorSession_FuseLog_m3059817302 (Il2CppObject * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::EnableData()
extern "C"  void FuseSDKEditorSession_EnableData_m1121304006 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::DisableData()
extern "C"  void FuseSDKEditorSession_DisableData_m844927483 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean FuseSDKEditorSession::DataEnabled()
extern "C"  bool FuseSDKEditorSession_DataEnabled_m4285368076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void FuseSDKEditorSession::UpdateFriendsListFromServer()
extern "C"  void FuseSDKEditorSession_UpdateFriendsListFromServer_m2790017152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.Generic.List`1<FuseMisc.Friend> FuseSDKEditorSession::GetFriendsList()
extern "C"  List_1_t1333671126 * FuseSDKEditorSession_GetFriendsList_m3329490966 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (List_1_t1333671126 *)NULL;
	}
}
// System.Void FuseSDKEditorSession::AddFriend(System.String)
extern "C"  void FuseSDKEditorSession_AddFriend_m3146199898 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::RemoveFriend(System.String)
extern "C"  void FuseSDKEditorSession_RemoveFriend_m3423788007 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::AcceptFriend(System.String)
extern "C"  void FuseSDKEditorSession_AcceptFriend_m481810915 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::RejectFriend(System.String)
extern "C"  void FuseSDKEditorSession_RejectFriend_m3348754348 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::MigrateFriends(System.String)
extern "C"  void FuseSDKEditorSession_MigrateFriends_m1704815551 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::UserPushNotification(System.String,System.String)
extern "C"  void FuseSDKEditorSession_UserPushNotification_m3852551125 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___message, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::FriendsPushNotification(System.String)
extern "C"  void FuseSDKEditorSession_FriendsPushNotification_m2593394111 (Il2CppObject * __this /* static, unused */, String_t* ___message, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String FuseSDKEditorSession::GetGameConfigurationValue(System.String)
extern "C"  String_t* FuseSDKEditorSession_GetGameConfigurationValue_m1369309113 (Il2CppObject * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	{
		return (String_t*)NULL;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> FuseSDKEditorSession::GetGameConfiguration()
extern "C"  Dictionary_2_t2606186806 * FuseSDKEditorSession_GetGameConfiguration_m1537596949 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (Dictionary_2_t2606186806 *)NULL;
	}
}
// System.Void FuseSDKEditorSession::<SessionStartReceived>m__0()
extern "C"  void FuseSDKEditorSession_U3CSessionStartReceivedU3Em__0_m888870641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::<SessionLoginError>m__1(FuseMisc.FuseError)
extern "C"  void FuseSDKEditorSession_U3CSessionLoginErrorU3Em__1_m2226130668 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FuseSDKEditorSession::<GameConfigurationReceived>m__2()
extern "C"  void FuseSDKEditorSession_U3CGameConfigurationReceivedU3Em__2_m3911220675 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void JSONObject::.ctor(JSONObject/Type)
extern TypeInfo* List_1_t2549335872_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1765447871_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1191993266_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m196212956_MetadataUsageId;
extern "C"  void JSONObject__ctor_m196212956 (JSONObject_t1752376903 * __this, int32_t ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m196212956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___t;
		__this->set_type_6(L_0);
		int32_t L_1 = ___t;
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)4)))
		{
			goto IL_0022;
		}
	}
	{
		goto IL_004d;
	}

IL_0022:
	{
		List_1_t2549335872 * L_4 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_4, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		__this->set_list_7(L_4);
		goto IL_004d;
	}

IL_0032:
	{
		List_1_t2549335872 * L_5 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_5, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		__this->set_list_7(L_5);
		List_1_t1765447871 * L_6 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_6, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_keys_8(L_6);
		goto IL_004d;
	}

IL_004d:
	{
		return;
	}
}
// System.Void JSONObject::.ctor(System.Boolean)
extern "C"  void JSONObject__ctor_m867327147 (JSONObject_t1752376903 * __this, bool ___b, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_type_6(5);
		bool L_0 = ___b;
		__this->set_b_11(L_0);
		return;
	}
}
// System.Void JSONObject::.ctor(System.Double)
extern "C"  void JSONObject__ctor_m2085330318 (JSONObject_t1752376903 * __this, double ___d, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_type_6(2);
		double L_0 = ___d;
		__this->set_n_10(L_0);
		return;
	}
}
// System.Void JSONObject::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* List_1_t1765447871_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t2549335872_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t2373214747_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1191993266_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3769107138_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3881634252_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m41001384_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m586818491_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2915303667_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m3806100325_MetadataUsageId;
extern "C"  void JSONObject__ctor_m3806100325 (JSONObject_t1752376903 * __this, Dictionary_2_t2606186806 * ___dic, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m3806100325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2094718104  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t2373214747  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_type_6(3);
		List_1_t1765447871 * L_0 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_0, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_keys_8(L_0);
		List_1_t2549335872 * L_1 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_1, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		__this->set_list_7(L_1);
		Dictionary_2_t2606186806 * L_2 = ___dic;
		NullCheck(L_2);
		Enumerator_t2373214747  L_3 = Dictionary_2_GetEnumerator_m3769107138(L_2, /*hidden argument*/Dictionary_2_GetEnumerator_m3769107138_MethodInfo_var);
		V_1 = L_3;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0060;
		}

IL_002f:
		{
			KeyValuePair_2_t2094718104  L_4 = Enumerator_get_Current_m3881634252((&V_1), /*hidden argument*/Enumerator_get_Current_m3881634252_MethodInfo_var);
			V_0 = L_4;
			List_1_t1765447871 * L_5 = __this->get_keys_8();
			String_t* L_6 = KeyValuePair_2_get_Key_m41001384((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m41001384_MethodInfo_var);
			NullCheck(L_5);
			VirtActionInvoker1< String_t* >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_5, L_6);
			List_1_t2549335872 * L_7 = __this->get_list_7();
			String_t* L_8 = KeyValuePair_2_get_Value_m586818491((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m586818491_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
			JSONObject_t1752376903 * L_9 = JSONObject_CreateStringObject_m3447038224(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			NullCheck(L_7);
			VirtActionInvoker1< JSONObject_t1752376903 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<JSONObject>::Add(!0) */, L_7, L_9);
		}

IL_0060:
		{
			bool L_10 = Enumerator_MoveNext_m2915303667((&V_1), /*hidden argument*/Enumerator_MoveNext_m2915303667_MethodInfo_var);
			if (L_10)
			{
				goto IL_002f;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x7D, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		Enumerator_t2373214747  L_11 = V_1;
		Enumerator_t2373214747  L_12 = L_11;
		Il2CppObject * L_13 = Box(Enumerator_t2373214747_il2cpp_TypeInfo_var, &L_12);
		NullCheck((Il2CppObject *)L_13);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
		IL2CPP_END_FINALLY(113)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		return;
	}
}
// System.Void JSONObject::.ctor(System.Collections.Generic.Dictionary`2<System.String,JSONObject>)
extern TypeInfo* List_1_t1765447871_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t2549335872_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t3157102748_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1191993266_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m4292868431_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1897259569_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m4135590241_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3289288146_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3884290940_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m665743720_MetadataUsageId;
extern "C"  void JSONObject__ctor_m665743720 (JSONObject_t1752376903 * __this, Dictionary_2_t3390074807 * ___dic, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m665743720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2878606105  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3157102748  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_type_6(3);
		List_1_t1765447871 * L_0 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_0, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_keys_8(L_0);
		List_1_t2549335872 * L_1 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_1, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		__this->set_list_7(L_1);
		Dictionary_2_t3390074807 * L_2 = ___dic;
		NullCheck(L_2);
		Enumerator_t3157102748  L_3 = Dictionary_2_GetEnumerator_m4292868431(L_2, /*hidden argument*/Dictionary_2_GetEnumerator_m4292868431_MethodInfo_var);
		V_1 = L_3;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005b;
		}

IL_002f:
		{
			KeyValuePair_2_t2878606105  L_4 = Enumerator_get_Current_m1897259569((&V_1), /*hidden argument*/Enumerator_get_Current_m1897259569_MethodInfo_var);
			V_0 = L_4;
			List_1_t1765447871 * L_5 = __this->get_keys_8();
			String_t* L_6 = KeyValuePair_2_get_Key_m4135590241((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m4135590241_MethodInfo_var);
			NullCheck(L_5);
			VirtActionInvoker1< String_t* >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_5, L_6);
			List_1_t2549335872 * L_7 = __this->get_list_7();
			JSONObject_t1752376903 * L_8 = KeyValuePair_2_get_Value_m3289288146((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3289288146_MethodInfo_var);
			NullCheck(L_7);
			VirtActionInvoker1< JSONObject_t1752376903 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<JSONObject>::Add(!0) */, L_7, L_8);
		}

IL_005b:
		{
			bool L_9 = Enumerator_MoveNext_m3884290940((&V_1), /*hidden argument*/Enumerator_MoveNext_m3884290940_MethodInfo_var);
			if (L_9)
			{
				goto IL_002f;
			}
		}

IL_0067:
		{
			IL2CPP_LEAVE(0x78, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		Enumerator_t3157102748  L_10 = V_1;
		Enumerator_t3157102748  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t3157102748_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x78, IL_0078)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0078:
	{
		return;
	}
}
// System.Void JSONObject::.ctor(JSONObject/AddJSONConents)
extern "C"  void JSONObject__ctor_m302908565 (JSONObject_t1752376903 * __this, AddJSONConents_t3346118721 * ___content, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		AddJSONConents_t3346118721 * L_0 = ___content;
		NullCheck(L_0);
		AddJSONConents_Invoke_m3702840851(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::.ctor(JSONObject[])
extern TypeInfo* List_1_t2549335872_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2405523276_MethodInfo_var;
extern const uint32_t JSONObject__ctor_m3419183851_MetadataUsageId;
extern "C"  void JSONObject__ctor_m3419183851 (JSONObject_t1752376903 * __this, JSONObjectU5BU5D_t1578151102* ___objs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__ctor_m3419183851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_type_6(4);
		JSONObjectU5BU5D_t1578151102* L_0 = ___objs;
		List_1_t2549335872 * L_1 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m2405523276(L_1, (Il2CppObject*)(Il2CppObject*)L_0, /*hidden argument*/List_1__ctor_m2405523276_MethodInfo_var);
		__this->set_list_7(L_1);
		return;
	}
}
// System.Void JSONObject::.ctor()
extern "C"  void JSONObject__ctor_m2883686964 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::.ctor(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  void JSONObject__ctor_m4073880297 (JSONObject_t1752376903 * __this, String_t* ___str, int32_t ___maxDepth, bool ___storeExcessLevels, bool ___strict, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___str;
		int32_t L_1 = ___maxDepth;
		bool L_2 = ___storeExcessLevels;
		bool L_3 = ___strict;
		JSONObject_Parse_m3981292282(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::.cctor()
extern TypeInfo* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* Stopwatch_t2509581612_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238937____U24U24fieldU2D0_0_FieldInfo_var;
extern const uint32_t JSONObject__cctor_m3012853753_MetadataUsageId;
extern "C"  void JSONObject__cctor_m3012853753 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject__cctor_m3012853753_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t3416858730* L_0 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)4));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238937____U24U24fieldU2D0_0_FieldInfo_var), /*hidden argument*/NULL);
		((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->set_WHITESPACE_5(L_0);
		Stopwatch_t2509581612 * L_1 = (Stopwatch_t2509581612 *)il2cpp_codegen_object_new(Stopwatch_t2509581612_il2cpp_TypeInfo_var);
		Stopwatch__ctor_m435104496(L_1, /*hidden argument*/NULL);
		((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->set_printWatch_12(L_1);
		return;
	}
}
// System.Boolean JSONObject::get_isContainer()
extern "C"  bool JSONObject_get_isContainer_m3265258380 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((((int32_t)L_0) == ((int32_t)4)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = __this->get_type_6();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)3))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Int32 JSONObject::get_Count()
extern "C"  int32_t JSONObject_get_Count_m2181271146 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		List_1_t2549335872 * L_0 = __this->get_list_7();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		List_1_t2549335872 * L_1 = __this->get_list_7();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_1);
		return L_2;
	}
}
// System.Single JSONObject::get_f()
extern "C"  float JSONObject_get_f_m3938007459 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_n_10();
		return (((float)((float)L_0)));
	}
}
// JSONObject JSONObject::get_nullJO()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_get_nullJO_m744661531_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_get_nullJO_m744661531 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_nullJO_m744661531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m1311846254(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::get_obj()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_get_obj_m3039048682_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_get_obj_m3039048682 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_obj_m3039048682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m1311846254(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::get_arr()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_get_arr_m3026603732_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_get_arr_m3026603732 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_get_arr_m3026603732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m1311846254(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::StringObject(System.String)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_StringObject_m1278083052_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_StringObject_m1278083052 (Il2CppObject * __this /* static, unused */, String_t* ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_StringObject_m1278083052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_1 = JSONObject_CreateStringObject_m3447038224(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void JSONObject::Absorb(JSONObject)
extern const MethodInfo* List_1_AddRange_m835799688_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1631114187_MethodInfo_var;
extern const uint32_t JSONObject_Absorb_m3127658582_MetadataUsageId;
extern "C"  void JSONObject_Absorb_m3127658582 (JSONObject_t1752376903 * __this, JSONObject_t1752376903 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Absorb_m3127658582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2549335872 * L_0 = __this->get_list_7();
		JSONObject_t1752376903 * L_1 = ___obj;
		NullCheck(L_1);
		List_1_t2549335872 * L_2 = L_1->get_list_7();
		NullCheck(L_0);
		List_1_AddRange_m835799688(L_0, L_2, /*hidden argument*/List_1_AddRange_m835799688_MethodInfo_var);
		List_1_t1765447871 * L_3 = __this->get_keys_8();
		JSONObject_t1752376903 * L_4 = ___obj;
		NullCheck(L_4);
		List_1_t1765447871 * L_5 = L_4->get_keys_8();
		NullCheck(L_3);
		List_1_AddRange_m1631114187(L_3, L_5, /*hidden argument*/List_1_AddRange_m1631114187_MethodInfo_var);
		JSONObject_t1752376903 * L_6 = ___obj;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_str_9();
		__this->set_str_9(L_7);
		JSONObject_t1752376903 * L_8 = ___obj;
		NullCheck(L_8);
		double L_9 = L_8->get_n_10();
		__this->set_n_10(L_9);
		JSONObject_t1752376903 * L_10 = ___obj;
		NullCheck(L_10);
		bool L_11 = L_10->get_b_11();
		__this->set_b_11(L_11);
		JSONObject_t1752376903 * L_12 = ___obj;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_type_6();
		__this->set_type_6(L_13);
		return;
	}
}
// JSONObject JSONObject::Create()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m4162996706_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_Create_m4162996706 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m4162996706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONObject_t1752376903 * L_0 = (JSONObject_t1752376903 *)il2cpp_codegen_object_new(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject__ctor_m2883686964(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// JSONObject JSONObject::Create(JSONObject/Type)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t2549335872_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1765447871_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1191993266_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const uint32_t JSONObject_Create_m1311846254_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_Create_m1311846254 (Il2CppObject * __this /* static, unused */, int32_t ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m1311846254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m4162996706(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = V_0;
		int32_t L_2 = ___t;
		NullCheck(L_1);
		L_1->set_type_6(L_2);
		int32_t L_3 = ___t;
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)4)))
		{
			goto IL_0022;
		}
	}
	{
		goto IL_004d;
	}

IL_0022:
	{
		JSONObject_t1752376903 * L_6 = V_0;
		List_1_t2549335872 * L_7 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_7, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_list_7(L_7);
		goto IL_004d;
	}

IL_0032:
	{
		JSONObject_t1752376903 * L_8 = V_0;
		List_1_t2549335872 * L_9 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_9, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		NullCheck(L_8);
		L_8->set_list_7(L_9);
		JSONObject_t1752376903 * L_10 = V_0;
		List_1_t1765447871 * L_11 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_11, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		NullCheck(L_10);
		L_10->set_keys_8(L_11);
		goto IL_004d;
	}

IL_004d:
	{
		JSONObject_t1752376903 * L_12 = V_0;
		return L_12;
	}
}
// JSONObject JSONObject::Create(System.Boolean)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m4228451289_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_Create_m4228451289 (Il2CppObject * __this /* static, unused */, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m4228451289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m4162996706(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(5);
		JSONObject_t1752376903 * L_2 = V_0;
		bool L_3 = ___val;
		NullCheck(L_2);
		L_2->set_b_11(L_3);
		JSONObject_t1752376903 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::Create(System.Single)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m919295753_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_Create_m919295753 (Il2CppObject * __this /* static, unused */, float ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m919295753_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m4162996706(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(2);
		JSONObject_t1752376903 * L_2 = V_0;
		float L_3 = ___val;
		NullCheck(L_2);
		L_2->set_n_10((((double)((double)L_3))));
		JSONObject_t1752376903 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::Create(System.Int32)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m3627433011_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_Create_m3627433011 (Il2CppObject * __this /* static, unused */, int32_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m3627433011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m4162996706(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(2);
		JSONObject_t1752376903 * L_2 = V_0;
		int32_t L_3 = ___val;
		NullCheck(L_2);
		L_2->set_n_10((((double)((double)L_3))));
		JSONObject_t1752376903 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::CreateStringObject(System.String)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_CreateStringObject_m3447038224_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_CreateStringObject_m3447038224 (Il2CppObject * __this /* static, unused */, String_t* ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_CreateStringObject_m3447038224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m4162996706(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(1);
		JSONObject_t1752376903 * L_2 = V_0;
		String_t* L_3 = ___val;
		NullCheck(L_2);
		L_2->set_str_9(L_3);
		JSONObject_t1752376903 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::CreateBakedObject(System.String)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_CreateBakedObject_m2786003864_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_CreateBakedObject_m2786003864 (Il2CppObject * __this /* static, unused */, String_t* ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_CreateBakedObject_m2786003864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m4162996706(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(6);
		JSONObject_t1752376903 * L_2 = V_0;
		String_t* L_3 = ___val;
		NullCheck(L_2);
		L_2->set_str_9(L_3);
		JSONObject_t1752376903 * L_4 = V_0;
		return L_4;
	}
}
// JSONObject JSONObject::Create(System.String,System.Int32,System.Boolean,System.Boolean)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m3693105303_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_Create_m3693105303 (Il2CppObject * __this /* static, unused */, String_t* ___val, int32_t ___maxDepth, bool ___storeExcessLevels, bool ___strict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m3693105303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m4162996706(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = V_0;
		String_t* L_2 = ___val;
		int32_t L_3 = ___maxDepth;
		bool L_4 = ___storeExcessLevels;
		bool L_5 = ___strict;
		NullCheck(L_1);
		JSONObject_Parse_m3981292282(L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_6 = V_0;
		return L_6;
	}
}
// JSONObject JSONObject::Create(JSONObject/AddJSONConents)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Create_m2851056807_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_Create_m2851056807 (Il2CppObject * __this /* static, unused */, AddJSONConents_t3346118721 * ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m2851056807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m4162996706(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		AddJSONConents_t3346118721 * L_1 = ___content;
		JSONObject_t1752376903 * L_2 = V_0;
		NullCheck(L_1);
		AddJSONConents_Invoke_m3702840851(L_1, L_2, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_3 = V_0;
		return L_3;
	}
}
// JSONObject JSONObject::Create(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1765447871_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t2549335872_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t2373214747_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1191993266_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3769107138_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3881634252_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m41001384_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m586818491_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2915303667_MethodInfo_var;
extern const uint32_t JSONObject_Create_m1646345235_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_Create_m1646345235 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___dic, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Create_m1646345235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t1752376903 * V_0 = NULL;
	KeyValuePair_2_t2094718104  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2373214747  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_0 = JSONObject_Create_m4162996706(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONObject_t1752376903 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_type_6(3);
		JSONObject_t1752376903 * L_2 = V_0;
		List_1_t1765447871 * L_3 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_3, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		NullCheck(L_2);
		L_2->set_keys_8(L_3);
		JSONObject_t1752376903 * L_4 = V_0;
		List_1_t2549335872 * L_5 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_5, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		NullCheck(L_4);
		L_4->set_list_7(L_5);
		Dictionary_2_t2606186806 * L_6 = ___dic;
		NullCheck(L_6);
		Enumerator_t2373214747  L_7 = Dictionary_2_GetEnumerator_m3769107138(L_6, /*hidden argument*/Dictionary_2_GetEnumerator_m3769107138_MethodInfo_var);
		V_2 = L_7;
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0060;
		}

IL_002f:
		{
			KeyValuePair_2_t2094718104  L_8 = Enumerator_get_Current_m3881634252((&V_2), /*hidden argument*/Enumerator_get_Current_m3881634252_MethodInfo_var);
			V_1 = L_8;
			JSONObject_t1752376903 * L_9 = V_0;
			NullCheck(L_9);
			List_1_t1765447871 * L_10 = L_9->get_keys_8();
			String_t* L_11 = KeyValuePair_2_get_Key_m41001384((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m41001384_MethodInfo_var);
			NullCheck(L_10);
			VirtActionInvoker1< String_t* >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_10, L_11);
			JSONObject_t1752376903 * L_12 = V_0;
			NullCheck(L_12);
			List_1_t2549335872 * L_13 = L_12->get_list_7();
			String_t* L_14 = KeyValuePair_2_get_Value_m586818491((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m586818491_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
			JSONObject_t1752376903 * L_15 = JSONObject_CreateStringObject_m3447038224(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			NullCheck(L_13);
			VirtActionInvoker1< JSONObject_t1752376903 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<JSONObject>::Add(!0) */, L_13, L_15);
		}

IL_0060:
		{
			bool L_16 = Enumerator_MoveNext_m2915303667((&V_2), /*hidden argument*/Enumerator_MoveNext_m2915303667_MethodInfo_var);
			if (L_16)
			{
				goto IL_002f;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x7D, FINALLY_0071);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0071;
	}

FINALLY_0071:
	{ // begin finally (depth: 1)
		Enumerator_t2373214747  L_17 = V_2;
		Enumerator_t2373214747  L_18 = L_17;
		Il2CppObject * L_19 = Box(Enumerator_t2373214747_il2cpp_TypeInfo_var, &L_18);
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
		IL2CPP_END_FINALLY(113)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(113)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007d:
	{
		JSONObject_t1752376903 * L_20 = V_0;
		return L_20;
	}
}
// System.Void JSONObject::Parse(System.String,System.Int32,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1765447871_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t2549335872_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1097883944_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t2404802957_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1191993266_MethodInfo_var;
extern const MethodInfo* Array_IndexOf_TisChar_t2778706699_m3290527695_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3804182813;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral1473507608;
extern Il2CppCodeGenString* _stringLiteral3449600172;
extern Il2CppCodeGenString* _stringLiteral33819081;
extern Il2CppCodeGenString* _stringLiteral1966586729;
extern const uint32_t JSONObject_Parse_m3981292282_MetadataUsageId;
extern "C"  void JSONObject_Parse_m3981292282 (JSONObject_t1752376903 * __this, String_t* ___str, int32_t ___maxDepth, bool ___storeExcessLevels, bool ___strict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Parse_m3981292282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	uint16_t V_7 = 0x0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B65_0 = NULL;
	List_1_t2549335872 * G_B65_1 = NULL;
	String_t* G_B64_0 = NULL;
	List_1_t2549335872 * G_B64_1 = NULL;
	int32_t G_B66_0 = 0;
	String_t* G_B66_1 = NULL;
	List_1_t2549335872 * G_B66_2 = NULL;
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_03e7;
		}
	}
	{
		String_t* L_2 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_3 = ((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->get_WHITESPACE_5();
		NullCheck(L_2);
		String_t* L_4 = String_Trim_m1469603388(L_2, L_3, /*hidden argument*/NULL);
		___str = L_4;
		bool L_5 = ___strict;
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_6 = ___str;
		NullCheck(L_6);
		uint16_t L_7 = String_get_Chars_m3015341861(L_6, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)((int32_t)91))))
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_8 = ___str;
		NullCheck(L_8);
		uint16_t L_9 = String_get_Chars_m3015341861(L_8, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)123))))
		{
			goto IL_004d;
		}
	}
	{
		__this->set_type_6(0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral3804182813, /*hidden argument*/NULL);
		return;
	}

IL_004d:
	{
		String_t* L_10 = ___str;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m2979997331(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_03db;
		}
	}
	{
		String_t* L_12 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_13 = String_Compare_m1309590114(NULL /*static, unused*/, L_12, _stringLiteral3569038, (bool)1, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_007d;
		}
	}
	{
		__this->set_type_6(5);
		__this->set_b_11((bool)1);
		goto IL_03d6;
	}

IL_007d:
	{
		String_t* L_14 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_15 = String_Compare_m1309590114(NULL /*static, unused*/, L_14, _stringLiteral97196323, (bool)1, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00a1;
		}
	}
	{
		__this->set_type_6(5);
		__this->set_b_11((bool)0);
		goto IL_03d6;
	}

IL_00a1:
	{
		String_t* L_16 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_17 = String_Compare_m1309590114(NULL /*static, unused*/, L_16, _stringLiteral3392903, (bool)1, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_type_6(0);
		goto IL_03d6;
	}

IL_00be:
	{
		String_t* L_18 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_18, _stringLiteral1473507608, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e9;
		}
	}
	{
		__this->set_type_6(2);
		__this->set_n_10((std::numeric_limits<double>::infinity()));
		goto IL_03d6;
	}

IL_00e9:
	{
		String_t* L_20 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_20, _stringLiteral3449600172, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0114;
		}
	}
	{
		__this->set_type_6(2);
		__this->set_n_10((-std::numeric_limits<double>::infinity()));
		goto IL_03d6;
	}

IL_0114:
	{
		String_t* L_22 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_22, _stringLiteral33819081, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_013f;
		}
	}
	{
		__this->set_type_6(2);
		__this->set_n_10((std::numeric_limits<double>::quiet_NaN()));
		goto IL_03d6;
	}

IL_013f:
	{
		String_t* L_24 = ___str;
		NullCheck(L_24);
		uint16_t L_25 = String_get_Chars_m3015341861(L_24, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_016e;
		}
	}
	{
		__this->set_type_6(1);
		String_t* L_26 = ___str;
		String_t* L_27 = ___str;
		NullCheck(L_27);
		int32_t L_28 = String_get_Length_m2979997331(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_29 = String_Substring_m675079568(L_26, 1, ((int32_t)((int32_t)L_28-(int32_t)2)), /*hidden argument*/NULL);
		__this->set_str_9(L_29);
		goto IL_03d6;
	}

IL_016e:
	{
		V_0 = 1;
		V_1 = 0;
		String_t* L_30 = ___str;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		uint16_t L_32 = String_get_Chars_m3015341861(L_30, L_31, /*hidden argument*/NULL);
		V_7 = L_32;
		uint16_t L_33 = V_7;
		if ((((int32_t)L_33) == ((int32_t)((int32_t)91))))
		{
			goto IL_01b4;
		}
	}
	{
		uint16_t L_34 = V_7;
		if ((((int32_t)L_34) == ((int32_t)((int32_t)123))))
		{
			goto IL_0192;
		}
	}
	{
		goto IL_01cb;
	}

IL_0192:
	{
		__this->set_type_6(3);
		List_1_t1765447871 * L_35 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_35, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_keys_8(L_35);
		List_1_t2549335872 * L_36 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_36, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		__this->set_list_7(L_36);
		goto IL_0201;
	}

IL_01b4:
	{
		__this->set_type_6(4);
		List_1_t2549335872 * L_37 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_37, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		__this->set_list_7(L_37);
		goto IL_0201;
	}

IL_01cb:
	try
	{ // begin try (depth: 1)
		String_t* L_38 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		double L_39 = Convert_ToDouble_m1719184653(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		__this->set_n_10(L_39);
		__this->set_type_6(2);
		goto IL_0200;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2404802957_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01e3;
		throw e;
	}

CATCH_01e3:
	{ // begin catch(System.FormatException)
		__this->set_type_6(0);
		String_t* L_40 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1966586729, L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		goto IL_0200;
	} // end catch (depth: 1)

IL_0200:
	{
		return;
	}

IL_0201:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_42;
		V_3 = (bool)0;
		V_4 = (bool)0;
		V_5 = 0;
		goto IL_03c6;
	}

IL_0214:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_43 = ((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->get_WHITESPACE_5();
		String_t* L_44 = ___str;
		int32_t L_45 = V_1;
		NullCheck(L_44);
		uint16_t L_46 = String_get_Chars_m3015341861(L_44, L_45, /*hidden argument*/NULL);
		int32_t L_47 = Array_IndexOf_TisChar_t2778706699_m3290527695(NULL /*static, unused*/, L_43, L_46, /*hidden argument*/Array_IndexOf_TisChar_t2778706699_m3290527695_MethodInfo_var);
		if ((((int32_t)L_47) <= ((int32_t)(-1))))
		{
			goto IL_0230;
		}
	}
	{
		goto IL_03c6;
	}

IL_0230:
	{
		String_t* L_48 = ___str;
		int32_t L_49 = V_1;
		NullCheck(L_48);
		uint16_t L_50 = String_get_Chars_m3015341861(L_48, L_49, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0247;
		}
	}
	{
		int32_t L_51 = V_1;
		V_1 = ((int32_t)((int32_t)L_51+(int32_t)1));
		goto IL_03c6;
	}

IL_0247:
	{
		String_t* L_52 = ___str;
		int32_t L_53 = V_1;
		NullCheck(L_52);
		uint16_t L_54 = String_get_Chars_m3015341861(L_52, L_53, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_54) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_02a2;
		}
	}
	{
		bool L_55 = V_3;
		if (!L_55)
		{
			goto IL_028b;
		}
	}
	{
		bool L_56 = V_4;
		if (L_56)
		{
			goto IL_0284;
		}
	}
	{
		int32_t L_57 = V_5;
		if (L_57)
		{
			goto IL_0284;
		}
	}
	{
		int32_t L_58 = __this->get_type_6();
		if ((!(((uint32_t)L_58) == ((uint32_t)3))))
		{
			goto IL_0284;
		}
	}
	{
		String_t* L_59 = ___str;
		int32_t L_60 = V_0;
		int32_t L_61 = V_1;
		int32_t L_62 = V_0;
		NullCheck(L_59);
		String_t* L_63 = String_Substring_m675079568(L_59, ((int32_t)((int32_t)L_60+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_61-(int32_t)L_62))-(int32_t)1)), /*hidden argument*/NULL);
		V_2 = L_63;
	}

IL_0284:
	{
		V_3 = (bool)0;
		goto IL_02a2;
	}

IL_028b:
	{
		int32_t L_64 = V_5;
		if (L_64)
		{
			goto IL_02a0;
		}
	}
	{
		int32_t L_65 = __this->get_type_6();
		if ((!(((uint32_t)L_65) == ((uint32_t)3))))
		{
			goto IL_02a0;
		}
	}
	{
		int32_t L_66 = V_1;
		V_0 = L_66;
	}

IL_02a0:
	{
		V_3 = (bool)1;
	}

IL_02a2:
	{
		bool L_67 = V_3;
		if (!L_67)
		{
			goto IL_02ad;
		}
	}
	{
		goto IL_03c6;
	}

IL_02ad:
	{
		int32_t L_68 = __this->get_type_6();
		if ((!(((uint32_t)L_68) == ((uint32_t)3))))
		{
			goto IL_02d5;
		}
	}
	{
		int32_t L_69 = V_5;
		if (L_69)
		{
			goto IL_02d5;
		}
	}
	{
		String_t* L_70 = ___str;
		int32_t L_71 = V_1;
		NullCheck(L_70);
		uint16_t L_72 = String_get_Chars_m3015341861(L_70, L_71, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_72) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_02d5;
		}
	}
	{
		int32_t L_73 = V_1;
		V_0 = ((int32_t)((int32_t)L_73+(int32_t)1));
		V_4 = (bool)1;
	}

IL_02d5:
	{
		String_t* L_74 = ___str;
		int32_t L_75 = V_1;
		NullCheck(L_74);
		uint16_t L_76 = String_get_Chars_m3015341861(L_74, L_75, /*hidden argument*/NULL);
		if ((((int32_t)L_76) == ((int32_t)((int32_t)91))))
		{
			goto IL_02f1;
		}
	}
	{
		String_t* L_77 = ___str;
		int32_t L_78 = V_1;
		NullCheck(L_77);
		uint16_t L_79 = String_get_Chars_m3015341861(L_77, L_78, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_79) == ((uint32_t)((int32_t)123)))))
		{
			goto IL_02fc;
		}
	}

IL_02f1:
	{
		int32_t L_80 = V_5;
		V_5 = ((int32_t)((int32_t)L_80+(int32_t)1));
		goto IL_031e;
	}

IL_02fc:
	{
		String_t* L_81 = ___str;
		int32_t L_82 = V_1;
		NullCheck(L_81);
		uint16_t L_83 = String_get_Chars_m3015341861(L_81, L_82, /*hidden argument*/NULL);
		if ((((int32_t)L_83) == ((int32_t)((int32_t)93))))
		{
			goto IL_0318;
		}
	}
	{
		String_t* L_84 = ___str;
		int32_t L_85 = V_1;
		NullCheck(L_84);
		uint16_t L_86 = String_get_Chars_m3015341861(L_84, L_85, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_86) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_031e;
		}
	}

IL_0318:
	{
		int32_t L_87 = V_5;
		V_5 = ((int32_t)((int32_t)L_87-(int32_t)1));
	}

IL_031e:
	{
		String_t* L_88 = ___str;
		int32_t L_89 = V_1;
		NullCheck(L_88);
		uint16_t L_90 = String_get_Chars_m3015341861(L_88, L_89, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_90) == ((uint32_t)((int32_t)44)))))
		{
			goto IL_0333;
		}
	}
	{
		int32_t L_91 = V_5;
		if (!L_91)
		{
			goto IL_033b;
		}
	}

IL_0333:
	{
		int32_t L_92 = V_5;
		if ((((int32_t)L_92) >= ((int32_t)0)))
		{
			goto IL_03c6;
		}
	}

IL_033b:
	{
		V_4 = (bool)0;
		String_t* L_93 = ___str;
		int32_t L_94 = V_0;
		int32_t L_95 = V_1;
		int32_t L_96 = V_0;
		NullCheck(L_93);
		String_t* L_97 = String_Substring_m675079568(L_93, L_94, ((int32_t)((int32_t)L_95-(int32_t)L_96)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		CharU5BU5D_t3416858730* L_98 = ((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->get_WHITESPACE_5();
		NullCheck(L_97);
		String_t* L_99 = String_Trim_m1469603388(L_97, L_98, /*hidden argument*/NULL);
		V_6 = L_99;
		String_t* L_100 = V_6;
		NullCheck(L_100);
		int32_t L_101 = String_get_Length_m2979997331(L_100, /*hidden argument*/NULL);
		if ((((int32_t)L_101) <= ((int32_t)0)))
		{
			goto IL_03c2;
		}
	}
	{
		int32_t L_102 = __this->get_type_6();
		if ((!(((uint32_t)L_102) == ((uint32_t)3))))
		{
			goto IL_0379;
		}
	}
	{
		List_1_t1765447871 * L_103 = __this->get_keys_8();
		String_t* L_104 = V_2;
		NullCheck(L_103);
		VirtActionInvoker1< String_t* >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_103, L_104);
	}

IL_0379:
	{
		int32_t L_105 = ___maxDepth;
		if ((((int32_t)L_105) == ((int32_t)(-1))))
		{
			goto IL_03aa;
		}
	}
	{
		List_1_t2549335872 * L_106 = __this->get_list_7();
		String_t* L_107 = V_6;
		int32_t L_108 = ___maxDepth;
		G_B64_0 = L_107;
		G_B64_1 = L_106;
		if ((((int32_t)L_108) >= ((int32_t)(-1))))
		{
			G_B65_0 = L_107;
			G_B65_1 = L_106;
			goto IL_0396;
		}
	}
	{
		G_B66_0 = ((int32_t)-2);
		G_B66_1 = G_B64_0;
		G_B66_2 = G_B64_1;
		goto IL_0399;
	}

IL_0396:
	{
		int32_t L_109 = ___maxDepth;
		G_B66_0 = ((int32_t)((int32_t)L_109-(int32_t)1));
		G_B66_1 = G_B65_0;
		G_B66_2 = G_B65_1;
	}

IL_0399:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_110 = JSONObject_Create_m3693105303(NULL /*static, unused*/, G_B66_1, G_B66_0, (bool)0, (bool)0, /*hidden argument*/NULL);
		NullCheck(G_B66_2);
		VirtActionInvoker1< JSONObject_t1752376903 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<JSONObject>::Add(!0) */, G_B66_2, L_110);
		goto IL_03c2;
	}

IL_03aa:
	{
		bool L_111 = ___storeExcessLevels;
		if (!L_111)
		{
			goto IL_03c2;
		}
	}
	{
		List_1_t2549335872 * L_112 = __this->get_list_7();
		String_t* L_113 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_114 = JSONObject_CreateBakedObject_m2786003864(NULL /*static, unused*/, L_113, /*hidden argument*/NULL);
		NullCheck(L_112);
		VirtActionInvoker1< JSONObject_t1752376903 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<JSONObject>::Add(!0) */, L_112, L_114);
	}

IL_03c2:
	{
		int32_t L_115 = V_1;
		V_0 = ((int32_t)((int32_t)L_115+(int32_t)1));
	}

IL_03c6:
	{
		int32_t L_116 = V_1;
		int32_t L_117 = ((int32_t)((int32_t)L_116+(int32_t)1));
		V_1 = L_117;
		String_t* L_118 = ___str;
		NullCheck(L_118);
		int32_t L_119 = String_get_Length_m2979997331(L_118, /*hidden argument*/NULL);
		if ((((int32_t)L_117) < ((int32_t)L_119)))
		{
			goto IL_0214;
		}
	}

IL_03d6:
	{
		goto IL_03e2;
	}

IL_03db:
	{
		__this->set_type_6(0);
	}

IL_03e2:
	{
		goto IL_03ee;
	}

IL_03e7:
	{
		__this->set_type_6(0);
	}

IL_03ee:
	{
		return;
	}
}
// System.Boolean JSONObject::get_IsNumber()
extern "C"  bool JSONObject_get_IsNumber_m253170720 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsNull()
extern "C"  bool JSONObject_get_IsNull_m1971188830 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsString()
extern "C"  bool JSONObject_get_IsString_m3934619368 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsBool()
extern "C"  bool JSONObject_get_IsBool_m1622187265 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)5))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsArray()
extern "C"  bool JSONObject_get_IsArray_m2244069444 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)4))? 1 : 0);
	}
}
// System.Boolean JSONObject::get_IsObject()
extern "C"  bool JSONObject_get_IsObject_m2230105750 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		return (bool)((((int32_t)L_0) == ((int32_t)3))? 1 : 0);
	}
}
// System.Void JSONObject::Add(System.Boolean)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m1644854314_MetadataUsageId;
extern "C"  void JSONObject_Add_m1644854314 (JSONObject_t1752376903 * __this, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m1644854314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_1 = JSONObject_Create_m4228451289(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m1349655052(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(System.Single)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m1805785240_MetadataUsageId;
extern "C"  void JSONObject_Add_m1805785240 (JSONObject_t1752376903 * __this, float ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m1805785240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_1 = JSONObject_Create_m919295753(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m1349655052(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(System.Int32)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m1439272132_MetadataUsageId;
extern "C"  void JSONObject_Add_m1439272132 (JSONObject_t1752376903 * __this, int32_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m1439272132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_1 = JSONObject_Create_m3627433011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m1349655052(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(System.String)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m2124461551_MetadataUsageId;
extern "C"  void JSONObject_Add_m2124461551 (JSONObject_t1752376903 * __this, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m2124461551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_1 = JSONObject_CreateStringObject_m3447038224(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m1349655052(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(JSONObject/AddJSONConents)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Add_m1356697526_MetadataUsageId;
extern "C"  void JSONObject_Add_m1356697526 (JSONObject_t1752376903 * __this, AddJSONConents_t3346118721 * ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m1356697526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AddJSONConents_t3346118721 * L_0 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_1 = JSONObject_Create_m2851056807(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		JSONObject_Add_m1349655052(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::Add(JSONObject)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t2549335872_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1191993266_MethodInfo_var;
extern const uint32_t JSONObject_Add_m1349655052_MetadataUsageId;
extern "C"  void JSONObject_Add_m1349655052 (JSONObject_t1752376903 * __this, JSONObject_t1752376903 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Add_m1349655052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONObject_t1752376903 * L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		bool L_1 = JSONObject_op_Implicit_m2520452346(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_2 = __this->get_type_6();
		if ((((int32_t)L_2) == ((int32_t)4)))
		{
			goto IL_0034;
		}
	}
	{
		__this->set_type_6(4);
		List_1_t2549335872 * L_3 = __this->get_list_7();
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		List_1_t2549335872 * L_4 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_4, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		__this->set_list_7(L_4);
	}

IL_0034:
	{
		List_1_t2549335872 * L_5 = __this->get_list_7();
		JSONObject_t1752376903 * L_6 = ___obj;
		NullCheck(L_5);
		VirtActionInvoker1< JSONObject_t1752376903 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<JSONObject>::Add(!0) */, L_5, L_6);
	}

IL_0040:
	{
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Boolean)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m396898404_MetadataUsageId;
extern "C"  void JSONObject_AddField_m396898404 (JSONObject_t1752376903 * __this, String_t* ___name, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m396898404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		bool L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_2 = JSONObject_Create_m4228451289(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m574028614(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Single)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m2042623262_MetadataUsageId;
extern "C"  void JSONObject_AddField_m2042623262 (JSONObject_t1752376903 * __this, String_t* ___name, float ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m2042623262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		float L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_2 = JSONObject_Create_m919295753(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m574028614(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.Int32)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m3386574718_MetadataUsageId;
extern "C"  void JSONObject_AddField_m3386574718 (JSONObject_t1752376903 * __this, String_t* ___name, int32_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m3386574718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_2 = JSONObject_Create_m3627433011(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m574028614(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,JSONObject/AddJSONConents)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m2376520508_MetadataUsageId;
extern "C"  void JSONObject_AddField_m2376520508 (JSONObject_t1752376903 * __this, String_t* ___name, AddJSONConents_t3346118721 * ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m2376520508_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		AddJSONConents_t3346118721 * L_1 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_2 = JSONObject_Create_m2851056807(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m574028614(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,System.String)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_AddField_m2361299573_MetadataUsageId;
extern "C"  void JSONObject_AddField_m2361299573 (JSONObject_t1752376903 * __this, String_t* ___name, String_t* ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m2361299573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		String_t* L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_2 = JSONObject_CreateStringObject_m3447038224(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_AddField_m574028614(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::AddField(System.String,JSONObject)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1765447871_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t2549335872_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1191993266_MethodInfo_var;
extern const uint32_t JSONObject_AddField_m574028614_MetadataUsageId;
extern "C"  void JSONObject_AddField_m574028614 (JSONObject_t1752376903 * __this, String_t* ___name, JSONObject_t1752376903 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_AddField_m574028614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		JSONObject_t1752376903 * L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		bool L_1 = JSONObject_op_Implicit_m2520452346(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00aa;
		}
	}
	{
		int32_t L_2 = __this->get_type_6();
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0092;
		}
	}
	{
		List_1_t1765447871 * L_3 = __this->get_keys_8();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		List_1_t1765447871 * L_4 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_4, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_keys_8(L_4);
	}

IL_002d:
	{
		int32_t L_5 = __this->get_type_6();
		if ((!(((uint32_t)L_5) == ((uint32_t)4))))
		{
			goto IL_0075;
		}
	}
	{
		V_0 = 0;
		goto IL_005f;
	}

IL_0040:
	{
		List_1_t1765447871 * L_6 = __this->get_keys_8();
		int32_t L_7 = V_0;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_11 = String_Concat_m389863537(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_6, L_11);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_13 = V_0;
		List_1_t2549335872 * L_14 = __this->get_list_7();
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_14);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0040;
		}
	}
	{
		goto IL_008b;
	}

IL_0075:
	{
		List_1_t2549335872 * L_16 = __this->get_list_7();
		if (L_16)
		{
			goto IL_008b;
		}
	}
	{
		List_1_t2549335872 * L_17 = (List_1_t2549335872 *)il2cpp_codegen_object_new(List_1_t2549335872_il2cpp_TypeInfo_var);
		List_1__ctor_m1191993266(L_17, /*hidden argument*/List_1__ctor_m1191993266_MethodInfo_var);
		__this->set_list_7(L_17);
	}

IL_008b:
	{
		__this->set_type_6(3);
	}

IL_0092:
	{
		List_1_t1765447871 * L_18 = __this->get_keys_8();
		String_t* L_19 = ___name;
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_18, L_19);
		List_1_t2549335872 * L_20 = __this->get_list_7();
		JSONObject_t1752376903 * L_21 = ___obj;
		NullCheck(L_20);
		VirtActionInvoker1< JSONObject_t1752376903 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<JSONObject>::Add(!0) */, L_20, L_21);
	}

IL_00aa:
	{
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.Boolean)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m1192398083_MetadataUsageId;
extern "C"  void JSONObject_SetField_m1192398083 (JSONObject_t1752376903 * __this, String_t* ___name, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m1192398083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		bool L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_2 = JSONObject_Create_m4228451289(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m1867415397(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.Single)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m3176663199_MetadataUsageId;
extern "C"  void JSONObject_SetField_m3176663199 (JSONObject_t1752376903 * __this, String_t* ___name, float ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m3176663199_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		float L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_2 = JSONObject_Create_m919295753(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m1867415397(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,System.Int32)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_SetField_m790757341_MetadataUsageId;
extern "C"  void JSONObject_SetField_m790757341 (JSONObject_t1752376903 * __this, String_t* ___name, int32_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_SetField_m790757341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_2 = JSONObject_Create_m3627433011(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONObject_SetField_m1867415397(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::SetField(System.String,JSONObject)
extern "C"  void JSONObject_SetField_m1867415397 (JSONObject_t1752376903 * __this, String_t* ___name, JSONObject_t1752376903 * ___obj, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		bool L_1 = JSONObject_HasField_m1466444350(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t2549335872 * L_2 = __this->get_list_7();
		String_t* L_3 = ___name;
		JSONObject_t1752376903 * L_4 = JSONObject_get_Item_m3957353536(__this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtFuncInvoker1< bool, JSONObject_t1752376903 * >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<JSONObject>::Remove(!0) */, L_2, L_4);
		List_1_t1765447871 * L_5 = __this->get_keys_8();
		String_t* L_6 = ___name;
		NullCheck(L_5);
		VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<System.String>::Remove(!0) */, L_5, L_6);
	}

IL_002c:
	{
		String_t* L_7 = ___name;
		JSONObject_t1752376903 * L_8 = ___obj;
		JSONObject_AddField_m574028614(__this, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::RemoveField(System.String)
extern "C"  void JSONObject_RemoveField_m279507994 (JSONObject_t1752376903 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_keys_8();
		String_t* L_1 = ___name;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(!0) */, L_0, L_1);
		if ((((int32_t)L_2) <= ((int32_t)(-1))))
		{
			goto IL_0036;
		}
	}
	{
		List_1_t2549335872 * L_3 = __this->get_list_7();
		List_1_t1765447871 * L_4 = __this->get_keys_8();
		String_t* L_5 = ___name;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(!0) */, L_4, L_5);
		NullCheck(L_3);
		VirtActionInvoker1< int32_t >::Invoke(27 /* System.Void System.Collections.Generic.List`1<JSONObject>::RemoveAt(System.Int32) */, L_3, L_6);
		List_1_t1765447871 * L_7 = __this->get_keys_8();
		String_t* L_8 = ___name;
		NullCheck(L_7);
		VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<System.String>::Remove(!0) */, L_7, L_8);
	}

IL_0036:
	{
		return;
	}
}
// System.Void JSONObject::GetField(System.Boolean&,System.String,JSONObject/FieldNotFound)
extern "C"  void JSONObject_GetField_m2579965128 (JSONObject_t1752376903 * __this, bool* ___field, String_t* ___name, FieldNotFound_t2014471017 * ___fail, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0034;
		}
	}
	{
		List_1_t1765447871 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name;
		NullCheck(L_1);
		int32_t L_3 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(!0) */, L_1, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		bool* L_5 = ___field;
		List_1_t2549335872 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1752376903 * L_8 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		bool L_9 = L_8->get_b_11();
		*((int8_t*)(L_5)) = (int8_t)L_9;
		return;
	}

IL_0034:
	{
		FieldNotFound_t2014471017 * L_10 = ___fail;
		if (!L_10)
		{
			goto IL_0041;
		}
	}
	{
		FieldNotFound_t2014471017 * L_11 = ___fail;
		String_t* L_12 = ___name;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1255490080(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void JSONObject::GetField(System.Double&,System.String,JSONObject/FieldNotFound)
extern "C"  void JSONObject_GetField_m3652509803 (JSONObject_t1752376903 * __this, double* ___field, String_t* ___name, FieldNotFound_t2014471017 * ___fail, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0034;
		}
	}
	{
		List_1_t1765447871 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name;
		NullCheck(L_1);
		int32_t L_3 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(!0) */, L_1, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		double* L_5 = ___field;
		List_1_t2549335872 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1752376903 * L_8 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		double L_9 = L_8->get_n_10();
		*((double*)(L_5)) = (double)L_9;
		return;
	}

IL_0034:
	{
		FieldNotFound_t2014471017 * L_10 = ___fail;
		if (!L_10)
		{
			goto IL_0041;
		}
	}
	{
		FieldNotFound_t2014471017 * L_11 = ___fail;
		String_t* L_12 = ___name;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1255490080(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void JSONObject::GetField(System.Int32&,System.String,JSONObject/FieldNotFound)
extern "C"  void JSONObject_GetField_m1723647906 (JSONObject_t1752376903 * __this, int32_t* ___field, String_t* ___name, FieldNotFound_t2014471017 * ___fail, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1765447871 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name;
		NullCheck(L_1);
		int32_t L_3 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(!0) */, L_1, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		int32_t* L_5 = ___field;
		List_1_t2549335872 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1752376903 * L_8 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		double L_9 = L_8->get_n_10();
		*((int32_t*)(L_5)) = (int32_t)(((int32_t)((int32_t)L_9)));
		return;
	}

IL_0035:
	{
		FieldNotFound_t2014471017 * L_10 = ___fail;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t2014471017 * L_11 = ___fail;
		String_t* L_12 = ___name;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1255490080(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void JSONObject::GetField(System.UInt32&,System.String,JSONObject/FieldNotFound)
extern "C"  void JSONObject_GetField_m345062947 (JSONObject_t1752376903 * __this, uint32_t* ___field, String_t* ___name, FieldNotFound_t2014471017 * ___fail, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1765447871 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name;
		NullCheck(L_1);
		int32_t L_3 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(!0) */, L_1, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		uint32_t* L_5 = ___field;
		List_1_t2549335872 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1752376903 * L_8 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		double L_9 = L_8->get_n_10();
		*((int32_t*)(L_5)) = (int32_t)(((int32_t)((uint32_t)L_9)));
		return;
	}

IL_0035:
	{
		FieldNotFound_t2014471017 * L_10 = ___fail;
		if (!L_10)
		{
			goto IL_0042;
		}
	}
	{
		FieldNotFound_t2014471017 * L_11 = ___fail;
		String_t* L_12 = ___name;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1255490080(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void JSONObject::GetField(System.String&,System.String,JSONObject/FieldNotFound)
extern "C"  void JSONObject_GetField_m3105145899 (JSONObject_t1752376903 * __this, String_t** ___field, String_t* ___name, FieldNotFound_t2014471017 * ___fail, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0034;
		}
	}
	{
		List_1_t1765447871 * L_1 = __this->get_keys_8();
		String_t* L_2 = ___name;
		NullCheck(L_1);
		int32_t L_3 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(!0) */, L_1, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		String_t** L_5 = ___field;
		List_1_t2549335872 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1752376903 * L_8 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		String_t* L_9 = L_8->get_str_9();
		*((Il2CppObject **)(L_5)) = (Il2CppObject *)L_9;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_5), (Il2CppObject *)L_9);
		return;
	}

IL_0034:
	{
		FieldNotFound_t2014471017 * L_10 = ___fail;
		if (!L_10)
		{
			goto IL_0041;
		}
	}
	{
		FieldNotFound_t2014471017 * L_11 = ___fail;
		String_t* L_12 = ___name;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1255490080(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void JSONObject::GetField(System.String,JSONObject/GetFieldResponse,JSONObject/FieldNotFound)
extern "C"  void JSONObject_GetField_m2967078072 (JSONObject_t1752376903 * __this, String_t* ___name, GetFieldResponse_t1656258501 * ___response, FieldNotFound_t2014471017 * ___fail, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GetFieldResponse_t1656258501 * L_0 = ___response;
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_1 = __this->get_type_6();
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0039;
		}
	}
	{
		List_1_t1765447871 * L_2 = __this->get_keys_8();
		String_t* L_3 = ___name;
		NullCheck(L_2);
		int32_t L_4 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(!0) */, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		GetFieldResponse_t1656258501 * L_6 = ___response;
		List_1_t2549335872 * L_7 = __this->get_list_7();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		JSONObject_t1752376903 * L_9 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_7, L_8);
		NullCheck(L_6);
		GetFieldResponse_Invoke_m194478999(L_6, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0039:
	{
		FieldNotFound_t2014471017 * L_10 = ___fail;
		if (!L_10)
		{
			goto IL_0046;
		}
	}
	{
		FieldNotFound_t2014471017 * L_11 = ___fail;
		String_t* L_12 = ___name;
		NullCheck(L_11);
		FieldNotFound_Invoke_m1255490080(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// JSONObject JSONObject::GetField(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_GetField_m2116260440_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_GetField_m2116260440 (JSONObject_t1752376903 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_GetField_m2116260440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_003b;
	}

IL_0013:
	{
		List_1_t1765447871 * L_1 = __this->get_keys_8();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_1, L_2);
		String_t* L_4 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		List_1_t2549335872 * L_6 = __this->get_list_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JSONObject_t1752376903 * L_8 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_6, L_7);
		return L_8;
	}

IL_0037:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_10 = V_0;
		List_1_t1765447871 * L_11 = __this->get_keys_8();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0013;
		}
	}

IL_004c:
	{
		return (JSONObject_t1752376903 *)NULL;
	}
}
// System.Boolean JSONObject::HasFields(System.String[])
extern "C"  bool JSONObject_HasFields_m57373359 (JSONObject_t1752376903 * __this, StringU5BU5D_t2956870243* ___names, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0020;
	}

IL_0007:
	{
		List_1_t1765447871 * L_0 = __this->get_keys_8();
		StringU5BU5D_t2956870243* L_1 = ___names;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck(L_0);
		bool L_4 = VirtFuncInvoker1< bool, String_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.List`1<System.String>::Contains(!0) */, L_0, ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3))));
		if (L_4)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_6 = V_0;
		StringU5BU5D_t2956870243* L_7 = ___names;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean JSONObject::HasField(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_HasField_m1466444350_MetadataUsageId;
extern "C"  bool JSONObject_HasField_m1466444350 (JSONObject_t1752376903 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_HasField_m1466444350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0041;
		}
	}
	{
		V_0 = 0;
		goto IL_0030;
	}

IL_0013:
	{
		List_1_t1765447871 * L_1 = __this->get_keys_8();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_1, L_2);
		String_t* L_4 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		return (bool)1;
	}

IL_002c:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_7 = V_0;
		List_1_t1765447871 * L_8 = __this->get_keys_8();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_8);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0013;
		}
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Void JSONObject::Clear()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Clear_m289820255_MetadataUsageId;
extern "C"  void JSONObject_Clear_m289820255 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Clear_m289820255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_type_6(0);
		List_1_t2549335872 * L_0 = __this->get_list_7();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		List_1_t2549335872 * L_1 = __this->get_list_7();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<JSONObject>::Clear() */, L_1);
	}

IL_001d:
	{
		List_1_t1765447871 * L_2 = __this->get_keys_8();
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		List_1_t1765447871 * L_3 = __this->get_keys_8();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_3);
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_str_9(L_4);
		__this->set_n_10((0.0));
		__this->set_b_11((bool)0);
		return;
	}
}
// JSONObject JSONObject::Copy()
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Copy_m4158329691_MetadataUsageId;
extern "C"  JSONObject_t1752376903 * JSONObject_Copy_m4158329691 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Copy_m4158329691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = JSONObject_Print_m93291571(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_t1752376903 * L_1 = JSONObject_Create_m3693105303(NULL /*static, unused*/, L_0, ((int32_t)-2), (bool)0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void JSONObject::Merge(JSONObject)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Merge_m683772643_MetadataUsageId;
extern "C"  void JSONObject_Merge_m683772643 (JSONObject_t1752376903 * __this, JSONObject_t1752376903 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Merge_m683772643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONObject_t1752376903 * L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_MergeRecur_m1383547553(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JSONObject::MergeRecur(JSONObject,JSONObject)
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3056781636;
extern const uint32_t JSONObject_MergeRecur_m1383547553_MetadataUsageId;
extern "C"  void JSONObject_MergeRecur_m1383547553 (Il2CppObject * __this /* static, unused */, JSONObject_t1752376903 * ___left, JSONObject_t1752376903 * ___right, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_MergeRecur_m1383547553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		JSONObject_t1752376903 * L_0 = ___left;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_type_6();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		JSONObject_t1752376903 * L_2 = ___left;
		JSONObject_t1752376903 * L_3 = ___right;
		NullCheck(L_2);
		JSONObject_Absorb_m3127658582(L_2, L_3, /*hidden argument*/NULL);
		goto IL_0176;
	}

IL_0017:
	{
		JSONObject_t1752376903 * L_4 = ___left;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_type_6();
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_00d2;
		}
	}
	{
		JSONObject_t1752376903 * L_6 = ___right;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_type_6();
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_00d2;
		}
	}
	{
		V_0 = 0;
		goto IL_00bc;
	}

IL_0036:
	{
		JSONObject_t1752376903 * L_8 = ___right;
		NullCheck(L_8);
		List_1_t1765447871 * L_9 = L_8->get_keys_8();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_9, L_10);
		V_1 = L_11;
		JSONObject_t1752376903 * L_12 = ___right;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		JSONObject_t1752376903 * L_14 = JSONObject_get_Item_m528566355(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_15 = JSONObject_get_isContainer_m3265258380(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008b;
		}
	}
	{
		JSONObject_t1752376903 * L_16 = ___left;
		String_t* L_17 = V_1;
		NullCheck(L_16);
		bool L_18 = JSONObject_HasField_m1466444350(L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0078;
		}
	}
	{
		JSONObject_t1752376903 * L_19 = ___left;
		String_t* L_20 = V_1;
		NullCheck(L_19);
		JSONObject_t1752376903 * L_21 = JSONObject_get_Item_m3957353536(L_19, L_20, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_22 = ___right;
		int32_t L_23 = V_0;
		NullCheck(L_22);
		JSONObject_t1752376903 * L_24 = JSONObject_get_Item_m528566355(L_22, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_MergeRecur_m1383547553(NULL /*static, unused*/, L_21, L_24, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0078:
	{
		JSONObject_t1752376903 * L_25 = ___left;
		String_t* L_26 = V_1;
		JSONObject_t1752376903 * L_27 = ___right;
		int32_t L_28 = V_0;
		NullCheck(L_27);
		JSONObject_t1752376903 * L_29 = JSONObject_get_Item_m528566355(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_25);
		JSONObject_AddField_m574028614(L_25, L_26, L_29, /*hidden argument*/NULL);
	}

IL_0086:
	{
		goto IL_00b8;
	}

IL_008b:
	{
		JSONObject_t1752376903 * L_30 = ___left;
		String_t* L_31 = V_1;
		NullCheck(L_30);
		bool L_32 = JSONObject_HasField_m1466444350(L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00aa;
		}
	}
	{
		JSONObject_t1752376903 * L_33 = ___left;
		String_t* L_34 = V_1;
		JSONObject_t1752376903 * L_35 = ___right;
		int32_t L_36 = V_0;
		NullCheck(L_35);
		JSONObject_t1752376903 * L_37 = JSONObject_get_Item_m528566355(L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_33);
		JSONObject_SetField_m1867415397(L_33, L_34, L_37, /*hidden argument*/NULL);
		goto IL_00b8;
	}

IL_00aa:
	{
		JSONObject_t1752376903 * L_38 = ___left;
		String_t* L_39 = V_1;
		JSONObject_t1752376903 * L_40 = ___right;
		int32_t L_41 = V_0;
		NullCheck(L_40);
		JSONObject_t1752376903 * L_42 = JSONObject_get_Item_m528566355(L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_38);
		JSONObject_AddField_m574028614(L_38, L_39, L_42, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		int32_t L_43 = V_0;
		V_0 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_00bc:
	{
		int32_t L_44 = V_0;
		JSONObject_t1752376903 * L_45 = ___right;
		NullCheck(L_45);
		List_1_t2549335872 * L_46 = L_45->get_list_7();
		NullCheck(L_46);
		int32_t L_47 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_46);
		if ((((int32_t)L_44) < ((int32_t)L_47)))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_0176;
	}

IL_00d2:
	{
		JSONObject_t1752376903 * L_48 = ___left;
		NullCheck(L_48);
		int32_t L_49 = L_48->get_type_6();
		if ((!(((uint32_t)L_49) == ((uint32_t)4))))
		{
			goto IL_0176;
		}
	}
	{
		JSONObject_t1752376903 * L_50 = ___right;
		NullCheck(L_50);
		int32_t L_51 = L_50->get_type_6();
		if ((!(((uint32_t)L_51) == ((uint32_t)4))))
		{
			goto IL_0176;
		}
	}
	{
		JSONObject_t1752376903 * L_52 = ___right;
		NullCheck(L_52);
		int32_t L_53 = JSONObject_get_Count_m2181271146(L_52, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_54 = ___left;
		NullCheck(L_54);
		int32_t L_55 = JSONObject_get_Count_m2181271146(L_54, /*hidden argument*/NULL);
		if ((((int32_t)L_53) <= ((int32_t)L_55)))
		{
			goto IL_0106;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3056781636, /*hidden argument*/NULL);
		return;
	}

IL_0106:
	{
		V_2 = 0;
		goto IL_0165;
	}

IL_010d:
	{
		JSONObject_t1752376903 * L_56 = ___left;
		int32_t L_57 = V_2;
		NullCheck(L_56);
		JSONObject_t1752376903 * L_58 = JSONObject_get_Item_m528566355(L_56, L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		int32_t L_59 = L_58->get_type_6();
		JSONObject_t1752376903 * L_60 = ___right;
		int32_t L_61 = V_2;
		NullCheck(L_60);
		JSONObject_t1752376903 * L_62 = JSONObject_get_Item_m528566355(L_60, L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = L_62->get_type_6();
		if ((!(((uint32_t)L_59) == ((uint32_t)L_63))))
		{
			goto IL_0161;
		}
	}
	{
		JSONObject_t1752376903 * L_64 = ___left;
		int32_t L_65 = V_2;
		NullCheck(L_64);
		JSONObject_t1752376903 * L_66 = JSONObject_get_Item_m528566355(L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		bool L_67 = JSONObject_get_isContainer_m3265258380(L_66, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_0153;
		}
	}
	{
		JSONObject_t1752376903 * L_68 = ___left;
		int32_t L_69 = V_2;
		NullCheck(L_68);
		JSONObject_t1752376903 * L_70 = JSONObject_get_Item_m528566355(L_68, L_69, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_71 = ___right;
		int32_t L_72 = V_2;
		NullCheck(L_71);
		JSONObject_t1752376903 * L_73 = JSONObject_get_Item_m528566355(L_71, L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		JSONObject_MergeRecur_m1383547553(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
		goto IL_0161;
	}

IL_0153:
	{
		JSONObject_t1752376903 * L_74 = ___left;
		int32_t L_75 = V_2;
		JSONObject_t1752376903 * L_76 = ___right;
		int32_t L_77 = V_2;
		NullCheck(L_76);
		JSONObject_t1752376903 * L_78 = JSONObject_get_Item_m528566355(L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_74);
		JSONObject_set_Item_m2540949262(L_74, L_75, L_78, /*hidden argument*/NULL);
	}

IL_0161:
	{
		int32_t L_79 = V_2;
		V_2 = ((int32_t)((int32_t)L_79+(int32_t)1));
	}

IL_0165:
	{
		int32_t L_80 = V_2;
		JSONObject_t1752376903 * L_81 = ___right;
		NullCheck(L_81);
		List_1_t2549335872 * L_82 = L_81->get_list_7();
		NullCheck(L_82);
		int32_t L_83 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_82);
		if ((((int32_t)L_80) < ((int32_t)L_83)))
		{
			goto IL_010d;
		}
	}

IL_0176:
	{
		return;
	}
}
// System.Void JSONObject::Bake()
extern "C"  void JSONObject_Bake_m3018782761 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_6();
		if ((((int32_t)L_0) == ((int32_t)6)))
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_1 = JSONObject_Print_m93291571(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_str_9(L_1);
		__this->set_type_6(6);
	}

IL_0020:
	{
		return;
	}
}
// System.Collections.IEnumerable JSONObject::BakeAsync()
extern TypeInfo* U3CBakeAsyncU3Ec__Iterator1_t726194327_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_BakeAsync_m1036293873_MetadataUsageId;
extern "C"  Il2CppObject * JSONObject_BakeAsync_m1036293873 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_BakeAsync_m1036293873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBakeAsyncU3Ec__Iterator1_t726194327 * V_0 = NULL;
	{
		U3CBakeAsyncU3Ec__Iterator1_t726194327 * L_0 = (U3CBakeAsyncU3Ec__Iterator1_t726194327 *)il2cpp_codegen_object_new(U3CBakeAsyncU3Ec__Iterator1_t726194327_il2cpp_TypeInfo_var);
		U3CBakeAsyncU3Ec__Iterator1__ctor_m3188929708(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBakeAsyncU3Ec__Iterator1_t726194327 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CBakeAsyncU3Ec__Iterator1_t726194327 * L_2 = V_0;
		U3CBakeAsyncU3Ec__Iterator1_t726194327 * L_3 = L_2;
		NullCheck(L_3);
		L_3->set_U24PC_2(((int32_t)-2));
		return L_3;
	}
}
// System.String JSONObject::Print(System.Boolean)
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_Print_m93291571_MetadataUsageId;
extern "C"  String_t* JSONObject_Print_m93291571 (JSONObject_t1752376903 * __this, bool ___pretty, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Print_m93291571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t3822575854 * L_1 = V_0;
		bool L_2 = ___pretty;
		JSONObject_Stringify_m2669947243(__this, 0, L_1, L_2, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = StringBuilder_ToString_m350379841(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<System.String> JSONObject::PrintAsync(System.Boolean)
extern TypeInfo* U3CPrintAsyncU3Ec__Iterator2_t1765946676_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_PrintAsync_m4207404562_MetadataUsageId;
extern "C"  Il2CppObject* JSONObject_PrintAsync_m4207404562 (JSONObject_t1752376903 * __this, bool ___pretty, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_PrintAsync_m4207404562_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPrintAsyncU3Ec__Iterator2_t1765946676 * V_0 = NULL;
	{
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_0 = (U3CPrintAsyncU3Ec__Iterator2_t1765946676 *)il2cpp_codegen_object_new(U3CPrintAsyncU3Ec__Iterator2_t1765946676_il2cpp_TypeInfo_var);
		U3CPrintAsyncU3Ec__Iterator2__ctor_m3005972207(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_1 = V_0;
		bool L_2 = ___pretty;
		NullCheck(L_1);
		L_1->set_pretty_1(L_2);
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_3 = V_0;
		bool L_4 = ___pretty;
		NullCheck(L_3);
		L_3->set_U3CU24U3Epretty_6(L_4);
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_7(__this);
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_6 = V_0;
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_7 = L_6;
		NullCheck(L_7);
		L_7->set_U24PC_4(((int32_t)-2));
		return L_7;
	}
}
// System.Collections.IEnumerable JSONObject::StringifyAsync(System.Int32,System.Text.StringBuilder,System.Boolean)
extern TypeInfo* U3CStringifyAsyncU3Ec__Iterator3_t555783763_il2cpp_TypeInfo_var;
extern const uint32_t JSONObject_StringifyAsync_m3909536131_MetadataUsageId;
extern "C"  Il2CppObject * JSONObject_StringifyAsync_m3909536131 (JSONObject_t1752376903 * __this, int32_t ___depth, StringBuilder_t3822575854 * ___builder, bool ___pretty, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_StringifyAsync_m3909536131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CStringifyAsyncU3Ec__Iterator3_t555783763 * V_0 = NULL;
	{
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_0 = (U3CStringifyAsyncU3Ec__Iterator3_t555783763 *)il2cpp_codegen_object_new(U3CStringifyAsyncU3Ec__Iterator3_t555783763_il2cpp_TypeInfo_var);
		U3CStringifyAsyncU3Ec__Iterator3__ctor_m3303104688(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_1 = V_0;
		int32_t L_2 = ___depth;
		NullCheck(L_1);
		L_1->set_depth_0(L_2);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_3 = V_0;
		StringBuilder_t3822575854 * L_4 = ___builder;
		NullCheck(L_3);
		L_3->set_builder_1(L_4);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_5 = V_0;
		bool L_6 = ___pretty;
		NullCheck(L_5);
		L_5->set_pretty_2(L_6);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_7 = V_0;
		int32_t L_8 = ___depth;
		NullCheck(L_7);
		L_7->set_U3CU24U3Edepth_17(L_8);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_9 = V_0;
		StringBuilder_t3822575854 * L_10 = ___builder;
		NullCheck(L_9);
		L_9->set_U3CU24U3Ebuilder_18(L_10);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_11 = V_0;
		bool L_12 = ___pretty;
		NullCheck(L_11);
		L_11->set_U3CU24U3Epretty_19(L_12);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_13 = V_0;
		NullCheck(L_13);
		L_13->set_U3CU3Ef__this_20(__this);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_14 = V_0;
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_15 = L_14;
		NullCheck(L_15);
		L_15->set_U24PC_15(((int32_t)-2));
		return L_15;
	}
}
// System.Void JSONObject::Stringify(System.Int32,System.Text.StringBuilder,System.Boolean)
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1037112200;
extern Il2CppCodeGenString* _stringLiteral35114044;
extern Il2CppCodeGenString* _stringLiteral1473507608;
extern Il2CppCodeGenString* _stringLiteral3449600172;
extern Il2CppCodeGenString* _stringLiteral33819081;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral9;
extern Il2CppCodeGenString* _stringLiteral1088535422;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral125;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t JSONObject_Stringify_m2669947243_MetadataUsageId;
extern "C"  void JSONObject_Stringify_m2669947243 (JSONObject_t1752376903 * __this, int32_t ___depth, StringBuilder_t3822575854 * ___builder, bool ___pretty, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_Stringify_m2669947243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	JSONObject_t1752376903 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		int32_t L_0 = ___depth;
		int32_t L_1 = L_0;
		___depth = ((int32_t)((int32_t)L_1+(int32_t)1));
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)100))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1037112200, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		int32_t L_2 = __this->get_type_6();
		V_8 = L_2;
		int32_t L_3 = V_8;
		if (L_3 == 0)
		{
			goto IL_039f;
		}
		if (L_3 == 1)
		{
			goto IL_005a;
		}
		if (L_3 == 2)
		{
			goto IL_0071;
		}
		if (L_3 == 3)
		{
			goto IL_00eb;
		}
		if (L_3 == 4)
		{
			goto IL_0232;
		}
		if (L_3 == 5)
		{
			goto IL_0372;
		}
		if (L_3 == 6)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_03b0;
	}

IL_0048:
	{
		StringBuilder_t3822575854 * L_4 = ___builder;
		String_t* L_5 = __this->get_str_9();
		NullCheck(L_4);
		StringBuilder_Append_m3898090075(L_4, L_5, /*hidden argument*/NULL);
		goto IL_03b0;
	}

IL_005a:
	{
		StringBuilder_t3822575854 * L_6 = ___builder;
		String_t* L_7 = __this->get_str_9();
		NullCheck(L_6);
		StringBuilder_AppendFormat_m3723191730(L_6, _stringLiteral35114044, L_7, /*hidden argument*/NULL);
		goto IL_03b0;
	}

IL_0071:
	{
		double L_8 = __this->get_n_10();
		bool L_9 = Double_IsInfinity_m3094155474(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0092;
		}
	}
	{
		StringBuilder_t3822575854 * L_10 = ___builder;
		NullCheck(L_10);
		StringBuilder_Append_m3898090075(L_10, _stringLiteral1473507608, /*hidden argument*/NULL);
		goto IL_00e6;
	}

IL_0092:
	{
		double L_11 = __this->get_n_10();
		bool L_12 = Double_IsNegativeInfinity_m553043933(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00b3;
		}
	}
	{
		StringBuilder_t3822575854 * L_13 = ___builder;
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, _stringLiteral3449600172, /*hidden argument*/NULL);
		goto IL_00e6;
	}

IL_00b3:
	{
		double L_14 = __this->get_n_10();
		bool L_15 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00d4;
		}
	}
	{
		StringBuilder_t3822575854 * L_16 = ___builder;
		NullCheck(L_16);
		StringBuilder_Append_m3898090075(L_16, _stringLiteral33819081, /*hidden argument*/NULL);
		goto IL_00e6;
	}

IL_00d4:
	{
		StringBuilder_t3822575854 * L_17 = ___builder;
		double* L_18 = __this->get_address_of_n_10();
		String_t* L_19 = Double_ToString_m3380246633(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, L_19, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		goto IL_03b0;
	}

IL_00eb:
	{
		StringBuilder_t3822575854 * L_20 = ___builder;
		NullCheck(L_20);
		StringBuilder_Append_m3898090075(L_20, _stringLiteral123, /*hidden argument*/NULL);
		List_1_t2549335872 * L_21 = __this->get_list_7();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_21);
		if ((((int32_t)L_22) <= ((int32_t)0)))
		{
			goto IL_01da;
		}
	}
	{
		bool L_23 = ___pretty;
		if (!L_23)
		{
			goto IL_011a;
		}
	}
	{
		StringBuilder_t3822575854 * L_24 = ___builder;
		NullCheck(L_24);
		StringBuilder_Append_m3898090075(L_24, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_011a:
	{
		V_0 = 0;
		goto IL_01a2;
	}

IL_0121:
	{
		List_1_t1765447871 * L_25 = __this->get_keys_8();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		String_t* L_27 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_25, L_26);
		V_1 = L_27;
		List_1_t2549335872 * L_28 = __this->get_list_7();
		int32_t L_29 = V_0;
		NullCheck(L_28);
		JSONObject_t1752376903 * L_30 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_28, L_29);
		V_2 = L_30;
		JSONObject_t1752376903 * L_31 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		bool L_32 = JSONObject_op_Implicit_m2520452346(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_019e;
		}
	}
	{
		bool L_33 = ___pretty;
		if (!L_33)
		{
			goto IL_016a;
		}
	}
	{
		V_3 = 0;
		goto IL_0163;
	}

IL_0153:
	{
		StringBuilder_t3822575854 * L_34 = ___builder;
		NullCheck(L_34);
		StringBuilder_Append_m3898090075(L_34, _stringLiteral9, /*hidden argument*/NULL);
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_0163:
	{
		int32_t L_36 = V_3;
		int32_t L_37 = ___depth;
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_0153;
		}
	}

IL_016a:
	{
		StringBuilder_t3822575854 * L_38 = ___builder;
		String_t* L_39 = V_1;
		NullCheck(L_38);
		StringBuilder_AppendFormat_m3723191730(L_38, _stringLiteral1088535422, L_39, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_40 = V_2;
		int32_t L_41 = ___depth;
		StringBuilder_t3822575854 * L_42 = ___builder;
		bool L_43 = ___pretty;
		NullCheck(L_40);
		JSONObject_Stringify_m2669947243(L_40, L_41, L_42, L_43, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_44 = ___builder;
		NullCheck(L_44);
		StringBuilder_Append_m3898090075(L_44, _stringLiteral44, /*hidden argument*/NULL);
		bool L_45 = ___pretty;
		if (!L_45)
		{
			goto IL_019e;
		}
	}
	{
		StringBuilder_t3822575854 * L_46 = ___builder;
		NullCheck(L_46);
		StringBuilder_Append_m3898090075(L_46, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_019e:
	{
		int32_t L_47 = V_0;
		V_0 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_01a2:
	{
		int32_t L_48 = V_0;
		List_1_t2549335872 * L_49 = __this->get_list_7();
		NullCheck(L_49);
		int32_t L_50 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_49);
		if ((((int32_t)L_48) < ((int32_t)L_50)))
		{
			goto IL_0121;
		}
	}
	{
		bool L_51 = ___pretty;
		if (!L_51)
		{
			goto IL_01cc;
		}
	}
	{
		StringBuilder_t3822575854 * L_52 = ___builder;
		StringBuilder_t3822575854 * L_53 = L_52;
		NullCheck(L_53);
		int32_t L_54 = StringBuilder_get_Length_m2443133099(L_53, /*hidden argument*/NULL);
		NullCheck(L_53);
		StringBuilder_set_Length_m1952332172(L_53, ((int32_t)((int32_t)L_54-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_01da;
	}

IL_01cc:
	{
		StringBuilder_t3822575854 * L_55 = ___builder;
		StringBuilder_t3822575854 * L_56 = L_55;
		NullCheck(L_56);
		int32_t L_57 = StringBuilder_get_Length_m2443133099(L_56, /*hidden argument*/NULL);
		NullCheck(L_56);
		StringBuilder_set_Length_m1952332172(L_56, ((int32_t)((int32_t)L_57-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_01da:
	{
		bool L_58 = ___pretty;
		if (!L_58)
		{
			goto IL_0221;
		}
	}
	{
		List_1_t2549335872 * L_59 = __this->get_list_7();
		NullCheck(L_59);
		int32_t L_60 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_59);
		if ((((int32_t)L_60) <= ((int32_t)0)))
		{
			goto IL_0221;
		}
	}
	{
		StringBuilder_t3822575854 * L_61 = ___builder;
		NullCheck(L_61);
		StringBuilder_Append_m3898090075(L_61, _stringLiteral10, /*hidden argument*/NULL);
		V_4 = 0;
		goto IL_0217;
	}

IL_0205:
	{
		StringBuilder_t3822575854 * L_62 = ___builder;
		NullCheck(L_62);
		StringBuilder_Append_m3898090075(L_62, _stringLiteral9, /*hidden argument*/NULL);
		int32_t L_63 = V_4;
		V_4 = ((int32_t)((int32_t)L_63+(int32_t)1));
	}

IL_0217:
	{
		int32_t L_64 = V_4;
		int32_t L_65 = ___depth;
		if ((((int32_t)L_64) < ((int32_t)((int32_t)((int32_t)L_65-(int32_t)1)))))
		{
			goto IL_0205;
		}
	}

IL_0221:
	{
		StringBuilder_t3822575854 * L_66 = ___builder;
		NullCheck(L_66);
		StringBuilder_Append_m3898090075(L_66, _stringLiteral125, /*hidden argument*/NULL);
		goto IL_03b0;
	}

IL_0232:
	{
		StringBuilder_t3822575854 * L_67 = ___builder;
		NullCheck(L_67);
		StringBuilder_Append_m3898090075(L_67, _stringLiteral91, /*hidden argument*/NULL);
		List_1_t2549335872 * L_68 = __this->get_list_7();
		NullCheck(L_68);
		int32_t L_69 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_68);
		if ((((int32_t)L_69) <= ((int32_t)0)))
		{
			goto IL_031a;
		}
	}
	{
		bool L_70 = ___pretty;
		if (!L_70)
		{
			goto IL_0261;
		}
	}
	{
		StringBuilder_t3822575854 * L_71 = ___builder;
		NullCheck(L_71);
		StringBuilder_Append_m3898090075(L_71, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_0261:
	{
		V_5 = 0;
		goto IL_02e1;
	}

IL_0269:
	{
		List_1_t2549335872 * L_72 = __this->get_list_7();
		int32_t L_73 = V_5;
		NullCheck(L_72);
		JSONObject_t1752376903 * L_74 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_72, L_73);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		bool L_75 = JSONObject_op_Implicit_m2520452346(NULL /*static, unused*/, L_74, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_02db;
		}
	}
	{
		bool L_76 = ___pretty;
		if (!L_76)
		{
			goto IL_02a8;
		}
	}
	{
		V_6 = 0;
		goto IL_02a0;
	}

IL_028e:
	{
		StringBuilder_t3822575854 * L_77 = ___builder;
		NullCheck(L_77);
		StringBuilder_Append_m3898090075(L_77, _stringLiteral9, /*hidden argument*/NULL);
		int32_t L_78 = V_6;
		V_6 = ((int32_t)((int32_t)L_78+(int32_t)1));
	}

IL_02a0:
	{
		int32_t L_79 = V_6;
		int32_t L_80 = ___depth;
		if ((((int32_t)L_79) < ((int32_t)L_80)))
		{
			goto IL_028e;
		}
	}

IL_02a8:
	{
		List_1_t2549335872 * L_81 = __this->get_list_7();
		int32_t L_82 = V_5;
		NullCheck(L_81);
		JSONObject_t1752376903 * L_83 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_81, L_82);
		int32_t L_84 = ___depth;
		StringBuilder_t3822575854 * L_85 = ___builder;
		bool L_86 = ___pretty;
		NullCheck(L_83);
		JSONObject_Stringify_m2669947243(L_83, L_84, L_85, L_86, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_87 = ___builder;
		NullCheck(L_87);
		StringBuilder_Append_m3898090075(L_87, _stringLiteral44, /*hidden argument*/NULL);
		bool L_88 = ___pretty;
		if (!L_88)
		{
			goto IL_02db;
		}
	}
	{
		StringBuilder_t3822575854 * L_89 = ___builder;
		NullCheck(L_89);
		StringBuilder_Append_m3898090075(L_89, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_02db:
	{
		int32_t L_90 = V_5;
		V_5 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_02e1:
	{
		int32_t L_91 = V_5;
		List_1_t2549335872 * L_92 = __this->get_list_7();
		NullCheck(L_92);
		int32_t L_93 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_92);
		if ((((int32_t)L_91) < ((int32_t)L_93)))
		{
			goto IL_0269;
		}
	}
	{
		bool L_94 = ___pretty;
		if (!L_94)
		{
			goto IL_030c;
		}
	}
	{
		StringBuilder_t3822575854 * L_95 = ___builder;
		StringBuilder_t3822575854 * L_96 = L_95;
		NullCheck(L_96);
		int32_t L_97 = StringBuilder_get_Length_m2443133099(L_96, /*hidden argument*/NULL);
		NullCheck(L_96);
		StringBuilder_set_Length_m1952332172(L_96, ((int32_t)((int32_t)L_97-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_031a;
	}

IL_030c:
	{
		StringBuilder_t3822575854 * L_98 = ___builder;
		StringBuilder_t3822575854 * L_99 = L_98;
		NullCheck(L_99);
		int32_t L_100 = StringBuilder_get_Length_m2443133099(L_99, /*hidden argument*/NULL);
		NullCheck(L_99);
		StringBuilder_set_Length_m1952332172(L_99, ((int32_t)((int32_t)L_100-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_031a:
	{
		bool L_101 = ___pretty;
		if (!L_101)
		{
			goto IL_0361;
		}
	}
	{
		List_1_t2549335872 * L_102 = __this->get_list_7();
		NullCheck(L_102);
		int32_t L_103 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_102);
		if ((((int32_t)L_103) <= ((int32_t)0)))
		{
			goto IL_0361;
		}
	}
	{
		StringBuilder_t3822575854 * L_104 = ___builder;
		NullCheck(L_104);
		StringBuilder_Append_m3898090075(L_104, _stringLiteral10, /*hidden argument*/NULL);
		V_7 = 0;
		goto IL_0357;
	}

IL_0345:
	{
		StringBuilder_t3822575854 * L_105 = ___builder;
		NullCheck(L_105);
		StringBuilder_Append_m3898090075(L_105, _stringLiteral9, /*hidden argument*/NULL);
		int32_t L_106 = V_7;
		V_7 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_0357:
	{
		int32_t L_107 = V_7;
		int32_t L_108 = ___depth;
		if ((((int32_t)L_107) < ((int32_t)((int32_t)((int32_t)L_108-(int32_t)1)))))
		{
			goto IL_0345;
		}
	}

IL_0361:
	{
		StringBuilder_t3822575854 * L_109 = ___builder;
		NullCheck(L_109);
		StringBuilder_Append_m3898090075(L_109, _stringLiteral93, /*hidden argument*/NULL);
		goto IL_03b0;
	}

IL_0372:
	{
		bool L_110 = __this->get_b_11();
		if (!L_110)
		{
			goto IL_038e;
		}
	}
	{
		StringBuilder_t3822575854 * L_111 = ___builder;
		NullCheck(L_111);
		StringBuilder_Append_m3898090075(L_111, _stringLiteral3569038, /*hidden argument*/NULL);
		goto IL_039a;
	}

IL_038e:
	{
		StringBuilder_t3822575854 * L_112 = ___builder;
		NullCheck(L_112);
		StringBuilder_Append_m3898090075(L_112, _stringLiteral97196323, /*hidden argument*/NULL);
	}

IL_039a:
	{
		goto IL_03b0;
	}

IL_039f:
	{
		StringBuilder_t3822575854 * L_113 = ___builder;
		NullCheck(L_113);
		StringBuilder_Append_m3898090075(L_113, _stringLiteral3392903, /*hidden argument*/NULL);
		goto IL_03b0;
	}

IL_03b0:
	{
		return;
	}
}
// JSONObject JSONObject::get_Item(System.Int32)
extern "C"  JSONObject_t1752376903 * JSONObject_get_Item_m528566355 (JSONObject_t1752376903 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		List_1_t2549335872 * L_0 = __this->get_list_7();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_0);
		int32_t L_2 = ___index;
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t2549335872 * L_3 = __this->get_list_7();
		int32_t L_4 = ___index;
		NullCheck(L_3);
		JSONObject_t1752376903 * L_5 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_3, L_4);
		return L_5;
	}

IL_001e:
	{
		return (JSONObject_t1752376903 *)NULL;
	}
}
// System.Void JSONObject::set_Item(System.Int32,JSONObject)
extern "C"  void JSONObject_set_Item_m2540949262 (JSONObject_t1752376903 * __this, int32_t ___index, JSONObject_t1752376903 * ___value, const MethodInfo* method)
{
	{
		List_1_t2549335872 * L_0 = __this->get_list_7();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_0);
		int32_t L_2 = ___index;
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t2549335872 * L_3 = __this->get_list_7();
		int32_t L_4 = ___index;
		JSONObject_t1752376903 * L_5 = ___value;
		NullCheck(L_3);
		VirtActionInvoker2< int32_t, JSONObject_t1752376903 * >::Invoke(29 /* System.Void System.Collections.Generic.List`1<JSONObject>::set_Item(System.Int32,!0) */, L_3, L_4, L_5);
	}

IL_001e:
	{
		return;
	}
}
// JSONObject JSONObject::get_Item(System.String)
extern "C"  JSONObject_t1752376903 * JSONObject_get_Item_m3957353536 (JSONObject_t1752376903 * __this, String_t* ___index, const MethodInfo* method)
{
	{
		String_t* L_0 = ___index;
		JSONObject_t1752376903 * L_1 = JSONObject_GetField_m2116260440(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void JSONObject::set_Item(System.String,JSONObject)
extern "C"  void JSONObject_set_Item_m3795707005 (JSONObject_t1752376903 * __this, String_t* ___index, JSONObject_t1752376903 * ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___index;
		JSONObject_t1752376903 * L_1 = ___value;
		JSONObject_SetField_m1867415397(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String JSONObject::ToString()
extern "C"  String_t* JSONObject_ToString_m4285807199 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = JSONObject_Print_m93291571(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String JSONObject::ToString(System.Boolean)
extern "C"  String_t* JSONObject_ToString_m4116307350 (JSONObject_t1752376903 * __this, bool ___pretty, const MethodInfo* method)
{
	{
		bool L_0 = ___pretty;
		String_t* L_1 = JSONObject_Print_m93291571(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> JSONObject::ToDictionary()
extern TypeInfo* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t534516614_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t211005341_il2cpp_TypeInfo_var;
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1597524044_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral805616270;
extern Il2CppCodeGenString* _stringLiteral566196805;
extern Il2CppCodeGenString* _stringLiteral3597207593;
extern const uint32_t JSONObject_ToDictionary_m660917039_MetadataUsageId;
extern "C"  Dictionary_2_t2606186806 * JSONObject_ToDictionary_m660917039 (JSONObject_t1752376903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_ToDictionary_m660917039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2606186806 * V_0 = NULL;
	int32_t V_1 = 0;
	JSONObject_t1752376903 * V_2 = NULL;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_00ff;
		}
	}
	{
		Dictionary_2_t2606186806 * L_1 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1597524044(L_1, /*hidden argument*/Dictionary_2__ctor_m1597524044_MethodInfo_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_00ec;
	}

IL_0019:
	{
		List_1_t2549335872 * L_2 = __this->get_list_7();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		JSONObject_t1752376903 * L_4 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_2, L_3);
		V_2 = L_4;
		JSONObject_t1752376903 * L_5 = V_2;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_type_6();
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 0)
		{
			goto IL_004e;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 1)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 2)
		{
			goto IL_00c3;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 3)
		{
			goto IL_00c3;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)1)) == 4)
		{
			goto IL_0097;
		}
	}
	{
		goto IL_00c3;
	}

IL_004e:
	{
		Dictionary_2_t2606186806 * L_8 = V_0;
		List_1_t1765447871 * L_9 = __this->get_keys_8();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_9, L_10);
		JSONObject_t1752376903 * L_12 = V_2;
		NullCheck(L_12);
		String_t* L_13 = L_12->get_str_9();
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_8, L_11, L_13);
		goto IL_00e8;
	}

IL_006b:
	{
		Dictionary_2_t2606186806 * L_14 = V_0;
		List_1_t1765447871 * L_15 = __this->get_keys_8();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		String_t* L_17 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_15, L_16);
		JSONObject_t1752376903 * L_18 = V_2;
		NullCheck(L_18);
		double L_19 = L_18->get_n_10();
		double L_20 = L_19;
		Il2CppObject * L_21 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_23 = String_Concat_m389863537(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_14, L_17, L_23);
		goto IL_00e8;
	}

IL_0097:
	{
		Dictionary_2_t2606186806 * L_24 = V_0;
		List_1_t1765447871 * L_25 = __this->get_keys_8();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		String_t* L_27 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_25, L_26);
		JSONObject_t1752376903 * L_28 = V_2;
		NullCheck(L_28);
		bool L_29 = L_28->get_b_11();
		bool L_30 = L_29;
		Il2CppObject * L_31 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_30);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_33 = String_Concat_m389863537(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_24);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_24, L_27, L_33);
		goto IL_00e8;
	}

IL_00c3:
	{
		List_1_t1765447871 * L_34 = __this->get_keys_8();
		int32_t L_35 = V_1;
		NullCheck(L_34);
		String_t* L_36 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_34, L_35);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral805616270, L_36, _stringLiteral566196805, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		goto IL_00e8;
	}

IL_00e8:
	{
		int32_t L_38 = V_1;
		V_1 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00ec:
	{
		int32_t L_39 = V_1;
		List_1_t2549335872 * L_40 = __this->get_list_7();
		NullCheck(L_40);
		int32_t L_41 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_40);
		if ((((int32_t)L_39) < ((int32_t)L_41)))
		{
			goto IL_0019;
		}
	}
	{
		Dictionary_2_t2606186806 * L_42 = V_0;
		return L_42;
	}

IL_00ff:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral3597207593, /*hidden argument*/NULL);
		return (Dictionary_2_t2606186806 *)NULL;
	}
}
// UnityEngine.WWWForm JSONObject::op_Implicit(JSONObject)
extern TypeInfo* WWWForm_t3999572776_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral34;
extern const uint32_t JSONObject_op_Implicit_m1266288287_MetadataUsageId;
extern "C"  WWWForm_t3999572776 * JSONObject_op_Implicit_m1266288287 (Il2CppObject * __this /* static, unused */, JSONObject_t1752376903 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONObject_op_Implicit_m1266288287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WWWForm_t3999572776 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		WWWForm_t3999572776 * L_0 = (WWWForm_t3999572776 *)il2cpp_codegen_object_new(WWWForm_t3999572776_il2cpp_TypeInfo_var);
		WWWForm__ctor_m1417930174(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_007d;
	}

IL_000d:
	{
		int32_t L_1 = V_1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		JSONObject_t1752376903 * L_6 = ___obj;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_type_6();
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0037;
		}
	}
	{
		JSONObject_t1752376903 * L_8 = ___obj;
		NullCheck(L_8);
		List_1_t1765447871 * L_9 = L_8->get_keys_8();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_9, L_10);
		V_2 = L_11;
	}

IL_0037:
	{
		JSONObject_t1752376903 * L_12 = ___obj;
		NullCheck(L_12);
		List_1_t2549335872 * L_13 = L_12->get_list_7();
		int32_t L_14 = V_1;
		NullCheck(L_13);
		JSONObject_t1752376903 * L_15 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_13, L_14);
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String JSONObject::ToString() */, L_15);
		V_3 = L_16;
		JSONObject_t1752376903 * L_17 = ___obj;
		NullCheck(L_17);
		List_1_t2549335872 * L_18 = L_17->get_list_7();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		JSONObject_t1752376903 * L_20 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_18, L_19);
		NullCheck(L_20);
		int32_t L_21 = L_20->get_type_6();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_22 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_22);
		String_t* L_24 = String_Replace_m2915759397(L_22, _stringLiteral34, L_23, /*hidden argument*/NULL);
		V_3 = L_24;
	}

IL_0071:
	{
		WWWForm_t3999572776 * L_25 = V_0;
		String_t* L_26 = V_2;
		String_t* L_27 = V_3;
		NullCheck(L_25);
		WWWForm_AddField_m2890504319(L_25, L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_007d:
	{
		int32_t L_29 = V_1;
		JSONObject_t1752376903 * L_30 = ___obj;
		NullCheck(L_30);
		List_1_t2549335872 * L_31 = L_30->get_list_7();
		NullCheck(L_31);
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_31);
		if ((((int32_t)L_29) < ((int32_t)L_32)))
		{
			goto IL_000d;
		}
	}
	{
		WWWForm_t3999572776 * L_33 = V_0;
		return L_33;
	}
}
// System.Boolean JSONObject::op_Implicit(JSONObject)
extern "C"  bool JSONObject_op_Implicit_m2520452346 (Il2CppObject * __this /* static, unused */, JSONObject_t1752376903 * ___o, const MethodInfo* method)
{
	{
		JSONObject_t1752376903 * L_0 = ___o;
		return (bool)((((int32_t)((((Il2CppObject*)(JSONObject_t1752376903 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void JSONObject/<BakeAsync>c__Iterator1::.ctor()
extern "C"  void U3CBakeAsyncU3Ec__Iterator1__ctor_m3188929708 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object JSONObject/<BakeAsync>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2863517552 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object JSONObject/<BakeAsync>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m315405060 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Collections.IEnumerator JSONObject/<BakeAsync>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m2377416901 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CBakeAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1949287823(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<BakeAsync>c__Iterator1::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern TypeInfo* U3CBakeAsyncU3Ec__Iterator1_t726194327_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1949287823_MetadataUsageId;
extern "C"  Il2CppObject* U3CBakeAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1949287823 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1949287823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBakeAsyncU3Ec__Iterator1_t726194327 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_2();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CBakeAsyncU3Ec__Iterator1_t726194327 * L_2 = (U3CBakeAsyncU3Ec__Iterator1_t726194327 *)il2cpp_codegen_object_new(U3CBakeAsyncU3Ec__Iterator1_t726194327_il2cpp_TypeInfo_var);
		U3CBakeAsyncU3Ec__Iterator1__ctor_m3188929708(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CBakeAsyncU3Ec__Iterator1_t726194327 * L_3 = V_0;
		JSONObject_t1752376903 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_4(L_4);
		U3CBakeAsyncU3Ec__Iterator1_t726194327 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean JSONObject/<BakeAsync>c__Iterator1::MoveNext()
extern TypeInfo* IEnumerable_1_t3840643258_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t2451595350_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator1_MoveNext_m827557040_MetadataUsageId;
extern "C"  bool U3CBakeAsyncU3Ec__Iterator1_MoveNext_m827557040 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator1_MoveNext_m827557040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_00ec;
	}

IL_0023:
	{
		JSONObject_t1752376903 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_type_6();
		if ((((int32_t)L_3) == ((int32_t)6)))
		{
			goto IL_00e5;
		}
	}
	{
		JSONObject_t1752376903 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_4);
		Il2CppObject* L_5 = JSONObject_PrintAsync_m4207404562(L_4, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t3840643258_il2cpp_TypeInfo_var, L_5);
		__this->set_U3CU24s_13U3E__0_0(L_6);
		V_0 = ((int32_t)-3);
	}

IL_004e:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_7 = V_0;
			if (((int32_t)((int32_t)L_7-(int32_t)1)) == 0)
			{
				goto IL_0095;
			}
		}

IL_005a:
		{
			goto IL_00ab;
		}

IL_005f:
		{
			Il2CppObject* L_8 = __this->get_U3CU24s_13U3E__0_0();
			NullCheck(L_8);
			String_t* L_9 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t2451595350_il2cpp_TypeInfo_var, L_8);
			__this->set_U3CsU3E__1_1(L_9);
			String_t* L_10 = __this->get_U3CsU3E__1_1();
			if (L_10)
			{
				goto IL_009a;
			}
		}

IL_007b:
		{
			String_t* L_11 = __this->get_U3CsU3E__1_1();
			__this->set_U24current_3(L_11);
			__this->set_U24PC_2(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xEE, FINALLY_00c0);
		}

IL_0095:
		{
			goto IL_00ab;
		}

IL_009a:
		{
			JSONObject_t1752376903 * L_12 = __this->get_U3CU3Ef__this_4();
			String_t* L_13 = __this->get_U3CsU3E__1_1();
			NullCheck(L_12);
			L_12->set_str_9(L_13);
		}

IL_00ab:
		{
			Il2CppObject* L_14 = __this->get_U3CU24s_13U3E__0_0();
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_005f;
			}
		}

IL_00bb:
		{
			IL2CPP_LEAVE(0xD9, FINALLY_00c0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c0;
	}

FINALLY_00c0:
	{ // begin finally (depth: 1)
		{
			bool L_16 = V_1;
			if (!L_16)
			{
				goto IL_00c4;
			}
		}

IL_00c3:
		{
			IL2CPP_END_FINALLY(192)
		}

IL_00c4:
		{
			Il2CppObject* L_17 = __this->get_U3CU24s_13U3E__0_0();
			if (L_17)
			{
				goto IL_00cd;
			}
		}

IL_00cc:
		{
			IL2CPP_END_FINALLY(192)
		}

IL_00cd:
		{
			Il2CppObject* L_18 = __this->get_U3CU24s_13U3E__0_0();
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_18);
			IL2CPP_END_FINALLY(192)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(192)
	{
		IL2CPP_JUMP_TBL(0xEE, IL_00ee)
		IL2CPP_JUMP_TBL(0xD9, IL_00d9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00d9:
	{
		JSONObject_t1752376903 * L_19 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_19);
		L_19->set_type_6(6);
	}

IL_00e5:
	{
		__this->set_U24PC_2((-1));
	}

IL_00ec:
	{
		return (bool)0;
	}

IL_00ee:
	{
		return (bool)1;
	}
	// Dead block : IL_00f0: ldloc.2
}
// System.Void JSONObject/<BakeAsync>c__Iterator1::Dispose()
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator1_Dispose_m2148050409_MetadataUsageId;
extern "C"  void U3CBakeAsyncU3Ec__Iterator1_Dispose_m2148050409 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator1_Dispose_m2148050409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U3CU24s_13U3E__0_0();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = __this->get_U3CU24s_13U3E__0_0();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void JSONObject/<BakeAsync>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CBakeAsyncU3Ec__Iterator1_Reset_m835362649_MetadataUsageId;
extern "C"  void U3CBakeAsyncU3Ec__Iterator1_Reset_m835362649 (U3CBakeAsyncU3Ec__Iterator1_t726194327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBakeAsyncU3Ec__Iterator1_Reset_m835362649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void JSONObject/<PrintAsync>c__Iterator2::.ctor()
extern "C"  void U3CPrintAsyncU3Ec__Iterator2__ctor_m3005972207 (U3CPrintAsyncU3Ec__Iterator2_t1765946676 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String JSONObject/<PrintAsync>c__Iterator2::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CPrintAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m1139538819 (U3CPrintAsyncU3Ec__Iterator2_t1765946676 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object JSONObject/<PrintAsync>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2443552727 (U3CPrintAsyncU3Ec__Iterator2_t1765946676 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator JSONObject/<PrintAsync>c__Iterator2::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator2_System_Collections_IEnumerable_GetEnumerator_m1035866514 (U3CPrintAsyncU3Ec__Iterator2_t1765946676 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CPrintAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3909426492(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.String> JSONObject/<PrintAsync>c__Iterator2::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern TypeInfo* U3CPrintAsyncU3Ec__Iterator2_t1765946676_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3909426492_MetadataUsageId;
extern "C"  Il2CppObject* U3CPrintAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3909426492 (U3CPrintAsyncU3Ec__Iterator2_t1765946676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3909426492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPrintAsyncU3Ec__Iterator2_t1765946676 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_2 = (U3CPrintAsyncU3Ec__Iterator2_t1765946676 *)il2cpp_codegen_object_new(U3CPrintAsyncU3Ec__Iterator2_t1765946676_il2cpp_TypeInfo_var);
		U3CPrintAsyncU3Ec__Iterator2__ctor_m3005972207(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_3 = V_0;
		JSONObject_t1752376903 * L_4 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_7(L_4);
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_5 = V_0;
		bool L_6 = __this->get_U3CU24U3Epretty_6();
		NullCheck(L_5);
		L_5->set_pretty_1(L_6);
		U3CPrintAsyncU3Ec__Iterator2_t1765946676 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean JSONObject/<PrintAsync>c__Iterator2::MoveNext()
extern TypeInfo* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator2_MoveNext_m1153384101_MetadataUsageId;
extern "C"  bool U3CPrintAsyncU3Ec__Iterator2_MoveNext_m1153384101 (U3CPrintAsyncU3Ec__Iterator2_t1765946676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator2_MoveNext_m1153384101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_006c;
		}
		if (L_1 == 2)
		{
			goto IL_00f5;
		}
	}
	{
		goto IL_00fc;
	}

IL_0027:
	{
		StringBuilder_t3822575854 * L_2 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_2, /*hidden argument*/NULL);
		__this->set_U3CbuilderU3E__0_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		Stopwatch_t2509581612 * L_3 = ((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->get_printWatch_12();
		NullCheck(L_3);
		Stopwatch_Reset_m2376504733(L_3, /*hidden argument*/NULL);
		Stopwatch_t2509581612 * L_4 = ((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->get_printWatch_12();
		NullCheck(L_4);
		Stopwatch_Start_m3677209584(L_4, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_5 = __this->get_U3CU3Ef__this_7();
		StringBuilder_t3822575854 * L_6 = __this->get_U3CbuilderU3E__0_0();
		bool L_7 = __this->get_pretty_1();
		NullCheck(L_5);
		Il2CppObject * L_8 = JSONObject_StringifyAsync_m3909536131(L_5, 0, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Il2CppObject * L_9 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_8);
		__this->set_U3CU24s_14U3E__1_2(L_9);
		V_0 = ((int32_t)-3);
	}

IL_006c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_10 = V_0;
			if (((int32_t)((int32_t)L_10-(int32_t)1)) == 0)
			{
				goto IL_00a8;
			}
		}

IL_0078:
		{
			goto IL_00a8;
		}

IL_007d:
		{
			Il2CppObject * L_11 = __this->get_U3CU24s_14U3E__1_2();
			NullCheck(L_11);
			Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_11);
			__this->set_U3CeU3E__2_3(((Il2CppObject *)Castclass(L_12, IEnumerable_t287189635_il2cpp_TypeInfo_var)));
			__this->set_U24current_5((String_t*)NULL);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xFE, FINALLY_00bd);
		}

IL_00a8:
		{
			Il2CppObject * L_13 = __this->get_U3CU24s_14U3E__1_2();
			NullCheck(L_13);
			bool L_14 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_007d;
			}
		}

IL_00b8:
		{
			IL2CPP_LEAVE(0xD8, FINALLY_00bd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00bd;
	}

FINALLY_00bd:
	{ // begin finally (depth: 1)
		{
			bool L_15 = V_1;
			if (!L_15)
			{
				goto IL_00c1;
			}
		}

IL_00c0:
		{
			IL2CPP_END_FINALLY(189)
		}

IL_00c1:
		{
			Il2CppObject * L_16 = __this->get_U3CU24s_14U3E__1_2();
			V_2 = ((Il2CppObject *)IsInst(L_16, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_17 = V_2;
			if (L_17)
			{
				goto IL_00d1;
			}
		}

IL_00d0:
		{
			IL2CPP_END_FINALLY(189)
		}

IL_00d1:
		{
			Il2CppObject * L_18 = V_2;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_18);
			IL2CPP_END_FINALLY(189)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(189)
	{
		IL2CPP_JUMP_TBL(0xFE, IL_00fe)
		IL2CPP_JUMP_TBL(0xD8, IL_00d8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00d8:
	{
		StringBuilder_t3822575854 * L_19 = __this->get_U3CbuilderU3E__0_0();
		NullCheck(L_19);
		String_t* L_20 = StringBuilder_ToString_m350379841(L_19, /*hidden argument*/NULL);
		__this->set_U24current_5(L_20);
		__this->set_U24PC_4(2);
		goto IL_00fe;
	}

IL_00f5:
	{
		__this->set_U24PC_4((-1));
	}

IL_00fc:
	{
		return (bool)0;
	}

IL_00fe:
	{
		return (bool)1;
	}
	// Dead block : IL_0100: ldloc.3
}
// System.Void JSONObject/<PrintAsync>c__Iterator2::Dispose()
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator2_Dispose_m2419551084_MetadataUsageId;
extern "C"  void U3CPrintAsyncU3Ec__Iterator2_Dispose_m2419551084 (U3CPrintAsyncU3Ec__Iterator2_t1765946676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator2_Dispose_m2419551084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0041;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_0041;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x41, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_U3CU24s_14U3E__1_2();
			V_1 = ((Il2CppObject *)IsInst(L_2, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_003a;
			}
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_003a:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck(L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_4);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Void JSONObject/<PrintAsync>c__Iterator2::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CPrintAsyncU3Ec__Iterator2_Reset_m652405148_MetadataUsageId;
extern "C"  void U3CPrintAsyncU3Ec__Iterator2_Reset_m652405148 (U3CPrintAsyncU3Ec__Iterator2_t1765946676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPrintAsyncU3Ec__Iterator2_Reset_m652405148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void JSONObject/<StringifyAsync>c__Iterator3::.ctor()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator3__ctor_m3303104688 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object JSONObject/<StringifyAsync>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2629633442 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_16();
		return L_0;
	}
}
// System.Object JSONObject/<StringifyAsync>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3869412662 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_16();
		return L_0;
	}
}
// System.Collections.IEnumerator JSONObject/<StringifyAsync>c__Iterator3::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator3_System_Collections_IEnumerable_GetEnumerator_m2370473137 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CStringifyAsyncU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m2456447359(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<StringifyAsync>c__Iterator3::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern TypeInfo* U3CStringifyAsyncU3Ec__Iterator3_t555783763_il2cpp_TypeInfo_var;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m2456447359_MetadataUsageId;
extern "C"  Il2CppObject* U3CStringifyAsyncU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m2456447359 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m2456447359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CStringifyAsyncU3Ec__Iterator3_t555783763 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_15();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_2 = (U3CStringifyAsyncU3Ec__Iterator3_t555783763 *)il2cpp_codegen_object_new(U3CStringifyAsyncU3Ec__Iterator3_t555783763_il2cpp_TypeInfo_var);
		U3CStringifyAsyncU3Ec__Iterator3__ctor_m3303104688(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_3 = V_0;
		JSONObject_t1752376903 * L_4 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_20(L_4);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_5 = V_0;
		int32_t L_6 = __this->get_U3CU24U3Edepth_17();
		NullCheck(L_5);
		L_5->set_depth_0(L_6);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_7 = V_0;
		StringBuilder_t3822575854 * L_8 = __this->get_U3CU24U3Ebuilder_18();
		NullCheck(L_7);
		L_7->set_builder_1(L_8);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_9 = V_0;
		bool L_10 = __this->get_U3CU24U3Epretty_19();
		NullCheck(L_9);
		L_9->set_pretty_2(L_10);
		U3CStringifyAsyncU3Ec__Iterator3_t555783763 * L_11 = V_0;
		return L_11;
	}
}
// System.Boolean JSONObject/<StringifyAsync>c__Iterator3::MoveNext()
extern TypeInfo* Debug_t1588791936_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t1752376903_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1037112200;
extern Il2CppCodeGenString* _stringLiteral35114044;
extern Il2CppCodeGenString* _stringLiteral1473507608;
extern Il2CppCodeGenString* _stringLiteral3449600172;
extern Il2CppCodeGenString* _stringLiteral33819081;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral9;
extern Il2CppCodeGenString* _stringLiteral1088535422;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral125;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator3_MoveNext_m685978436_MetadataUsageId;
extern "C"  bool U3CStringifyAsyncU3Ec__Iterator3_MoveNext_m685978436 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator3_MoveNext_m685978436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	TimeSpan_t763862892  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	bool V_7 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_15();
		V_0 = L_0;
		__this->set_U24PC_15((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_008f;
		}
		if (L_1 == 2)
		{
			goto IL_02cd;
		}
		if (L_1 == 3)
		{
			goto IL_0544;
		}
	}
	{
		goto IL_0723;
	}

IL_002b:
	{
		int32_t L_2 = __this->get_depth_0();
		int32_t L_3 = L_2;
		V_2 = L_3;
		__this->set_depth_0(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = V_2;
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)100))))
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1037112200, /*hidden argument*/NULL);
		goto IL_0723;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		Stopwatch_t2509581612 * L_5 = ((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->get_printWatch_12();
		NullCheck(L_5);
		TimeSpan_t763862892  L_6 = Stopwatch_get_Elapsed_m892540484(L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		double L_7 = TimeSpan_get_TotalSeconds_m3163750087((&V_3), /*hidden argument*/NULL);
		if ((!(((double)L_7) > ((double)(0.00800000037997961)))))
		{
			goto IL_0099;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		Stopwatch_t2509581612 * L_8 = ((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->get_printWatch_12();
		NullCheck(L_8);
		Stopwatch_Reset_m2376504733(L_8, /*hidden argument*/NULL);
		__this->set_U24current_16(NULL);
		__this->set_U24PC_15(1);
		goto IL_0725;
	}

IL_008f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		Stopwatch_t2509581612 * L_9 = ((JSONObject_t1752376903_StaticFields*)JSONObject_t1752376903_il2cpp_TypeInfo_var->static_fields)->get_printWatch_12();
		NullCheck(L_9);
		Stopwatch_Start_m3677209584(L_9, /*hidden argument*/NULL);
	}

IL_0099:
	{
		JSONObject_t1752376903 * L_10 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_type_6();
		V_4 = L_11;
		int32_t L_12 = V_4;
		if (L_12 == 0)
		{
			goto IL_0706;
		}
		if (L_12 == 1)
		{
			goto IL_00ea;
		}
		if (L_12 == 2)
		{
			goto IL_010b;
		}
		if (L_12 == 3)
		{
			goto IL_01ad;
		}
		if (L_12 == 4)
		{
			goto IL_0453;
		}
		if (L_12 == 5)
		{
			goto IL_06ca;
		}
		if (L_12 == 6)
		{
			goto IL_00ce;
		}
	}
	{
		goto IL_071c;
	}

IL_00ce:
	{
		StringBuilder_t3822575854 * L_13 = __this->get_builder_1();
		JSONObject_t1752376903 * L_14 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_14);
		String_t* L_15 = L_14->get_str_9();
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, L_15, /*hidden argument*/NULL);
		goto IL_071c;
	}

IL_00ea:
	{
		StringBuilder_t3822575854 * L_16 = __this->get_builder_1();
		JSONObject_t1752376903 * L_17 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_17);
		String_t* L_18 = L_17->get_str_9();
		NullCheck(L_16);
		StringBuilder_AppendFormat_m3723191730(L_16, _stringLiteral35114044, L_18, /*hidden argument*/NULL);
		goto IL_071c;
	}

IL_010b:
	{
		JSONObject_t1752376903 * L_19 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_19);
		double L_20 = L_19->get_n_10();
		bool L_21 = Double_IsInfinity_m3094155474(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0136;
		}
	}
	{
		StringBuilder_t3822575854 * L_22 = __this->get_builder_1();
		NullCheck(L_22);
		StringBuilder_Append_m3898090075(L_22, _stringLiteral1473507608, /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_0136:
	{
		JSONObject_t1752376903 * L_23 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_23);
		double L_24 = L_23->get_n_10();
		bool L_25 = Double_IsNegativeInfinity_m553043933(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0161;
		}
	}
	{
		StringBuilder_t3822575854 * L_26 = __this->get_builder_1();
		NullCheck(L_26);
		StringBuilder_Append_m3898090075(L_26, _stringLiteral3449600172, /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_0161:
	{
		JSONObject_t1752376903 * L_27 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_27);
		double L_28 = L_27->get_n_10();
		bool L_29 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_018c;
		}
	}
	{
		StringBuilder_t3822575854 * L_30 = __this->get_builder_1();
		NullCheck(L_30);
		StringBuilder_Append_m3898090075(L_30, _stringLiteral33819081, /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_018c:
	{
		StringBuilder_t3822575854 * L_31 = __this->get_builder_1();
		JSONObject_t1752376903 * L_32 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_32);
		double* L_33 = L_32->get_address_of_n_10();
		String_t* L_34 = Double_ToString_m3380246633(L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		StringBuilder_Append_m3898090075(L_31, L_34, /*hidden argument*/NULL);
	}

IL_01a8:
	{
		goto IL_071c;
	}

IL_01ad:
	{
		StringBuilder_t3822575854 * L_35 = __this->get_builder_1();
		NullCheck(L_35);
		StringBuilder_Append_m3898090075(L_35, _stringLiteral123, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_36 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_36);
		List_1_t2549335872 * L_37 = L_36->get_list_7();
		NullCheck(L_37);
		int32_t L_38 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_37);
		if ((((int32_t)L_38) <= ((int32_t)0)))
		{
			goto IL_03cd;
		}
	}
	{
		bool L_39 = __this->get_pretty_2();
		if (!L_39)
		{
			goto IL_01f0;
		}
	}
	{
		StringBuilder_t3822575854 * L_40 = __this->get_builder_1();
		NullCheck(L_40);
		StringBuilder_Append_m3898090075(L_40, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_01f0:
	{
		__this->set_U3CiU3E__0_3(0);
		goto IL_037c;
	}

IL_01fc:
	{
		JSONObject_t1752376903 * L_41 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_41);
		List_1_t1765447871 * L_42 = L_41->get_keys_8();
		int32_t L_43 = __this->get_U3CiU3E__0_3();
		NullCheck(L_42);
		String_t* L_44 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_42, L_43);
		__this->set_U3CkeyU3E__1_4(L_44);
		JSONObject_t1752376903 * L_45 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_45);
		List_1_t2549335872 * L_46 = L_45->get_list_7();
		int32_t L_47 = __this->get_U3CiU3E__0_3();
		NullCheck(L_46);
		JSONObject_t1752376903 * L_48 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_46, L_47);
		__this->set_U3CobjU3E__2_5(L_48);
		JSONObject_t1752376903 * L_49 = __this->get_U3CobjU3E__2_5();
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		bool L_50 = JSONObject_op_Implicit_m2520452346(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_036e;
		}
	}
	{
		bool L_51 = __this->get_pretty_2();
		if (!L_51)
		{
			goto IL_028b;
		}
	}
	{
		__this->set_U3CjU3E__3_6(0);
		goto IL_027a;
	}

IL_025b:
	{
		StringBuilder_t3822575854 * L_52 = __this->get_builder_1();
		NullCheck(L_52);
		StringBuilder_Append_m3898090075(L_52, _stringLiteral9, /*hidden argument*/NULL);
		int32_t L_53 = __this->get_U3CjU3E__3_6();
		__this->set_U3CjU3E__3_6(((int32_t)((int32_t)L_53+(int32_t)1)));
	}

IL_027a:
	{
		int32_t L_54 = __this->get_U3CjU3E__3_6();
		int32_t L_55 = __this->get_depth_0();
		if ((((int32_t)L_54) < ((int32_t)L_55)))
		{
			goto IL_025b;
		}
	}

IL_028b:
	{
		StringBuilder_t3822575854 * L_56 = __this->get_builder_1();
		String_t* L_57 = __this->get_U3CkeyU3E__1_4();
		NullCheck(L_56);
		StringBuilder_AppendFormat_m3723191730(L_56, _stringLiteral1088535422, L_57, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_58 = __this->get_U3CobjU3E__2_5();
		int32_t L_59 = __this->get_depth_0();
		StringBuilder_t3822575854 * L_60 = __this->get_builder_1();
		bool L_61 = __this->get_pretty_2();
		NullCheck(L_58);
		Il2CppObject * L_62 = JSONObject_StringifyAsync_m3909536131(L_58, L_59, L_60, L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		Il2CppObject * L_63 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_62);
		__this->set_U3CU24s_15U3E__4_7(L_63);
		V_0 = ((int32_t)-3);
	}

IL_02cd:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_64 = V_0;
			if (((int32_t)((int32_t)L_64-(int32_t)2)) == 0)
			{
				goto IL_030e;
			}
		}

IL_02d9:
		{
			goto IL_030e;
		}

IL_02de:
		{
			Il2CppObject * L_65 = __this->get_U3CU24s_15U3E__4_7();
			NullCheck(L_65);
			Il2CppObject * L_66 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_65);
			__this->set_U3CeU3E__5_8(((Il2CppObject *)Castclass(L_66, IEnumerable_t287189635_il2cpp_TypeInfo_var)));
			Il2CppObject * L_67 = __this->get_U3CeU3E__5_8();
			__this->set_U24current_16(L_67);
			__this->set_U24PC_15(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x725, FINALLY_0323);
		}

IL_030e:
		{
			Il2CppObject * L_68 = __this->get_U3CU24s_15U3E__4_7();
			NullCheck(L_68);
			bool L_69 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_68);
			if (L_69)
			{
				goto IL_02de;
			}
		}

IL_031e:
		{
			IL2CPP_LEAVE(0x341, FINALLY_0323);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0323;
	}

FINALLY_0323:
	{ // begin finally (depth: 1)
		{
			bool L_70 = V_1;
			if (!L_70)
			{
				goto IL_0327;
			}
		}

IL_0326:
		{
			IL2CPP_END_FINALLY(803)
		}

IL_0327:
		{
			Il2CppObject * L_71 = __this->get_U3CU24s_15U3E__4_7();
			V_5 = ((Il2CppObject *)IsInst(L_71, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_72 = V_5;
			if (L_72)
			{
				goto IL_0339;
			}
		}

IL_0338:
		{
			IL2CPP_END_FINALLY(803)
		}

IL_0339:
		{
			Il2CppObject * L_73 = V_5;
			NullCheck(L_73);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_73);
			IL2CPP_END_FINALLY(803)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(803)
	{
		IL2CPP_JUMP_TBL(0x725, IL_0725)
		IL2CPP_JUMP_TBL(0x341, IL_0341)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0341:
	{
		StringBuilder_t3822575854 * L_74 = __this->get_builder_1();
		NullCheck(L_74);
		StringBuilder_Append_m3898090075(L_74, _stringLiteral44, /*hidden argument*/NULL);
		bool L_75 = __this->get_pretty_2();
		if (!L_75)
		{
			goto IL_036e;
		}
	}
	{
		StringBuilder_t3822575854 * L_76 = __this->get_builder_1();
		NullCheck(L_76);
		StringBuilder_Append_m3898090075(L_76, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_036e:
	{
		int32_t L_77 = __this->get_U3CiU3E__0_3();
		__this->set_U3CiU3E__0_3(((int32_t)((int32_t)L_77+(int32_t)1)));
	}

IL_037c:
	{
		int32_t L_78 = __this->get_U3CiU3E__0_3();
		JSONObject_t1752376903 * L_79 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_79);
		List_1_t2549335872 * L_80 = L_79->get_list_7();
		NullCheck(L_80);
		int32_t L_81 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_80);
		if ((((int32_t)L_78) < ((int32_t)L_81)))
		{
			goto IL_01fc;
		}
	}
	{
		bool L_82 = __this->get_pretty_2();
		if (!L_82)
		{
			goto IL_03ba;
		}
	}
	{
		StringBuilder_t3822575854 * L_83 = __this->get_builder_1();
		StringBuilder_t3822575854 * L_84 = L_83;
		NullCheck(L_84);
		int32_t L_85 = StringBuilder_get_Length_m2443133099(L_84, /*hidden argument*/NULL);
		NullCheck(L_84);
		StringBuilder_set_Length_m1952332172(L_84, ((int32_t)((int32_t)L_85-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_03cd;
	}

IL_03ba:
	{
		StringBuilder_t3822575854 * L_86 = __this->get_builder_1();
		StringBuilder_t3822575854 * L_87 = L_86;
		NullCheck(L_87);
		int32_t L_88 = StringBuilder_get_Length_m2443133099(L_87, /*hidden argument*/NULL);
		NullCheck(L_87);
		StringBuilder_set_Length_m1952332172(L_87, ((int32_t)((int32_t)L_88-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_03cd:
	{
		bool L_89 = __this->get_pretty_2();
		if (!L_89)
		{
			goto IL_043d;
		}
	}
	{
		JSONObject_t1752376903 * L_90 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_90);
		List_1_t2549335872 * L_91 = L_90->get_list_7();
		NullCheck(L_91);
		int32_t L_92 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_91);
		if ((((int32_t)L_92) <= ((int32_t)0)))
		{
			goto IL_043d;
		}
	}
	{
		StringBuilder_t3822575854 * L_93 = __this->get_builder_1();
		NullCheck(L_93);
		StringBuilder_Append_m3898090075(L_93, _stringLiteral10, /*hidden argument*/NULL);
		__this->set_U3CjU3E__6_9(0);
		goto IL_042a;
	}

IL_040b:
	{
		StringBuilder_t3822575854 * L_94 = __this->get_builder_1();
		NullCheck(L_94);
		StringBuilder_Append_m3898090075(L_94, _stringLiteral9, /*hidden argument*/NULL);
		int32_t L_95 = __this->get_U3CjU3E__6_9();
		__this->set_U3CjU3E__6_9(((int32_t)((int32_t)L_95+(int32_t)1)));
	}

IL_042a:
	{
		int32_t L_96 = __this->get_U3CjU3E__6_9();
		int32_t L_97 = __this->get_depth_0();
		if ((((int32_t)L_96) < ((int32_t)((int32_t)((int32_t)L_97-(int32_t)1)))))
		{
			goto IL_040b;
		}
	}

IL_043d:
	{
		StringBuilder_t3822575854 * L_98 = __this->get_builder_1();
		NullCheck(L_98);
		StringBuilder_Append_m3898090075(L_98, _stringLiteral125, /*hidden argument*/NULL);
		goto IL_071c;
	}

IL_0453:
	{
		StringBuilder_t3822575854 * L_99 = __this->get_builder_1();
		NullCheck(L_99);
		StringBuilder_Append_m3898090075(L_99, _stringLiteral91, /*hidden argument*/NULL);
		JSONObject_t1752376903 * L_100 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_100);
		List_1_t2549335872 * L_101 = L_100->get_list_7();
		NullCheck(L_101);
		int32_t L_102 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_101);
		if ((((int32_t)L_102) <= ((int32_t)0)))
		{
			goto IL_0644;
		}
	}
	{
		bool L_103 = __this->get_pretty_2();
		if (!L_103)
		{
			goto IL_0496;
		}
	}
	{
		StringBuilder_t3822575854 * L_104 = __this->get_builder_1();
		NullCheck(L_104);
		StringBuilder_Append_m3898090075(L_104, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_0496:
	{
		__this->set_U3CiU3E__7_10(0);
		goto IL_05f3;
	}

IL_04a2:
	{
		JSONObject_t1752376903 * L_105 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_105);
		List_1_t2549335872 * L_106 = L_105->get_list_7();
		int32_t L_107 = __this->get_U3CiU3E__7_10();
		NullCheck(L_106);
		JSONObject_t1752376903 * L_108 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_106, L_107);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t1752376903_il2cpp_TypeInfo_var);
		bool L_109 = JSONObject_op_Implicit_m2520452346(NULL /*static, unused*/, L_108, /*hidden argument*/NULL);
		if (!L_109)
		{
			goto IL_05e5;
		}
	}
	{
		bool L_110 = __this->get_pretty_2();
		if (!L_110)
		{
			goto IL_0509;
		}
	}
	{
		__this->set_U3CjU3E__8_11(0);
		goto IL_04f8;
	}

IL_04d9:
	{
		StringBuilder_t3822575854 * L_111 = __this->get_builder_1();
		NullCheck(L_111);
		StringBuilder_Append_m3898090075(L_111, _stringLiteral9, /*hidden argument*/NULL);
		int32_t L_112 = __this->get_U3CjU3E__8_11();
		__this->set_U3CjU3E__8_11(((int32_t)((int32_t)L_112+(int32_t)1)));
	}

IL_04f8:
	{
		int32_t L_113 = __this->get_U3CjU3E__8_11();
		int32_t L_114 = __this->get_depth_0();
		if ((((int32_t)L_113) < ((int32_t)L_114)))
		{
			goto IL_04d9;
		}
	}

IL_0509:
	{
		JSONObject_t1752376903 * L_115 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_115);
		List_1_t2549335872 * L_116 = L_115->get_list_7();
		int32_t L_117 = __this->get_U3CiU3E__7_10();
		NullCheck(L_116);
		JSONObject_t1752376903 * L_118 = VirtFuncInvoker1< JSONObject_t1752376903 *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<JSONObject>::get_Item(System.Int32) */, L_116, L_117);
		int32_t L_119 = __this->get_depth_0();
		StringBuilder_t3822575854 * L_120 = __this->get_builder_1();
		bool L_121 = __this->get_pretty_2();
		NullCheck(L_118);
		Il2CppObject * L_122 = JSONObject_StringifyAsync_m3909536131(L_118, L_119, L_120, L_121, /*hidden argument*/NULL);
		NullCheck(L_122);
		Il2CppObject * L_123 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_122);
		__this->set_U3CU24s_16U3E__9_12(L_123);
		V_0 = ((int32_t)-3);
	}

IL_0544:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_124 = V_0;
			if (((int32_t)((int32_t)L_124-(int32_t)3)) == 0)
			{
				goto IL_0585;
			}
		}

IL_0550:
		{
			goto IL_0585;
		}

IL_0555:
		{
			Il2CppObject * L_125 = __this->get_U3CU24s_16U3E__9_12();
			NullCheck(L_125);
			Il2CppObject * L_126 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_125);
			__this->set_U3CeU3E__10_13(((Il2CppObject *)Castclass(L_126, IEnumerable_t287189635_il2cpp_TypeInfo_var)));
			Il2CppObject * L_127 = __this->get_U3CeU3E__10_13();
			__this->set_U24current_16(L_127);
			__this->set_U24PC_15(3);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x725, FINALLY_059a);
		}

IL_0585:
		{
			Il2CppObject * L_128 = __this->get_U3CU24s_16U3E__9_12();
			NullCheck(L_128);
			bool L_129 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_128);
			if (L_129)
			{
				goto IL_0555;
			}
		}

IL_0595:
		{
			IL2CPP_LEAVE(0x5B8, FINALLY_059a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_059a;
	}

FINALLY_059a:
	{ // begin finally (depth: 1)
		{
			bool L_130 = V_1;
			if (!L_130)
			{
				goto IL_059e;
			}
		}

IL_059d:
		{
			IL2CPP_END_FINALLY(1434)
		}

IL_059e:
		{
			Il2CppObject * L_131 = __this->get_U3CU24s_16U3E__9_12();
			V_6 = ((Il2CppObject *)IsInst(L_131, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_132 = V_6;
			if (L_132)
			{
				goto IL_05b0;
			}
		}

IL_05af:
		{
			IL2CPP_END_FINALLY(1434)
		}

IL_05b0:
		{
			Il2CppObject * L_133 = V_6;
			NullCheck(L_133);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_133);
			IL2CPP_END_FINALLY(1434)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1434)
	{
		IL2CPP_JUMP_TBL(0x725, IL_0725)
		IL2CPP_JUMP_TBL(0x5B8, IL_05b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_05b8:
	{
		StringBuilder_t3822575854 * L_134 = __this->get_builder_1();
		NullCheck(L_134);
		StringBuilder_Append_m3898090075(L_134, _stringLiteral44, /*hidden argument*/NULL);
		bool L_135 = __this->get_pretty_2();
		if (!L_135)
		{
			goto IL_05e5;
		}
	}
	{
		StringBuilder_t3822575854 * L_136 = __this->get_builder_1();
		NullCheck(L_136);
		StringBuilder_Append_m3898090075(L_136, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_05e5:
	{
		int32_t L_137 = __this->get_U3CiU3E__7_10();
		__this->set_U3CiU3E__7_10(((int32_t)((int32_t)L_137+(int32_t)1)));
	}

IL_05f3:
	{
		int32_t L_138 = __this->get_U3CiU3E__7_10();
		JSONObject_t1752376903 * L_139 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_139);
		List_1_t2549335872 * L_140 = L_139->get_list_7();
		NullCheck(L_140);
		int32_t L_141 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_140);
		if ((((int32_t)L_138) < ((int32_t)L_141)))
		{
			goto IL_04a2;
		}
	}
	{
		bool L_142 = __this->get_pretty_2();
		if (!L_142)
		{
			goto IL_0631;
		}
	}
	{
		StringBuilder_t3822575854 * L_143 = __this->get_builder_1();
		StringBuilder_t3822575854 * L_144 = L_143;
		NullCheck(L_144);
		int32_t L_145 = StringBuilder_get_Length_m2443133099(L_144, /*hidden argument*/NULL);
		NullCheck(L_144);
		StringBuilder_set_Length_m1952332172(L_144, ((int32_t)((int32_t)L_145-(int32_t)2)), /*hidden argument*/NULL);
		goto IL_0644;
	}

IL_0631:
	{
		StringBuilder_t3822575854 * L_146 = __this->get_builder_1();
		StringBuilder_t3822575854 * L_147 = L_146;
		NullCheck(L_147);
		int32_t L_148 = StringBuilder_get_Length_m2443133099(L_147, /*hidden argument*/NULL);
		NullCheck(L_147);
		StringBuilder_set_Length_m1952332172(L_147, ((int32_t)((int32_t)L_148-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0644:
	{
		bool L_149 = __this->get_pretty_2();
		if (!L_149)
		{
			goto IL_06b4;
		}
	}
	{
		JSONObject_t1752376903 * L_150 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_150);
		List_1_t2549335872 * L_151 = L_150->get_list_7();
		NullCheck(L_151);
		int32_t L_152 = VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<JSONObject>::get_Count() */, L_151);
		if ((((int32_t)L_152) <= ((int32_t)0)))
		{
			goto IL_06b4;
		}
	}
	{
		StringBuilder_t3822575854 * L_153 = __this->get_builder_1();
		NullCheck(L_153);
		StringBuilder_Append_m3898090075(L_153, _stringLiteral10, /*hidden argument*/NULL);
		__this->set_U3CjU3E__11_14(0);
		goto IL_06a1;
	}

IL_0682:
	{
		StringBuilder_t3822575854 * L_154 = __this->get_builder_1();
		NullCheck(L_154);
		StringBuilder_Append_m3898090075(L_154, _stringLiteral9, /*hidden argument*/NULL);
		int32_t L_155 = __this->get_U3CjU3E__11_14();
		__this->set_U3CjU3E__11_14(((int32_t)((int32_t)L_155+(int32_t)1)));
	}

IL_06a1:
	{
		int32_t L_156 = __this->get_U3CjU3E__11_14();
		int32_t L_157 = __this->get_depth_0();
		if ((((int32_t)L_156) < ((int32_t)((int32_t)((int32_t)L_157-(int32_t)1)))))
		{
			goto IL_0682;
		}
	}

IL_06b4:
	{
		StringBuilder_t3822575854 * L_158 = __this->get_builder_1();
		NullCheck(L_158);
		StringBuilder_Append_m3898090075(L_158, _stringLiteral93, /*hidden argument*/NULL);
		goto IL_071c;
	}

IL_06ca:
	{
		JSONObject_t1752376903 * L_159 = __this->get_U3CU3Ef__this_20();
		NullCheck(L_159);
		bool L_160 = L_159->get_b_11();
		if (!L_160)
		{
			goto IL_06f0;
		}
	}
	{
		StringBuilder_t3822575854 * L_161 = __this->get_builder_1();
		NullCheck(L_161);
		StringBuilder_Append_m3898090075(L_161, _stringLiteral3569038, /*hidden argument*/NULL);
		goto IL_0701;
	}

IL_06f0:
	{
		StringBuilder_t3822575854 * L_162 = __this->get_builder_1();
		NullCheck(L_162);
		StringBuilder_Append_m3898090075(L_162, _stringLiteral97196323, /*hidden argument*/NULL);
	}

IL_0701:
	{
		goto IL_071c;
	}

IL_0706:
	{
		StringBuilder_t3822575854 * L_163 = __this->get_builder_1();
		NullCheck(L_163);
		StringBuilder_Append_m3898090075(L_163, _stringLiteral3392903, /*hidden argument*/NULL);
		goto IL_071c;
	}

IL_071c:
	{
		__this->set_U24PC_15((-1));
	}

IL_0723:
	{
		return (bool)0;
	}

IL_0725:
	{
		return (bool)1;
	}
	// Dead block : IL_0727: ldloc.s V_7
}
// System.Void JSONObject/<StringifyAsync>c__Iterator3::Dispose()
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator3_Dispose_m201056493_MetadataUsageId;
extern "C"  void U3CStringifyAsyncU3Ec__Iterator3_Dispose_m201056493 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator3_Dispose_m201056493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_15();
		V_0 = L_0;
		__this->set_U24PC_15((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0066;
		}
		if (L_1 == 1)
		{
			goto IL_0066;
		}
		if (L_1 == 2)
		{
			goto IL_0029;
		}
		if (L_1 == 3)
		{
			goto IL_004a;
		}
	}
	{
		goto IL_0066;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x45, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = __this->get_U3CU24s_15U3E__4_7();
			V_1 = ((Il2CppObject *)IsInst(L_2, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_003e;
			}
		}

IL_003d:
		{
			IL2CPP_END_FINALLY(46)
		}

IL_003e:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck(L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_4);
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0045:
	{
		goto IL_0066;
	}

IL_004a:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x66, FINALLY_004f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_5 = __this->get_U3CU24s_16U3E__9_12();
			V_2 = ((Il2CppObject *)IsInst(L_5, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_6 = V_2;
			if (L_6)
			{
				goto IL_005f;
			}
		}

IL_005e:
		{
			IL2CPP_END_FINALLY(79)
		}

IL_005f:
		{
			Il2CppObject * L_7 = V_2;
			NullCheck(L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_7);
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0066:
	{
		return;
	}
}
// System.Void JSONObject/<StringifyAsync>c__Iterator3::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CStringifyAsyncU3Ec__Iterator3_Reset_m949537629_MetadataUsageId;
extern "C"  void U3CStringifyAsyncU3Ec__Iterator3_Reset_m949537629 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStringifyAsyncU3Ec__Iterator3_Reset_m949537629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void JSONObject/AddJSONConents::.ctor(System.Object,System.IntPtr)
extern "C"  void AddJSONConents__ctor_m3113130080 (AddJSONConents_t3346118721 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void JSONObject/AddJSONConents::Invoke(JSONObject)
extern "C"  void AddJSONConents_Invoke_m3702840851 (AddJSONConents_t3346118721 * __this, JSONObject_t1752376903 * ___self, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AddJSONConents_Invoke_m3702840851((AddJSONConents_t3346118721 *)__this->get_prev_9(),___self, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JSONObject_t1752376903 * ___self, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___self,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JSONObject_t1752376903 * ___self, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___self,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___self,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_AddJSONConents_t3346118721(Il2CppObject* delegate, JSONObject_t1752376903 * ___self)
{
	// Marshaling of parameter '___self' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'JSONObject'."));
}
// System.IAsyncResult JSONObject/AddJSONConents::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddJSONConents_BeginInvoke_m4052807732 (AddJSONConents_t3346118721 * __this, JSONObject_t1752376903 * ___self, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___self;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void JSONObject/AddJSONConents::EndInvoke(System.IAsyncResult)
extern "C"  void AddJSONConents_EndInvoke_m4139596400 (AddJSONConents_t3346118721 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void JSONObject/FieldNotFound::.ctor(System.Object,System.IntPtr)
extern "C"  void FieldNotFound__ctor_m3702607304 (FieldNotFound_t2014471017 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void JSONObject/FieldNotFound::Invoke(System.String)
extern "C"  void FieldNotFound_Invoke_m1255490080 (FieldNotFound_t2014471017 * __this, String_t* ___name, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FieldNotFound_Invoke_m1255490080((FieldNotFound_t2014471017 *)__this->get_prev_9(),___name, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___name, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___name,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___name, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___name,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___name,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_FieldNotFound_t2014471017(Il2CppObject* delegate, String_t* ___name)
{
	typedef void (STDCALL *native_function_ptr_type)(char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___name' to native representation
	char* ____name_marshaled = NULL;
	____name_marshaled = il2cpp_codegen_marshal_string(___name);

	// Native function invocation
	_il2cpp_pinvoke_func(____name_marshaled);

	// Marshaling cleanup of parameter '___name' native representation
	il2cpp_codegen_marshal_free(____name_marshaled);
	____name_marshaled = NULL;

}
// System.IAsyncResult JSONObject/FieldNotFound::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FieldNotFound_BeginInvoke_m2796968613 (FieldNotFound_t2014471017 * __this, String_t* ___name, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___name;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void JSONObject/FieldNotFound::EndInvoke(System.IAsyncResult)
extern "C"  void FieldNotFound_EndInvoke_m2166547416 (FieldNotFound_t2014471017 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void JSONObject/GetFieldResponse::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFieldResponse__ctor_m4162696676 (GetFieldResponse_t1656258501 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->set_method_ptr_0((methodPointerType)((MethodInfo*)___method.get_m_value_0())->method);
	__this->set_method_3(___method);
	__this->set_m_target_2(___object);
}
// System.Void JSONObject/GetFieldResponse::Invoke(JSONObject)
extern "C"  void GetFieldResponse_Invoke_m194478999 (GetFieldResponse_t1656258501 * __this, JSONObject_t1752376903 * ___obj, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetFieldResponse_Invoke_m194478999((GetFieldResponse_t1656258501 *)__this->get_prev_9(),___obj, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JSONObject_t1752376903 * ___obj, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JSONObject_t1752376903 * ___obj, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_GetFieldResponse_t1656258501(Il2CppObject* delegate, JSONObject_t1752376903 * ___obj)
{
	// Marshaling of parameter '___obj' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'JSONObject'."));
}
// System.IAsyncResult JSONObject/GetFieldResponse::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFieldResponse_BeginInvoke_m2545021624 (GetFieldResponse_t1656258501 * __this, JSONObject_t1752376903 * ___obj, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void JSONObject/GetFieldResponse::EndInvoke(System.IAsyncResult)
extern "C"  void GetFieldResponse_EndInvoke_m1365228020 (GetFieldResponse_t1656258501 * __this, Il2CppObject * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void SDKManager::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SDKManager__ctor_m334162888_MetadataUsageId;
extern "C"  void SDKManager__ctor_m334162888 (SDKManager_t2583098931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager__ctor_m334162888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_ZoneID_2(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_logs_3(L_1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SDKManager::Start()
extern TypeInfo* GUIStyle_t1006925219_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2682510823;
extern const uint32_t SDKManager_Start_m3576267976_MetadataUsageId;
extern "C"  void SDKManager_Start_m3576267976 (SDKManager_t2583098931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_Start_m3576267976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t1006925219 * L_0 = (GUIStyle_t1006925219 *)il2cpp_codegen_object_new(GUIStyle_t1006925219_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m478034167(L_0, /*hidden argument*/NULL);
		__this->set_gs_4(L_0);
		GUIStyle_t1006925219 * L_1 = __this->get_gs_4();
		NullCheck(L_1);
		GUIStyle_set_alignment_m4252900834(L_1, 6, /*hidden argument*/NULL);
		GUIStyle_t1006925219 * L_2 = __this->get_gs_4();
		NullCheck(L_2);
		GUIStyle_set_fontSize_m3621764235(L_2, ((int32_t)30), /*hidden argument*/NULL);
		GUIStyle_t1006925219 * L_3 = __this->get_gs_4();
		NullCheck(L_3);
		GUIStyleState_t47287592 * L_4 = GUIStyle_get_normal_m42729964(L_3, /*hidden argument*/NULL);
		Color_t1588175760  L_5 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyleState_set_textColor_m3058467057(L_4, L_5, /*hidden argument*/NULL);
		String_t* L_6 = PlayerPrefs_GetString_m378864272(NULL /*static, unused*/, _stringLiteral2682510823, /*hidden argument*/NULL);
		__this->set_ZoneID_2(L_6);
		return;
	}
}
// System.Void SDKManager::Awake()
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3211021459_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t978424956_il2cpp_TypeInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_SessionStartReceived_m1241951151_MethodInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_SessionLoginError_m1624667535_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2736694297_MethodInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_AdAvailabilityResponse_m1213840872_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m2591975409_MethodInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_AdWillClose_m2278286083_MethodInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_RewardedAdCompletedWithObject_m1785257430_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3714754526_MethodInfo_var;
extern const uint32_t SDKManager_Awake_m571768107_MetadataUsageId;
extern "C"  void SDKManager_Awake_m571768107 (SDKManager_t2583098931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_Awake_m571768107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)SDKManager_FuseSDK_SessionStartReceived_m1241951151_MethodInfo_var);
		Action_t437523947 * L_1 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_add_SessionStartReceived_m1374611232(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)SDKManager_FuseSDK_SessionLoginError_m1624667535_MethodInfo_var);
		Action_1_t592684423 * L_3 = (Action_1_t592684423 *)il2cpp_codegen_object_new(Action_1_t592684423_il2cpp_TypeInfo_var);
		Action_1__ctor_m2736694297(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m2736694297_MethodInfo_var);
		FuseSDK_add_SessionLoginError_m2857792873(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)SDKManager_FuseSDK_AdAvailabilityResponse_m1213840872_MethodInfo_var);
		Action_2_t3211021459 * L_5 = (Action_2_t3211021459 *)il2cpp_codegen_object_new(Action_2_t3211021459_il2cpp_TypeInfo_var);
		Action_2__ctor_m2591975409(L_5, __this, L_4, /*hidden argument*/Action_2__ctor_m2591975409_MethodInfo_var);
		FuseSDK_add_AdAvailabilityResponse_m2892673221(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)SDKManager_FuseSDK_AdWillClose_m2278286083_MethodInfo_var);
		Action_t437523947 * L_7 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, __this, L_6, /*hidden argument*/NULL);
		FuseSDK_add_AdWillClose_m1177472552(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)SDKManager_FuseSDK_RewardedAdCompletedWithObject_m1785257430_MethodInfo_var);
		Action_1_t978424956 * L_9 = (Action_1_t978424956 *)il2cpp_codegen_object_new(Action_1_t978424956_il2cpp_TypeInfo_var);
		Action_1__ctor_m3714754526(L_9, __this, L_8, /*hidden argument*/Action_1__ctor_m3714754526_MethodInfo_var);
		FuseSDK_add_RewardedAdCompletedWithObject_m1341894586(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SDKManager::onDestroy()
extern TypeInfo* Action_t437523947_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t592684423_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t3211021459_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t978424956_il2cpp_TypeInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_SessionStartReceived_m1241951151_MethodInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_SessionLoginError_m1624667535_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2736694297_MethodInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_AdAvailabilityResponse_m1213840872_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m2591975409_MethodInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_AdWillClose_m2278286083_MethodInfo_var;
extern const MethodInfo* SDKManager_FuseSDK_RewardedAdCompletedWithObject_m1785257430_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3714754526_MethodInfo_var;
extern const uint32_t SDKManager_onDestroy_m904393953_MetadataUsageId;
extern "C"  void SDKManager_onDestroy_m904393953 (SDKManager_t2583098931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_onDestroy_m904393953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)SDKManager_FuseSDK_SessionStartReceived_m1241951151_MethodInfo_var);
		Action_t437523947 * L_1 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_remove_SessionStartReceived_m2813762949(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)SDKManager_FuseSDK_SessionLoginError_m1624667535_MethodInfo_var);
		Action_1_t592684423 * L_3 = (Action_1_t592684423 *)il2cpp_codegen_object_new(Action_1_t592684423_il2cpp_TypeInfo_var);
		Action_1__ctor_m2736694297(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m2736694297_MethodInfo_var);
		FuseSDK_remove_SessionLoginError_m1248891620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)SDKManager_FuseSDK_AdAvailabilityResponse_m1213840872_MethodInfo_var);
		Action_2_t3211021459 * L_5 = (Action_2_t3211021459 *)il2cpp_codegen_object_new(Action_2_t3211021459_il2cpp_TypeInfo_var);
		Action_2__ctor_m2591975409(L_5, __this, L_4, /*hidden argument*/Action_2__ctor_m2591975409_MethodInfo_var);
		FuseSDK_remove_AdAvailabilityResponse_m2631025344(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)SDKManager_FuseSDK_AdWillClose_m2278286083_MethodInfo_var);
		Action_t437523947 * L_7 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, __this, L_6, /*hidden argument*/NULL);
		FuseSDK_remove_AdWillClose_m2705220131(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)SDKManager_FuseSDK_RewardedAdCompletedWithObject_m1785257430_MethodInfo_var);
		Action_1_t978424956 * L_9 = (Action_1_t978424956 *)il2cpp_codegen_object_new(Action_1_t978424956_il2cpp_TypeInfo_var);
		Action_1__ctor_m3714754526(L_9, __this, L_8, /*hidden argument*/Action_1__ctor_m3714754526_MethodInfo_var);
		FuseSDK_remove_RewardedAdCompletedWithObject_m534351071(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SDKManager::FuseSDK_SessionStartReceived()
extern Il2CppCodeGenString* _stringLiteral1551054601;
extern const uint32_t SDKManager_FuseSDK_SessionStartReceived_m1241951151_MetadataUsageId;
extern "C"  void SDKManager_FuseSDK_SessionStartReceived_m1241951151 (SDKManager_t2583098931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_FuseSDK_SessionStartReceived_m1241951151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SDKManager_printLog_m2701162351(__this, _stringLiteral1551054601, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SDKManager::FuseSDK_SessionLoginError(FuseMisc.FuseError)
extern TypeInfo* FuseError_t444231718_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3776846093;
extern const uint32_t SDKManager_FuseSDK_SessionLoginError_m1624667535_MetadataUsageId;
extern "C"  void SDKManager_FuseSDK_SessionLoginError_m1624667535 (SDKManager_t2583098931 * __this, int32_t ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_FuseSDK_SessionLoginError_m1624667535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___obj;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(FuseError_t444231718_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2778772662 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2778772662 *)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3776846093, L_3, /*hidden argument*/NULL);
		SDKManager_printLog_m2701162351(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SDKManager::PreloadAd()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral311902090;
extern const uint32_t SDKManager_PreloadAd_m4269218674_MetadataUsageId;
extern "C"  void SDKManager_PreloadAd_m4269218674 (SDKManager_t2583098931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_PreloadAd_m4269218674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_ZoneID_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral311902090, L_0, /*hidden argument*/NULL);
		SDKManager_printLog_m2701162351(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_ZoneID_2();
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_PreloadAdForZoneID_m3995970508(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SDKManager::FuseSDK_AdAvailabilityResponse(System.Boolean,FuseMisc.FuseError)
extern Il2CppCodeGenString* _stringLiteral4065500940;
extern Il2CppCodeGenString* _stringLiteral4267223398;
extern const uint32_t SDKManager_FuseSDK_AdAvailabilityResponse_m1213840872_MetadataUsageId;
extern "C"  void SDKManager_FuseSDK_AdAvailabilityResponse_m1213840872 (SDKManager_t2583098931 * __this, bool ___adAvailable, int32_t ___error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_FuseSDK_AdAvailabilityResponse_m1213840872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___adAvailable;
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		SDKManager_printLog_m2701162351(__this, _stringLiteral4065500940, /*hidden argument*/NULL);
		__this->set_inLoop_5((bool)0);
		SDKManager_ShowAdForZoneID_m2237971062(__this, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_0023:
	{
		bool L_1 = __this->get_inLoop_5();
		if (!L_1)
		{
			goto IL_003f;
		}
	}
	{
		SDKManager_printLog_m2701162351(__this, _stringLiteral4267223398, /*hidden argument*/NULL);
		SDKManager_PreloadAd_m4269218674(__this, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void SDKManager::ShowAdForZoneID()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FuseSDK_t1159654649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral779651798;
extern const uint32_t SDKManager_ShowAdForZoneID_m2237971062_MetadataUsageId;
extern "C"  void SDKManager_ShowAdForZoneID_m2237971062 (SDKManager_t2583098931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_ShowAdForZoneID_m2237971062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_ZoneID_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral779651798, L_0, /*hidden argument*/NULL);
		SDKManager_printLog_m2701162351(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_ZoneID_2();
		IL2CPP_RUNTIME_CLASS_INIT(FuseSDK_t1159654649_il2cpp_TypeInfo_var);
		FuseSDK_ShowAdForZoneID_m620550069(NULL /*static, unused*/, L_2, (Dictionary_2_t2606186806 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SDKManager::FuseSDK_AdWillClose()
extern Il2CppCodeGenString* _stringLiteral1625587751;
extern const uint32_t SDKManager_FuseSDK_AdWillClose_m2278286083_MetadataUsageId;
extern "C"  void SDKManager_FuseSDK_AdWillClose_m2278286083 (SDKManager_t2583098931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_FuseSDK_AdWillClose_m2278286083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SDKManager_printLog_m2701162351(__this, _stringLiteral1625587751, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SDKManager::FuseSDK_RewardedAdCompletedWithObject(FuseMisc.RewardedInfo)
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2154874614;
extern Il2CppCodeGenString* _stringLiteral1362864247;
extern Il2CppCodeGenString* _stringLiteral2980739762;
extern Il2CppCodeGenString* _stringLiteral1507326356;
extern const uint32_t SDKManager_FuseSDK_RewardedAdCompletedWithObject_m1785257430_MetadataUsageId;
extern "C"  void SDKManager_FuseSDK_RewardedAdCompletedWithObject_m1785257430 (SDKManager_t2583098931 * __this, RewardedInfo_t829972251  ___rewardedInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_FuseSDK_RewardedAdCompletedWithObject_m1785257430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2154874614);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2154874614);
		ObjectU5BU5D_t11523773* L_1 = L_0;
		String_t* L_2 = (&___rewardedInfo)->get_PreRollMessage_0();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_2);
		ObjectU5BU5D_t11523773* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral1362864247);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1362864247);
		ObjectU5BU5D_t11523773* L_4 = L_3;
		int32_t L_5 = (&___rewardedInfo)->get_RewardAmount_4();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = L_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral2980739762);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2980739762);
		ObjectU5BU5D_t11523773* L_9 = L_8;
		String_t* L_10 = (&___rewardedInfo)->get_RewardItem_2();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 5);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_10);
		ObjectU5BU5D_t11523773* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, _stringLiteral1507326356);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral1507326356);
		ObjectU5BU5D_t11523773* L_12 = L_11;
		String_t* L_13 = (&___rewardedInfo)->get_RewardMessage_1();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		SDKManager_printLog_m2701162351(__this, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SDKManager::OnGUI()
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2030513044;
extern Il2CppCodeGenString* _stringLiteral3557334392;
extern Il2CppCodeGenString* _stringLiteral2682510823;
extern Il2CppCodeGenString* _stringLiteral3040652834;
extern Il2CppCodeGenString* _stringLiteral83935637;
extern const uint32_t SDKManager_OnGUI_m4124528834_MetadataUsageId;
extern "C"  void SDKManager_OnGUI_m4124528834 (SDKManager_t2583098931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_OnGUI_m4124528834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m3291325233(&L_2, (0.0f), (0.0f), (((float)((float)L_0))), (((float)((float)L_1))), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_logs_3();
		GUIStyle_t1006925219 * L_4 = __this->get_gs_4();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_Label_m4283747336(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		bool L_5 = __this->get_inLoop_5();
		if (L_5)
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_6 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Rect__ctor_m3291325233(&L_7, (((float)((float)((int32_t)((int32_t)L_6-(int32_t)((int32_t)120)))))), (10.0f), (100.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_8 = GUI_Button_m885093907(NULL /*static, unused*/, L_7, _stringLiteral2030513044, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_9 = __this->get_ZoneID_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3557334392, L_9, /*hidden argument*/NULL);
		SDKManager_printLog_m2701162351(__this, L_10, /*hidden argument*/NULL);
		__this->set_inLoop_5((bool)1);
		String_t* L_11 = __this->get_ZoneID_2();
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral2682510823, L_11, /*hidden argument*/NULL);
		PlayerPrefs_Save_m3891538519(NULL /*static, unused*/, /*hidden argument*/NULL);
		SDKManager_PreloadAd_m4269218674(__this, /*hidden argument*/NULL);
	}

IL_009b:
	{
		goto IL_00de;
	}

IL_00a0:
	{
		int32_t L_12 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Rect__ctor_m3291325233(&L_13, (((float)((float)((int32_t)((int32_t)L_12-(int32_t)((int32_t)120)))))), (10.0f), (100.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_14 = GUI_Button_m885093907(NULL /*static, unused*/, L_13, _stringLiteral3040652834, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00de;
		}
	}
	{
		SDKManager_printLog_m2701162351(__this, _stringLiteral83935637, /*hidden argument*/NULL);
		__this->set_inLoop_5((bool)0);
	}

IL_00de:
	{
		int32_t L_15 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Rect__ctor_m3291325233(&L_16, (((float)((float)((int32_t)((int32_t)L_15-(int32_t)((int32_t)300)))))), (10.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		String_t* L_17 = __this->get_ZoneID_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		String_t* L_18 = GUI_TextField_m3177770189(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		__this->set_ZoneID_2(L_18);
		return;
	}
}
// System.Void SDKManager::printLog(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral11564;
extern Il2CppCodeGenString* _stringLiteral1131022473;
extern const uint32_t SDKManager_printLog_m2701162351_MetadataUsageId;
extern "C"  void SDKManager_printLog_m2701162351 (SDKManager_t2583098931 * __this, String_t* ___log, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SDKManager_printLog_m2701162351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_logs_3();
		String_t* L_1 = ___log;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, L_0, _stringLiteral11564, L_1, /*hidden argument*/NULL);
		__this->set_logs_3(L_2);
		String_t* L_3 = __this->get_logs_3();
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)10000))))
		{
			goto IL_004e;
		}
	}
	{
		String_t* L_5 = __this->get_logs_3();
		String_t* L_6 = __this->get_logs_3();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m2979997331(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_8 = String_Substring_m2809233063(L_5, ((int32_t)((int32_t)L_7-(int32_t)((int32_t)1000))), /*hidden argument*/NULL);
		__this->set_logs_3(L_8);
	}

IL_004e:
	{
		String_t* L_9 = ___log;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1131022473, L_9, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
