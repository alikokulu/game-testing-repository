﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3981798581.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_FuseMisc_Product4074308078.h"

// System.Void System.Array/InternalEnumerator`1<FuseMisc.Product>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4158687871_gshared (InternalEnumerator_1_t3981798581 * __this, Il2CppArray * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4158687871(__this, ___array, method) ((  void (*) (InternalEnumerator_1_t3981798581 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4158687871_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<FuseMisc.Product>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2403779777_gshared (InternalEnumerator_1_t3981798581 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2403779777(__this, method) ((  void (*) (InternalEnumerator_1_t3981798581 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2403779777_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<FuseMisc.Product>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2394754093_gshared (InternalEnumerator_1_t3981798581 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2394754093(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3981798581 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2394754093_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<FuseMisc.Product>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3545388822_gshared (InternalEnumerator_1_t3981798581 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3545388822(__this, method) ((  void (*) (InternalEnumerator_1_t3981798581 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3545388822_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<FuseMisc.Product>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m778661421_gshared (InternalEnumerator_1_t3981798581 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m778661421(__this, method) ((  bool (*) (InternalEnumerator_1_t3981798581 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m778661421_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<FuseMisc.Product>::get_Current()
extern "C"  Product_t4074308078  InternalEnumerator_1_get_Current_m1737925190_gshared (InternalEnumerator_1_t3981798581 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1737925190(__this, method) ((  Product_t4074308078  (*) (InternalEnumerator_1_t3981798581 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1737925190_gshared)(__this, method)
