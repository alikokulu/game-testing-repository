﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuseSDK_Unibill_iOS
struct FuseSDK_Unibill_iOS_t2534778463;

#include "codegen/il2cpp-codegen.h"

// System.Void FuseSDK_Unibill_iOS::.ctor()
extern "C"  void FuseSDK_Unibill_iOS__ctor_m2034509804 (FuseSDK_Unibill_iOS_t2534778463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
