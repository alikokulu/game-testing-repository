﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseSDK/<SetupPushNotifications>c__Iterator0
struct  U3CSetupPushNotificationsU3Ec__Iterator0_t552020816  : public Il2CppObject
{
public:
	// System.Byte[] FuseSDK/<SetupPushNotifications>c__Iterator0::<token>__0
	ByteU5BU5D_t58506160* ___U3CtokenU3E__0_0;
	// System.String FuseSDK/<SetupPushNotifications>c__Iterator0::<error>__1
	String_t* ___U3CerrorU3E__1_1;
	// System.Int32 FuseSDK/<SetupPushNotifications>c__Iterator0::$PC
	int32_t ___U24PC_2;
	// System.Object FuseSDK/<SetupPushNotifications>c__Iterator0::$current
	Il2CppObject * ___U24current_3;

public:
	inline static int32_t get_offset_of_U3CtokenU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSetupPushNotificationsU3Ec__Iterator0_t552020816, ___U3CtokenU3E__0_0)); }
	inline ByteU5BU5D_t58506160* get_U3CtokenU3E__0_0() const { return ___U3CtokenU3E__0_0; }
	inline ByteU5BU5D_t58506160** get_address_of_U3CtokenU3E__0_0() { return &___U3CtokenU3E__0_0; }
	inline void set_U3CtokenU3E__0_0(ByteU5BU5D_t58506160* value)
	{
		___U3CtokenU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtokenU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSetupPushNotificationsU3Ec__Iterator0_t552020816, ___U3CerrorU3E__1_1)); }
	inline String_t* get_U3CerrorU3E__1_1() const { return ___U3CerrorU3E__1_1; }
	inline String_t** get_address_of_U3CerrorU3E__1_1() { return &___U3CerrorU3E__1_1; }
	inline void set_U3CerrorU3E__1_1(String_t* value)
	{
		___U3CerrorU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CSetupPushNotificationsU3Ec__Iterator0_t552020816, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CSetupPushNotificationsU3Ec__Iterator0_t552020816, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
