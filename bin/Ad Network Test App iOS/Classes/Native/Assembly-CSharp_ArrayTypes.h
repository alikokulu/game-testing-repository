﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// JSONObject
struct JSONObject_t1752376903;

#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_FuseMisc_Friend536712157.h"
#include "AssemblyU2DCSharp_FuseMisc_Product4074308078.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"

#pragma once
// FuseMisc.Friend[]
struct FriendU5BU5D_t1733384208  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Friend_t536712157  m_Items[1];

public:
	inline Friend_t536712157  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Friend_t536712157 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Friend_t536712157  value)
	{
		m_Items[index] = value;
	}
};
// FuseMisc.Product[]
struct ProductU5BU5D_t4249436827  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Product_t4074308078  m_Items[1];

public:
	inline Product_t4074308078  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Product_t4074308078 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Product_t4074308078  value)
	{
		m_Items[index] = value;
	}
};
// JSONObject[]
struct JSONObjectU5BU5D_t1578151102  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) JSONObject_t1752376903 * m_Items[1];

public:
	inline JSONObject_t1752376903 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSONObject_t1752376903 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSONObject_t1752376903 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
