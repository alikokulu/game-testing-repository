﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseMisc.Friend
struct  Friend_t536712157 
{
public:
	// System.String FuseMisc.Friend::FuseId
	String_t* ___FuseId_0;
	// System.String FuseMisc.Friend::AccountId
	String_t* ___AccountId_1;
	// System.String FuseMisc.Friend::Alias
	String_t* ___Alias_2;
	// System.Boolean FuseMisc.Friend::Pending
	bool ___Pending_3;

public:
	inline static int32_t get_offset_of_FuseId_0() { return static_cast<int32_t>(offsetof(Friend_t536712157, ___FuseId_0)); }
	inline String_t* get_FuseId_0() const { return ___FuseId_0; }
	inline String_t** get_address_of_FuseId_0() { return &___FuseId_0; }
	inline void set_FuseId_0(String_t* value)
	{
		___FuseId_0 = value;
		Il2CppCodeGenWriteBarrier(&___FuseId_0, value);
	}

	inline static int32_t get_offset_of_AccountId_1() { return static_cast<int32_t>(offsetof(Friend_t536712157, ___AccountId_1)); }
	inline String_t* get_AccountId_1() const { return ___AccountId_1; }
	inline String_t** get_address_of_AccountId_1() { return &___AccountId_1; }
	inline void set_AccountId_1(String_t* value)
	{
		___AccountId_1 = value;
		Il2CppCodeGenWriteBarrier(&___AccountId_1, value);
	}

	inline static int32_t get_offset_of_Alias_2() { return static_cast<int32_t>(offsetof(Friend_t536712157, ___Alias_2)); }
	inline String_t* get_Alias_2() const { return ___Alias_2; }
	inline String_t** get_address_of_Alias_2() { return &___Alias_2; }
	inline void set_Alias_2(String_t* value)
	{
		___Alias_2 = value;
		Il2CppCodeGenWriteBarrier(&___Alias_2, value);
	}

	inline static int32_t get_offset_of_Pending_3() { return static_cast<int32_t>(offsetof(Friend_t536712157, ___Pending_3)); }
	inline bool get_Pending_3() const { return ___Pending_3; }
	inline bool* get_address_of_Pending_3() { return &___Pending_3; }
	inline void set_Pending_3(bool value)
	{
		___Pending_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: FuseMisc.Friend
struct Friend_t536712157_marshaled
{
	char* ___FuseId_0;
	char* ___AccountId_1;
	char* ___Alias_2;
	int32_t ___Pending_3;
};
