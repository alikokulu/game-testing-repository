﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1006925219;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SDKManager
struct  SDKManager_t2583098931  : public MonoBehaviour_t3012272455
{
public:
	// System.String SDKManager::ZoneID
	String_t* ___ZoneID_2;
	// System.String SDKManager::logs
	String_t* ___logs_3;
	// UnityEngine.GUIStyle SDKManager::gs
	GUIStyle_t1006925219 * ___gs_4;
	// System.Boolean SDKManager::inLoop
	bool ___inLoop_5;

public:
	inline static int32_t get_offset_of_ZoneID_2() { return static_cast<int32_t>(offsetof(SDKManager_t2583098931, ___ZoneID_2)); }
	inline String_t* get_ZoneID_2() const { return ___ZoneID_2; }
	inline String_t** get_address_of_ZoneID_2() { return &___ZoneID_2; }
	inline void set_ZoneID_2(String_t* value)
	{
		___ZoneID_2 = value;
		Il2CppCodeGenWriteBarrier(&___ZoneID_2, value);
	}

	inline static int32_t get_offset_of_logs_3() { return static_cast<int32_t>(offsetof(SDKManager_t2583098931, ___logs_3)); }
	inline String_t* get_logs_3() const { return ___logs_3; }
	inline String_t** get_address_of_logs_3() { return &___logs_3; }
	inline void set_logs_3(String_t* value)
	{
		___logs_3 = value;
		Il2CppCodeGenWriteBarrier(&___logs_3, value);
	}

	inline static int32_t get_offset_of_gs_4() { return static_cast<int32_t>(offsetof(SDKManager_t2583098931, ___gs_4)); }
	inline GUIStyle_t1006925219 * get_gs_4() const { return ___gs_4; }
	inline GUIStyle_t1006925219 ** get_address_of_gs_4() { return &___gs_4; }
	inline void set_gs_4(GUIStyle_t1006925219 * value)
	{
		___gs_4 = value;
		Il2CppCodeGenWriteBarrier(&___gs_4, value);
	}

	inline static int32_t get_offset_of_inLoop_5() { return static_cast<int32_t>(offsetof(SDKManager_t2583098931, ___inLoop_5)); }
	inline bool get_inLoop_5() const { return ___inLoop_5; }
	inline bool* get_address_of_inLoop_5() { return &___inLoop_5; }
	inline void set_inLoop_5(bool value)
	{
		___inLoop_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
