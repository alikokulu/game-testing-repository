﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseMisc.Product
struct  Product_t4074308078 
{
public:
	// System.String FuseMisc.Product::ProductId
	String_t* ___ProductId_0;
	// System.Single FuseMisc.Product::Price
	float ___Price_1;
	// System.String FuseMisc.Product::PriceLocale
	String_t* ___PriceLocale_2;

public:
	inline static int32_t get_offset_of_ProductId_0() { return static_cast<int32_t>(offsetof(Product_t4074308078, ___ProductId_0)); }
	inline String_t* get_ProductId_0() const { return ___ProductId_0; }
	inline String_t** get_address_of_ProductId_0() { return &___ProductId_0; }
	inline void set_ProductId_0(String_t* value)
	{
		___ProductId_0 = value;
		Il2CppCodeGenWriteBarrier(&___ProductId_0, value);
	}

	inline static int32_t get_offset_of_Price_1() { return static_cast<int32_t>(offsetof(Product_t4074308078, ___Price_1)); }
	inline float get_Price_1() const { return ___Price_1; }
	inline float* get_address_of_Price_1() { return &___Price_1; }
	inline void set_Price_1(float value)
	{
		___Price_1 = value;
	}

	inline static int32_t get_offset_of_PriceLocale_2() { return static_cast<int32_t>(offsetof(Product_t4074308078, ___PriceLocale_2)); }
	inline String_t* get_PriceLocale_2() const { return ___PriceLocale_2; }
	inline String_t** get_address_of_PriceLocale_2() { return &___PriceLocale_2; }
	inline void set_PriceLocale_2(String_t* value)
	{
		___PriceLocale_2 = value;
		Il2CppCodeGenWriteBarrier(&___PriceLocale_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: FuseMisc.Product
struct Product_t4074308078_marshaled
{
	char* ___ProductId_0;
	float ___Price_1;
	char* ___PriceLocale_2;
};
