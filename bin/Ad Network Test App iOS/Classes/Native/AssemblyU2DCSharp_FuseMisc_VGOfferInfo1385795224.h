﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseMisc.VGOfferInfo
struct  VGOfferInfo_t1385795224 
{
public:
	// System.Int32 FuseMisc.VGOfferInfo::CurrencyID
	int32_t ___CurrencyID_0;
	// System.String FuseMisc.VGOfferInfo::PurchaseCurrency
	String_t* ___PurchaseCurrency_1;
	// System.Single FuseMisc.VGOfferInfo::PurchasePrice
	float ___PurchasePrice_2;
	// System.Int32 FuseMisc.VGOfferInfo::VirtualGoodID
	int32_t ___VirtualGoodID_3;
	// System.String FuseMisc.VGOfferInfo::ItemName
	String_t* ___ItemName_4;
	// System.Int32 FuseMisc.VGOfferInfo::ItemAmount
	int32_t ___ItemAmount_5;
	// System.DateTime FuseMisc.VGOfferInfo::StartTime
	DateTime_t339033936  ___StartTime_6;
	// System.DateTime FuseMisc.VGOfferInfo::EndTime
	DateTime_t339033936  ___EndTime_7;
	// System.String FuseMisc.VGOfferInfo::Metadata
	String_t* ___Metadata_8;

public:
	inline static int32_t get_offset_of_CurrencyID_0() { return static_cast<int32_t>(offsetof(VGOfferInfo_t1385795224, ___CurrencyID_0)); }
	inline int32_t get_CurrencyID_0() const { return ___CurrencyID_0; }
	inline int32_t* get_address_of_CurrencyID_0() { return &___CurrencyID_0; }
	inline void set_CurrencyID_0(int32_t value)
	{
		___CurrencyID_0 = value;
	}

	inline static int32_t get_offset_of_PurchaseCurrency_1() { return static_cast<int32_t>(offsetof(VGOfferInfo_t1385795224, ___PurchaseCurrency_1)); }
	inline String_t* get_PurchaseCurrency_1() const { return ___PurchaseCurrency_1; }
	inline String_t** get_address_of_PurchaseCurrency_1() { return &___PurchaseCurrency_1; }
	inline void set_PurchaseCurrency_1(String_t* value)
	{
		___PurchaseCurrency_1 = value;
		Il2CppCodeGenWriteBarrier(&___PurchaseCurrency_1, value);
	}

	inline static int32_t get_offset_of_PurchasePrice_2() { return static_cast<int32_t>(offsetof(VGOfferInfo_t1385795224, ___PurchasePrice_2)); }
	inline float get_PurchasePrice_2() const { return ___PurchasePrice_2; }
	inline float* get_address_of_PurchasePrice_2() { return &___PurchasePrice_2; }
	inline void set_PurchasePrice_2(float value)
	{
		___PurchasePrice_2 = value;
	}

	inline static int32_t get_offset_of_VirtualGoodID_3() { return static_cast<int32_t>(offsetof(VGOfferInfo_t1385795224, ___VirtualGoodID_3)); }
	inline int32_t get_VirtualGoodID_3() const { return ___VirtualGoodID_3; }
	inline int32_t* get_address_of_VirtualGoodID_3() { return &___VirtualGoodID_3; }
	inline void set_VirtualGoodID_3(int32_t value)
	{
		___VirtualGoodID_3 = value;
	}

	inline static int32_t get_offset_of_ItemName_4() { return static_cast<int32_t>(offsetof(VGOfferInfo_t1385795224, ___ItemName_4)); }
	inline String_t* get_ItemName_4() const { return ___ItemName_4; }
	inline String_t** get_address_of_ItemName_4() { return &___ItemName_4; }
	inline void set_ItemName_4(String_t* value)
	{
		___ItemName_4 = value;
		Il2CppCodeGenWriteBarrier(&___ItemName_4, value);
	}

	inline static int32_t get_offset_of_ItemAmount_5() { return static_cast<int32_t>(offsetof(VGOfferInfo_t1385795224, ___ItemAmount_5)); }
	inline int32_t get_ItemAmount_5() const { return ___ItemAmount_5; }
	inline int32_t* get_address_of_ItemAmount_5() { return &___ItemAmount_5; }
	inline void set_ItemAmount_5(int32_t value)
	{
		___ItemAmount_5 = value;
	}

	inline static int32_t get_offset_of_StartTime_6() { return static_cast<int32_t>(offsetof(VGOfferInfo_t1385795224, ___StartTime_6)); }
	inline DateTime_t339033936  get_StartTime_6() const { return ___StartTime_6; }
	inline DateTime_t339033936 * get_address_of_StartTime_6() { return &___StartTime_6; }
	inline void set_StartTime_6(DateTime_t339033936  value)
	{
		___StartTime_6 = value;
	}

	inline static int32_t get_offset_of_EndTime_7() { return static_cast<int32_t>(offsetof(VGOfferInfo_t1385795224, ___EndTime_7)); }
	inline DateTime_t339033936  get_EndTime_7() const { return ___EndTime_7; }
	inline DateTime_t339033936 * get_address_of_EndTime_7() { return &___EndTime_7; }
	inline void set_EndTime_7(DateTime_t339033936  value)
	{
		___EndTime_7 = value;
	}

	inline static int32_t get_offset_of_Metadata_8() { return static_cast<int32_t>(offsetof(VGOfferInfo_t1385795224, ___Metadata_8)); }
	inline String_t* get_Metadata_8() const { return ___Metadata_8; }
	inline String_t** get_address_of_Metadata_8() { return &___Metadata_8; }
	inline void set_Metadata_8(String_t* value)
	{
		___Metadata_8 = value;
		Il2CppCodeGenWriteBarrier(&___Metadata_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
