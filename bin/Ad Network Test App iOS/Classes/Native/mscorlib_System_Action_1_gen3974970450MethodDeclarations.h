﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<FuseMisc.IAPOfferInfo>
struct Action_1_t3974970450;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPOfferInfo3826517745.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<FuseMisc.IAPOfferInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2984012305_gshared (Action_1_t3974970450 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Action_1__ctor_m2984012305(__this, ___object, ___method, method) ((  void (*) (Action_1_t3974970450 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2984012305_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<FuseMisc.IAPOfferInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m3001813907_gshared (Action_1_t3974970450 * __this, IAPOfferInfo_t3826517745  ___obj, const MethodInfo* method);
#define Action_1_Invoke_m3001813907(__this, ___obj, method) ((  void (*) (Action_1_t3974970450 *, IAPOfferInfo_t3826517745 , const MethodInfo*))Action_1_Invoke_m3001813907_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<FuseMisc.IAPOfferInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3243595488_gshared (Action_1_t3974970450 * __this, IAPOfferInfo_t3826517745  ___obj, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method);
#define Action_1_BeginInvoke_m3243595488(__this, ___obj, ___callback, ___object, method) ((  Il2CppObject * (*) (Action_1_t3974970450 *, IAPOfferInfo_t3826517745 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m3243595488_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<FuseMisc.IAPOfferInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2463206561_gshared (Action_1_t3974970450 * __this, Il2CppObject * ___result, const MethodInfo* method);
#define Action_1_EndInvoke_m2463206561(__this, ___result, method) ((  void (*) (Action_1_t3974970450 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m2463206561_gshared)(__this, ___result, method)
