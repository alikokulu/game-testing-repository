﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void FuseMisc.FuseExtensions::.cctor()
extern "C"  void FuseExtensions__cctor_m635126936 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 FuseMisc.FuseExtensions::ToUnixTimestamp(System.DateTime)
extern "C"  int64_t FuseExtensions_ToUnixTimestamp_m1753829763 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dateTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime FuseMisc.FuseExtensions::ToDateTime(System.Int64)
extern "C"  DateTime_t339033936  FuseExtensions_ToDateTime_m670214590 (Il2CppObject * __this /* static, unused */, int64_t ___timestamp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
