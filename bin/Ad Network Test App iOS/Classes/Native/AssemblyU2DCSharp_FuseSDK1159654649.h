﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// FuseSDK
struct FuseSDK_t1159654649;
// System.Action`1<System.String>
struct Action_1_t1116941607;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.List`1<FuseMisc.Friend>
struct List_1_t1333671126;
// System.Action
struct Action_t437523947;
// System.Action`1<FuseMisc.FuseError>
struct Action_1_t592684423;
// System.Action`2<FuseMisc.AccountType,System.String>
struct Action_2_t535153670;
// System.Action`2<System.String,FuseMisc.FuseError>
struct Action_2_t2362964390;
// System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>
struct Action_1_t1482123831;
// System.Action`3<System.Int32,System.String,System.String>
struct Action_3_t3047918030;
// System.Action`2<System.Boolean,FuseMisc.FuseError>
struct Action_2_t3211021459;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t3619260338;
// System.Action`1<FuseMisc.RewardedInfo>
struct Action_1_t978424956;
// System.Action`1<FuseMisc.IAPOfferInfo>
struct Action_1_t3974970450;
// System.Action`1<FuseMisc.VGOfferInfo>
struct Action_1_t1534247929;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// System.Action`2<System.Int32,FuseMisc.FuseError>
struct Action_2_t1216077269;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Action`4<System.Int32,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Action_4_t4157318299;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseSDK
struct  FuseSDK_t1159654649  : public MonoBehaviour_t3012272455
{
public:
	// System.String FuseSDK::AndroidAppID
	String_t* ___AndroidAppID_2;
	// System.String FuseSDK::iOSAppID
	String_t* ___iOSAppID_3;
	// System.Boolean FuseSDK::StartAutomatically
	bool ___StartAutomatically_4;
	// System.String FuseSDK::GCM_SenderID
	String_t* ___GCM_SenderID_5;
	// System.Boolean FuseSDK::registerForPushNotifications
	bool ___registerForPushNotifications_6;
	// System.Boolean FuseSDK::logging
	bool ___logging_7;
	// System.Boolean FuseSDK::editorSessions
	bool ___editorSessions_8;
	// System.Boolean FuseSDK::standaloneSessions
	bool ___standaloneSessions_9;
	// System.Boolean FuseSDK::androidIAB
	bool ___androidIAB_10;
	// System.Boolean FuseSDK::androidUnibill
	bool ___androidUnibill_11;
	// System.Boolean FuseSDK::iosStoreKit
	bool ___iosStoreKit_12;
	// System.Boolean FuseSDK::iosUnibill
	bool ___iosUnibill_13;
	// System.Boolean FuseSDK::soomlaStore
	bool ___soomlaStore_14;

public:
	inline static int32_t get_offset_of_AndroidAppID_2() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___AndroidAppID_2)); }
	inline String_t* get_AndroidAppID_2() const { return ___AndroidAppID_2; }
	inline String_t** get_address_of_AndroidAppID_2() { return &___AndroidAppID_2; }
	inline void set_AndroidAppID_2(String_t* value)
	{
		___AndroidAppID_2 = value;
		Il2CppCodeGenWriteBarrier(&___AndroidAppID_2, value);
	}

	inline static int32_t get_offset_of_iOSAppID_3() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___iOSAppID_3)); }
	inline String_t* get_iOSAppID_3() const { return ___iOSAppID_3; }
	inline String_t** get_address_of_iOSAppID_3() { return &___iOSAppID_3; }
	inline void set_iOSAppID_3(String_t* value)
	{
		___iOSAppID_3 = value;
		Il2CppCodeGenWriteBarrier(&___iOSAppID_3, value);
	}

	inline static int32_t get_offset_of_StartAutomatically_4() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___StartAutomatically_4)); }
	inline bool get_StartAutomatically_4() const { return ___StartAutomatically_4; }
	inline bool* get_address_of_StartAutomatically_4() { return &___StartAutomatically_4; }
	inline void set_StartAutomatically_4(bool value)
	{
		___StartAutomatically_4 = value;
	}

	inline static int32_t get_offset_of_GCM_SenderID_5() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___GCM_SenderID_5)); }
	inline String_t* get_GCM_SenderID_5() const { return ___GCM_SenderID_5; }
	inline String_t** get_address_of_GCM_SenderID_5() { return &___GCM_SenderID_5; }
	inline void set_GCM_SenderID_5(String_t* value)
	{
		___GCM_SenderID_5 = value;
		Il2CppCodeGenWriteBarrier(&___GCM_SenderID_5, value);
	}

	inline static int32_t get_offset_of_registerForPushNotifications_6() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___registerForPushNotifications_6)); }
	inline bool get_registerForPushNotifications_6() const { return ___registerForPushNotifications_6; }
	inline bool* get_address_of_registerForPushNotifications_6() { return &___registerForPushNotifications_6; }
	inline void set_registerForPushNotifications_6(bool value)
	{
		___registerForPushNotifications_6 = value;
	}

	inline static int32_t get_offset_of_logging_7() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___logging_7)); }
	inline bool get_logging_7() const { return ___logging_7; }
	inline bool* get_address_of_logging_7() { return &___logging_7; }
	inline void set_logging_7(bool value)
	{
		___logging_7 = value;
	}

	inline static int32_t get_offset_of_editorSessions_8() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___editorSessions_8)); }
	inline bool get_editorSessions_8() const { return ___editorSessions_8; }
	inline bool* get_address_of_editorSessions_8() { return &___editorSessions_8; }
	inline void set_editorSessions_8(bool value)
	{
		___editorSessions_8 = value;
	}

	inline static int32_t get_offset_of_standaloneSessions_9() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___standaloneSessions_9)); }
	inline bool get_standaloneSessions_9() const { return ___standaloneSessions_9; }
	inline bool* get_address_of_standaloneSessions_9() { return &___standaloneSessions_9; }
	inline void set_standaloneSessions_9(bool value)
	{
		___standaloneSessions_9 = value;
	}

	inline static int32_t get_offset_of_androidIAB_10() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___androidIAB_10)); }
	inline bool get_androidIAB_10() const { return ___androidIAB_10; }
	inline bool* get_address_of_androidIAB_10() { return &___androidIAB_10; }
	inline void set_androidIAB_10(bool value)
	{
		___androidIAB_10 = value;
	}

	inline static int32_t get_offset_of_androidUnibill_11() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___androidUnibill_11)); }
	inline bool get_androidUnibill_11() const { return ___androidUnibill_11; }
	inline bool* get_address_of_androidUnibill_11() { return &___androidUnibill_11; }
	inline void set_androidUnibill_11(bool value)
	{
		___androidUnibill_11 = value;
	}

	inline static int32_t get_offset_of_iosStoreKit_12() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___iosStoreKit_12)); }
	inline bool get_iosStoreKit_12() const { return ___iosStoreKit_12; }
	inline bool* get_address_of_iosStoreKit_12() { return &___iosStoreKit_12; }
	inline void set_iosStoreKit_12(bool value)
	{
		___iosStoreKit_12 = value;
	}

	inline static int32_t get_offset_of_iosUnibill_13() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___iosUnibill_13)); }
	inline bool get_iosUnibill_13() const { return ___iosUnibill_13; }
	inline bool* get_address_of_iosUnibill_13() { return &___iosUnibill_13; }
	inline void set_iosUnibill_13(bool value)
	{
		___iosUnibill_13 = value;
	}

	inline static int32_t get_offset_of_soomlaStore_14() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649, ___soomlaStore_14)); }
	inline bool get_soomlaStore_14() const { return ___soomlaStore_14; }
	inline bool* get_address_of_soomlaStore_14() { return &___soomlaStore_14; }
	inline void set_soomlaStore_14(bool value)
	{
		___soomlaStore_14 = value;
	}
};

struct FuseSDK_t1159654649_StaticFields
{
public:
	// System.Boolean FuseSDK::_sessionStarted
	bool ____sessionStarted_15;
	// FuseSDK FuseSDK::_instance
	FuseSDK_t1159654649 * ____instance_16;
	// System.Action`1<System.String> FuseSDK::_adClickedwithURL
	Action_1_t1116941607 * ____adClickedwithURL_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> FuseSDK::_gameConfig
	Dictionary_2_t2606186806 * ____gameConfig_18;
	// System.Collections.Generic.List`1<FuseMisc.Friend> FuseSDK::_friendsList
	List_1_t1333671126 * ____friendsList_19;
	// System.Action FuseSDK::SessionStartReceived
	Action_t437523947 * ___SessionStartReceived_20;
	// System.Action`1<FuseMisc.FuseError> FuseSDK::SessionLoginError
	Action_1_t592684423 * ___SessionLoginError_21;
	// System.Action FuseSDK::GameConfigurationReceived
	Action_t437523947 * ___GameConfigurationReceived_22;
	// System.Action`2<FuseMisc.AccountType,System.String> FuseSDK::AccountLoginComplete
	Action_2_t535153670 * ___AccountLoginComplete_23;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDK::AccountLoginError
	Action_2_t2362964390 * ___AccountLoginError_24;
	// System.Action`1<System.String> FuseSDK::NotificationAction
	Action_1_t1116941607 * ___NotificationAction_25;
	// System.Action FuseSDK::NotificationWillClose
	Action_t437523947 * ___NotificationWillClose_26;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDK::FriendAdded
	Action_2_t2362964390 * ___FriendAdded_27;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDK::FriendRemoved
	Action_2_t2362964390 * ___FriendRemoved_28;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDK::FriendAccepted
	Action_2_t2362964390 * ___FriendAccepted_29;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDK::FriendRejected
	Action_2_t2362964390 * ___FriendRejected_30;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDK::FriendsMigrated
	Action_2_t2362964390 * ___FriendsMigrated_31;
	// System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>> FuseSDK::FriendsListUpdated
	Action_1_t1482123831 * ___FriendsListUpdated_32;
	// System.Action`1<FuseMisc.FuseError> FuseSDK::FriendsListError
	Action_1_t592684423 * ___FriendsListError_33;
	// System.Action`3<System.Int32,System.String,System.String> FuseSDK::PurchaseVerification
	Action_3_t3047918030 * ___PurchaseVerification_34;
	// System.Action`2<System.Boolean,FuseMisc.FuseError> FuseSDK::AdAvailabilityResponse
	Action_2_t3211021459 * ___AdAvailabilityResponse_35;
	// System.Action FuseSDK::AdWillClose
	Action_t437523947 * ___AdWillClose_36;
	// System.Action FuseSDK::AdFailedToDisplay
	Action_t437523947 * ___AdFailedToDisplay_37;
	// System.Action`2<System.Int32,System.Int32> FuseSDK::AdDidShow
	Action_2_t3619260338 * ___AdDidShow_38;
	// System.Action`1<FuseMisc.RewardedInfo> FuseSDK::RewardedAdCompletedWithObject
	Action_1_t978424956 * ___RewardedAdCompletedWithObject_39;
	// System.Action`1<FuseMisc.IAPOfferInfo> FuseSDK::IAPOfferAcceptedWithObject
	Action_1_t3974970450 * ___IAPOfferAcceptedWithObject_40;
	// System.Action`1<FuseMisc.VGOfferInfo> FuseSDK::VirtualGoodsOfferAcceptedWithObject
	Action_1_t1534247929 * ___VirtualGoodsOfferAcceptedWithObject_41;
	// System.Action`1<System.DateTime> FuseSDK::TimeUpdated
	Action_1_t487486641 * ___TimeUpdated_42;
	// System.Action`2<System.Int32,FuseMisc.FuseError> FuseSDK::GameDataError
	Action_2_t1216077269 * ___GameDataError_43;
	// System.Action`1<System.Int32> FuseSDK::GameDataSetAcknowledged
	Action_1_t2995867492 * ___GameDataSetAcknowledged_44;
	// System.Action`4<System.Int32,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>> FuseSDK::GameDataReceived
	Action_4_t4157318299 * ___GameDataReceived_45;

public:
	inline static int32_t get_offset_of__sessionStarted_15() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ____sessionStarted_15)); }
	inline bool get__sessionStarted_15() const { return ____sessionStarted_15; }
	inline bool* get_address_of__sessionStarted_15() { return &____sessionStarted_15; }
	inline void set__sessionStarted_15(bool value)
	{
		____sessionStarted_15 = value;
	}

	inline static int32_t get_offset_of__instance_16() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ____instance_16)); }
	inline FuseSDK_t1159654649 * get__instance_16() const { return ____instance_16; }
	inline FuseSDK_t1159654649 ** get_address_of__instance_16() { return &____instance_16; }
	inline void set__instance_16(FuseSDK_t1159654649 * value)
	{
		____instance_16 = value;
		Il2CppCodeGenWriteBarrier(&____instance_16, value);
	}

	inline static int32_t get_offset_of__adClickedwithURL_17() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ____adClickedwithURL_17)); }
	inline Action_1_t1116941607 * get__adClickedwithURL_17() const { return ____adClickedwithURL_17; }
	inline Action_1_t1116941607 ** get_address_of__adClickedwithURL_17() { return &____adClickedwithURL_17; }
	inline void set__adClickedwithURL_17(Action_1_t1116941607 * value)
	{
		____adClickedwithURL_17 = value;
		Il2CppCodeGenWriteBarrier(&____adClickedwithURL_17, value);
	}

	inline static int32_t get_offset_of__gameConfig_18() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ____gameConfig_18)); }
	inline Dictionary_2_t2606186806 * get__gameConfig_18() const { return ____gameConfig_18; }
	inline Dictionary_2_t2606186806 ** get_address_of__gameConfig_18() { return &____gameConfig_18; }
	inline void set__gameConfig_18(Dictionary_2_t2606186806 * value)
	{
		____gameConfig_18 = value;
		Il2CppCodeGenWriteBarrier(&____gameConfig_18, value);
	}

	inline static int32_t get_offset_of__friendsList_19() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ____friendsList_19)); }
	inline List_1_t1333671126 * get__friendsList_19() const { return ____friendsList_19; }
	inline List_1_t1333671126 ** get_address_of__friendsList_19() { return &____friendsList_19; }
	inline void set__friendsList_19(List_1_t1333671126 * value)
	{
		____friendsList_19 = value;
		Il2CppCodeGenWriteBarrier(&____friendsList_19, value);
	}

	inline static int32_t get_offset_of_SessionStartReceived_20() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___SessionStartReceived_20)); }
	inline Action_t437523947 * get_SessionStartReceived_20() const { return ___SessionStartReceived_20; }
	inline Action_t437523947 ** get_address_of_SessionStartReceived_20() { return &___SessionStartReceived_20; }
	inline void set_SessionStartReceived_20(Action_t437523947 * value)
	{
		___SessionStartReceived_20 = value;
		Il2CppCodeGenWriteBarrier(&___SessionStartReceived_20, value);
	}

	inline static int32_t get_offset_of_SessionLoginError_21() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___SessionLoginError_21)); }
	inline Action_1_t592684423 * get_SessionLoginError_21() const { return ___SessionLoginError_21; }
	inline Action_1_t592684423 ** get_address_of_SessionLoginError_21() { return &___SessionLoginError_21; }
	inline void set_SessionLoginError_21(Action_1_t592684423 * value)
	{
		___SessionLoginError_21 = value;
		Il2CppCodeGenWriteBarrier(&___SessionLoginError_21, value);
	}

	inline static int32_t get_offset_of_GameConfigurationReceived_22() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___GameConfigurationReceived_22)); }
	inline Action_t437523947 * get_GameConfigurationReceived_22() const { return ___GameConfigurationReceived_22; }
	inline Action_t437523947 ** get_address_of_GameConfigurationReceived_22() { return &___GameConfigurationReceived_22; }
	inline void set_GameConfigurationReceived_22(Action_t437523947 * value)
	{
		___GameConfigurationReceived_22 = value;
		Il2CppCodeGenWriteBarrier(&___GameConfigurationReceived_22, value);
	}

	inline static int32_t get_offset_of_AccountLoginComplete_23() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___AccountLoginComplete_23)); }
	inline Action_2_t535153670 * get_AccountLoginComplete_23() const { return ___AccountLoginComplete_23; }
	inline Action_2_t535153670 ** get_address_of_AccountLoginComplete_23() { return &___AccountLoginComplete_23; }
	inline void set_AccountLoginComplete_23(Action_2_t535153670 * value)
	{
		___AccountLoginComplete_23 = value;
		Il2CppCodeGenWriteBarrier(&___AccountLoginComplete_23, value);
	}

	inline static int32_t get_offset_of_AccountLoginError_24() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___AccountLoginError_24)); }
	inline Action_2_t2362964390 * get_AccountLoginError_24() const { return ___AccountLoginError_24; }
	inline Action_2_t2362964390 ** get_address_of_AccountLoginError_24() { return &___AccountLoginError_24; }
	inline void set_AccountLoginError_24(Action_2_t2362964390 * value)
	{
		___AccountLoginError_24 = value;
		Il2CppCodeGenWriteBarrier(&___AccountLoginError_24, value);
	}

	inline static int32_t get_offset_of_NotificationAction_25() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___NotificationAction_25)); }
	inline Action_1_t1116941607 * get_NotificationAction_25() const { return ___NotificationAction_25; }
	inline Action_1_t1116941607 ** get_address_of_NotificationAction_25() { return &___NotificationAction_25; }
	inline void set_NotificationAction_25(Action_1_t1116941607 * value)
	{
		___NotificationAction_25 = value;
		Il2CppCodeGenWriteBarrier(&___NotificationAction_25, value);
	}

	inline static int32_t get_offset_of_NotificationWillClose_26() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___NotificationWillClose_26)); }
	inline Action_t437523947 * get_NotificationWillClose_26() const { return ___NotificationWillClose_26; }
	inline Action_t437523947 ** get_address_of_NotificationWillClose_26() { return &___NotificationWillClose_26; }
	inline void set_NotificationWillClose_26(Action_t437523947 * value)
	{
		___NotificationWillClose_26 = value;
		Il2CppCodeGenWriteBarrier(&___NotificationWillClose_26, value);
	}

	inline static int32_t get_offset_of_FriendAdded_27() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___FriendAdded_27)); }
	inline Action_2_t2362964390 * get_FriendAdded_27() const { return ___FriendAdded_27; }
	inline Action_2_t2362964390 ** get_address_of_FriendAdded_27() { return &___FriendAdded_27; }
	inline void set_FriendAdded_27(Action_2_t2362964390 * value)
	{
		___FriendAdded_27 = value;
		Il2CppCodeGenWriteBarrier(&___FriendAdded_27, value);
	}

	inline static int32_t get_offset_of_FriendRemoved_28() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___FriendRemoved_28)); }
	inline Action_2_t2362964390 * get_FriendRemoved_28() const { return ___FriendRemoved_28; }
	inline Action_2_t2362964390 ** get_address_of_FriendRemoved_28() { return &___FriendRemoved_28; }
	inline void set_FriendRemoved_28(Action_2_t2362964390 * value)
	{
		___FriendRemoved_28 = value;
		Il2CppCodeGenWriteBarrier(&___FriendRemoved_28, value);
	}

	inline static int32_t get_offset_of_FriendAccepted_29() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___FriendAccepted_29)); }
	inline Action_2_t2362964390 * get_FriendAccepted_29() const { return ___FriendAccepted_29; }
	inline Action_2_t2362964390 ** get_address_of_FriendAccepted_29() { return &___FriendAccepted_29; }
	inline void set_FriendAccepted_29(Action_2_t2362964390 * value)
	{
		___FriendAccepted_29 = value;
		Il2CppCodeGenWriteBarrier(&___FriendAccepted_29, value);
	}

	inline static int32_t get_offset_of_FriendRejected_30() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___FriendRejected_30)); }
	inline Action_2_t2362964390 * get_FriendRejected_30() const { return ___FriendRejected_30; }
	inline Action_2_t2362964390 ** get_address_of_FriendRejected_30() { return &___FriendRejected_30; }
	inline void set_FriendRejected_30(Action_2_t2362964390 * value)
	{
		___FriendRejected_30 = value;
		Il2CppCodeGenWriteBarrier(&___FriendRejected_30, value);
	}

	inline static int32_t get_offset_of_FriendsMigrated_31() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___FriendsMigrated_31)); }
	inline Action_2_t2362964390 * get_FriendsMigrated_31() const { return ___FriendsMigrated_31; }
	inline Action_2_t2362964390 ** get_address_of_FriendsMigrated_31() { return &___FriendsMigrated_31; }
	inline void set_FriendsMigrated_31(Action_2_t2362964390 * value)
	{
		___FriendsMigrated_31 = value;
		Il2CppCodeGenWriteBarrier(&___FriendsMigrated_31, value);
	}

	inline static int32_t get_offset_of_FriendsListUpdated_32() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___FriendsListUpdated_32)); }
	inline Action_1_t1482123831 * get_FriendsListUpdated_32() const { return ___FriendsListUpdated_32; }
	inline Action_1_t1482123831 ** get_address_of_FriendsListUpdated_32() { return &___FriendsListUpdated_32; }
	inline void set_FriendsListUpdated_32(Action_1_t1482123831 * value)
	{
		___FriendsListUpdated_32 = value;
		Il2CppCodeGenWriteBarrier(&___FriendsListUpdated_32, value);
	}

	inline static int32_t get_offset_of_FriendsListError_33() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___FriendsListError_33)); }
	inline Action_1_t592684423 * get_FriendsListError_33() const { return ___FriendsListError_33; }
	inline Action_1_t592684423 ** get_address_of_FriendsListError_33() { return &___FriendsListError_33; }
	inline void set_FriendsListError_33(Action_1_t592684423 * value)
	{
		___FriendsListError_33 = value;
		Il2CppCodeGenWriteBarrier(&___FriendsListError_33, value);
	}

	inline static int32_t get_offset_of_PurchaseVerification_34() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___PurchaseVerification_34)); }
	inline Action_3_t3047918030 * get_PurchaseVerification_34() const { return ___PurchaseVerification_34; }
	inline Action_3_t3047918030 ** get_address_of_PurchaseVerification_34() { return &___PurchaseVerification_34; }
	inline void set_PurchaseVerification_34(Action_3_t3047918030 * value)
	{
		___PurchaseVerification_34 = value;
		Il2CppCodeGenWriteBarrier(&___PurchaseVerification_34, value);
	}

	inline static int32_t get_offset_of_AdAvailabilityResponse_35() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___AdAvailabilityResponse_35)); }
	inline Action_2_t3211021459 * get_AdAvailabilityResponse_35() const { return ___AdAvailabilityResponse_35; }
	inline Action_2_t3211021459 ** get_address_of_AdAvailabilityResponse_35() { return &___AdAvailabilityResponse_35; }
	inline void set_AdAvailabilityResponse_35(Action_2_t3211021459 * value)
	{
		___AdAvailabilityResponse_35 = value;
		Il2CppCodeGenWriteBarrier(&___AdAvailabilityResponse_35, value);
	}

	inline static int32_t get_offset_of_AdWillClose_36() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___AdWillClose_36)); }
	inline Action_t437523947 * get_AdWillClose_36() const { return ___AdWillClose_36; }
	inline Action_t437523947 ** get_address_of_AdWillClose_36() { return &___AdWillClose_36; }
	inline void set_AdWillClose_36(Action_t437523947 * value)
	{
		___AdWillClose_36 = value;
		Il2CppCodeGenWriteBarrier(&___AdWillClose_36, value);
	}

	inline static int32_t get_offset_of_AdFailedToDisplay_37() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___AdFailedToDisplay_37)); }
	inline Action_t437523947 * get_AdFailedToDisplay_37() const { return ___AdFailedToDisplay_37; }
	inline Action_t437523947 ** get_address_of_AdFailedToDisplay_37() { return &___AdFailedToDisplay_37; }
	inline void set_AdFailedToDisplay_37(Action_t437523947 * value)
	{
		___AdFailedToDisplay_37 = value;
		Il2CppCodeGenWriteBarrier(&___AdFailedToDisplay_37, value);
	}

	inline static int32_t get_offset_of_AdDidShow_38() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___AdDidShow_38)); }
	inline Action_2_t3619260338 * get_AdDidShow_38() const { return ___AdDidShow_38; }
	inline Action_2_t3619260338 ** get_address_of_AdDidShow_38() { return &___AdDidShow_38; }
	inline void set_AdDidShow_38(Action_2_t3619260338 * value)
	{
		___AdDidShow_38 = value;
		Il2CppCodeGenWriteBarrier(&___AdDidShow_38, value);
	}

	inline static int32_t get_offset_of_RewardedAdCompletedWithObject_39() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___RewardedAdCompletedWithObject_39)); }
	inline Action_1_t978424956 * get_RewardedAdCompletedWithObject_39() const { return ___RewardedAdCompletedWithObject_39; }
	inline Action_1_t978424956 ** get_address_of_RewardedAdCompletedWithObject_39() { return &___RewardedAdCompletedWithObject_39; }
	inline void set_RewardedAdCompletedWithObject_39(Action_1_t978424956 * value)
	{
		___RewardedAdCompletedWithObject_39 = value;
		Il2CppCodeGenWriteBarrier(&___RewardedAdCompletedWithObject_39, value);
	}

	inline static int32_t get_offset_of_IAPOfferAcceptedWithObject_40() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___IAPOfferAcceptedWithObject_40)); }
	inline Action_1_t3974970450 * get_IAPOfferAcceptedWithObject_40() const { return ___IAPOfferAcceptedWithObject_40; }
	inline Action_1_t3974970450 ** get_address_of_IAPOfferAcceptedWithObject_40() { return &___IAPOfferAcceptedWithObject_40; }
	inline void set_IAPOfferAcceptedWithObject_40(Action_1_t3974970450 * value)
	{
		___IAPOfferAcceptedWithObject_40 = value;
		Il2CppCodeGenWriteBarrier(&___IAPOfferAcceptedWithObject_40, value);
	}

	inline static int32_t get_offset_of_VirtualGoodsOfferAcceptedWithObject_41() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___VirtualGoodsOfferAcceptedWithObject_41)); }
	inline Action_1_t1534247929 * get_VirtualGoodsOfferAcceptedWithObject_41() const { return ___VirtualGoodsOfferAcceptedWithObject_41; }
	inline Action_1_t1534247929 ** get_address_of_VirtualGoodsOfferAcceptedWithObject_41() { return &___VirtualGoodsOfferAcceptedWithObject_41; }
	inline void set_VirtualGoodsOfferAcceptedWithObject_41(Action_1_t1534247929 * value)
	{
		___VirtualGoodsOfferAcceptedWithObject_41 = value;
		Il2CppCodeGenWriteBarrier(&___VirtualGoodsOfferAcceptedWithObject_41, value);
	}

	inline static int32_t get_offset_of_TimeUpdated_42() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___TimeUpdated_42)); }
	inline Action_1_t487486641 * get_TimeUpdated_42() const { return ___TimeUpdated_42; }
	inline Action_1_t487486641 ** get_address_of_TimeUpdated_42() { return &___TimeUpdated_42; }
	inline void set_TimeUpdated_42(Action_1_t487486641 * value)
	{
		___TimeUpdated_42 = value;
		Il2CppCodeGenWriteBarrier(&___TimeUpdated_42, value);
	}

	inline static int32_t get_offset_of_GameDataError_43() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___GameDataError_43)); }
	inline Action_2_t1216077269 * get_GameDataError_43() const { return ___GameDataError_43; }
	inline Action_2_t1216077269 ** get_address_of_GameDataError_43() { return &___GameDataError_43; }
	inline void set_GameDataError_43(Action_2_t1216077269 * value)
	{
		___GameDataError_43 = value;
		Il2CppCodeGenWriteBarrier(&___GameDataError_43, value);
	}

	inline static int32_t get_offset_of_GameDataSetAcknowledged_44() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___GameDataSetAcknowledged_44)); }
	inline Action_1_t2995867492 * get_GameDataSetAcknowledged_44() const { return ___GameDataSetAcknowledged_44; }
	inline Action_1_t2995867492 ** get_address_of_GameDataSetAcknowledged_44() { return &___GameDataSetAcknowledged_44; }
	inline void set_GameDataSetAcknowledged_44(Action_1_t2995867492 * value)
	{
		___GameDataSetAcknowledged_44 = value;
		Il2CppCodeGenWriteBarrier(&___GameDataSetAcknowledged_44, value);
	}

	inline static int32_t get_offset_of_GameDataReceived_45() { return static_cast<int32_t>(offsetof(FuseSDK_t1159654649_StaticFields, ___GameDataReceived_45)); }
	inline Action_4_t4157318299 * get_GameDataReceived_45() const { return ___GameDataReceived_45; }
	inline Action_4_t4157318299 ** get_address_of_GameDataReceived_45() { return &___GameDataReceived_45; }
	inline void set_GameDataReceived_45(Action_4_t4157318299 * value)
	{
		___GameDataReceived_45 = value;
		Il2CppCodeGenWriteBarrier(&___GameDataReceived_45, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
