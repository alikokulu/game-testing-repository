﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseMisc.FuseExtensions
struct  FuseExtensions_t2761543572  : public Il2CppObject
{
public:

public:
};

struct FuseExtensions_t2761543572_StaticFields
{
public:
	// System.DateTime FuseMisc.FuseExtensions::unixEpoch
	DateTime_t339033936  ___unixEpoch_0;

public:
	inline static int32_t get_offset_of_unixEpoch_0() { return static_cast<int32_t>(offsetof(FuseExtensions_t2761543572_StaticFields, ___unixEpoch_0)); }
	inline DateTime_t339033936  get_unixEpoch_0() const { return ___unixEpoch_0; }
	inline DateTime_t339033936 * get_address_of_unixEpoch_0() { return &___unixEpoch_0; }
	inline void set_unixEpoch_0(DateTime_t339033936  value)
	{
		___unixEpoch_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
