﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuseSDK_SoomlaIAP
struct FuseSDK_SoomlaIAP_t316686275;

#include "codegen/il2cpp-codegen.h"

// System.Void FuseSDK_SoomlaIAP::.ctor()
extern "C"  void FuseSDK_SoomlaIAP__ctor_m3887823112 (FuseSDK_SoomlaIAP_t316686275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
