﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/<StringifyAsync>c__Iterator3
struct U3CStringifyAsyncU3Ec__Iterator3_t555783763;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void JSONObject/<StringifyAsync>c__Iterator3::.ctor()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator3__ctor_m3303104688 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<StringifyAsync>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2629633442 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<StringifyAsync>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3869412662 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<StringifyAsync>c__Iterator3::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator3_System_Collections_IEnumerable_GetEnumerator_m2370473137 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<StringifyAsync>c__Iterator3::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CStringifyAsyncU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m2456447359 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<StringifyAsync>c__Iterator3::MoveNext()
extern "C"  bool U3CStringifyAsyncU3Ec__Iterator3_MoveNext_m685978436 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<StringifyAsync>c__Iterator3::Dispose()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator3_Dispose_m201056493 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<StringifyAsync>c__Iterator3::Reset()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator3_Reset_m949537629 (U3CStringifyAsyncU3Ec__Iterator3_t555783763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
