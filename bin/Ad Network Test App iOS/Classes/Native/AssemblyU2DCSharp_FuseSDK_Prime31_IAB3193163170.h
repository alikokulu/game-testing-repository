﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseSDK_Prime31_IAB
struct  FuseSDK_Prime31_IAB_t3193163170  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean FuseSDK_Prime31_IAB::logging
	bool ___logging_2;

public:
	inline static int32_t get_offset_of_logging_2() { return static_cast<int32_t>(offsetof(FuseSDK_Prime31_IAB_t3193163170, ___logging_2)); }
	inline bool get_logging_2() const { return ___logging_2; }
	inline bool* get_address_of_logging_2() { return &___logging_2; }
	inline void set_logging_2(bool value)
	{
		___logging_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
