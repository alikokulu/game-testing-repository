﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseMisc.RewardedInfo
struct  RewardedInfo_t829972251 
{
public:
	// System.String FuseMisc.RewardedInfo::PreRollMessage
	String_t* ___PreRollMessage_0;
	// System.String FuseMisc.RewardedInfo::RewardMessage
	String_t* ___RewardMessage_1;
	// System.String FuseMisc.RewardedInfo::RewardItem
	String_t* ___RewardItem_2;
	// System.Int32 FuseMisc.RewardedInfo::RewardItemId
	int32_t ___RewardItemId_3;
	// System.Int32 FuseMisc.RewardedInfo::RewardAmount
	int32_t ___RewardAmount_4;

public:
	inline static int32_t get_offset_of_PreRollMessage_0() { return static_cast<int32_t>(offsetof(RewardedInfo_t829972251, ___PreRollMessage_0)); }
	inline String_t* get_PreRollMessage_0() const { return ___PreRollMessage_0; }
	inline String_t** get_address_of_PreRollMessage_0() { return &___PreRollMessage_0; }
	inline void set_PreRollMessage_0(String_t* value)
	{
		___PreRollMessage_0 = value;
		Il2CppCodeGenWriteBarrier(&___PreRollMessage_0, value);
	}

	inline static int32_t get_offset_of_RewardMessage_1() { return static_cast<int32_t>(offsetof(RewardedInfo_t829972251, ___RewardMessage_1)); }
	inline String_t* get_RewardMessage_1() const { return ___RewardMessage_1; }
	inline String_t** get_address_of_RewardMessage_1() { return &___RewardMessage_1; }
	inline void set_RewardMessage_1(String_t* value)
	{
		___RewardMessage_1 = value;
		Il2CppCodeGenWriteBarrier(&___RewardMessage_1, value);
	}

	inline static int32_t get_offset_of_RewardItem_2() { return static_cast<int32_t>(offsetof(RewardedInfo_t829972251, ___RewardItem_2)); }
	inline String_t* get_RewardItem_2() const { return ___RewardItem_2; }
	inline String_t** get_address_of_RewardItem_2() { return &___RewardItem_2; }
	inline void set_RewardItem_2(String_t* value)
	{
		___RewardItem_2 = value;
		Il2CppCodeGenWriteBarrier(&___RewardItem_2, value);
	}

	inline static int32_t get_offset_of_RewardItemId_3() { return static_cast<int32_t>(offsetof(RewardedInfo_t829972251, ___RewardItemId_3)); }
	inline int32_t get_RewardItemId_3() const { return ___RewardItemId_3; }
	inline int32_t* get_address_of_RewardItemId_3() { return &___RewardItemId_3; }
	inline void set_RewardItemId_3(int32_t value)
	{
		___RewardItemId_3 = value;
	}

	inline static int32_t get_offset_of_RewardAmount_4() { return static_cast<int32_t>(offsetof(RewardedInfo_t829972251, ___RewardAmount_4)); }
	inline int32_t get_RewardAmount_4() const { return ___RewardAmount_4; }
	inline int32_t* get_address_of_RewardAmount_4() { return &___RewardAmount_4; }
	inline void set_RewardAmount_4(int32_t value)
	{
		___RewardAmount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: FuseMisc.RewardedInfo
struct RewardedInfo_t829972251_marshaled
{
	char* ___PreRollMessage_0;
	char* ___RewardMessage_1;
	char* ___RewardItem_2;
	int32_t ___RewardItemId_3;
	int32_t ___RewardAmount_4;
};
