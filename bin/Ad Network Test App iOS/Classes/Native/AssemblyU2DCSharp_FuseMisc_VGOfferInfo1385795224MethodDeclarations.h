﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FuseMisc_VGOfferInfo1385795224.h"
#include "mscorlib_System_String968488902.h"

// System.Void FuseMisc.VGOfferInfo::.ctor(System.String)
extern "C"  void VGOfferInfo__ctor_m259242847 (VGOfferInfo_t1385795224 * __this, String_t* ___infoString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseMisc.VGOfferInfo::ToString()
extern "C"  String_t* VGOfferInfo_ToString_m254879216 (VGOfferInfo_t1385795224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
