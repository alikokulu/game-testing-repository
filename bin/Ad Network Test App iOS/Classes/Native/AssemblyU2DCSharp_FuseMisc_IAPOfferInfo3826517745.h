﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseMisc.IAPOfferInfo
struct  IAPOfferInfo_t3826517745 
{
public:
	// System.String FuseMisc.IAPOfferInfo::ProductId
	String_t* ___ProductId_0;
	// System.Single FuseMisc.IAPOfferInfo::ProductPrice
	float ___ProductPrice_1;
	// System.String FuseMisc.IAPOfferInfo::ItemName
	String_t* ___ItemName_2;
	// System.Int32 FuseMisc.IAPOfferInfo::ItemAmount
	int32_t ___ItemAmount_3;
	// System.DateTime FuseMisc.IAPOfferInfo::StartTime
	DateTime_t339033936  ___StartTime_4;
	// System.DateTime FuseMisc.IAPOfferInfo::EndTime
	DateTime_t339033936  ___EndTime_5;
	// System.String FuseMisc.IAPOfferInfo::Metadata
	String_t* ___Metadata_6;

public:
	inline static int32_t get_offset_of_ProductId_0() { return static_cast<int32_t>(offsetof(IAPOfferInfo_t3826517745, ___ProductId_0)); }
	inline String_t* get_ProductId_0() const { return ___ProductId_0; }
	inline String_t** get_address_of_ProductId_0() { return &___ProductId_0; }
	inline void set_ProductId_0(String_t* value)
	{
		___ProductId_0 = value;
		Il2CppCodeGenWriteBarrier(&___ProductId_0, value);
	}

	inline static int32_t get_offset_of_ProductPrice_1() { return static_cast<int32_t>(offsetof(IAPOfferInfo_t3826517745, ___ProductPrice_1)); }
	inline float get_ProductPrice_1() const { return ___ProductPrice_1; }
	inline float* get_address_of_ProductPrice_1() { return &___ProductPrice_1; }
	inline void set_ProductPrice_1(float value)
	{
		___ProductPrice_1 = value;
	}

	inline static int32_t get_offset_of_ItemName_2() { return static_cast<int32_t>(offsetof(IAPOfferInfo_t3826517745, ___ItemName_2)); }
	inline String_t* get_ItemName_2() const { return ___ItemName_2; }
	inline String_t** get_address_of_ItemName_2() { return &___ItemName_2; }
	inline void set_ItemName_2(String_t* value)
	{
		___ItemName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ItemName_2, value);
	}

	inline static int32_t get_offset_of_ItemAmount_3() { return static_cast<int32_t>(offsetof(IAPOfferInfo_t3826517745, ___ItemAmount_3)); }
	inline int32_t get_ItemAmount_3() const { return ___ItemAmount_3; }
	inline int32_t* get_address_of_ItemAmount_3() { return &___ItemAmount_3; }
	inline void set_ItemAmount_3(int32_t value)
	{
		___ItemAmount_3 = value;
	}

	inline static int32_t get_offset_of_StartTime_4() { return static_cast<int32_t>(offsetof(IAPOfferInfo_t3826517745, ___StartTime_4)); }
	inline DateTime_t339033936  get_StartTime_4() const { return ___StartTime_4; }
	inline DateTime_t339033936 * get_address_of_StartTime_4() { return &___StartTime_4; }
	inline void set_StartTime_4(DateTime_t339033936  value)
	{
		___StartTime_4 = value;
	}

	inline static int32_t get_offset_of_EndTime_5() { return static_cast<int32_t>(offsetof(IAPOfferInfo_t3826517745, ___EndTime_5)); }
	inline DateTime_t339033936  get_EndTime_5() const { return ___EndTime_5; }
	inline DateTime_t339033936 * get_address_of_EndTime_5() { return &___EndTime_5; }
	inline void set_EndTime_5(DateTime_t339033936  value)
	{
		___EndTime_5 = value;
	}

	inline static int32_t get_offset_of_Metadata_6() { return static_cast<int32_t>(offsetof(IAPOfferInfo_t3826517745, ___Metadata_6)); }
	inline String_t* get_Metadata_6() const { return ___Metadata_6; }
	inline String_t** get_address_of_Metadata_6() { return &___Metadata_6; }
	inline void set_Metadata_6(String_t* value)
	{
		___Metadata_6 = value;
		Il2CppCodeGenWriteBarrier(&___Metadata_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
