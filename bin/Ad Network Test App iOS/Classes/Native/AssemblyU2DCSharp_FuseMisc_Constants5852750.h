﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseMisc.Constants
struct  Constants_t5852750  : public Il2CppObject
{
public:

public:
};

struct Constants_t5852750_StaticFields
{
public:
	// System.String FuseMisc.Constants::RewardedAdOptionKey_ShowPreRoll
	String_t* ___RewardedAdOptionKey_ShowPreRoll_0;
	// System.String FuseMisc.Constants::RewardedAdOptionKey_ShowPostRoll
	String_t* ___RewardedAdOptionKey_ShowPostRoll_1;
	// System.String FuseMisc.Constants::RewardedOptionKey_PreRollYesButtonText
	String_t* ___RewardedOptionKey_PreRollYesButtonText_2;
	// System.String FuseMisc.Constants::RewardedOptionKey_PreRollNoButtonText
	String_t* ___RewardedOptionKey_PreRollNoButtonText_3;
	// System.String FuseMisc.Constants::RewardedOptionKey_PostRollContinueButtonText
	String_t* ___RewardedOptionKey_PostRollContinueButtonText_4;

public:
	inline static int32_t get_offset_of_RewardedAdOptionKey_ShowPreRoll_0() { return static_cast<int32_t>(offsetof(Constants_t5852750_StaticFields, ___RewardedAdOptionKey_ShowPreRoll_0)); }
	inline String_t* get_RewardedAdOptionKey_ShowPreRoll_0() const { return ___RewardedAdOptionKey_ShowPreRoll_0; }
	inline String_t** get_address_of_RewardedAdOptionKey_ShowPreRoll_0() { return &___RewardedAdOptionKey_ShowPreRoll_0; }
	inline void set_RewardedAdOptionKey_ShowPreRoll_0(String_t* value)
	{
		___RewardedAdOptionKey_ShowPreRoll_0 = value;
		Il2CppCodeGenWriteBarrier(&___RewardedAdOptionKey_ShowPreRoll_0, value);
	}

	inline static int32_t get_offset_of_RewardedAdOptionKey_ShowPostRoll_1() { return static_cast<int32_t>(offsetof(Constants_t5852750_StaticFields, ___RewardedAdOptionKey_ShowPostRoll_1)); }
	inline String_t* get_RewardedAdOptionKey_ShowPostRoll_1() const { return ___RewardedAdOptionKey_ShowPostRoll_1; }
	inline String_t** get_address_of_RewardedAdOptionKey_ShowPostRoll_1() { return &___RewardedAdOptionKey_ShowPostRoll_1; }
	inline void set_RewardedAdOptionKey_ShowPostRoll_1(String_t* value)
	{
		___RewardedAdOptionKey_ShowPostRoll_1 = value;
		Il2CppCodeGenWriteBarrier(&___RewardedAdOptionKey_ShowPostRoll_1, value);
	}

	inline static int32_t get_offset_of_RewardedOptionKey_PreRollYesButtonText_2() { return static_cast<int32_t>(offsetof(Constants_t5852750_StaticFields, ___RewardedOptionKey_PreRollYesButtonText_2)); }
	inline String_t* get_RewardedOptionKey_PreRollYesButtonText_2() const { return ___RewardedOptionKey_PreRollYesButtonText_2; }
	inline String_t** get_address_of_RewardedOptionKey_PreRollYesButtonText_2() { return &___RewardedOptionKey_PreRollYesButtonText_2; }
	inline void set_RewardedOptionKey_PreRollYesButtonText_2(String_t* value)
	{
		___RewardedOptionKey_PreRollYesButtonText_2 = value;
		Il2CppCodeGenWriteBarrier(&___RewardedOptionKey_PreRollYesButtonText_2, value);
	}

	inline static int32_t get_offset_of_RewardedOptionKey_PreRollNoButtonText_3() { return static_cast<int32_t>(offsetof(Constants_t5852750_StaticFields, ___RewardedOptionKey_PreRollNoButtonText_3)); }
	inline String_t* get_RewardedOptionKey_PreRollNoButtonText_3() const { return ___RewardedOptionKey_PreRollNoButtonText_3; }
	inline String_t** get_address_of_RewardedOptionKey_PreRollNoButtonText_3() { return &___RewardedOptionKey_PreRollNoButtonText_3; }
	inline void set_RewardedOptionKey_PreRollNoButtonText_3(String_t* value)
	{
		___RewardedOptionKey_PreRollNoButtonText_3 = value;
		Il2CppCodeGenWriteBarrier(&___RewardedOptionKey_PreRollNoButtonText_3, value);
	}

	inline static int32_t get_offset_of_RewardedOptionKey_PostRollContinueButtonText_4() { return static_cast<int32_t>(offsetof(Constants_t5852750_StaticFields, ___RewardedOptionKey_PostRollContinueButtonText_4)); }
	inline String_t* get_RewardedOptionKey_PostRollContinueButtonText_4() const { return ___RewardedOptionKey_PostRollContinueButtonText_4; }
	inline String_t** get_address_of_RewardedOptionKey_PostRollContinueButtonText_4() { return &___RewardedOptionKey_PostRollContinueButtonText_4; }
	inline void set_RewardedOptionKey_PostRollContinueButtonText_4(String_t* value)
	{
		___RewardedOptionKey_PostRollContinueButtonText_4 = value;
		Il2CppCodeGenWriteBarrier(&___RewardedOptionKey_PostRollContinueButtonText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
