﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuseSDK_Prime31_IAB
struct FuseSDK_Prime31_IAB_t3193163170;

#include "codegen/il2cpp-codegen.h"

// System.Void FuseSDK_Prime31_IAB::.ctor()
extern "C"  void FuseSDK_Prime31_IAB__ctor_m3074063369 (FuseSDK_Prime31_IAB_t3193163170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
