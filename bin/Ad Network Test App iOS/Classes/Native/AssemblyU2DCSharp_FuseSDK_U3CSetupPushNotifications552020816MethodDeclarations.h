﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuseSDK/<SetupPushNotifications>c__Iterator0
struct U3CSetupPushNotificationsU3Ec__Iterator0_t552020816;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FuseSDK/<SetupPushNotifications>c__Iterator0::.ctor()
extern "C"  void U3CSetupPushNotificationsU3Ec__Iterator0__ctor_m2154447541 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FuseSDK/<SetupPushNotifications>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSetupPushNotificationsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2105890503 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FuseSDK/<SetupPushNotifications>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSetupPushNotificationsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m984860763 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK/<SetupPushNotifications>c__Iterator0::MoveNext()
extern "C"  bool U3CSetupPushNotificationsU3Ec__Iterator0_MoveNext_m2881469703 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK/<SetupPushNotifications>c__Iterator0::Dispose()
extern "C"  void U3CSetupPushNotificationsU3Ec__Iterator0_Dispose_m148133298 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK/<SetupPushNotifications>c__Iterator0::Reset()
extern "C"  void U3CSetupPushNotificationsU3Ec__Iterator0_Reset_m4095847778 (U3CSetupPushNotificationsU3Ec__Iterator0_t552020816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
