﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FuseMisc_RewardedInfo829972251.h"
#include "mscorlib_System_String968488902.h"

// System.Void FuseMisc.RewardedInfo::.ctor(System.String)
extern "C"  void RewardedInfo__ctor_m1550529012 (RewardedInfo_t829972251 * __this, String_t* ___infoString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseMisc.RewardedInfo::ToString()
extern "C"  String_t* RewardedInfo_ToString_m26200799 (RewardedInfo_t829972251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct RewardedInfo_t829972251;
struct RewardedInfo_t829972251_marshaled;

extern "C" void RewardedInfo_t829972251_marshal(const RewardedInfo_t829972251& unmarshaled, RewardedInfo_t829972251_marshaled& marshaled);
extern "C" void RewardedInfo_t829972251_marshal_back(const RewardedInfo_t829972251_marshaled& marshaled, RewardedInfo_t829972251& unmarshaled);
extern "C" void RewardedInfo_t829972251_marshal_cleanup(RewardedInfo_t829972251_marshaled& marshaled);
