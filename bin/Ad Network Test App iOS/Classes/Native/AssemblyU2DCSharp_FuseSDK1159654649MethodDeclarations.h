﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuseSDK
struct FuseSDK_t1159654649;
// System.Action
struct Action_t437523947;
// System.Action`1<FuseMisc.FuseError>
struct Action_1_t592684423;
// System.Action`2<FuseMisc.AccountType,System.String>
struct Action_2_t535153670;
// System.Action`2<System.String,FuseMisc.FuseError>
struct Action_2_t2362964390;
// System.Action`1<System.String>
struct Action_1_t1116941607;
// System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>
struct Action_1_t1482123831;
// System.Action`3<System.Int32,System.String,System.String>
struct Action_3_t3047918030;
// System.Action`2<System.Boolean,FuseMisc.FuseError>
struct Action_2_t3211021459;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t3619260338;
// System.Action`1<FuseMisc.RewardedInfo>
struct Action_1_t978424956;
// System.Action`1<FuseMisc.IAPOfferInfo>
struct Action_1_t3974970450;
// System.Action`1<FuseMisc.VGOfferInfo>
struct Action_1_t1534247929;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// System.Action`2<System.Int32,FuseMisc.FuseError>
struct Action_2_t1216077269;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Action`4<System.Int32,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Action_4_t4157318299;
// System.String
struct String_t;
// System.Collections.Generic.List`1<FuseMisc.Friend>
struct List_1_t1333671126;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Double[]
struct DoubleU5BU5D_t1048280995;
// System.Single[]
struct SingleU5BU5D_t1219431280;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// FuseMisc.Product[]
struct ProductU5BU5D_t4249436827;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_FuseMisc_RewardedInfo829972251.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPOfferInfo3826517745.h"
#include "AssemblyU2DCSharp_FuseMisc_VGOfferInfo1385795224.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "AssemblyU2DCSharp_FuseMisc_IAPState1023998552.h"
#include "AssemblyU2DCSharp_FuseMisc_Gender553483264.h"
#include "AssemblyU2DCSharp_FuseMisc_AccountType958198790.h"

// System.Void FuseSDK::.ctor()
extern "C"  void FuseSDK__ctor_m2659371666 (FuseSDK_t1159654649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::.cctor()
extern "C"  void FuseSDK__cctor_m354046811 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_SessionStartReceived(System.Action)
extern "C"  void FuseSDK_add_SessionStartReceived_m1374611232 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_SessionStartReceived(System.Action)
extern "C"  void FuseSDK_remove_SessionStartReceived_m2813762949 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_SessionLoginError(System.Action`1<FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_SessionLoginError_m2857792873 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_SessionLoginError(System.Action`1<FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_SessionLoginError_m1248891620 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_GameConfigurationReceived(System.Action)
extern "C"  void FuseSDK_add_GameConfigurationReceived_m3228441574 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_GameConfigurationReceived(System.Action)
extern "C"  void FuseSDK_remove_GameConfigurationReceived_m761371809 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_AccountLoginComplete(System.Action`2<FuseMisc.AccountType,System.String>)
extern "C"  void FuseSDK_add_AccountLoginComplete_m2612660334 (Il2CppObject * __this /* static, unused */, Action_2_t535153670 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_AccountLoginComplete(System.Action`2<FuseMisc.AccountType,System.String>)
extern "C"  void FuseSDK_remove_AccountLoginComplete_m1634388755 (Il2CppObject * __this /* static, unused */, Action_2_t535153670 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_AccountLoginError(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_AccountLoginError_m4102543117 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_AccountLoginError(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_AccountLoginError_m1721188680 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_NotificationAction(System.Action`1<System.String>)
extern "C"  void FuseSDK_add_NotificationAction_m92288429 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_NotificationAction(System.Action`1<System.String>)
extern "C"  void FuseSDK_remove_NotificationAction_m992102568 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_NotificationWillClose(System.Action)
extern "C"  void FuseSDK_add_NotificationWillClose_m1592228912 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_NotificationWillClose(System.Action)
extern "C"  void FuseSDK_remove_NotificationWillClose_m3256259179 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_FriendAdded(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_FriendAdded_m1791049655 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_FriendAdded(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_FriendAdded_m1313845810 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_FriendRemoved(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_FriendRemoved_m1542110487 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_FriendRemoved(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_FriendRemoved_m2510716114 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_FriendAccepted(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_FriendAccepted_m3304322678 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_FriendAccepted(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_FriendAccepted_m3266326043 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_FriendRejected(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_FriendRejected_m3903105087 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_FriendRejected(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_FriendRejected_m3865108452 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_FriendsMigrated(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_FriendsMigrated_m1977570539 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_FriendsMigrated(System.Action`2<System.String,FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_FriendsMigrated_m799674854 (Il2CppObject * __this /* static, unused */, Action_2_t2362964390 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_FriendsListUpdated(System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>)
extern "C"  void FuseSDK_add_FriendsListUpdated_m2713688815 (Il2CppObject * __this /* static, unused */, Action_1_t1482123831 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_FriendsListUpdated(System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>)
extern "C"  void FuseSDK_remove_FriendsListUpdated_m3831519444 (Il2CppObject * __this /* static, unused */, Action_1_t1482123831 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_FriendsListError(System.Action`1<FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_FriendsListError_m1272986699 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_FriendsListError(System.Action`1<FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_FriendsListError_m2745107312 (Il2CppObject * __this /* static, unused */, Action_1_t592684423 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_PurchaseVerification(System.Action`3<System.Int32,System.String,System.String>)
extern "C"  void FuseSDK_add_PurchaseVerification_m2364053203 (Il2CppObject * __this /* static, unused */, Action_3_t3047918030 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_PurchaseVerification(System.Action`3<System.Int32,System.String,System.String>)
extern "C"  void FuseSDK_remove_PurchaseVerification_m3756263864 (Il2CppObject * __this /* static, unused */, Action_3_t3047918030 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_AdAvailabilityResponse(System.Action`2<System.Boolean,FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_AdAvailabilityResponse_m2892673221 (Il2CppObject * __this /* static, unused */, Action_2_t3211021459 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_AdAvailabilityResponse(System.Action`2<System.Boolean,FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_AdAvailabilityResponse_m2631025344 (Il2CppObject * __this /* static, unused */, Action_2_t3211021459 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_AdWillClose(System.Action)
extern "C"  void FuseSDK_add_AdWillClose_m1177472552 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_AdWillClose(System.Action)
extern "C"  void FuseSDK_remove_AdWillClose_m2705220131 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_AdFailedToDisplay(System.Action)
extern "C"  void FuseSDK_add_AdFailedToDisplay_m3402053476 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_AdFailedToDisplay(System.Action)
extern "C"  void FuseSDK_remove_AdFailedToDisplay_m1884857119 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_AdDidShow(System.Action`2<System.Int32,System.Int32>)
extern "C"  void FuseSDK_add_AdDidShow_m3167712772 (Il2CppObject * __this /* static, unused */, Action_2_t3619260338 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_AdDidShow(System.Action`2<System.Int32,System.Int32>)
extern "C"  void FuseSDK_remove_AdDidShow_m344866089 (Il2CppObject * __this /* static, unused */, Action_2_t3619260338 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_RewardedAdCompletedWithObject(System.Action`1<FuseMisc.RewardedInfo>)
extern "C"  void FuseSDK_add_RewardedAdCompletedWithObject_m1341894586 (Il2CppObject * __this /* static, unused */, Action_1_t978424956 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_RewardedAdCompletedWithObject(System.Action`1<FuseMisc.RewardedInfo>)
extern "C"  void FuseSDK_remove_RewardedAdCompletedWithObject_m534351071 (Il2CppObject * __this /* static, unused */, Action_1_t978424956 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_IAPOfferAcceptedWithObject(System.Action`1<FuseMisc.IAPOfferInfo>)
extern "C"  void FuseSDK_add_IAPOfferAcceptedWithObject_m1561552895 (Il2CppObject * __this /* static, unused */, Action_1_t3974970450 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_IAPOfferAcceptedWithObject(System.Action`1<FuseMisc.IAPOfferInfo>)
extern "C"  void FuseSDK_remove_IAPOfferAcceptedWithObject_m383657210 (Il2CppObject * __this /* static, unused */, Action_1_t3974970450 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_VirtualGoodsOfferAcceptedWithObject(System.Action`1<FuseMisc.VGOfferInfo>)
extern "C"  void FuseSDK_add_VirtualGoodsOfferAcceptedWithObject_m1175353235 (Il2CppObject * __this /* static, unused */, Action_1_t1534247929 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_VirtualGoodsOfferAcceptedWithObject(System.Action`1<FuseMisc.VGOfferInfo>)
extern "C"  void FuseSDK_remove_VirtualGoodsOfferAcceptedWithObject_m913705358 (Il2CppObject * __this /* static, unused */, Action_1_t1534247929 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_TimeUpdated(System.Action`1<System.DateTime>)
extern "C"  void FuseSDK_add_TimeUpdated_m1170725926 (Il2CppObject * __this /* static, unused */, Action_1_t487486641 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_TimeUpdated(System.Action`1<System.DateTime>)
extern "C"  void FuseSDK_remove_TimeUpdated_m2753468491 (Il2CppObject * __this /* static, unused */, Action_1_t487486641 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_GameDataError(System.Action`2<System.Int32,FuseMisc.FuseError>)
extern "C"  void FuseSDK_add_GameDataError_m1659727886 (Il2CppObject * __this /* static, unused */, Action_2_t1216077269 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_GameDataError(System.Action`2<System.Int32,FuseMisc.FuseError>)
extern "C"  void FuseSDK_remove_GameDataError_m4046277875 (Il2CppObject * __this /* static, unused */, Action_2_t1216077269 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_GameDataSetAcknowledged(System.Action`1<System.Int32>)
extern "C"  void FuseSDK_add_GameDataSetAcknowledged_m928918873 (Il2CppObject * __this /* static, unused */, Action_1_t2995867492 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_GameDataSetAcknowledged(System.Action`1<System.Int32>)
extern "C"  void FuseSDK_remove_GameDataSetAcknowledged_m3614984916 (Il2CppObject * __this /* static, unused */, Action_1_t2995867492 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::add_GameDataReceived(System.Action`4<System.Int32,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>)
extern "C"  void FuseSDK_add_GameDataReceived_m1887878304 (Il2CppObject * __this /* static, unused */, Action_4_t4157318299 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::remove_GameDataReceived(System.Action`4<System.Int32,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>)
extern "C"  void FuseSDK_remove_GameDataReceived_m4228795995 (Il2CppObject * __this /* static, unused */, Action_4_t4157318299 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnSessionStartReceived()
extern "C"  void FuseSDK_OnSessionStartReceived_m2049253662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnSessionLoginError(System.Int32)
extern "C"  void FuseSDK_OnSessionLoginError_m1579096951 (Il2CppObject * __this /* static, unused */, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnPurchaseVerification(System.Int32,System.String,System.String)
extern "C"  void FuseSDK_OnPurchaseVerification_m650257910 (Il2CppObject * __this /* static, unused */, int32_t ___verified, String_t* ___transactionId, String_t* ___originalTransactionId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnAdAvailabilityResponse(System.Int32,System.Int32)
extern "C"  void FuseSDK_OnAdAvailabilityResponse_m3625410166 (Il2CppObject * __this /* static, unused */, int32_t ___available, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnAdWillClose()
extern "C"  void FuseSDK_OnAdWillClose_m2256651444 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnAdFailedToDisplay()
extern "C"  void FuseSDK_OnAdFailedToDisplay_m559046328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnAdDidShow(System.Int32,System.Int32)
extern "C"  void FuseSDK_OnAdDidShow_m262328604 (Il2CppObject * __this /* static, unused */, int32_t ___networkId, int32_t ___mediaType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnRewardedAdCompleted(FuseMisc.RewardedInfo)
extern "C"  void FuseSDK_OnRewardedAdCompleted_m3162148842 (Il2CppObject * __this /* static, unused */, RewardedInfo_t829972251  ___rewardInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnIAPOfferAccepted(FuseMisc.IAPOfferInfo)
extern "C"  void FuseSDK_OnIAPOfferAccepted_m1326213027 (Il2CppObject * __this /* static, unused */, IAPOfferInfo_t3826517745  ___offerInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnVirtualGoodsOfferAccepted(FuseMisc.VGOfferInfo)
extern "C"  void FuseSDK_OnVirtualGoodsOfferAccepted_m3383985617 (Il2CppObject * __this /* static, unused */, VGOfferInfo_t1385795224  ___offerInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnAdClickedWithURL(System.String)
extern "C"  void FuseSDK_OnAdClickedWithURL_m3254180204 (Il2CppObject * __this /* static, unused */, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnNotificationAction(System.String)
extern "C"  void FuseSDK_OnNotificationAction_m222392976 (Il2CppObject * __this /* static, unused */, String_t* ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnNotificationWillClose()
extern "C"  void FuseSDK_OnNotificationWillClose_m1695164140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnAccountLoginComplete(System.Int32,System.String)
extern "C"  void FuseSDK_OnAccountLoginComplete_m1802209427 (Il2CppObject * __this /* static, unused */, int32_t ___type, String_t* ___accountId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnAccountLoginError(System.String,System.Int32)
extern "C"  void FuseSDK_OnAccountLoginError_m3668425490 (Il2CppObject * __this /* static, unused */, String_t* ___accountId, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnTimeUpdated(System.DateTime)
extern "C"  void FuseSDK_OnTimeUpdated_m2041530777 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnFriendAdded(System.String,System.Int32)
extern "C"  void FuseSDK_OnFriendAdded_m4135937256 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnFriendRemoved(System.String,System.Int32)
extern "C"  void FuseSDK_OnFriendRemoved_m1751502792 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnFriendAccepted(System.String,System.Int32)
extern "C"  void FuseSDK_OnFriendAccepted_m4288202667 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnFriendRejected(System.String,System.Int32)
extern "C"  void FuseSDK_OnFriendRejected_m1189618690 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnFriendsMigrated(System.String,System.Int32)
extern "C"  void FuseSDK_OnFriendsMigrated_m4030513844 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnFriendsListUpdated(System.Collections.Generic.List`1<FuseMisc.Friend>)
extern "C"  void FuseSDK_OnFriendsListUpdated_m502898524 (Il2CppObject * __this /* static, unused */, List_1_t1333671126 * ___friends, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnFriendsListError(System.Int32)
extern "C"  void FuseSDK_OnFriendsListError_m1439699959 (Il2CppObject * __this /* static, unused */, int32_t ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnGameConfigurationReceived()
extern "C"  void FuseSDK_OnGameConfigurationReceived_m3123175414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnGameDataError(System.Int32,System.Int32)
extern "C"  void FuseSDK_OnGameDataError_m719612681 (Il2CppObject * __this /* static, unused */, int32_t ___error, int32_t ___requestId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnGameDataSetAcknowledged(System.Int32)
extern "C"  void FuseSDK_OnGameDataSetAcknowledged_m2838243536 (Il2CppObject * __this /* static, unused */, int32_t ___requestId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnGameDataReceived(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int32)
extern "C"  void FuseSDK_OnGameDataReceived_m883737616 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___dataKey, Dictionary_2_t2606186806 * ___data, int32_t ___requestId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_StartSession(System.String,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void FuseSDK_Native_StartSession_m1071771623 (Il2CppObject * __this /* static, unused */, String_t* ___gameId, bool ___registerForPush, bool ___handleAdURLs, bool ___enableCrashDetection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterPushToken(System.Byte[],System.Int32)
extern "C"  void FuseSDK_Native_RegisterPushToken_m49313396 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___token, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_ReceivedRemoteNotification(System.String)
extern "C"  void FuseSDK_Native_ReceivedRemoteNotification_m1602007352 (Il2CppObject * __this /* static, unused */, String_t* ___notificationId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_SetUnityGameObject(System.String)
extern "C"  void FuseSDK_Native_SetUnityGameObject_m3710335398 (Il2CppObject * __this /* static, unused */, String_t* ___gameObjectName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_RegisterEventVariable(System.String,System.String,System.String,System.String,System.Double)
extern "C"  bool FuseSDK_Native_RegisterEventVariable_m3671701833 (Il2CppObject * __this /* static, unused */, String_t* ___name, String_t* ___paramName, String_t* ___paramValue, String_t* ___variableName, double ___variableValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_RegisterEventWithDictionary(System.String,System.String,System.String,System.String[],System.Double[],System.Int32)
extern "C"  bool FuseSDK_Native_RegisterEventWithDictionary_m1672630642 (Il2CppObject * __this /* static, unused */, String_t* ___message, String_t* ___paramName, String_t* ___paramValue, StringU5BU5D_t2956870243* ___keys, DoubleU5BU5D_t1048280995* ___values, int32_t ___numEntries, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterVirtualGoodsPurchase(System.Int32,System.Int32,System.Int32)
extern "C"  void FuseSDK_Native_RegisterVirtualGoodsPurchase_m519104408 (Il2CppObject * __this /* static, unused */, int32_t ___virtualgoodID, int32_t ___currencyAmount, int32_t ___currencyID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterInAppPurchaseList(System.String[],System.String[],System.Single[],System.Int32)
extern "C"  void FuseSDK_Native_RegisterInAppPurchaseList_m3231503004 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___productId, StringU5BU5D_t2956870243* ___priceLocale, SingleU5BU5D_t1219431280* ___price, int32_t ___numEntries, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterInAppPurchase(System.String,System.String,System.Byte[],System.Int32,System.Int32)
extern "C"  void FuseSDK_Native_RegisterInAppPurchase_m348866365 (Il2CppObject * __this /* static, unused */, String_t* ___productId, String_t* ___transactionId, ByteU5BU5D_t58506160* ___transactionReceiptBuffer, int32_t ___transactionReceiptLength, int32_t ___transactionState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterUnibillPurchase(System.String,System.Byte[],System.Int32)
extern "C"  void FuseSDK_Native_RegisterUnibillPurchase_m3955303697 (Il2CppObject * __this /* static, unused */, String_t* ___productID, ByteU5BU5D_t58506160* ___receipt, int32_t ___receiptLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_IsAdAvailableForZoneID(System.String)
extern "C"  bool FuseSDK_Native_IsAdAvailableForZoneID_m1070722658 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_ZoneHasRewarded(System.String)
extern "C"  bool FuseSDK_Native_ZoneHasRewarded_m574832144 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_ZoneHasIAPOffer(System.String)
extern "C"  bool FuseSDK_Native_ZoneHasIAPOffer_m2720625786 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_ZoneHasVirtualGoodsOffer(System.String)
extern "C"  bool FuseSDK_Native_ZoneHasVirtualGoodsOffer_m2877255123 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::Native_GetRewardedInfoForZone(System.String)
extern "C"  String_t* FuseSDK_Native_GetRewardedInfoForZone_m2171737092 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::Native_GetVirtualGoodsOfferInfoForZoneID(System.String)
extern "C"  String_t* FuseSDK_Native_GetVirtualGoodsOfferInfoForZoneID_m3907036564 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::Native_GetIAPOfferInfoForZoneID(System.String)
extern "C"  String_t* FuseSDK_Native_GetIAPOfferInfoForZoneID_m3128281759 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_ShowAdForZoneID(System.String,System.String[],System.String[],System.Int32)
extern "C"  void FuseSDK_Native_ShowAdForZoneID_m1395443739 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, StringU5BU5D_t2956870243* ___optionKeys, StringU5BU5D_t2956870243* ___optionValues, int32_t ___numOptions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_PreloadAdForZone(System.String)
extern "C"  void FuseSDK_Native_PreloadAdForZone_m2574987585 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_SetRewardedVideoUserID(System.String)
extern "C"  void FuseSDK_Native_SetRewardedVideoUserID_m1352186393 (Il2CppObject * __this /* static, unused */, String_t* ___userID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_DisplayNotifications()
extern "C"  void FuseSDK_Native_DisplayNotifications_m3833872766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_IsNotificationAvailable()
extern "C"  bool FuseSDK_Native_IsNotificationAvailable_m2029103306 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterGender(System.Int32)
extern "C"  void FuseSDK_Native_RegisterGender_m3465403021 (Il2CppObject * __this /* static, unused */, int32_t ___gender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterLevel(System.Int32)
extern "C"  void FuseSDK_Native_RegisterLevel_m3367885148 (Il2CppObject * __this /* static, unused */, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_RegisterCurrency(System.Int32,System.Int32)
extern "C"  bool FuseSDK_Native_RegisterCurrency_m3225143142 (Il2CppObject * __this /* static, unused */, int32_t ___type, int32_t ___balance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterAge(System.Int32)
extern "C"  void FuseSDK_Native_RegisterAge_m4022095127 (Il2CppObject * __this /* static, unused */, int32_t ___age, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterBirthday(System.Int32,System.Int32,System.Int32)
extern "C"  void FuseSDK_Native_RegisterBirthday_m2218280681 (Il2CppObject * __this /* static, unused */, int32_t ___year, int32_t ___month, int32_t ___day, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RegisterParentalConsent(System.Boolean)
extern "C"  void FuseSDK_Native_RegisterParentalConsent_m2154152387 (Il2CppObject * __this /* static, unused */, bool ___consentGranted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_RegisterCustomEventInt(System.Int32,System.Int32)
extern "C"  bool FuseSDK_Native_RegisterCustomEventInt_m2604151633 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_RegisterCustomEventString(System.Int32,System.String)
extern "C"  bool FuseSDK_Native_RegisterCustomEventString_m946012154 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::Native_GetFuseId()
extern "C"  String_t* FuseSDK_Native_GetFuseId_m1427507577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::Native_GetOriginalAccountAlias()
extern "C"  String_t* FuseSDK_Native_GetOriginalAccountAlias_m960986801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::Native_GetOriginalAccountId()
extern "C"  String_t* FuseSDK_Native_GetOriginalAccountId_m4035001340 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FuseSDK::Native_GetOriginalAccountType()
extern "C"  int32_t FuseSDK_Native_GetOriginalAccountType_m783477574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_GameCenterLogin()
extern "C"  void FuseSDK_Native_GameCenterLogin_m1956526124 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_FacebookLogin(System.String,System.String,System.String)
extern "C"  void FuseSDK_Native_FacebookLogin_m2276549741 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId, String_t* ___name, String_t* ___accessToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_TwitterLogin(System.String,System.String)
extern "C"  void FuseSDK_Native_TwitterLogin_m3795967248 (Il2CppObject * __this /* static, unused */, String_t* ___twitterId, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_FuseLogin(System.String,System.String)
extern "C"  void FuseSDK_Native_FuseLogin_m2087177036 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_EmailLogin(System.String,System.String)
extern "C"  void FuseSDK_Native_EmailLogin_m114416185 (Il2CppObject * __this /* static, unused */, String_t* ___email, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_DeviceLogin(System.String)
extern "C"  void FuseSDK_Native_DeviceLogin_m1563937733 (Il2CppObject * __this /* static, unused */, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_GooglePlayLogin(System.String,System.String)
extern "C"  void FuseSDK_Native_GooglePlayLogin_m4184420536 (Il2CppObject * __this /* static, unused */, String_t* ___alias, String_t* ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FuseSDK::Native_GamesPlayed()
extern "C"  int32_t FuseSDK_Native_GamesPlayed_m3762996496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::Native_LibraryVersion()
extern "C"  String_t* FuseSDK_Native_LibraryVersion_m605514616 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_Connected()
extern "C"  bool FuseSDK_Native_Connected_m497903679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_TimeFromServer()
extern "C"  void FuseSDK_Native_TimeFromServer_m815599794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_EnableData()
extern "C"  void FuseSDK_Native_EnableData_m1268546821 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_DisableData()
extern "C"  void FuseSDK_Native_DisableData_m1114487452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Native_DataEnabled()
extern "C"  bool FuseSDK_Native_DataEnabled_m4242094253 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_UpdateFriendsListFromServer()
extern "C"  void FuseSDK_Native_UpdateFriendsListFromServer_m3118599969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_AddFriend(System.String)
extern "C"  void FuseSDK_Native_AddFriend_m1063652377 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RemoveFriend(System.String)
extern "C"  void FuseSDK_Native_RemoveFriend_m3053180616 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_AcceptFriend(System.String)
extern "C"  void FuseSDK_Native_AcceptFriend_m111203524 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_RejectFriend(System.String)
extern "C"  void FuseSDK_Native_RejectFriend_m2978146957 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_MigrateFriends(System.String)
extern "C"  void FuseSDK_Native_MigrateFriends_m2033398368 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_UserPushNotification(System.String,System.String)
extern "C"  void FuseSDK_Native_UserPushNotification_m2529849334 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Native_FriendsPushNotification(System.String)
extern "C"  void FuseSDK_Native_FriendsPushNotification_m776809534 (Il2CppObject * __this /* static, unused */, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::Native_GetGameConfigurationValue(System.String)
extern "C"  String_t* FuseSDK_Native_GetGameConfigurationValue_m777724088 (Il2CppObject * __this /* static, unused */, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FuseSDK::Native_SetGameData(System.String,System.String,System.String[],System.String[],System.Int32)
extern "C"  int32_t FuseSDK_Native_SetGameData_m2957575807 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___key, StringU5BU5D_t2956870243* ___varKeys, StringU5BU5D_t2956870243* ___varValues, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FuseSDK::Native_GetGameData(System.String,System.String,System.String[],System.Int32)
extern "C"  int32_t FuseSDK_Native_GetGameData_m4250402445 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___key, StringU5BU5D_t2956870243* ___keys, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Awake()
extern "C"  void FuseSDK_Awake_m2896976885 (FuseSDK_t1159654649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Start()
extern "C"  void FuseSDK_Start_m1606509458 (FuseSDK_t1159654649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::OnApplicationPause(System.Boolean)
extern "C"  void FuseSDK_OnApplicationPause_m2720010542 (FuseSDK_t1159654649 * __this, bool ___pausing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::StartSession()
extern "C"  void FuseSDK_StartSession_m3050284614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_StartSession(System.String,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void FuseSDK__StartSession_m627633534 (Il2CppObject * __this /* static, unused */, String_t* ___gameId, bool ___registerForPush, bool ___handleAdURLs, bool ___enableCrashDetection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::RegisterEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  bool FuseSDK_RegisterEvent_m3517488528 (Il2CppObject * __this /* static, unused */, String_t* ___name, Dictionary_2_t2606186806 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::RegisterEvent(System.String,System.String,System.String,System.Collections.Hashtable)
extern "C"  bool FuseSDK_RegisterEvent_m412989983 (Il2CppObject * __this /* static, unused */, String_t* ___name, String_t* ___paramName, String_t* ___paramValue, Hashtable_t3875263730 * ___variables, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::RegisterEvent(System.String,System.String,System.String,System.String,System.Double)
extern "C"  bool FuseSDK_RegisterEvent_m2414525879 (Il2CppObject * __this /* static, unused */, String_t* ___name, String_t* ___paramName, String_t* ___paramValue, String_t* ___variableName, double ___variableValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterVirtualGoodsPurchase(System.Int32,System.Int32,System.Int32)
extern "C"  void FuseSDK_RegisterVirtualGoodsPurchase_m316273874 (Il2CppObject * __this /* static, unused */, int32_t ___virtualgoodID, int32_t ___currencyAmount, int32_t ___currencyID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterIOSInAppPurchaseList(FuseMisc.Product[])
extern "C"  void FuseSDK_RegisterIOSInAppPurchaseList_m2037236147 (Il2CppObject * __this /* static, unused */, ProductU5BU5D_t4249436827* ___products, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterIOSInAppPurchase(System.String,System.String,System.Byte[],FuseMisc.IAPState)
extern "C"  void FuseSDK_RegisterIOSInAppPurchase_m2575870670 (Il2CppObject * __this /* static, unused */, String_t* ___productId, String_t* ___transactionId, ByteU5BU5D_t58506160* ___transactionReceipt, int32_t ___transactionState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterUnibillPurchase(System.String,System.Byte[])
extern "C"  void FuseSDK_RegisterUnibillPurchase_m1122362048 (Il2CppObject * __this /* static, unused */, String_t* ___productID, ByteU5BU5D_t58506160* ___receipt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterAndroidInAppPurchase(FuseMisc.IAPState,System.String,System.String,System.String,System.DateTime,System.String,System.Double,System.String)
extern "C"  void FuseSDK_RegisterAndroidInAppPurchase_m2341887311 (Il2CppObject * __this /* static, unused */, int32_t ___purchaseState, String_t* ___purchaseToken, String_t* ___productId, String_t* ___orderId, DateTime_t339033936  ___purchaseTime, String_t* ___developerPayload, double ___price, String_t* ___currency, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterAndroidInAppPurchase(FuseMisc.IAPState,System.String,System.String,System.String,System.Int64,System.String,System.Double,System.String)
extern "C"  void FuseSDK_RegisterAndroidInAppPurchase_m3043945475 (Il2CppObject * __this /* static, unused */, int32_t ___purchaseState, String_t* ___purchaseToken, String_t* ___productId, String_t* ___orderId, int64_t ___purchaseTime, String_t* ___developerPayload, double ___price, String_t* ___currency, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::IsAdAvailableForZoneID(System.String)
extern "C"  bool FuseSDK_IsAdAvailableForZoneID_m380410448 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::ZoneHasRewarded(System.String)
extern "C"  bool FuseSDK_ZoneHasRewarded_m3490578914 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::ZoneHasIAPOffer(System.String)
extern "C"  bool FuseSDK_ZoneHasIAPOffer_m1341405260 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::ZoneHasVirtualGoodsOffer(System.String)
extern "C"  bool FuseSDK_ZoneHasVirtualGoodsOffer_m912184897 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FuseMisc.RewardedInfo FuseSDK::GetRewardedInfoForZone(System.String)
extern "C"  RewardedInfo_t829972251  FuseSDK_GetRewardedInfoForZone_m357494019 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FuseMisc.VGOfferInfo FuseSDK::GetVGOfferInfoForZone(System.String)
extern "C"  VGOfferInfo_t1385795224  FuseSDK_GetVGOfferInfoForZone_m1031517147 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FuseMisc.IAPOfferInfo FuseSDK::GetIAPOfferInfoForZone(System.String)
extern "C"  IAPOfferInfo_t3826517745  FuseSDK_GetIAPOfferInfoForZone_m18495939 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::ShowAdForZoneID(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void FuseSDK_ShowAdForZoneID_m620550069 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, Dictionary_2_t2606186806 * ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::PreloadAdForZoneID(System.String)
extern "C"  void FuseSDK_PreloadAdForZoneID_m3995970508 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::SetRewardedVideoUserID(System.String)
extern "C"  void FuseSDK_SetRewardedVideoUserID_m202042399 (Il2CppObject * __this /* static, unused */, String_t* ___userID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::DisplayNotifications()
extern "C"  void FuseSDK_DisplayNotifications_m431031352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::IsNotificationAvailable()
extern "C"  bool FuseSDK_IsNotificationAvailable_m3572255160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterGender(FuseMisc.Gender)
extern "C"  void FuseSDK_RegisterGender_m135225242 (Il2CppObject * __this /* static, unused */, int32_t ___gender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterAge(System.Int32)
extern "C"  void FuseSDK_RegisterAge_m4086618141 (Il2CppObject * __this /* static, unused */, int32_t ___age, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterBirthday(System.Int32,System.Int32,System.Int32)
extern "C"  void FuseSDK_RegisterBirthday_m1764743971 (Il2CppObject * __this /* static, unused */, int32_t ___year, int32_t ___month, int32_t ___day, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterLevel(System.Int32)
extern "C"  void FuseSDK_RegisterLevel_m949992162 (Il2CppObject * __this /* static, unused */, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::RegisterCurrency(System.Int32,System.Int32)
extern "C"  bool FuseSDK_RegisterCurrency_m2201307348 (Il2CppObject * __this /* static, unused */, int32_t ___currencyType, int32_t ___balance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RegisterParentalConsent(System.Boolean)
extern "C"  void FuseSDK_RegisterParentalConsent_m672369225 (Il2CppObject * __this /* static, unused */, bool ___consentGranted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::RegisterCustomEvent(System.Int32,System.String)
extern "C"  bool FuseSDK_RegisterCustomEvent_m4096309751 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::RegisterCustomEvent(System.Int32,System.Int32)
extern "C"  bool FuseSDK_RegisterCustomEvent_m3165448124 (Il2CppObject * __this /* static, unused */, int32_t ___eventNumber, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::GetFuseId()
extern "C"  String_t* FuseSDK_GetFuseId_m1990982053 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::GetOriginalAccountAlias()
extern "C"  String_t* FuseSDK_GetOriginalAccountAlias_m2540679645 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::GetOriginalAccountId()
extern "C"  String_t* FuseSDK_GetOriginalAccountId_m1297987664 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FuseMisc.AccountType FuseSDK::GetOriginalAccountType()
extern "C"  int32_t FuseSDK_GetOriginalAccountType_m3287745243 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::GameCenterLogin()
extern "C"  void FuseSDK_GameCenterLogin_m811411762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::FacebookLogin(System.String,System.String,System.String)
extern "C"  void FuseSDK_FacebookLogin_m1823013031 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId, String_t* ___name, String_t* ___accessToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::TwitterLogin(System.String,System.String)
extern "C"  void FuseSDK_TwitterLogin_m1436523542 (Il2CppObject * __this /* static, unused */, String_t* ___twitterId, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::FuseLogin(System.String,System.String)
extern "C"  void FuseSDK_FuseLogin_m792451590 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::EmailLogin(System.String,System.String)
extern "C"  void FuseSDK_EmailLogin_m2927600319 (Il2CppObject * __this /* static, unused */, String_t* ___email, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::DeviceLogin(System.String)
extern "C"  void FuseSDK_DeviceLogin_m3564151167 (Il2CppObject * __this /* static, unused */, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::GooglePlayLogin(System.String,System.String)
extern "C"  void FuseSDK_GooglePlayLogin_m1136774130 (Il2CppObject * __this /* static, unused */, String_t* ___alias, String_t* ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::ManualRegisterForPushNotifications(System.String)
extern "C"  void FuseSDK_ManualRegisterForPushNotifications_m2411783554 (Il2CppObject * __this /* static, unused */, String_t* ____, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FuseSDK::GamesPlayed()
extern "C"  int32_t FuseSDK_GamesPlayed_m3460410354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::LibraryVersion()
extern "C"  String_t* FuseSDK_LibraryVersion_m2379003596 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::Connected()
extern "C"  bool FuseSDK_Connected_m253239725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::UTCTimeFromServer()
extern "C"  void FuseSDK_UTCTimeFromServer_m1427789486 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::FuseLog(System.String)
extern "C"  void FuseSDK_FuseLog_m2054806159 (Il2CppObject * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::EnableData()
extern "C"  void FuseSDK_EnableData_m2615181375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::DisableData()
extern "C"  void FuseSDK_DisableData_m4205452962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FuseSDK::DataEnabled()
extern "C"  bool FuseSDK_DataEnabled_m1048268443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::UpdateFriendsListFromServer()
extern "C"  void FuseSDK_UpdateFriendsListFromServer_m3100747559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FuseMisc.Friend> FuseSDK::GetFriendsList()
extern "C"  List_1_t1333671126 * FuseSDK_GetFriendsList_m2688560911 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::AddFriend(System.String)
extern "C"  void FuseSDK_AddFriend_m3698133075 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RemoveFriend(System.String)
extern "C"  void FuseSDK_RemoveFriend_m635287630 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::AcceptFriend(System.String)
extern "C"  void FuseSDK_AcceptFriend_m1988277834 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::RejectFriend(System.String)
extern "C"  void FuseSDK_RejectFriend_m560253971 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::MigrateFriends(System.String)
extern "C"  void FuseSDK_MigrateFriends_m2015545958 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::UserPushNotification(System.String,System.String)
extern "C"  void FuseSDK_UserPushNotification_m3851667708 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::FriendsPushNotification(System.String)
extern "C"  void FuseSDK_FriendsPushNotification_m3777051384 (Il2CppObject * __this /* static, unused */, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FuseSDK::GetGameConfigurationValue(System.String)
extern "C"  String_t* FuseSDK_GetGameConfigurationValue_m2407051276 (Il2CppObject * __this /* static, unused */, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> FuseSDK::GetGameConfiguration()
extern "C"  Dictionary_2_t2606186806 * FuseSDK_GetGameConfiguration_m586707122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FuseSDK::SetGameData(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.String,System.String)
extern "C"  int32_t FuseSDK_SetGameData_m592660709 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___data, String_t* ___fuseId, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FuseSDK::GetGameData(System.String[])
extern "C"  int32_t FuseSDK_GetGameData_m1213875184 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FuseSDK::GetGameDataForFuseId(System.String,System.String,System.String[])
extern "C"  int32_t FuseSDK_GetGameDataForFuseId_m969065313 (Il2CppObject * __this /* static, unused */, String_t* ___fuseId, String_t* ___key, StringU5BU5D_t2956870243* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FuseSDK::SetupPushNotifications()
extern "C"  Il2CppObject * FuseSDK_SetupPushNotifications_m4090347419 (FuseSDK_t1159654649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_SessionStartReceived(System.String)
extern "C"  void FuseSDK__CB_SessionStartReceived_m1829964418 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_SessionLoginError(System.String)
extern "C"  void FuseSDK__CB_SessionLoginError_m645033598 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_PurchaseVerification(System.String)
extern "C"  void FuseSDK__CB_PurchaseVerification_m1024716371 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_AdAvailabilityResponse(System.String)
extern "C"  void FuseSDK__CB_AdAvailabilityResponse_m307949232 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_AdWillClose(System.String)
extern "C"  void FuseSDK__CB_AdWillClose_m3403440432 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_AdFailedToDisplay(System.String)
extern "C"  void FuseSDK__CB_AdFailedToDisplay_m1463180332 (FuseSDK_t1159654649 * __this, String_t* ____, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_AdDidShow(System.String)
extern "C"  void FuseSDK__CB_AdDidShow_m3677769946 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_RewardedAdCompleted(System.String)
extern "C"  void FuseSDK__CB_RewardedAdCompleted_m1665520185 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_IAPOfferAccepted(System.String)
extern "C"  void FuseSDK__CB_IAPOfferAccepted_m2747711620 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_VirtualGoodsOfferAccepted(System.String)
extern "C"  void FuseSDK__CB_VirtualGoodsOfferAccepted_m369764507 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_HandleAdClickWithURL(System.String)
extern "C"  void FuseSDK__CB_HandleAdClickWithURL_m3303586275 (FuseSDK_t1159654649 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_NotificationAction(System.String)
extern "C"  void FuseSDK__CB_NotificationAction_m377101806 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_NotificationWillClose(System.String)
extern "C"  void FuseSDK__CB_NotificationWillClose_m2947106680 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_AccountLoginComplete(System.String)
extern "C"  void FuseSDK__CB_AccountLoginComplete_m3315127386 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_AccountLoginError(System.String)
extern "C"  void FuseSDK__CB_AccountLoginError_m3569700551 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_TimeUpdated(System.String)
extern "C"  void FuseSDK__CB_TimeUpdated_m2956122373 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_FriendAdded(System.String)
extern "C"  void FuseSDK__CB_FriendAdded_m1285632689 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_FriendRemoved(System.String)
extern "C"  void FuseSDK__CB_FriendRemoved_m570788689 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_FriendAccepted(System.String)
extern "C"  void FuseSDK__CB_FriendAccepted_m782915338 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_FriendRejected(System.String)
extern "C"  void FuseSDK__CB_FriendRejected_m260618579 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_FriendsMigrated(System.String)
extern "C"  void FuseSDK__CB_FriendsMigrated_m2973587557 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_FriendsListUpdated(System.String)
extern "C"  void FuseSDK__CB_FriendsListUpdated_m3324444839 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_FriendsListError(System.String)
extern "C"  void FuseSDK__CB_FriendsListError_m1257439226 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_GameConfigurationReceived(System.String)
extern "C"  void FuseSDK__CB_GameConfigurationReceived_m2261470638 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_GameDataSetAcknowledged(System.String)
extern "C"  void FuseSDK__CB_GameDataSetAcknowledged_m4268972741 (FuseSDK_t1159654649 * __this, String_t* ___requestId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_GameDataError(System.String)
extern "C"  void FuseSDK__CB_GameDataError_m1651336519 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::_CB_GameDataReceived(System.String)
extern "C"  void FuseSDK__CB_GameDataReceived_m806817842 (FuseSDK_t1159654649 * __this, String_t* ___param, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::Internal_StartSession(System.String,System.Boolean,System.Boolean)
extern "C"  void FuseSDK_Internal_StartSession_m337825724 (Il2CppObject * __this /* static, unused */, String_t* ___appID, bool ___registerForPush, bool ___enableCrashDetection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuseSDK::StartSession(System.Action`1<System.String>)
extern "C"  void FuseSDK_StartSession_m441714530 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___adClickedWithURLHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
