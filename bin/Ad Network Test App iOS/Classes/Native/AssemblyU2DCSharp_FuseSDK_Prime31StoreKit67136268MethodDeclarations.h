﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuseSDK_Prime31StoreKit
struct FuseSDK_Prime31StoreKit_t67136268;

#include "codegen/il2cpp-codegen.h"

// System.Void FuseSDK_Prime31StoreKit::.ctor()
extern "C"  void FuseSDK_Prime31StoreKit__ctor_m1472700895 (FuseSDK_Prime31StoreKit_t67136268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
