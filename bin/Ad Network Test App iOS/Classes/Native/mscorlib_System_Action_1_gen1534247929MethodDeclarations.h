﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<FuseMisc.VGOfferInfo>
struct Action_1_t1534247929;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_FuseMisc_VGOfferInfo1385795224.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<FuseMisc.VGOfferInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m6870718_gshared (Action_1_t1534247929 * __this, Il2CppObject * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Action_1__ctor_m6870718(__this, ___object, ___method, method) ((  void (*) (Action_1_t1534247929 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m6870718_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<FuseMisc.VGOfferInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m266750706_gshared (Action_1_t1534247929 * __this, VGOfferInfo_t1385795224  ___obj, const MethodInfo* method);
#define Action_1_Invoke_m266750706(__this, ___obj, method) ((  void (*) (Action_1_t1534247929 *, VGOfferInfo_t1385795224 , const MethodInfo*))Action_1_Invoke_m266750706_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<FuseMisc.VGOfferInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1844092699_gshared (Action_1_t1534247929 * __this, VGOfferInfo_t1385795224  ___obj, AsyncCallback_t1363551830 * ___callback, Il2CppObject * ___object, const MethodInfo* method);
#define Action_1_BeginInvoke_m1844092699(__this, ___obj, ___callback, ___object, method) ((  Il2CppObject * (*) (Action_1_t1534247929 *, VGOfferInfo_t1385795224 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m1844092699_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<FuseMisc.VGOfferInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3250673614_gshared (Action_1_t1534247929 * __this, Il2CppObject * ___result, const MethodInfo* method);
#define Action_1_EndInvoke_m3250673614(__this, ___result, method) ((  void (*) (Action_1_t1534247929 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3250673614_gshared)(__this, ___result, method)
