﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// System.Action`1<FuseMisc.FuseError>
struct Action_1_t592684423;
// System.Action`2<FuseMisc.AccountType,System.String>
struct Action_2_t535153670;
// System.Action`2<System.String,System.String>
struct Action_2_t2887221574;
// System.Action`1<System.String>
struct Action_1_t1116941607;
// System.Action`2<System.String,FuseMisc.FuseError>
struct Action_2_t2362964390;
// System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>>
struct Action_1_t1482123831;
// System.Action`3<System.Int32,System.String,System.String>
struct Action_3_t3047918030;
// System.Action`2<System.Boolean,FuseMisc.FuseError>
struct Action_2_t3211021459;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t3619260338;
// System.Action`1<FuseMisc.RewardedInfo>
struct Action_1_t978424956;
// System.Action`1<FuseMisc.IAPOfferInfo>
struct Action_1_t3974970450;
// System.Action`1<FuseMisc.VGOfferInfo>
struct Action_1_t1534247929;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuseSDKEditorSession
struct  FuseSDKEditorSession_t360633328  : public Il2CppObject
{
public:

public:
};

struct FuseSDKEditorSession_t360633328_StaticFields
{
public:
	// System.Action FuseSDKEditorSession::SessionStartReceived
	Action_t437523947 * ___SessionStartReceived_0;
	// System.Action`1<FuseMisc.FuseError> FuseSDKEditorSession::SessionLoginError
	Action_1_t592684423 * ___SessionLoginError_1;
	// System.Action FuseSDKEditorSession::GameConfigurationReceived
	Action_t437523947 * ___GameConfigurationReceived_2;
	// System.Action`2<FuseMisc.AccountType,System.String> FuseSDKEditorSession::AccountLoginComplete
	Action_2_t535153670 * ___AccountLoginComplete_3;
	// System.Action`2<System.String,System.String> FuseSDKEditorSession::AccountLoginError
	Action_2_t2887221574 * ___AccountLoginError_4;
	// System.Action`1<System.String> FuseSDKEditorSession::NotificationAction
	Action_1_t1116941607 * ___NotificationAction_5;
	// System.Action FuseSDKEditorSession::NotificationWillClose
	Action_t437523947 * ___NotificationWillClose_6;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDKEditorSession::FriendAdded
	Action_2_t2362964390 * ___FriendAdded_7;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDKEditorSession::FriendRemoved
	Action_2_t2362964390 * ___FriendRemoved_8;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDKEditorSession::FriendAccepted
	Action_2_t2362964390 * ___FriendAccepted_9;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDKEditorSession::FriendRejected
	Action_2_t2362964390 * ___FriendRejected_10;
	// System.Action`2<System.String,FuseMisc.FuseError> FuseSDKEditorSession::FriendsMigrated
	Action_2_t2362964390 * ___FriendsMigrated_11;
	// System.Action`1<System.Collections.Generic.List`1<FuseMisc.Friend>> FuseSDKEditorSession::FriendsListUpdated
	Action_1_t1482123831 * ___FriendsListUpdated_12;
	// System.Action`1<FuseMisc.FuseError> FuseSDKEditorSession::FriendsListError
	Action_1_t592684423 * ___FriendsListError_13;
	// System.Action`3<System.Int32,System.String,System.String> FuseSDKEditorSession::PurchaseVerification
	Action_3_t3047918030 * ___PurchaseVerification_14;
	// System.Action`2<System.Boolean,FuseMisc.FuseError> FuseSDKEditorSession::AdAvailabilityResponse
	Action_2_t3211021459 * ___AdAvailabilityResponse_15;
	// System.Action FuseSDKEditorSession::AdWillClose
	Action_t437523947 * ___AdWillClose_16;
	// System.Action FuseSDKEditorSession::AdFailedToDisplay
	Action_t437523947 * ___AdFailedToDisplay_17;
	// System.Action`2<System.Int32,System.Int32> FuseSDKEditorSession::AdDidShow
	Action_2_t3619260338 * ___AdDidShow_18;
	// System.Action`1<FuseMisc.RewardedInfo> FuseSDKEditorSession::RewardedAdCompletedWithObject
	Action_1_t978424956 * ___RewardedAdCompletedWithObject_19;
	// System.Action`1<FuseMisc.IAPOfferInfo> FuseSDKEditorSession::IAPOfferAcceptedWithObject
	Action_1_t3974970450 * ___IAPOfferAcceptedWithObject_20;
	// System.Action`1<FuseMisc.VGOfferInfo> FuseSDKEditorSession::VirtualGoodsOfferAcceptedWithObject
	Action_1_t1534247929 * ___VirtualGoodsOfferAcceptedWithObject_21;
	// System.Action`1<System.DateTime> FuseSDKEditorSession::TimeUpdated
	Action_1_t487486641 * ___TimeUpdated_22;
	// System.Action FuseSDKEditorSession::<>f__am$cache17
	Action_t437523947 * ___U3CU3Ef__amU24cache17_23;
	// System.Action`1<FuseMisc.FuseError> FuseSDKEditorSession::<>f__am$cache18
	Action_1_t592684423 * ___U3CU3Ef__amU24cache18_24;
	// System.Action FuseSDKEditorSession::<>f__am$cache19
	Action_t437523947 * ___U3CU3Ef__amU24cache19_25;

public:
	inline static int32_t get_offset_of_SessionStartReceived_0() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___SessionStartReceived_0)); }
	inline Action_t437523947 * get_SessionStartReceived_0() const { return ___SessionStartReceived_0; }
	inline Action_t437523947 ** get_address_of_SessionStartReceived_0() { return &___SessionStartReceived_0; }
	inline void set_SessionStartReceived_0(Action_t437523947 * value)
	{
		___SessionStartReceived_0 = value;
		Il2CppCodeGenWriteBarrier(&___SessionStartReceived_0, value);
	}

	inline static int32_t get_offset_of_SessionLoginError_1() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___SessionLoginError_1)); }
	inline Action_1_t592684423 * get_SessionLoginError_1() const { return ___SessionLoginError_1; }
	inline Action_1_t592684423 ** get_address_of_SessionLoginError_1() { return &___SessionLoginError_1; }
	inline void set_SessionLoginError_1(Action_1_t592684423 * value)
	{
		___SessionLoginError_1 = value;
		Il2CppCodeGenWriteBarrier(&___SessionLoginError_1, value);
	}

	inline static int32_t get_offset_of_GameConfigurationReceived_2() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___GameConfigurationReceived_2)); }
	inline Action_t437523947 * get_GameConfigurationReceived_2() const { return ___GameConfigurationReceived_2; }
	inline Action_t437523947 ** get_address_of_GameConfigurationReceived_2() { return &___GameConfigurationReceived_2; }
	inline void set_GameConfigurationReceived_2(Action_t437523947 * value)
	{
		___GameConfigurationReceived_2 = value;
		Il2CppCodeGenWriteBarrier(&___GameConfigurationReceived_2, value);
	}

	inline static int32_t get_offset_of_AccountLoginComplete_3() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___AccountLoginComplete_3)); }
	inline Action_2_t535153670 * get_AccountLoginComplete_3() const { return ___AccountLoginComplete_3; }
	inline Action_2_t535153670 ** get_address_of_AccountLoginComplete_3() { return &___AccountLoginComplete_3; }
	inline void set_AccountLoginComplete_3(Action_2_t535153670 * value)
	{
		___AccountLoginComplete_3 = value;
		Il2CppCodeGenWriteBarrier(&___AccountLoginComplete_3, value);
	}

	inline static int32_t get_offset_of_AccountLoginError_4() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___AccountLoginError_4)); }
	inline Action_2_t2887221574 * get_AccountLoginError_4() const { return ___AccountLoginError_4; }
	inline Action_2_t2887221574 ** get_address_of_AccountLoginError_4() { return &___AccountLoginError_4; }
	inline void set_AccountLoginError_4(Action_2_t2887221574 * value)
	{
		___AccountLoginError_4 = value;
		Il2CppCodeGenWriteBarrier(&___AccountLoginError_4, value);
	}

	inline static int32_t get_offset_of_NotificationAction_5() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___NotificationAction_5)); }
	inline Action_1_t1116941607 * get_NotificationAction_5() const { return ___NotificationAction_5; }
	inline Action_1_t1116941607 ** get_address_of_NotificationAction_5() { return &___NotificationAction_5; }
	inline void set_NotificationAction_5(Action_1_t1116941607 * value)
	{
		___NotificationAction_5 = value;
		Il2CppCodeGenWriteBarrier(&___NotificationAction_5, value);
	}

	inline static int32_t get_offset_of_NotificationWillClose_6() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___NotificationWillClose_6)); }
	inline Action_t437523947 * get_NotificationWillClose_6() const { return ___NotificationWillClose_6; }
	inline Action_t437523947 ** get_address_of_NotificationWillClose_6() { return &___NotificationWillClose_6; }
	inline void set_NotificationWillClose_6(Action_t437523947 * value)
	{
		___NotificationWillClose_6 = value;
		Il2CppCodeGenWriteBarrier(&___NotificationWillClose_6, value);
	}

	inline static int32_t get_offset_of_FriendAdded_7() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___FriendAdded_7)); }
	inline Action_2_t2362964390 * get_FriendAdded_7() const { return ___FriendAdded_7; }
	inline Action_2_t2362964390 ** get_address_of_FriendAdded_7() { return &___FriendAdded_7; }
	inline void set_FriendAdded_7(Action_2_t2362964390 * value)
	{
		___FriendAdded_7 = value;
		Il2CppCodeGenWriteBarrier(&___FriendAdded_7, value);
	}

	inline static int32_t get_offset_of_FriendRemoved_8() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___FriendRemoved_8)); }
	inline Action_2_t2362964390 * get_FriendRemoved_8() const { return ___FriendRemoved_8; }
	inline Action_2_t2362964390 ** get_address_of_FriendRemoved_8() { return &___FriendRemoved_8; }
	inline void set_FriendRemoved_8(Action_2_t2362964390 * value)
	{
		___FriendRemoved_8 = value;
		Il2CppCodeGenWriteBarrier(&___FriendRemoved_8, value);
	}

	inline static int32_t get_offset_of_FriendAccepted_9() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___FriendAccepted_9)); }
	inline Action_2_t2362964390 * get_FriendAccepted_9() const { return ___FriendAccepted_9; }
	inline Action_2_t2362964390 ** get_address_of_FriendAccepted_9() { return &___FriendAccepted_9; }
	inline void set_FriendAccepted_9(Action_2_t2362964390 * value)
	{
		___FriendAccepted_9 = value;
		Il2CppCodeGenWriteBarrier(&___FriendAccepted_9, value);
	}

	inline static int32_t get_offset_of_FriendRejected_10() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___FriendRejected_10)); }
	inline Action_2_t2362964390 * get_FriendRejected_10() const { return ___FriendRejected_10; }
	inline Action_2_t2362964390 ** get_address_of_FriendRejected_10() { return &___FriendRejected_10; }
	inline void set_FriendRejected_10(Action_2_t2362964390 * value)
	{
		___FriendRejected_10 = value;
		Il2CppCodeGenWriteBarrier(&___FriendRejected_10, value);
	}

	inline static int32_t get_offset_of_FriendsMigrated_11() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___FriendsMigrated_11)); }
	inline Action_2_t2362964390 * get_FriendsMigrated_11() const { return ___FriendsMigrated_11; }
	inline Action_2_t2362964390 ** get_address_of_FriendsMigrated_11() { return &___FriendsMigrated_11; }
	inline void set_FriendsMigrated_11(Action_2_t2362964390 * value)
	{
		___FriendsMigrated_11 = value;
		Il2CppCodeGenWriteBarrier(&___FriendsMigrated_11, value);
	}

	inline static int32_t get_offset_of_FriendsListUpdated_12() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___FriendsListUpdated_12)); }
	inline Action_1_t1482123831 * get_FriendsListUpdated_12() const { return ___FriendsListUpdated_12; }
	inline Action_1_t1482123831 ** get_address_of_FriendsListUpdated_12() { return &___FriendsListUpdated_12; }
	inline void set_FriendsListUpdated_12(Action_1_t1482123831 * value)
	{
		___FriendsListUpdated_12 = value;
		Il2CppCodeGenWriteBarrier(&___FriendsListUpdated_12, value);
	}

	inline static int32_t get_offset_of_FriendsListError_13() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___FriendsListError_13)); }
	inline Action_1_t592684423 * get_FriendsListError_13() const { return ___FriendsListError_13; }
	inline Action_1_t592684423 ** get_address_of_FriendsListError_13() { return &___FriendsListError_13; }
	inline void set_FriendsListError_13(Action_1_t592684423 * value)
	{
		___FriendsListError_13 = value;
		Il2CppCodeGenWriteBarrier(&___FriendsListError_13, value);
	}

	inline static int32_t get_offset_of_PurchaseVerification_14() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___PurchaseVerification_14)); }
	inline Action_3_t3047918030 * get_PurchaseVerification_14() const { return ___PurchaseVerification_14; }
	inline Action_3_t3047918030 ** get_address_of_PurchaseVerification_14() { return &___PurchaseVerification_14; }
	inline void set_PurchaseVerification_14(Action_3_t3047918030 * value)
	{
		___PurchaseVerification_14 = value;
		Il2CppCodeGenWriteBarrier(&___PurchaseVerification_14, value);
	}

	inline static int32_t get_offset_of_AdAvailabilityResponse_15() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___AdAvailabilityResponse_15)); }
	inline Action_2_t3211021459 * get_AdAvailabilityResponse_15() const { return ___AdAvailabilityResponse_15; }
	inline Action_2_t3211021459 ** get_address_of_AdAvailabilityResponse_15() { return &___AdAvailabilityResponse_15; }
	inline void set_AdAvailabilityResponse_15(Action_2_t3211021459 * value)
	{
		___AdAvailabilityResponse_15 = value;
		Il2CppCodeGenWriteBarrier(&___AdAvailabilityResponse_15, value);
	}

	inline static int32_t get_offset_of_AdWillClose_16() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___AdWillClose_16)); }
	inline Action_t437523947 * get_AdWillClose_16() const { return ___AdWillClose_16; }
	inline Action_t437523947 ** get_address_of_AdWillClose_16() { return &___AdWillClose_16; }
	inline void set_AdWillClose_16(Action_t437523947 * value)
	{
		___AdWillClose_16 = value;
		Il2CppCodeGenWriteBarrier(&___AdWillClose_16, value);
	}

	inline static int32_t get_offset_of_AdFailedToDisplay_17() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___AdFailedToDisplay_17)); }
	inline Action_t437523947 * get_AdFailedToDisplay_17() const { return ___AdFailedToDisplay_17; }
	inline Action_t437523947 ** get_address_of_AdFailedToDisplay_17() { return &___AdFailedToDisplay_17; }
	inline void set_AdFailedToDisplay_17(Action_t437523947 * value)
	{
		___AdFailedToDisplay_17 = value;
		Il2CppCodeGenWriteBarrier(&___AdFailedToDisplay_17, value);
	}

	inline static int32_t get_offset_of_AdDidShow_18() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___AdDidShow_18)); }
	inline Action_2_t3619260338 * get_AdDidShow_18() const { return ___AdDidShow_18; }
	inline Action_2_t3619260338 ** get_address_of_AdDidShow_18() { return &___AdDidShow_18; }
	inline void set_AdDidShow_18(Action_2_t3619260338 * value)
	{
		___AdDidShow_18 = value;
		Il2CppCodeGenWriteBarrier(&___AdDidShow_18, value);
	}

	inline static int32_t get_offset_of_RewardedAdCompletedWithObject_19() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___RewardedAdCompletedWithObject_19)); }
	inline Action_1_t978424956 * get_RewardedAdCompletedWithObject_19() const { return ___RewardedAdCompletedWithObject_19; }
	inline Action_1_t978424956 ** get_address_of_RewardedAdCompletedWithObject_19() { return &___RewardedAdCompletedWithObject_19; }
	inline void set_RewardedAdCompletedWithObject_19(Action_1_t978424956 * value)
	{
		___RewardedAdCompletedWithObject_19 = value;
		Il2CppCodeGenWriteBarrier(&___RewardedAdCompletedWithObject_19, value);
	}

	inline static int32_t get_offset_of_IAPOfferAcceptedWithObject_20() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___IAPOfferAcceptedWithObject_20)); }
	inline Action_1_t3974970450 * get_IAPOfferAcceptedWithObject_20() const { return ___IAPOfferAcceptedWithObject_20; }
	inline Action_1_t3974970450 ** get_address_of_IAPOfferAcceptedWithObject_20() { return &___IAPOfferAcceptedWithObject_20; }
	inline void set_IAPOfferAcceptedWithObject_20(Action_1_t3974970450 * value)
	{
		___IAPOfferAcceptedWithObject_20 = value;
		Il2CppCodeGenWriteBarrier(&___IAPOfferAcceptedWithObject_20, value);
	}

	inline static int32_t get_offset_of_VirtualGoodsOfferAcceptedWithObject_21() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___VirtualGoodsOfferAcceptedWithObject_21)); }
	inline Action_1_t1534247929 * get_VirtualGoodsOfferAcceptedWithObject_21() const { return ___VirtualGoodsOfferAcceptedWithObject_21; }
	inline Action_1_t1534247929 ** get_address_of_VirtualGoodsOfferAcceptedWithObject_21() { return &___VirtualGoodsOfferAcceptedWithObject_21; }
	inline void set_VirtualGoodsOfferAcceptedWithObject_21(Action_1_t1534247929 * value)
	{
		___VirtualGoodsOfferAcceptedWithObject_21 = value;
		Il2CppCodeGenWriteBarrier(&___VirtualGoodsOfferAcceptedWithObject_21, value);
	}

	inline static int32_t get_offset_of_TimeUpdated_22() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___TimeUpdated_22)); }
	inline Action_1_t487486641 * get_TimeUpdated_22() const { return ___TimeUpdated_22; }
	inline Action_1_t487486641 ** get_address_of_TimeUpdated_22() { return &___TimeUpdated_22; }
	inline void set_TimeUpdated_22(Action_1_t487486641 * value)
	{
		___TimeUpdated_22 = value;
		Il2CppCodeGenWriteBarrier(&___TimeUpdated_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_23() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___U3CU3Ef__amU24cache17_23)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache17_23() const { return ___U3CU3Ef__amU24cache17_23; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache17_23() { return &___U3CU3Ef__amU24cache17_23; }
	inline void set_U3CU3Ef__amU24cache17_23(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache17_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_24() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___U3CU3Ef__amU24cache18_24)); }
	inline Action_1_t592684423 * get_U3CU3Ef__amU24cache18_24() const { return ___U3CU3Ef__amU24cache18_24; }
	inline Action_1_t592684423 ** get_address_of_U3CU3Ef__amU24cache18_24() { return &___U3CU3Ef__amU24cache18_24; }
	inline void set_U3CU3Ef__amU24cache18_24(Action_1_t592684423 * value)
	{
		___U3CU3Ef__amU24cache18_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_25() { return static_cast<int32_t>(offsetof(FuseSDKEditorSession_t360633328_StaticFields, ___U3CU3Ef__amU24cache19_25)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache19_25() const { return ___U3CU3Ef__amU24cache19_25; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache19_25() { return &___U3CU3Ef__amU24cache19_25; }
	inline void set_U3CU3Ef__amU24cache19_25(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache19_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
